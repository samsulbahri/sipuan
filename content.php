<?php
// fungsi untuk pengecekan status login user
// jika user belum login, alihkan ke halaman "login-error"
if (empty($_SESSION['username']) && empty($_SESSION['password'])) {
	echo "<meta http-equiv='refresh' content='0; url=login-error'>";
}
// jika user sudah login
else {
	// jika halaman konten yang dipilih beranda, panggil file view beranda
	if ($_GET['module'] == 'beranda') {
		include "modules/beranda/view.php";
	}

	// jika halaman konten yang dipilih supplier, panggil file view supplier
	elseif ($_GET['module'] == 'supplier' && $_SESSION['hak_akses'] != 'Cashier') {
		include "modules/supplier/view.php";
	} elseif ($_GET['module'] == 'pemesanan' && $_SESSION['hak_akses'] == 'Purchasing') {
		include "modules/pemesanan/view.php";
	} elseif ($_GET['module'] == 'rekomendasi' && $_SESSION['hak_akses'] != 'Cashier') {
		include "modules/rekomendasi/view.php";
	} elseif ($_GET['module'] == 'rating' && $_SESSION['hak_akses'] != 'Cashier') {
		include "modules/rating/view.php";
	} elseif ($_GET['module'] == 'riwayat-pemesanan') {
		include "modules/riwayat-pemesanan/view.php";
	}

	// jika halaman konten yang dipilih bahanbaku, panggil file view bahanbaku
	elseif ($_GET['module'] == 'bahanbaku' && $_SESSION['hak_akses'] != 'Cashier') {
		include "modules/bahanbaku/view-jenis.php";
	} elseif (strpos($_GET['module'], 'bahanbaku-by-jenis') !== false && $_SESSION['hak_akses'] != 'Cashier') {
		$_SESSION['id_jenis_bahanbaku'] = explode("|", $_GET['module'])[1];
		include "modules/bahanbaku/view-bahanbaku.php";
	} elseif ($_GET['module'] == 'jenis-bahan-baku' && $_SESSION['hak_akses'] != 'Cashier') {
		include "modules/jenis-bahan-baku/view.php";
	}

	// jika halaman konten yang dipilih satuan, panggil file view satuan
	elseif ($_GET['module'] == 'satuan' && $_SESSION['hak_akses'] != 'Cashier') {
		include "modules/satuan/view.php";
	}

	// jika halaman konten yang dipilih pembelian, panggil file view pembelian
	elseif ($_GET['module'] == 'pembelian' && $_SESSION['hak_akses'] != 'Cashier') {
		include "modules/pembelian/view.php";
	}

	// jika halaman konten yang dipilih penjualan, panggil file view penjualan
	elseif ($_GET['module'] == 'penjualan' && $_SESSION['hak_akses'] != 'Purchasing') {
		include "modules/penjualan/view.php";
	}

	// jika halaman konten yang dipilih laporan stok, panggil file view laporan stok
	elseif ($_GET['module'] == 'lap_stok') {
		include "modules/laporan-stok/view.php";
	}

	// jika halaman konten yang dipilih laporan pembelian per periode, panggil file view laporan pembelian per periode
	elseif ($_GET['module'] == 'lap_pembelian_per_periode' && $_SESSION['hak_akses'] != 'Cashier') {
		include "modules/laporan-pembelian-per-periode/view.php";
	}

	// jika halaman konten yang dipilih laporan pembelian perincian, panggil file view laporan pembelian perincian
	elseif ($_GET['module'] == 'lap_pembelian_perincian' && $_SESSION['hak_akses'] != 'Cashier') {
		include "modules/laporan-pembelian-perincian/view.php";
	}

	// jika halaman konten yang dipilih laporan pembelian per barang, panggil file view laporan pembelian per barang
	elseif ($_GET['module'] == 'lap_pembelian_per_barang' && $_SESSION['hak_akses'] != 'Cashier') {
		include "modules/laporan-pembelian-per-bahanbaku/view.php";
	}

	// jika halaman konten yang dipilih laporan pembelian per supplier, panggil file view laporan pembelian per supplier
	elseif ($_GET['module'] == 'lap_pembelian_per_supplier' && $_SESSION['hak_akses'] != 'Cashier') {
		include "modules/laporan-pembelian-per-supplier/view.php";
	}

	// jika halaman konten yang dipilih laporan penjualan per periode, panggil file view laporan penjualan per periode
	elseif ($_GET['module'] == 'lap_penjualan_per_periode' && $_SESSION['hak_akses'] != 'Purchasing') {
		include "modules/laporan-penjualan-per-periode/view.php";
	}

	// jika halaman konten yang dipilih laporan penjualan perincian, panggil file view laporan penjualan perincian
	elseif ($_GET['module'] == 'lap_penjualan_perincian' && $_SESSION['hak_akses'] != 'Purchasing') {
		include "modules/laporan-penjualan-perincian/view.php";
	}

	// jika halaman konten yang dipilih laporan penjualan per barang, panggil file view laporan penjualan per barang
	elseif ($_GET['module'] == 'lap_penjualan_per_barang' && $_SESSION['hak_akses'] != 'Purchasing') {
		include "modules/laporan-penjualan-per-bahanbaku/view.php";
	}

	// jika halaman konten yang dipilih konfigurasi aplikasi, panggil file view konfigurasi aplikasi
	elseif ($_GET['module'] == 'config' && $_SESSION['hak_akses'] == 'Super Admin') {
		include "modules/konfigurasi/view.php";
	}

	// jika halaman konten yang dipilih manajemen user, panggil file view manajemen user
	elseif ($_GET['module'] == 'user' && $_SESSION['hak_akses'] == 'Super Admin') {
		include "modules/user/view.php";
	}

	// jika halaman konten yang dipilih backup database, panggil file view backup database
	elseif ($_GET['module'] == 'backupdb' && $_SESSION['hak_akses'] == 'Super Admin') {
		include "modules/backup-database/view.php";
	}

	// jika halaman konten yang dipilih audit trail, panggil file view audit trail
	elseif ($_GET['module'] == 'audit_trail' && $_SESSION['hak_akses'] == 'Super Admin') {
		include "modules/audit-trail/view.php";
	} elseif ($_GET['module'] == 'user-password' && $_SESSION['hak_akses'] == 'Super Admin') {
		include "modules/user-password/view.php";
	}

	// jika halaman konten yang dipilih password, panggil file view password
	elseif ($_GET['module'] == 'password') {
		include "modules/password/view.php";
	} elseif ($_GET['module'] == 'manajemen-pemesanan') {
		include "modules/manajemen-pemesanan/view.php";
	} elseif ($_GET['module'] == 'riwayat-pembelian') {
		include "modules/riwayat-pembelian/view.php";
	} elseif ($_GET['module'] == 'rating-saya') {
		include "modules/rating-saya/view.php";
	}
}
