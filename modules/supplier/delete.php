<?php
session_start();      // memulai session

// Mengecek AJAX Request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' )) {
    // panggil file "config.php" untuk koneksi ke database
    require_once "../../config/config.php";
    
    // mengecek data post dari ajax
    if (isset($_POST['kode_supplier'])) {
        try {
            // ambil "data" post dari ajax 
            $kode_supplier = $_POST['kode_supplier'];
            $nama_supplier = $_POST['nama_supplier'];
            $alamat        = $_POST['alamat'];
            $telepon       = $_POST['telepon'];

            // sql statement untuk menampilkan data "supplier" dari tabel "pembelian" berdasarkan "kode_supplier"
            $query = "SELECT supplier FROM pembelian WHERE supplier=:kode_supplier";
            // membuat prepared statements
            $stmt = $pdo->prepare($query);

            // hubungkan "data" dengan prepared statements
            $stmt->bindParam(':kode_supplier', $kode_supplier);

            // eksekusi query
            $stmt->execute();

            // cek hasil query
            // jika data "supplier" sudah ada di tabel "pembelian"
            if ($stmt->rowCount() <> 0) {
                // tampilkan pesan gagal delete data
                echo "gagal";
            }
            // jika data tidak ada, jalankan perintah delete
            else {
                // sql statement untuk delete data dari tabel "supplier"
                $query = "DELETE FROM supplier WHERE kode_supplier=:kode_supplier";
                // membuat prepared statements
                $stmt = $pdo->prepare($query);

                // hubungkan "data" dengan prepared statements
                $stmt->bindParam(':kode_supplier', $kode_supplier);

                // eksekusi query
                $stmt->execute();

                // cek hasil query
                // jika data "supplier" berhasil dihapus, jalankan perintah untuk insert data audit
                if ($stmt) {
                    // siapkan "data"
                    $username   = $_SESSION['id_user'];
                    $aksi       = "Delete";
                    $keterangan = "<b>Delete</b> data supplier pada tabel <b>supplier</b>. <br> <b>[Kode Supplier : </b>".$kode_supplier."<b>][Nama Supplier : </b>".$nama_supplier."<b>][Alamat : </b>".$alamat."<b>][Telepon : </b>".$telepon."<b>]";

                    // sql statement untuk insert data ke tabel "sys_audit_trail"
                    $query = "INSERT INTO sys_audit_trail(username, aksi, keterangan) VALUES (:username, :aksi, :keterangan)";
                    // membuat prepared statements
                    $stmt = $pdo->prepare($query);

                    // hubungkan "data" dengan prepared statements
                    $stmt->bindParam(':username', $username);
                    $stmt->bindParam(':aksi', $aksi);
                    $stmt->bindParam(':keterangan', $keterangan);

                    // eksekusi query
                    $stmt->execute();

                    // cek query
                    if ($stmt) {
                        // jika insert data audit berhasil tampilkan pesan "sukses"
                        echo "sukses";
                    }
                }
            }

            // tutup koneksi
            $pdo = null;
        } catch (PDOException $e) {
            // tampilkan pesan kesalahan
            echo $e->getMessage();
        }
    }  
} else {
    // jika tidak ada ajax request, maka alihkan ke halaman "login-error"
    echo '<script>window.location="../../login-error"</script>';
}
?>