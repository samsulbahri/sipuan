<?php
session_start();      // memulai session

// Mengecek AJAX Request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')) {
	// panggil file "config.php" untuk koneksi ke database
	require_once "../../config/config.php";

	try {
		// ambil "data" hasil post dari ajax
		$kode_supplier = trim($_POST['kode_supplier']);
		$nama_supplier = trim($_POST['nama_supplier']);
		$alamat        = trim($_POST['alamat']);
		$telepon       = trim($_POST['telepon']);
		$nama_bank       = trim($_POST['nama_bank']);
		$nama_pemilik_bank       = trim($_POST['nama_pemilik_bank']);
		$nomor_rekening       = trim($_POST['nomor_rekening']);
		// ambil "data" dari session
		$created_user  = $_SESSION['id_user'];

		// sql statement untuk insert data ke tabel "supplier"
		$query = "INSERT INTO supplier(kode_supplier, nama_supplier, alamat, telepon, nama_bank, nama_pemilik_bank, nomor_rekening, rating_keseluruhan, created_user) 
				  VALUES (:kode_supplier, :nama_supplier, :alamat, :telepon, :nama_bank, :nama_pemilik_bank, :nomor_rekening, 0, :created_user)";
		// membuat prepared statements
		$stmt = $pdo->prepare($query);

		// hubungkan "data" dengan prepared statements
		$stmt->bindParam(':kode_supplier', $kode_supplier);
		$stmt->bindParam(':nama_supplier', $nama_supplier);
		$stmt->bindParam(':alamat', $alamat);
		$stmt->bindParam(':telepon', $telepon);
		$stmt->bindParam(':nama_bank', $nama_bank);
		$stmt->bindParam(':nama_pemilik_bank', $nama_pemilik_bank);
		$stmt->bindParam(':nomor_rekening', $nomor_rekening);
		$stmt->bindParam(':created_user', $created_user);

		// eksekusi query
		$stmt->execute();

		// cek query
		if ($stmt) {
			// jika berhasil tampilkan pesan "sukses"
			echo "sukses";
		}

		// tutup koneksi
		$pdo = null;
	} catch (PDOException $e) {
		// tampilkan pesan kesalahan
		echo $e->getMessage();
	}
} else {
	// jika tidak ada ajax request, maka alihkan ke halaman "login-error"
	echo '<script>window.location="../../login-error"</script>';
}
