<?php
session_start();      // memulai session

// Mengecek AJAX Request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')) {
    // panggil file "config.php" untuk koneksi ke database
    require_once "../../config/config.php";

    // mengecek data post dari ajax
    if (isset($_POST['kode_supplier'])) {
        try {
            // ambil "data" hasil post dari ajax
            $kode_supplier = trim($_POST['kode_supplier']);
            $nama_supplier = trim($_POST['nama_supplier']);
            $alamat        = trim($_POST['alamat']);
            $telepon       = trim($_POST['telepon']);
            $nama_bank       = trim($_POST['nama_bank']);
            $nama_pemilik_bank       = trim($_POST['nama_pemilik_bank']);
            $nomor_rekening       = trim($_POST['nomor_rekening']);
            // ambil "data" dari session
            $updated_user  = $_SESSION['id_user'];
            // ambil waktu sekarang
            $updated_date  = gmdate("Y-m-d H:i:s", time() + 60 * 60 * 7);

            // sql statement untuk update data di tabel "supplier"
            $query = "UPDATE supplier SET nama_supplier = :nama_supplier, 
                                          alamat        = :alamat,
                                          telepon       = :telepon,
                                          nama_bank       = :nama_bank,
                                          nama_pemilik_bank       = :nama_pemilik_bank,
                                          nomor_rekening       = :nomor_rekening,
                                          updated_user  = :updated_user, 
                                          updated_date  = :updated_date 
                                    WHERE kode_supplier = :kode_supplier";
            // membuat prepared statements
            $stmt = $pdo->prepare($query);

            // hubungkan "data" dengan prepared statements
            $stmt->bindParam(':kode_supplier', $kode_supplier);
            $stmt->bindParam(':nama_supplier', $nama_supplier);
            $stmt->bindParam(':alamat', $alamat);
            $stmt->bindParam(':telepon', $telepon);
            $stmt->bindParam(':nama_bank', $nama_bank);
            $stmt->bindParam(':nama_pemilik_bank', $nama_pemilik_bank);
            $stmt->bindParam(':nomor_rekening', $nomor_rekening);
            $stmt->bindParam(':updated_user', $updated_user);
            $stmt->bindParam(':updated_date', $updated_date);

            // eksekusi query
            $stmt->execute();

            // cek query
            if ($stmt) {
                // jika berhasil tampilkan pesan "sukses"
                echo "sukses";
            }

            // tutup koneksi
            $pdo = null;
        } catch (PDOException $e) {
            // tampilkan pesan kesalahan
            echo $e->getMessage();
        }
    }
} else {
    // jika tidak ada ajax request, maka alihkan ke halaman "login-error"
    echo '<script>window.location="../../login-error"</script>';
}
