<?php
// Mengecek AJAX Request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' )) {
	// panggil file "config.php" untuk koneksi ke database
	require_once "../../config/config.php";

    try {
    	// sql statement untuk menampilkan 3 digit terakhir "kode_supplier" dari tabel "supplier"
        $query = "SELECT RIGHT(kode_supplier,3) as kode FROM supplier ORDER BY kode_supplier DESC LIMIT 1";
        // membuat prepared statements
        $stmt = $pdo->prepare($query);

        // eksekusi query
        $stmt->execute(); 

        // cek hasil query
        // jika kode supplier sudah ada
        if ($stmt->rowCount() <> 0) {
            // ambil data hasil query
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            // kode supplier + 1
            $kode = $data['kode']+1;
        }
        // jika kode supplier belum ada
        else {
            // kode supplier = 1
            $kode = 1;
        }

        // membuat kode supplier
        $buat_kode     = str_pad($kode, 3, "0", STR_PAD_LEFT);
        $kode_supplier = "SP".$buat_kode;
        
        // tampilkan data
        echo $kode_supplier;

        // tutup koneksi
        $pdo = null;
    } catch (PDOException $e) {
        // tampilkan pesan kesalahan
        echo "Query Error : ".$e->getMessage();
    }
} else {
    // jika tidak ada ajax request, maka alihkan ke halaman "login-error"
    echo '<script>window.location="../../login-error"</script>';
}
?>