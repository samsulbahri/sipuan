<?php
// fungsi untuk pengecekan status login user 
// jika user belum login, alihkan ke halaman "login-error"
if (empty($_SESSION['username']) && empty($_SESSION['password'])) {
    echo "<meta http-equiv='refresh' content='0; url=../../login-error'>";
}
// jika user sudah login
else { ?>
    <div class="row mb-3">
        <div class="col-md-12">
            <h5>
                <!-- judul halaman tampil data supplier -->
                <i class="fas fa-user-friends title-icon"></i> Data Supplier
                <!-- tombol tambah, dan export data supplier -->
                <div class="float-right">
                    <a class="btn btn-success mr-2" id="btnTambah" href="javascript:void(0);" data-toggle="modal" data-target="#modalSupplier" role="button">
                        <i class="fas fa-plus"></i> Tambah
                    </a>
                    <a class="btn btn-success" href="export-supplier" role="button">
                        <i class="fas fa-file-export title-icon"></i> Export
                    </a>
                </div>
            </h5>
        </div>
    </div>

    <div class="border mb-4"></div>

    <div class="row">
        <div class="col-md-12">
            <!-- Tabel supplier untuk menampilkan data supplier dari database -->
            <table id="tabel-supplier" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <!-- judul kolom pada bagian kepala (atas) tabel -->
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Kode Supplier</th>
                        <th>Nama Supplier</th>
                        <th>Alamat</th>
                        <th>Telepon</th>
                        <th>Nama Bank</th>
                        <th>Nama Pemilik Bank</th>
                        <th>Nomor Rekening</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    <!-- Modal form untuk entri dan ubah data supplier -->
    <div class="modal fade" id="modalSupplier" tabindex="-1" role="dialog" aria-labelledby="modalSupplier" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <!-- judul form data supplier -->
                <div class="modal-header">
                    <h5 class="modal-title"><i class="fas fa-edit title-icon"></i><span id="modalLabel"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!-- isi form data supplier -->
                <form id="formSupplier">
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Kode Supplier</label>
                            <input type="text" class="form-control" id="kode_supplier" name="kode_supplier" autocomplete="off" readonly>
                        </div>

                        <div class="form-group">
                            <label>Nama Supplier</label>
                            <input type="text" class="form-control" id="nama_supplier" name="nama_supplier" autocomplete="off">
                        </div>

                        <div class="form-group">
                            <label>Alamat</label>
                            <textarea class="form-control" id="alamat" name="alamat" rows="2"></textarea>
                        </div>

                        <div class="form-group">
                            <label>Telepon</label>
                            <input type="text" class="form-control" id="telepon" name="telepon" maxlength="13" onKeyPress="return goodchars(event,'0123456789',this)" autocomplete="off">
                        </div>

                        <div class="form-group">
                            <label>Nama Bank</label>
                            <input type="text" class="form-control" id="nama_bank" name="nama_bank" autocomplete="off">
                        </div>

                        <div class="form-group">
                            <label>Nama Pemilik Bank</label>
                            <input type="text" class="form-control" id="nama_pemilik_bank" name="nama_pemilik_bank" autocomplete="off">
                        </div>

                        <div class="form-group">
                            <label>Nomor Rekening</label>
                            <input type="text" class="form-control" id="nomor_rekening" name="nomor_rekening" autocomplete="off">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success mr-2" id="btnSimpan">Simpan</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            // ============================================ View =============================================
            // dataTables plugin untuk membuat nomor urut tabel
            $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
                return {
                    "iStart": oSettings._iDisplayStart,
                    "iEnd": oSettings.fnDisplayEnd(),
                    "iLength": oSettings._iDisplayLength,
                    "iTotal": oSettings.fnRecordsTotal(),
                    "iFilteredTotal": oSettings.fnRecordsDisplay(),
                    "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                    "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                };
            };

            // datatables serverside processing
            var table = $('#tabel-supplier').DataTable({
                "processing": true, // tampilkan loading saat proses data
                "serverSide": true, // server-side processing
                "ajax": 'modules/supplier/data.php', // panggil file "data.php" untuk menampilkan data supplier dari database
                // menampilkan data
                "columnDefs": [{
                        "targets": 0,
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "width": '30px',
                        "className": 'center'
                    },
                    {
                        "responsivePriority": 1,
                        "targets": 1,
                        "width": '80px',
                        "className": 'center'
                    },
                    {
                        "targets": 2,
                        "width": '200px'
                    },
                    {
                        "targets": 3,
                        "width": '200px'
                    },
                    {
                        "targets": 4,
                        "width": '80px',
                        "className": 'center'
                    },
                    {
                        "targets": 5,
                        "width": '200px'
                    },
                    {
                        "targets": 6,
                        "width": '200px'
                    }, {
                        "targets": 7,
                        "width": '200px'
                    },
                    {
                        "responsivePriority": 2,
                        "targets": 8,
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "width": '70px',
                        "className": 'center',
                        // tombol ubah dan hapus
                        "render": function(data, type, row) {
                            var btn = "<a style=\"margin-right:7px\" data-toggle=\"tooltip\" title=\"Ubah\" class=\"btn btn-success btn-sm getUbah\" href=\"javascript:void(0);\"><i class=\"fas fa-edit\"></i></a><a data-toggle=\"tooltip\" title=\"Hapus\" class=\"btn btn-danger btn-sm btnHapus\" href=\"javascript:void(0);\"><i class=\"fas fa-trash\"></i></a>";
                            return btn;
                        }
                    }
                ],
                "order": [
                    [1, "desc"]
                ], // urutkan data berdasarkan "kode_supplier" secara descending
                "iDisplayLength": 10, // tampilkan 10 data per halaman
                // membuat nomor urut tabel
                "rowCallback": function(row, data, iDisplayIndex) {
                    var info = this.fnPagingInfo();
                    var page = info.iPage;
                    var length = info.iLength;
                    var index = page * length + (iDisplayIndex + 1);
                    $('td:eq(0)', row).html(index);
                }
            });
            // ===============================================================================================

            // ============================================ Form =============================================
            // Tampilkan Modal Form Entri Data
            $('#btnTambah').click(function() {
                // reset form
                $('#formSupplier')[0].reset();
                // judul form
                $('#modalLabel').text('Entri Data Supplier');
                // membuat kode supplier
                $.ajax({
                    url: "modules/supplier/get_kode.php", // proses get kode supplier
                    success: function(result) { // ketika proses get kode supplier selesai
                        // tampilkan data kode supplier
                        $('#kode_supplier').val(result);
                    }
                });
            });

            // Tampilkan Modal Form Ubah Data
            $('#tabel-supplier tbody').on('click', '.getUbah', function() {
                // judul form
                $('#modalLabel').text('Ubah Data Supplier');
                // ambil data dari datatables 
                var data = table.row($(this).parents('tr')).data();
                // membuat variabel untuk menampung data "kode_supplier"
                var kode_supplier = data[1];

                $.ajax({
                    type: "GET", // mengirim data dengan method GET 
                    url: "modules/supplier/get_data.php", // proses get data supplier berdasarkan "kode_supplier"
                    data: {
                        kode_supplier: kode_supplier
                    }, // data yang dikirim
                    dataType: "JSON", // tipe data JSON
                    success: function(result) { // ketika proses get data selesai
                        // tampilkan modal ubah data supplier
                        $('#modalSupplier').modal('show');
                        // tampilkan data supplier
                        $('#kode_supplier').val(result.kode_supplier);
                        $('#nama_supplier').val(result.nama_supplier);
                        $('#alamat').val(result.alamat);
                        $('#telepon').val(result.telepon);
                        $('#nama_bank').val(result.nama_bank);
                        $('#nama_pemilik_bank').val(result.nama_pemilik_bank);
                        $('#nomor_rekening').val(result.nomor_rekening);
                    }
                });
            });
            // ===============================================================================================

            // ====================================== Insert dan Update ======================================
            // Proses Simpan Data
            $('#btnSimpan').click(function() {
                // validasi form input
                // jika nama supplier kosong
                if ($('#nama_supplier').val() == "") {
                    // focus ke input nama supplier
                    $("#nama_supplier").focus();
                    // tampilkan peringatan data tidak boleh kosong
                    $.notify({
                        title: '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                        message: 'Nama Supplier tidak boleh kosong.'
                    }, {
                        type: 'warning'
                    });
                }
                // jika alamat kosong
                else if ($('#alamat').val() == "") {
                    // focus ke input alamat
                    $("#alamat").focus();
                    // tampilkan peringatan data tidak boleh kosong
                    $.notify({
                        title: '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                        message: 'Alamat tidak boleh kosong.'
                    }, {
                        type: 'warning'
                    });
                }
                // jika telepon kosong
                else if ($('#telepon').val() == "") {
                    // focus ke input telepon
                    $("#telepon").focus();
                    // tampilkan peringatan data tidak boleh kosong
                    $.notify({
                        title: '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                        message: 'Telepon tidak boleh kosong.'
                    }, {
                        type: 'warning'
                    });
                }
                // jika telepon kosong
                else if ($('#nama_bank').val() == "") {
                    // focus ke input nama_bank
                    $("#nama_bank").focus();
                    // tampilkan peringatan data tidak boleh kosong
                    $.notify({
                        title: '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                        message: 'Nama Bank tidak boleh kosong.'
                    }, {
                        type: 'warning'
                    });
                }
                // jika telepon kosong
                else if ($('#nama_pemilik_bank').val() == "") {
                    // focus ke input nama_pemilik_bank
                    $("#nama_pemilik_bank").focus();
                    // tampilkan peringatan data tidak boleh kosong
                    $.notify({
                        title: '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                        message: 'Nama Pemilik Bank tidak boleh kosong.'
                    }, {
                        type: 'warning'
                    });
                }
                // jika telepon kosong
                else if ($('#nomor_rekening').val() == "") {
                    // focus ke input nomor_rekening
                    $("#nomor_rekening").focus();
                    // tampilkan peringatan data tidak boleh kosong
                    $.notify({
                        title: '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                        message: 'Nomor Rekening tidak boleh kosong.'
                    }, {
                        type: 'warning'
                    });
                }
                // jika semua data sudah terisi, jalankan perintah insert / update data
                else {
                    // jika form entri data supplier yang ditampilkan, jalankan perintah insert
                    if ($('#modalLabel').text() == "Entri Data Supplier") {
                        // membuat variabel untuk menampung data dari form entri data supplier
                        var data = $('#formSupplier').serialize();

                        $.ajax({
                            type: "POST", // mengirim data dengan method POST 
                            url: "modules/supplier/insert.php", // proses insert data
                            data: data, // data yang dikirim
                            success: function(result) { // ketika proses insert data selesai
                                // jika berhasil
                                if (result === "sukses") {
                                    // reset form
                                    $('#formSupplier')[0].reset();
                                    // tutup modal entri data supplier
                                    $('#modalSupplier').modal('hide');
                                    // tampilkan pesan sukses insert data
                                    $.notify({
                                        title: '<i class="fas fa-check-circle title-icon"></i><strong>Sukses!</strong><br>',
                                        message: 'Data Supplier berhasil disimpan.'
                                    }, {
                                        type: 'success'
                                    });
                                    // tampilkan view data supplier
                                    var table = $('#tabel-supplier').DataTable();
                                    table.ajax.reload(null, false);
                                }
                                // jika gagal
                                else {
                                    // tampilkan pesan gagal insert data dan error result
                                    $.notify({
                                        title: '<i class="fas fa-times-circle title-icon"></i><strong>Gagal!</strong><br>',
                                        message: 'Data Supplier tidak bisa disimpan. Query Error : ' + result
                                    }, {
                                        type: 'danger'
                                    });
                                }
                            }
                        });
                        return false;
                    }
                    // jika form ubah data supplier yang ditampilkan, jalankan perintah update 
                    else if ($('#modalLabel').text() == "Ubah Data Supplier") {
                        // membuat variabel untuk menampung data dari form ubah data supplier
                        var data = $('#formSupplier').serialize();

                        $.ajax({
                            type: "POST", // mengirim data dengan method POST 
                            url: "modules/supplier/update.php", // proses update data
                            data: data, // data yang dikirim
                            success: function(result) { // ketika proses update data selesai
                                // jika berhasil
                                if (result === "sukses") {
                                    // reset form
                                    $('#formSupplier')[0].reset();
                                    // tutup modal ubah data supplier
                                    $('#modalSupplier').modal('hide');
                                    // tampilkan pesan sukses update data
                                    $.notify({
                                        title: '<i class="fas fa-check-circle title-icon"></i><strong>Sukses!</strong><br>',
                                        message: 'Data Supplier berhasil diubah.'
                                    }, {
                                        type: 'success'
                                    });
                                    // tampilkan view data supplier
                                    var table = $('#tabel-supplier').DataTable();
                                    table.ajax.reload(null, false);
                                }
                                // jika gagal
                                else {
                                    // tampilkan pesan gagal update data dan error result
                                    $.notify({
                                        title: '<i class="fas fa-times-circle title-icon"></i><strong>Gagal!</strong><br>',
                                        message: 'Data Supplier tidak bisa diubah. Query Error : ' + result
                                    }, {
                                        type: 'danger'
                                    });
                                }
                            }
                        });
                        return false;
                    }
                }
            });
            // ===============================================================================================

            // =========================================== Delete ============================================
            $('#tabel-supplier tbody').on('click', '.btnHapus', function() {
                // ambil data dari datatables 
                var data = table.row($(this).parents('tr')).data();
                // tampilkan notifikasi saat akan menghapus data
                swal({
                        title: "Apakah Anda Yakin?",
                        text: "Anda akan menghapus data Supplier : " + data[1] + " - " + data[2],
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ya, Hapus!",
                        closeOnConfirm: false
                    },
                    // jika dipilih ya, maka jalankan perintah delete data
                    function() {
                        // membuat variabel untuk menampung data supplier
                        var kode_supplier = data[1];
                        var nama_supplier = data[2];
                        var alamat = data[3];
                        var telepon = data[4];

                        $.ajax({
                            type: "POST", // mengirim data dengan method POST 
                            url: "modules/supplier/delete.php", // proses delete data 
                            data: {
                                kode_supplier: kode_supplier,
                                nama_supplier: nama_supplier,
                                alamat: alamat,
                                telepon: telepon
                            }, // data yang dikirim
                            success: function(result) { // ketika proses delete data selesai
                                // jika berhasil
                                if (result === "sukses") {
                                    // tutup sweet alert
                                    swal.close();
                                    // tampilkan pesan sukses delete data
                                    $.notify({
                                        title: '<i class="fas fa-check-circle title-icon"></i><strong>Sukses!</strong><br>',
                                        message: 'Data Supplier berhasil dihapus.'
                                    }, {
                                        type: 'success'
                                    });
                                    // tampilkan view data supplier
                                    var table = $('#tabel-supplier').DataTable();
                                    table.ajax.reload(null, false);
                                }
                                // jika gagal karena data "supplier" sudah ada di tabel "pembelian"
                                else if (result === "gagal") {
                                    // tutup sweet alert
                                    swal.close();
                                    // tampilkan pesan gagal delete data
                                    $.notify({
                                        title: '<i class="fas fa-times-circle title-icon"></i><strong>Gagal!</strong><br>',
                                        message: 'Data Supplier <strong>' + data[1] + ' - ' + data[2] + '</strong> tidak bisa dihapus karena data supplier tersebut sudah tercatat pada data transaksi <strong>Pembelian</strong>.'
                                    }, {
                                        type: 'danger'
                                    });
                                }
                                // jika gagal karena script error
                                else {
                                    // tutup sweet alert
                                    swal.close();
                                    // tampilkan pesan gagal delete data dan error result
                                    $.notify({
                                        title: '<i class="fas fa-times-circle title-icon"></i><strong>Gagal!</strong><br>',
                                        message: 'Data Supplier tidak bisa dihapus. Query Error : ' + result
                                    }, {
                                        type: 'danger'
                                    });
                                }
                            }
                        });
                    });
            });
            // ===============================================================================================
        });
    </script>
<?php } ?>