<?php
// Mengecek AJAX Request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')) {
    // panggil file "config.php" untuk koneksi ke database
    require_once "../../config/config.php";

    // mengecek data get dari ajax
    if (isset($_GET['kode_supplier'])) {
        try {
            // ambil "data" get dari ajax
            $kode_supplier = $_GET['kode_supplier'];

            // sql statement untuk menampilkan data dari tabel "supplier" berdasarkan "kode_supplier"
            $query = "SELECT kode_supplier, nama_supplier, alamat, telepon, nama_bank, nama_pemilik_bank, nomor_rekening FROM supplier WHERE kode_supplier=:kode_supplier";
            // membuat prepared statements
            $stmt = $pdo->prepare($query);

            // hubungkan "data" dengan prepared statements
            $stmt->bindParam(':kode_supplier', $kode_supplier);

            // eksekusi query
            $stmt->execute();

            // ambil data hasil query
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            // tampilkan data
            echo json_encode($data);

            // tutup koneksi
            $pdo = null;
        } catch (PDOException $e) {
            // tampilkan pesan kesalahan
            echo "Query Error : " . $e->getMessage();
        }
    }
} else {
    // jika tidak ada ajax request, maka alihkan ke halaman "login-error"
    echo '<script>window.location="../../login-error"</script>';
}
