<?php
session_start();      // memulai session

// fungsi untuk pengecekan status login user 
// jika user belum login, alihkan ke halaman "login-error"
if (empty($_SESSION['username']) && empty($_SESSION['password'])){
    echo "<meta http-equiv='refresh' content='0; url=../../login-error'>";
}
// jika user sudah login
else {
    // fungsi untuk export data
    header("Content-Type: application/force-download");
    header("Cache-Control: no-cache, must-revalidate");
    // nama file hasil export
    header("content-disposition: attachment;filename=DATA-SUPPLIER.xls");
?>
    <!-- Judul file -->
    <center><h3>DATA SUPPLIER</h3></center>
    <!-- Table untuk di Export Ke Excel -->
    <table border='1'>
        <h3>
            <thead>
                <tr>
                    <th align="center" valign="middle">No.</th>
                    <th align="center" valign="middle">Kode Supplier</th>
                    <th align="center" valign="middle">Nama Supplier</th>
                    <th align="center" valign="middle">Alamat</th>
                    <th align="center" valign="middle">Telepon</th>
                </tr>
            </thead>
        </h3>

        <tbody>
        <?php  
        // panggil file "config.php" untuk koneksi ke database
        require_once "../../config/config.php";

        try {
            // variabel untuk nomor urut tabel
            $no = 1;
            // sql statement untuk menampilkan data dari tabel "supplier"
            $query = "SELECT kode_supplier, nama_supplier, alamat, telepon FROM supplier ORDER BY kode_supplier ASC";
            // membuat prepared statements
            $stmt = $pdo->prepare($query);

            // eksekusi query
            $stmt->execute();

            // tampilkan hasil query
            while ($data = $stmt->fetch(PDO::FETCH_ASSOC)) {
                echo "<tr>
                        <td width='50' align='center'>".$no."</td>
                        <td width='150' align='center'>".$data['kode_supplier']."</td>
                        <td width='300'>".$data['nama_supplier']."</td>
                        <td width='450'>".$data['alamat']."</td>
                        <td width='120' align='center'>".$data['telepon']."</td>
                    </tr>";
                $no++;
            };
            
            // tutup koneksi
            $pdo = null;
        } catch (PDOException $e) {
            // tampilkan pesan kesalahan
            echo "Query Error : ".$e->getMessage();
        }
        ?>
        </tbody>
    </table>
<?php } ?>