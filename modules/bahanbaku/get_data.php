<?php
// Mengecek AJAX Request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' )) {
	// panggil file "config.php" untuk koneksi ke database
	require_once "../../config/config.php";

	// mengecek data get dari ajax
    if (isset($_GET['kode_bahanbaku'])) {
        try {
            // ambil "data" get dari ajax
            $kode_bahanbaku = $_GET['kode_bahanbaku'];

        	// sql statement untuk menampilkan data dari tabel "bahanbaku" berdasarkan "kode_bahanbaku"
            $query = "SELECT a.kode_bahanbaku,a.nama_bahanbaku,a.harga_beli,a.harga_jual,a.satuan,a.min_stok,a.stok,b.nama_satuan
                      FROM bahanbaku as a INNER JOIN satuan as b ON a.satuan=b.kode_satuan WHERE a.kode_bahanbaku=:kode_bahanbaku";
            // membuat prepared statements
            $stmt = $pdo->prepare($query);

        	// hubungkan "data" dengan prepared statements
            $stmt->bindParam(':kode_bahanbaku', $kode_bahanbaku);

            // eksekusi query
            $stmt->execute();

            // ambil data hasil query
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            // tampilkan data
            echo json_encode($data);

            // tutup koneksi
            $pdo = null;
        } catch (PDOException $e) {
            // tampilkan pesan kesalahan
            echo "Query Error : ".$e->getMessage();
        }
	}
} else {
    // jika tidak ada ajax request, maka alihkan ke halaman "login-error"
    echo '<script>window.location="../../login-error"</script>';
}
?>
