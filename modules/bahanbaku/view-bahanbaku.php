<?php
// fungsi untuk pengecekan status login user
// jika user belum login, alihkan ke halaman "login-error"
if (empty($_SESSION['username']) && empty($_SESSION['password'])) {
    echo "<meta http-equiv='refresh' content='0; url=../../login-error'>";
}
// jika user sudah login
else { ?>
    <div class="row mb-3">
        <div class="col-md-12">
            <h5>
                <!-- judul halaman tampil data bahanbaku -->
                <i class="fas fa-boxes title-icon"></i> Data Bahan Baku
                <!-- tombol tambah data bahanbaku -->
                <a class="btn btn-success float-right" id="btnTambah" href="javascript:void(0);" data-toggle="modal" data-target="#modalbahanbaku" role="button">
                    <i class="fas fa-plus"></i> Tambah
                </a>
            </h5>
        </div>
    </div>

    <div class="border mb-4"></div>

    <div class="row">
        <div class="col-md-12">
            <!-- Tabel bahanbaku untuk menampilkan data bahanbaku dari database -->
            <table id="tabel-bahanbaku" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <!-- judul kolom pada bagian kepala (atas) tabel -->
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Kode Bahan Baku</th>
                        <th>Nama Bahan Baku</th>
                        <th>Harga Beli</th>
                        <th>Satuan</th>
                        <th>Min. Stok</th>
                        <th>Stok</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    <!-- Modal form untuk entri dan ubah data bahanbaku -->
    <div class="modal fade" id="modalbahanbaku" tabindex="-1" role="dialog" aria-labelledby="modalbahanbaku" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <!-- judul form data bahanbaku -->
                <div class="modal-header">
                    <h5 class="modal-title"><i class="fas fa-edit title-icon"></i><span id="modalLabel"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!-- isi form data bahanbaku -->
                <form id="formbahanbaku">
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Kode bahanbaku</label>
                            <input type="text" class="form-control" id="kode_bahanbaku" name="kode_bahanbaku" autocomplete="off" readonly>
                        </div>

                        <div class="form-group">
                            <label>Nama bahanbaku</label>
                            <input type="text" class="form-control" id="nama_bahanbaku" name="nama_bahanbaku" autocomplete="off">
                        </div>

                        <div class="form-group">
                            <label>Kategori Bahan Baku</label>
                            <select name="kategori" class="form-control">
                                <option value="">-- Pilih --</option>
                                <?php
                                try {
                                    // sql statement untuk menampilkan data dari tabel "satuan"
                                    $query = "SELECT * FROM jenis_bahanbaku ORDER BY jenis_bahanbaku ASC";
                                    // membuat prepared statements
                                    $stmt = $pdo->prepare($query);

                                    // eksekusi query
                                    $stmt->execute();

                                    // tampilkan hasil query
                                    while ($data_jenis = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                        if ($data_jenis['id_jenis_bahanbaku'] == $_SESSION['id_jenis_bahanbaku']) {
                                            echo "<option value='$data_jenis[id_jenis_bahanbaku]' selected> $data_jenis[jenis_bahanbaku] </option>";
                                        } else {
                                            echo "<option value='$data_jenis[id_jenis_bahanbaku]'> $data_jenis[jenis_bahanbaku] </option>";
                                        }
                                    }

                                    // tutup koneksi
                                } catch (PDOException $e) {
                                    // tampilkan pesan kesalahan
                                    echo $e->getMessage();
                                }
                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Harga Beli</label>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Rp.</div>
                                </div>
                                <input type="text" class="form-control" id="harga_beli" name="harga_beli" onKeyPress="return goodchars(event,'0123456789',this)" autocomplete="off">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Satuan</label>
                            <select class="chosen-select" id="satuan" name="satuan" autocomplete="off">
                                <option value="">-- Pilih --</option>
                                <?php
                                try {
                                    // sql statement untuk menampilkan data dari tabel "satuan"
                                    $query = "SELECT kode_satuan, nama_satuan FROM satuan ORDER BY nama_satuan ASC";
                                    // membuat prepared statements
                                    $stmt = $pdo->prepare($query);

                                    // eksekusi query
                                    $stmt->execute();

                                    // tampilkan hasil query
                                    while ($data_satuan = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                        echo "<option value='$data_satuan[kode_satuan]'> $data_satuan[nama_satuan] </option>";
                                    }

                                    // tutup koneksi
                                    $pdo = null;
                                } catch (PDOException $e) {
                                    // tampilkan pesan kesalahan
                                    echo $e->getMessage();
                                }
                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Minimum Stok</label>
                            <input type="text" class="form-control" id="min_stok" name="min_stok" onKeyPress="return goodchars(event,'0123456789',this)" autocomplete="off">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success mr-2" id="btnSimpan">Simpan</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            // ======================================= initiate plugin =======================================
            // chosen-select plugin
            $('.chosen-select').chosen();
            // ===============================================================================================

            // ============================================ Onload ===========================================
            // Format Rupiah / angka (menambahkan titik)
            var harga_beli = document.getElementById('harga_beli');
            harga_beli.addEventListener('keyup', function(e) {
                harga_beli.value = formatRupiah(this.value);
            });

            // ===============================================================================================

            // ============================================ View =============================================
            // dataTables plugin untuk membuat nomor urut tabel
            $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
                return {
                    "iStart": oSettings._iDisplayStart,
                    "iEnd": oSettings.fnDisplayEnd(),
                    "iLength": oSettings._iDisplayLength,
                    "iTotal": oSettings.fnRecordsTotal(),
                    "iFilteredTotal": oSettings.fnRecordsDisplay(),
                    "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                    "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                };
            };

            // datatables serverside processing
            var table = $('#tabel-bahanbaku').DataTable({
                "processing": true, // tampilkan loading saat proses data
                "serverSide": true, // server-side processing
                "ajax": 'modules/bahanbaku/data.php', // panggil file "data.php" untuk menampilkan data bahanbaku dari database
                // menampilkan data
                "columnDefs": [{
                        "targets": 0,
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "width": '30px',
                        "className": 'center'
                    },
                    {
                        "responsivePriority": 1,
                        "targets": 1,
                        "width": '70px',
                        "className": 'center'
                    },
                    {
                        "targets": 2,
                        "width": '200px'
                    },
                    {
                        "targets": 3,
                        "width": '100px',
                        "className": 'right',
                        render: $.fn.DataTable.render.number('.', ',', 0, 'Rp. ')
                    },
                    {
                        "targets": 4,
                        "width": '80px',
                        "className": 'center'
                    },
                    {
                        "targets": 5,
                        "width": '70px',
                        "className": 'center'
                    },
                    {
                        "targets": 6,
                        "width": '70px',
                        "className": 'center',
                        render: $.fn.DataTable.render.number('.', ',', 0, '')
                    },
                    {
                        "responsivePriority": 2,
                        "targets": 7,
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "width": '70px',
                        "className": 'center',
                        // tombol ubah dan hapus
                        "render": function(data, type, row) {
                            var btn = "<a style=\"margin-right:7px\" data-toggle=\"tooltip\" title=\"Ubah\" class=\"btn btn-success btn-sm getUbah\" href=\"javascript:void(0);\"><i class=\"fas fa-edit\"></i></a><a data-toggle=\"tooltip\" title=\"Hapus\" class=\"btn btn-danger btn-sm btnHapus\" href=\"javascript:void(0);\"><i class=\"fas fa-trash\"></i></a>";
                            return btn;
                        }
                    }
                ],
                "order": [
                    [1, "desc"]
                ], // urutkan data berdasarkan "kode_bahanbaku" secara descending
                "iDisplayLength": 10, // tampilkan 10 data per halaman
                // membuat nomor urut tabel
                "rowCallback": function(row, data, iDisplayIndex) {
                    var info = this.fnPagingInfo();
                    var page = info.iPage;
                    var length = info.iLength;
                    var index = page * length + (iDisplayIndex + 1);
                    $('td:eq(0)', row).html(index);
                }
            });
            // ===============================================================================================

            // ============================================ Form =============================================
            // Tampilkan Modal Form Entri Data
            $('#btnTambah').click(function() {
                // reset form
                $('#formbahanbaku')[0].reset();
                $('#satuan').val('').trigger('chosen:updated');
                // judul form
                $('#modalLabel').text('Entri Data bahanbaku');
                // membuat kode bahanbaku
                $.ajax({
                    url: "modules/bahanbaku/get_kode.php", // proses get kode bahanbaku
                    success: function(result) { // ketika proses get kode bahanbaku selesai
                        // tampilkan data kode bahanbaku
                        $('#kode_bahanbaku').val(result);
                    }
                });
            });

            // Tampilkan Modal Form Ubah Data
            $('#tabel-bahanbaku tbody').on('click', '.getUbah', function() {
                // judul form
                $('#modalLabel').text('Ubah Data bahanbaku');
                // ambil data dari datatables
                var data = table.row($(this).parents('tr')).data();
                // membuat variabel untuk menampung data "kode_bahanbaku"
                var kode_bahanbaku = data[1];

                $.ajax({
                    type: "GET", // mengirim data dengan method GET
                    url: "modules/bahanbaku/get_data.php", // proses get data bahanbaku berdasarkan "kode_bahanbaku"
                    data: {
                        kode_bahanbaku: kode_bahanbaku
                    }, // data yang dikirim
                    dataType: "JSON", // tipe data JSON
                    success: function(result) { // ketika proses get data selesai
                        // tampilkan modal ubah data bahanbaku
                        $('#modalbahanbaku').modal('show');
                        // tampilkan data bahanbaku
                        $('#kode_bahanbaku').val(result.kode_bahanbaku);
                        $('#nama_bahanbaku').val(result.nama_bahanbaku);
                        $('#harga_beli').val(formatRupiah(result.harga_beli));
                        $('#satuan').val(result.satuan).trigger('chosen:updated');
                        $('#min_stok').val(result.min_stok);
                    }
                });
            });
            // ===============================================================================================

            // ====================================== Insert dan Update ======================================
            // Proses Simpan Data
            $('#btnSimpan').click(function() {
                // validasi form input
                // jika nama bahanbaku kosong
                if ($('#nama_bahanbaku').val() == "") {
                    // focus ke input nama bahanbaku
                    $("#nama_bahanbaku").focus();
                    // tampilkan peringatan data tidak boleh kosong
                    $.notify({
                        title: '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                        message: 'Nama bahanbaku tidak boleh kosong.'
                    }, {
                        type: 'warning'
                    });
                }
                // jika harga beli kosong atau 0 (nol)
                else if ($('#harga_beli').val() == "" || $('#harga_beli').val() == 0) {
                    // focus ke input harga beli
                    $("#harga_beli").focus();
                    // tampilkan peringatan data tidak boleh kosong atau 0 (nol)
                    $.notify({
                        title: '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                        message: 'Harga Beli tidak boleh kosong atau 0 (nol).'
                    }, {
                        type: 'warning'
                    });
                }
                // jika satuan kosong
                else if ($('#satuan').val() == "") {
                    // focus ke input satuan
                    $("#satuan").focus();
                    // tampilkan peringatan data tidak boleh kosong
                    $.notify({
                        title: '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                        message: 'Satuan tidak boleh kosong.'
                    }, {
                        type: 'warning'
                    });
                }
                // jika minimum stok kosong atau 0 (nol)
                else if ($('#min_stok').val() == "" || $('#min_stok').val() == 0) {
                    // focus ke input minimum stok
                    $("#min_stok").focus();
                    // tampilkan peringatan data tidak boleh kosong atau 0 (nol)
                    $.notify({
                        title: '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                        message: 'Minimum Stok tidak boleh kosong atau 0 (nol).'
                    }, {
                        type: 'warning'
                    });
                }
                // jika semua data sudah terisi, jalankan perintah insert / update data
                else {
                    // jika form entri data bahanbaku yang ditampilkan, jalankan perintah insert
                    if ($('#modalLabel').text() == "Entri Data bahanbaku") {
                        // membuat variabel untuk menampung data dari form entri data bahanbaku
                        var data = $('#formbahanbaku').serialize();
                        $.ajax({
                            type: "POST", // mengirim data dengan method POST
                            url: "modules/bahanbaku/insert.php", // proses insert data
                            data: data,
                            //data : data,                                    // data yang dikirim
                            success: function(result) { // ketika proses insert data selesai
                                // jika berhasil
                                if (result === "sukses") {
                                    // reset form
                                    $('#formbahanbaku')[0].reset();
                                    $('#satuan').val('').trigger('chosen:updated');
                                    // tutup modal entri data bahanbaku
                                    $('#modalbahanbaku').modal('hide');
                                    // tampilkan pesan sukses insert data
                                    $.notify({
                                        title: '<i class="fas fa-check-circle title-icon"></i><strong>Sukses!</strong><br>',
                                        message: 'Data bahanbaku berhasil disimpan.'
                                    }, {
                                        type: 'success'
                                    });
                                    // tampilkan view data bahanbaku
                                    var table = $('#tabel-bahanbaku').DataTable();
                                    table.ajax.reload(null, false);
                                }
                                // jika gagal
                                else {
                                    // tampilkan pesan gagal insert data dan error result
                                    $.notify({
                                        title: '<i class="fas fa-times-circle title-icon"></i><strong>Gagal!</strong><br>',
                                        message: 'Data bahanbaku tidak bisa disimpan. Query Error : ' + result
                                    }, {
                                        type: 'danger'
                                    });
                                }
                            }
                        });
                        return false;
                    }
                    // jika form ubah data bahanbaku yang ditampilkan, jalankan perintah update
                    else if ($('#modalLabel').text() == "Ubah Data bahanbaku") {
                        // membuat variabel untuk menampung data dari form ubah data bahanbaku
                        var data = $('#formbahanbaku').serialize();

                        $.ajax({
                            type: "POST", // mengirim data dengan method POST
                            url: "modules/bahanbaku/update.php", // proses update data
                            data: data, // data yang dikirim
                            success: function(result) { // ketika proses update data selesai
                                // jika berhasil
                                if (result === "sukses") {
                                    // reset form
                                    $('#formbahanbaku')[0].reset();
                                    $('#satuan').val('').trigger('chosen:updated');
                                    // tutup modal ubah data bahanbaku
                                    $('#modalbahanbaku').modal('hide');
                                    // tampilkan pesan sukses update data
                                    $.notify({
                                        title: '<i class="fas fa-check-circle title-icon"></i><strong>Sukses!</strong><br>',
                                        message: 'Data bahanbaku berhasil diubah.'
                                    }, {
                                        type: 'success'
                                    });
                                    // tampilkan view data bahanbaku
                                    var table = $('#tabel-bahanbaku').DataTable();
                                    table.ajax.reload(null, false);
                                }
                                // jika gagal
                                else {
                                    // tampilkan pesan gagal update data dan error result
                                    $.notify({
                                        title: '<i class="fas fa-times-circle title-icon"></i><strong>Gagal!</strong><br>',
                                        message: 'Data bahanbaku tidak bisa diubah. Query Error : ' + result
                                    }, {
                                        type: 'danger'
                                    });
                                }
                            }
                        });
                        return false;
                    }
                }
            });
            // ===============================================================================================

            // =========================================== Delete ============================================
            $('#tabel-bahanbaku tbody').on('click', '.btnHapus', function() {
                // ambil data dari datatables
                var data = table.row($(this).parents('tr')).data();
                // tampilkan notifikasi saat akan menghapus data
                swal({
                        title: "Apakah Anda Yakin?",
                        text: "Anda akan menghapus data bahanbaku : " + data[1] + " - " + data[2],
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ya, Hapus!",
                        closeOnConfirm: false
                    },
                    // jika dipilih ya, maka jalankan perintah delete data
                    function() {
                        // membuat variabel untuk menampung data bahanbaku
                        var kode_bahanbaku = data[1];
                        var nama_bahanbaku = data[2];

                        $.ajax({
                            type: "POST", // mengirim data dengan method POST
                            url: "modules/bahanbaku/delete.php", // proses delete data
                            data: {
                                kode_bahanbaku: kode_bahanbaku,
                                nama_bahanbaku: nama_bahanbaku
                            }, // data yang dikirim
                            success: function(result) { // ketika proses delete data selesai
                                // jika berhasil
                                if (result === "sukses") {
                                    // tutup sweet alert
                                    swal.close();
                                    // tampilkan pesan sukses delete data
                                    $.notify({
                                        title: '<i class="fas fa-check-circle title-icon"></i><strong>Sukses!</strong><br>',
                                        message: 'Data bahanbaku berhasil dihapus.'
                                    }, {
                                        type: 'success'
                                    });
                                    // tampilkan view data bahanbaku
                                    var table = $('#tabel-bahanbaku').DataTable();
                                    table.ajax.reload(null, false);
                                }
                                // jika gagal karena data "bahanbaku" sudah tercatat di tabel pembelian / penjualan
                                else if (result === "gagal") {
                                    // tutup sweet alert
                                    swal.close();
                                    // tampilkan pesan gagal delete data
                                    $.notify({
                                        title: '<i class="fas fa-times-circle title-icon"></i><strong>Gagal!</strong><br>',
                                        message: 'Data bahanbaku <strong>' + data[1] + ' - ' + data[2] + '</strong> tidak bisa dihapus karena data bahanbaku tersebut sudah tercatat pada data transaksi.'
                                    }, {
                                        type: 'danger'
                                    });
                                }
                                // jika gagal karena script error
                                else {
                                    // tutup sweet alert
                                    swal.close();
                                    // tampilkan pesan gagal delete data dan error result
                                    $.notify({
                                        title: '<i class="fas fa-times-circle title-icon"></i><strong>Gagal!</strong><br>',
                                        message: 'Data bahanbaku tidak bisa dihapus. Query Error : ' + result
                                    }, {
                                        type: 'danger'
                                    });
                                }
                            }
                        });
                    });
            });
            // ===============================================================================================
        });
    </script>
<?php } ?>