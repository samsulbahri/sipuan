<?php
session_start();      // memulai session

// Mengecek AJAX Request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')) {
    // panggil file "config.php" untuk koneksi ke database
    require_once "../../config/config.php";

    // mengecek data post dari ajax
    if (isset($_POST['kode_bahanbaku'])) {
        try {
            // ambil "data" hasil post dari ajax
            $kode_bahanbaku    = trim($_POST['kode_bahanbaku']);
            $nama_bahanbaku    = trim($_POST['nama_bahanbaku']);
            $harga_beli   = str_replace('.', '', trim($_POST['harga_beli']));
            $satuan       = trim($_POST['satuan']);
            $min_stok     = trim($_POST['min_stok']);
            $kategori     = trim($_POST['kategori']);
            // ambil "data" dari session
            $updated_user = $_SESSION['id_user'];
            // ambil waktu sekarang
            $updated_date = gmdate("Y-m-d H:i:s", time() + 60 * 60 * 7);

            // sql statement untuk update data di tabel "bahanbaku"
            $query = "UPDATE bahanbaku SET nama_bahanbaku     = :nama_bahanbaku,
                                      harga_beli    = :harga_beli,
                                      satuan        = :satuan,
                                      min_stok      = :min_stok,
                                      updated_user  = :updated_user,
                                      updated_date  = :updated_date,
                                      id_jenis_bahanbaku = :kategori
                                WHERE kode_bahanbaku     = :kode_bahanbaku";
            // membuat prepared statements
            $stmt = $pdo->prepare($query);

            // hubungkan "data" dengan prepared statements
            $stmt->bindParam(':kode_bahanbaku', $kode_bahanbaku);
            $stmt->bindParam(':nama_bahanbaku', $nama_bahanbaku);
            $stmt->bindParam(':harga_beli', $harga_beli);
            $stmt->bindParam(':satuan', $satuan);
            $stmt->bindParam(':min_stok', $min_stok);
            $stmt->bindParam(':updated_user', $updated_user);
            $stmt->bindParam(':updated_date', $updated_date);
            $stmt->bindParam(':kategori', $kategori);

            // eksekusi query
            $stmt->execute();

            // cek query
            if ($stmt) {
                // jika berhasil tampilkan pesan "sukses"
                echo "sukses";
            }

            // tutup koneksi
            $pdo = null;
        } catch (PDOException $e) {
            // tampilkan pesan kesalahan
            echo $e->getMessage();
        }
    }
} else {
    // jika tidak ada ajax request, maka alihkan ke halaman "login-error"
    echo '<script>window.location="../../login-error"</script>';
}
