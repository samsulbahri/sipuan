<?php
// Mengecek AJAX Request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' )) {
	// panggil file "config.php" untuk koneksi ke database
	require_once "../../config/config.php";

    try {
    	// sql statement untuk menampilkan 4 digit terakhir "kode_bahanbaku" dari tabel "bahanbaku"
        $query = "SELECT RIGHT(kode_bahanbaku,4) as kode FROM bahanbaku ORDER BY kode_bahanbaku DESC LIMIT 1";
        // membuat prepared statements
        $stmt = $pdo->prepare($query);

        // eksekusi query
        $stmt->execute();

        // cek hasil query
        // jika kode bahanbaku sudah ada
        if ($stmt->rowCount() <> 0) {
            // ambil data hasil query
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            // kode bahanbaku + 1
            $kode = $data['kode']+1;
        }
        // jika kode bahanbaku belum ada
        else {
            // kode bahanbaku = 1
            $kode = 1;
        }

        // membuat kode bahanbaku
        $buat_kode = str_pad($kode, 4, "0", STR_PAD_LEFT);
        $kode_bahanbaku = "OB".$buat_kode;

        // tampilkan data
        echo $kode_bahanbaku;

        // tutup koneksi
        $pdo = null;
    } catch (PDOException $e) {
        // tampilkan pesan kesalahan
        echo "Query Error : ".$e->getMessage();
    }
} else {
    // jika tidak ada ajax request, maka alihkan ke halaman "login-error"
    echo '<script>window.location="../../login-error"</script>';
}
?>
