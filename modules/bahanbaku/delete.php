<?php
session_start();      // memulai session

// Mengecek AJAX Request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' )) {
    // panggil file "config.php" untuk koneksi ke database
    require_once "../../config/config.php";

    // mengecek data post dari ajax
    if (isset($_POST['kode_bahanbaku'])) {
        try {
            // ambil "data" post dari ajax
            $kode_bahanbaku = $_POST['kode_bahanbaku'];
            $nama_bahanbaku = $_POST['nama_bahanbaku'];

            // sql statement untuk menampilkan data "bahanbaku" dari tabel "pembelian_detail" berdasarkan "kode_bahanbaku"
            $query = "SELECT bahanbaku FROM pembelian_detail WHERE bahanbaku=:kode_bahanbaku";
            // membuat prepared statements
            $stmt = $pdo->prepare($query);
            // hubungkan "data" dengan prepared statements
            $stmt->bindParam(':kode_bahanbaku', $kode_bahanbaku);
            // eksekusi query
            $stmt->execute();
            // ambil jumlah data hasil query
            $row_pembelian = $stmt->rowCount();

            // sql statement untuk menampilkan data "bahanbaku" dari tabel "penjualan_detail" berdasarkan "kode_bahanbaku"
            $query = "SELECT bahanbaku FROM penjualan_detail WHERE bahanbaku=:kode_bahanbaku";
            // membuat prepared statements
            $stmt = $pdo->prepare($query);
            // hubungkan "data" dengan prepared statements
            $stmt->bindParam(':kode_bahanbaku', $kode_bahanbaku);
            // eksekusi query
            $stmt->execute();
            // ambil jumlah data hasil query
            $row_penjualan = $stmt->rowCount();

            // cek hasil query
            // jika data "bahanbaku" sudah ada di tabel "pembelian_detail" / "penjualan_detail"
            if ($row_pembelian <> 0 || $row_penjualan <> 0) {
                // tampilkan pesan gagal delete data
                echo "gagal";
            }
            // jika data tidak ada, jalankan perintah delete
            else {
                // sql statement untuk delete data dari tabel "bahanbaku"
                $query = "DELETE FROM bahanbaku WHERE kode_bahanbaku=:kode_bahanbaku";
                // membuat prepared statements
                $stmt = $pdo->prepare($query);

                // hubungkan "data" dengan prepared statements
                $stmt->bindParam(':kode_bahanbaku', $kode_bahanbaku);

                // eksekusi query
                $stmt->execute();

                // cek hasil query
                // jika data "bahanbaku" berhasil dihapus, jalankan perintah untuk insert data audit
                if ($stmt) {
                    // siapkan "data"
                    $username   = $_SESSION['id_user'];
                    $aksi       = "Delete";
                    $keterangan = "<b>Delete</b> data bahanbaku pada tabel <b>bahanbaku</b>. <br> <b>[Kode bahanbaku : </b>".$kode_bahanbaku."<b>][Nama bahanbaku : </b>".$nama_bahanbaku."<b>]";

                    // sql statement untuk insert data ke tabel "sys_audit_trail"
                    $query = "INSERT INTO sys_audit_trail(username, aksi, keterangan) VALUES (:username, :aksi, :keterangan)";
                    // membuat prepared statements
                    $stmt = $pdo->prepare($query);

                    // hubungkan "data" dengan prepared statements
                    $stmt->bindParam(':username', $username);
                    $stmt->bindParam(':aksi', $aksi);
                    $stmt->bindParam(':keterangan', $keterangan);

                    // eksekusi query
                    $stmt->execute();

                    // cek query
                    if ($stmt) {
                        // jika insert data audit berhasil tampilkan pesan "sukses"
                        echo "sukses";
                    }
                }
            }

            // tutup koneksi
            $pdo = null;
        } catch (PDOException $e) {
            // tampilkan pesan kesalahan
            echo $e->getMessage();
        }
    }
} else {
    // jika tidak ada ajax request, maka alihkan ke halaman "login-error"
    echo '<script>window.location="../../login-error"</script>';
}
?>
