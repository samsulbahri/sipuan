<?php
// fungsi untuk pengecekan status login user
// jika user belum login, alihkan ke halaman "login-error"
if (empty($_SESSION['username']) && empty($_SESSION['password'])) {
    echo "<meta http-equiv='refresh' content='0; url=../../login-error'>";
}
// jika user sudah login
else { ?>
    <div class="row mb-3">
        <div class="col-md-12">
            <h5>
                <!-- judul halaman tampil data bahanbaku -->
                <i class="fas fa-boxes title-icon"></i> Pilih Jenis Bahan Baku
                <!-- tombol tambah data bahanbaku -->
            </h5>
        </div>
    </div>

    <div class="border mb-4"></div>

    <div class="row">
        <?php
        try {
            // sql statement untuk menampilkan data dari tabel "satuan"
            $query = "SELECT * FROM jenis_bahanbaku";
            // membuat prepared statements
            $stmt = $pdo->prepare($query);

            // eksekusi query
            $stmt->execute();

            // tampilkan hasil query
            while ($data_jenis = $stmt->fetch(PDO::FETCH_ASSOC)) { ?>
                <div class="col-md-4">
                    <a href="bahanbaku-by-jenis-<?= $data_jenis['id_jenis_bahanbaku']; ?>" style="text-decoration: none;">
                        <div class="card">
                            <div class="card-body text-center">
                                <h3 class="font-weight-normal" style="color: black;"><?= $data_jenis['jenis_bahanbaku']; ?></h3>
                                <p class="font-weight-light" style="line-height: 25px; color: black;"><?= $data_jenis['deskripsi_jenis_bahanbaku']; ?></p>
                            </div>
                        </div>
                    </a>
                </div>
        <?php }

            // tutup koneksi
            $pdo = null;
        } catch (PDOException $e) {
            // tampilkan pesan kesalahan
            echo $e->getMessage();
        }
        ?>

    </div>
<?php } ?>