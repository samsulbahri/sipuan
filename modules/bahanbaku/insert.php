<?php
session_start();      // memulai session

// Mengecek AJAX Request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')) {
	// panggil file "config.php" untuk koneksi ke database
	require_once "../../config/config.php";

	try {
		// ambil "data" hasil post dari ajax
		$kode_bahanbaku    = trim($_POST['kode_bahanbaku']);
		$nama_bahanbaku    = trim($_POST['nama_bahanbaku']);
		$harga_beli   = str_replace('.', '', trim($_POST['harga_beli']));
		$harga_jual   = 0;
		$satuan       = trim($_POST['satuan']);
		$min_stok     = trim($_POST['min_stok']);
		// ambil "data" dari session
		$created_user = $_SESSION['id_user'];

		// sql statement untuk insert data ke tabel "bahanbaku"
		$query = "INSERT INTO bahanbaku(kode_bahanbaku, nama_bahanbaku, harga_beli, harga_jual, satuan, min_stok, created_user, id_jenis_bahanbaku)
				  VALUES (:kode_bahanbaku, :nama_bahanbaku, :harga_beli, :harga_jual, :satuan, :min_stok, :created_user, :id_jenis_bahanbaku)";
		// membuat prepared statements
		$stmt = $pdo->prepare($query);

		// hubungkan "data" dengan prepared statements
		$stmt->bindParam(':kode_bahanbaku', $kode_bahanbaku);
		$stmt->bindParam(':nama_bahanbaku', $nama_bahanbaku);
		$stmt->bindParam(':harga_beli', $harga_beli);
		$stmt->bindParam(':harga_jual', $harga_jual);
		$stmt->bindParam(':satuan', $satuan);
		$stmt->bindParam(':min_stok', $min_stok);
		$stmt->bindParam(':created_user', $created_user);
		$stmt->bindParam(':id_jenis_bahanbaku', $_SESSION['id_jenis_bahanbaku']);

		// eksekusi query
		$stmt->execute();

		// cek query
		if ($stmt) {
			// jika berhasil tampilkan pesan "sukses"
			echo "sukses";
		}

		// tutup koneksi
		$pdo = null;
	} catch (PDOException $e) {
		// tampilkan pesan kesalahan
		echo $e->getMessage();
	}
} else {
	// jika tidak ada ajax request, maka alihkan ke halaman "login-error"
	echo '<script>window.location="../../login-error"</script>';
}
