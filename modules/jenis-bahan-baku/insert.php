<?php
session_start();      // memulai session

// Mengecek AJAX Request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')) {
    // panggil file "config.php" untuk koneksi ke database
    require_once "../../config/config.php";

    try {
        // ambil "data" hasil post dari ajax
        $jenis    = trim($_POST['jenis_bahanbaku']);
        $deskripsi    = trim($_POST['deskripsi']);

        // sql statement untuk insert data ke tabel "bahanbaku"
        $query = "INSERT INTO jenis_bahanbaku(jenis_bahanbaku, deskripsi_jenis_bahanbaku)
				  VALUES (:jenis_bahanbaku, :deksripsi_jenis_bahanbaku)";
        // membuat prepared statements
        $stmt = $pdo->prepare($query);

        // hubungkan "data" dengan prepared statements
        $stmt->bindParam(':jenis_bahanbaku', $jenis);
        $stmt->bindParam(':deksripsi_jenis_bahanbaku', $deskripsi);

        // eksekusi query
        $stmt->execute();

        // cek query
        if ($stmt) {
            // jika berhasil tampilkan pesan "sukses"
            echo "sukses";
        }

        // tutup koneksi
        $pdo = null;
    } catch (PDOException $e) {
        // tampilkan pesan kesalahan
        echo $e->getMessage();
    }
} else {
    // jika tidak ada ajax request, maka alihkan ke halaman "login-error"
    echo '<script>window.location="../../login-error"</script>';
}
