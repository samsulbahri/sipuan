<?php
session_start();      // memulai session

// Mengecek AJAX Request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')) {
    // panggil file "config.php" untuk koneksi ke database
    require_once "../../config/config.php";

    // mengecek data post dari ajax
    if (isset($_POST['jenis_bahanbaku'])) {
        try {
            // ambil "data" post dari ajax
            $jenis_bahanbaku = $_POST['jenis_bahanbaku'];


            // sql statement untuk delete data dari tabel "bahanbaku"
            $query = "DELETE FROM jenis_bahanbaku WHERE jenis_bahanbaku=:jenis_bahanbaku";
            // membuat prepared statements
            $stmt = $pdo->prepare($query);

            // hubungkan "data" dengan prepared statements
            $stmt->bindParam(':jenis_bahanbaku', $jenis_bahanbaku);

            // eksekusi query
            $stmt->execute();

            // cek hasil query
            // jika data "bahanbaku" berhasil dihapus, jalankan perintah untuk insert data audit
            if ($stmt) {
                echo "sukses";
            }


            // tutup koneksi
            $pdo = null;
        } catch (PDOException $e) {
            // tampilkan pesan kesalahan
            echo $e->getMessage();
        }
    }
} else {
    // jika tidak ada ajax request, maka alihkan ke halaman "login-error"
    echo '<script>window.location="../../login-error"</script>';
}
