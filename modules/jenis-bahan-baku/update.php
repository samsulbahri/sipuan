<?php
session_start();      // memulai session

// Mengecek AJAX Request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')) {
    // panggil file "config.php" untuk koneksi ke database
    require_once "../../config/config.php";

    // mengecek data post dari ajax
    if (isset($_POST['id_jenis_bahanbaku'])) {
        try {
            // ambil "data" hasil post dari ajax
            $id_jenis_bahanbaku = trim($_POST['id_jenis_bahanbaku']);
            $jenis_bahanbaku = trim($_POST['jenis_bahanbaku']);
            $deskripsi_jenis_bahanbaku = trim($_POST['deskripsi']);

            // sql statement untuk update data di tabel "bahanbaku"
            $query = "UPDATE jenis_bahanbaku SET jenis_bahanbaku = :jenis_bahanbaku, deskripsi_jenis_bahanbaku = :deskripsi_jenis_bahanbaku WHERE id_jenis_bahanbaku = :id_jenis_bahanbaku";
            // membuat prepared statements
            $stmt = $pdo->prepare($query);

            // hubungkan "data" dengan prepared statements
            $stmt->bindParam(':id_jenis_bahanbaku', $id_jenis_bahanbaku);
            $stmt->bindParam(':jenis_bahanbaku', $jenis_bahanbaku);
            $stmt->bindParam(':deskripsi_jenis_bahanbaku', $deskripsi_jenis_bahanbaku);

            // eksekusi query
            $stmt->execute();

            // cek query
            if ($stmt) {
                // jika berhasil tampilkan pesan "sukses"
                echo "sukses";
            }

            // tutup koneksi
            $pdo = null;
        } catch (PDOException $e) {
            // tampilkan pesan kesalahan
            echo $e->getMessage();
        }
    }
} else {
    // jika tidak ada ajax request, maka alihkan ke halaman "login-error"
    echo '<script>window.location="../../login-error"</script>';
}
