<?php
// Mengecek AJAX Request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')) {
    // panggil file "config.php" untuk koneksi ke database
    require_once "../../config/config.php";

    // mengecek data get dari ajax
    if (isset($_GET['nama_jenis'])) {
        try {
            // ambil "data" get dari ajax
            $jenis_bahanbaku = $_GET['nama_jenis'];

            // sql statement untuk menampilkan data dari tabel "bahanbaku" berdasarkan "kode_bahanbaku"
            $query = "SELECT * FROM jenis_bahanbaku WHERE jenis_bahanbaku=:jenis_bahanbaku";
            // membuat prepared statements
            $stmt = $pdo->prepare($query);

            // hubungkan "data" dengan prepared statements
            $stmt->bindParam(':jenis_bahanbaku', $jenis_bahanbaku);

            // eksekusi query
            $stmt->execute();

            // ambil data hasil query
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            // tampilkan data
            echo json_encode($data);

            // tutup koneksi
            $pdo = null;
        } catch (PDOException $e) {
            // tampilkan pesan kesalahan
            echo "Query Error : " . $e->getMessage();
        }
    }
} else {
    // jika tidak ada ajax request, maka alihkan ke halaman "login-error"
    echo '<script>window.location="../../login-error"</script>';
}
