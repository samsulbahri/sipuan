<?php
// Mengecek AJAX Request
session_start();
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')) {

    // sql statement untuk join table
    $table = <<<EOT
(
    SELECT * FROM jenis_bahanbaku 
) temp
EOT;

    // primary key tabel
    $primaryKey = 'id_jenis_bahanbaku';

    // membuat array untuk menampilkan isi tabel.
    // Parameter 'db' mewakili nama kolom dalam database.
    // parameter 'dt' mewakili pengenal kolom pada DataTable.
    $columns = array(
        array('db' => 'jenis_bahanbaku', 'dt' => 1),
        array('db' => 'deskripsi_jenis_bahanbaku', 'dt' => 2),
        array('db' => 'id_jenis_bahanbaku', 'dt' => 3)
    );

    // memanggil file "database.php" untuk informasi koneksi ke server SQL
    require_once "../../config/database.php";
    // memanggil file "ssp.class.php" untuk menjalankan datatables server-side processing
    require '../../config/ssp.class.php';

    echo json_encode(
        SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns)
    );
} else {
    // jika tidak ada ajax request, maka alihkan ke halaman "login-error"
    echo '<script>window.location="../../login-error"</script>';
}
