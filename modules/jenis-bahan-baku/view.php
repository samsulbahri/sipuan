<?php
// fungsi untuk pengecekan status login user
// jika user belum login, alihkan ke halaman "login-error"
if (empty($_SESSION['username']) && empty($_SESSION['password'])) {
    echo "<meta http-equiv='refresh' content='0; url=../../login-error'>";
}
// jika user sudah login
else { ?>
    <div class="row mb-3">
        <div class="col-md-12">
            <h5>
                <!-- judul halaman tampil data bahanbaku -->
                <i class="fas fa-boxes title-icon"></i> Data Jenis Bahan Baku
                <!-- tombol tambah data bahanbaku -->
                <a class="btn btn-success float-right" id="btnTambah" href="javascript:void(0);" data-toggle="modal" data-target="#modalbahanbaku" role="button">
                    <i class="fas fa-plus"></i> Tambah
                </a>
            </h5>
        </div>
    </div>

    <div class="border mb-4"></div>

    <div class="row">
        <div class="col-md-12">
            <!-- Tabel bahanbaku untuk menampilkan data bahanbaku dari database -->
            <div class="table-responsive">
                <table id="tabel-bahanbaku" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <!-- judul kolom pada bagian kepala (atas) tabel -->
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Jenis</th>
                            <th>Deskripsi</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <!-- Modal form untuk entri dan ubah data bahanbaku -->
    <div class="modal fade" id="modalbahanbaku" tabindex="-1" role="dialog" aria-labelledby="modalbahanbaku" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <!-- judul form data bahanbaku -->
                <div class="modal-header">
                    <h5 class="modal-title"><i class="fas fa-edit title-icon"></i><span id="modalLabel"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!-- isi form data bahanbaku -->
                <form id="formbahanbaku">
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Nama Jenis Bahan Baku</label>
                            <input type="hidden" id="id_jenis_bahanbaku" name="id_jenis_bahanbaku">
                            <input type="text" class="form-control" id="jenis_bahanbaku" name="jenis_bahanbaku" autocomplete="off">
                        </div>

                        <div class="form-group">
                            <label>Deskripsi</label>
                            <textarea name="deskripsi" id="deskripsi" cols="30" rows="5" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success mr-2" id="btnSimpan">Simpan</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {

            // ===============================================================================================

            // ============================================ View =============================================
            // dataTables plugin untuk membuat nomor urut tabel
            $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
                return {
                    "iStart": oSettings._iDisplayStart,
                    "iEnd": oSettings.fnDisplayEnd(),
                    "iLength": oSettings._iDisplayLength,
                    "iTotal": oSettings.fnRecordsTotal(),
                    "iFilteredTotal": oSettings.fnRecordsDisplay(),
                    "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                    "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                };
            };

            // datatables serverside processing
            var table = $('#tabel-bahanbaku').DataTable({
                "processing": true, // tampilkan loading saat proses data
                "serverSide": true, // server-side processing
                "ajax": 'modules/jenis-bahan-baku/data.php', // panggil file "data.php" untuk menampilkan data bahanbaku dari database
                // menampilkan data
                "columnDefs": [{
                        "targets": 0,
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "width": '30px',
                        "className": 'center'
                    },
                    {
                        "responsivePriority": 1,
                        "targets": 1,
                        "width": '70px',
                    },
                    {
                        "targets": 2,
                        "width": '200px'
                    },
                    {
                        "responsivePriority": 2,
                        "targets": 3,
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "width": '70px',
                        "className": 'center',
                        // tombol ubah dan hapus
                        "render": function(data, type, row) {
                            var btn = "<a style=\"margin-right:7px\" data-toggle=\"tooltip\" title=\"Ubah\" class=\"btn btn-success btn-sm getUbah\" href=\"javascript:void(0);\"><i class=\"fas fa-edit\"></i></a><a data-toggle=\"tooltip\" title=\"Hapus\" class=\"btn btn-danger btn-sm btnHapus\" href=\"javascript:void(0);\"><i class=\"fas fa-trash\"></i></a>";
                            return btn;
                        }
                    }
                ],
                "order": [
                    [1, "desc"]
                ], // urutkan data berdasarkan "kode_bahanbaku" secara descending
                "iDisplayLength": 10, // tampilkan 10 data per halaman
                // membuat nomor urut tabel
                "rowCallback": function(row, data, iDisplayIndex) {
                    var info = this.fnPagingInfo();
                    var page = info.iPage;
                    var length = info.iLength;
                    var index = page * length + (iDisplayIndex + 1);
                    $('td:eq(0)', row).html(index);
                }
            });
            // ===============================================================================================

            // ============================================ Form =============================================
            // Tampilkan Modal Form Entri Data
            $('#btnTambah').click(function() {
                // reset form
                $('#formbahanbaku')[0].reset();
                // judul form
                $('#modalLabel').text('Entri Data Jenis Bahan Baku');
            });

            // Tampilkan Modal Form Ubah Data
            $('#tabel-bahanbaku tbody').on('click', '.getUbah', function() {
                // judul form
                $('#modalLabel').text('Ubah Data Jenis Bahan Baku');
                // ambil data dari datatables
                var data = table.row($(this).parents('tr')).data();
                // membuat variabel untuk menampung data "kode_bahanbaku"
                var nama_jenis = data[1];

                $.ajax({
                    type: "GET", // mengirim data dengan method GET
                    url: "modules/jenis-bahan-baku/get_data.php", // proses get data bahanbaku berdasarkan "nama_jenis"
                    data: {
                        nama_jenis: nama_jenis
                    }, // data yang dikirim
                    dataType: "JSON", // tipe data JSON
                    success: function(result) { // ketika proses get data selesai
                        // tampilkan modal ubah data bahanbaku
                        $('#modalbahanbaku').modal('show');
                        // tampilkan data bahanbaku
                        $('#id_jenis_bahanbaku').val(result.id_jenis_bahanbaku);
                        $('#jenis_bahanbaku').val(result.jenis_bahanbaku);
                        $('#deskripsi').val(result.deskripsi_jenis_bahanbaku);
                    },
                    error: function(result) {
                        console.log(result.responseText);
                    }
                });
            });
            // ===============================================================================================

            // ====================================== Insert dan Update ======================================
            // Proses Simpan Data
            $('#btnSimpan').click(function() {
                // validasi form input
                // jika nama bahanbaku kosong
                if ($('#jenis_bahanbaku').val() == "") {
                    // focus ke input nama bahanbaku
                    $("#jenis_bahanbaku").focus();
                    // tampilkan peringatan data tidak boleh kosong
                    $.notify({
                        title: '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                        message: 'Jenis Bahan Baku tidak boleh kosong.'
                    }, {
                        type: 'warning'
                    });
                } else if ($('#deskripsi').val() == "") {
                    // focus ke input nama bahanbaku
                    $("#deskripsi").focus();
                    // tampilkan peringatan data tidak boleh kosong
                    $.notify({
                        title: '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                        message: 'Deskripsi tidak boleh kosong.'
                    }, {
                        type: 'warning'
                    });
                } else {
                    // jika form entri data bahanbaku yang ditampilkan, jalankan perintah insert
                    if ($('#modalLabel').text() == "Entri Data Jenis Bahan Baku") {
                        // membuat variabel untuk menampung data dari form entri data bahanbaku
                        var data = $('#formbahanbaku').serialize();
                        $.ajax({
                            type: "POST", // mengirim data dengan method POST
                            url: "modules/jenis-bahan-baku/insert.php", // proses insert data
                            data: data,
                            //data : data,                                    // data yang dikirim
                            success: function(result) { // ketika proses insert data selesai
                                // jika berhasil
                                if (result === "sukses") {
                                    // reset form
                                    $('#formbahanbaku')[0].reset();
                                    $('#satuan').val('').trigger('chosen:updated');
                                    // tutup modal entri data bahanbaku
                                    $('#modalbahanbaku').modal('hide');
                                    // tampilkan pesan sukses insert data
                                    $.notify({
                                        title: '<i class="fas fa-check-circle title-icon"></i><strong>Sukses!</strong><br>',
                                        message: 'Data jenis bahan baku berhasil disimpan.'
                                    }, {
                                        type: 'success'
                                    });
                                    // tampilkan view data bahanbaku
                                    var table = $('#tabel-bahanbaku').DataTable();
                                    table.ajax.reload(null, false);
                                }
                                // jika gagal
                                else {
                                    // tampilkan pesan gagal insert data dan error result
                                    $.notify({
                                        title: '<i class="fas fa-times-circle title-icon"></i><strong>Gagal!</strong><br>',
                                        message: 'Data jenis bahan baku tidak bisa disimpan. Query Error : ' + result
                                    }, {
                                        type: 'danger'
                                    });
                                }
                            }
                        });
                        return false;
                    }
                    // jika form ubah data bahanbaku yang ditampilkan, jalankan perintah update
                    else if ($('#modalLabel').text() == "Ubah Data Jenis Bahan Baku") {
                        // membuat variabel untuk menampung data dari form ubah data bahanbaku
                        var data = $('#formbahanbaku').serialize();

                        $.ajax({
                            type: "POST", // mengirim data dengan method POST
                            url: "modules/jenis-bahan-baku/update.php", // proses update data
                            data: data, // data yang dikirim
                            success: function(result) { // ketika proses update data selesai
                                // jika berhasil
                                if (result === "sukses") {
                                    // reset form
                                    $('#formbahanbaku')[0].reset();
                                    // tutup modal ubah data bahanbaku
                                    $('#modalbahanbaku').modal('hide');
                                    // tampilkan pesan sukses update data
                                    $.notify({
                                        title: '<i class="fas fa-check-circle title-icon"></i><strong>Sukses!</strong><br>',
                                        message: 'Data jenis bahan baku berhasil diubah.'
                                    }, {
                                        type: 'success'
                                    });
                                    // tampilkan view data bahanbaku
                                    var table = $('#tabel-bahanbaku').DataTable();
                                    table.ajax.reload(null, false);
                                }
                                // jika gagal
                                else {
                                    // tampilkan pesan gagal update data dan error result
                                    $.notify({
                                        title: '<i class="fas fa-times-circle title-icon"></i><strong>Gagal!</strong><br>',
                                        message: 'Data jenis bahan baku tidak bisa diubah. Query Error : ' + result
                                    }, {
                                        type: 'danger'
                                    });
                                    console.log(result);
                                }
                            }
                        });
                        return false;
                    }
                }
            });
            // ===============================================================================================

            // =========================================== Delete ============================================
            $('#tabel-bahanbaku tbody').on('click', '.btnHapus', function() {
                // ambil data dari datatables
                var data = table.row($(this).parents('tr')).data();
                // tampilkan notifikasi saat akan menghapus data
                swal({
                        title: "Apakah Anda Yakin?",
                        text: "Anda akan menghapus data jenis bahan baku : " + data[1],
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ya, Hapus!",
                        closeOnConfirm: false
                    },
                    // jika dipilih ya, maka jalankan perintah delete data
                    function() {
                        // membuat variabel untuk menampung data bahanbaku
                        var jenis_bahanbaku = data[1];

                        $.ajax({
                            type: "POST", // mengirim data dengan method POST
                            url: "modules/jenis-bahan-baku/delete.php", // proses delete data
                            data: {
                                jenis_bahanbaku: jenis_bahanbaku,
                            }, // data yang dikirim
                            success: function(result) { // ketika proses delete data selesai
                                // jika berhasil
                                if (result === "sukses") {
                                    // tutup sweet alert
                                    swal.close();
                                    // tampilkan pesan sukses delete data
                                    $.notify({
                                        title: '<i class="fas fa-check-circle title-icon"></i><strong>Sukses!</strong><br>',
                                        message: 'Data jenis bahan baku berhasil dihapus.'
                                    }, {
                                        type: 'success'
                                    });
                                    // tampilkan view data bahanbaku
                                    var table = $('#tabel-bahanbaku').DataTable();
                                    table.ajax.reload(null, false);
                                }
                                // jika gagal karena data "bahanbaku" sudah tercatat di tabel pembelian / penjualan
                                else if (result === "gagal") {
                                    // tutup sweet alert
                                    swal.close();
                                    // tampilkan pesan gagal delete data
                                    $.notify({
                                        title: '<i class="fas fa-times-circle title-icon"></i><strong>Gagal!</strong><br>',
                                        message: 'Data jenis bahan baku <strong>' + data[1] + ' - ' + data[2] + '</strong> tidak bisa dihapu. Terjadi kesalahan.'
                                    }, {
                                        type: 'danger'
                                    });
                                }
                                // jika gagal karena script error
                                else {
                                    // tutup sweet alert
                                    swal.close();
                                    // tampilkan pesan gagal delete data dan error result
                                    $.notify({
                                        title: '<i class="fas fa-times-circle title-icon"></i><strong>Gagal!</strong><br>',
                                        message: 'Data jenis bahan baku tidak bisa dihapus. Query Error : ' + result
                                    }, {
                                        type: 'danger'
                                    });
                                }
                            }
                        });
                    });
            });
            // ===============================================================================================
        });
    </script>
<?php } ?>