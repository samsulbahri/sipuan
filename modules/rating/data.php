<?php
// Mengecek AJAX Request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')) {

    // sql statement untuk join table
    $table = <<<EOT
(
    SELECT d.kode_bahanbaku, d.nama_bahanbaku, e.nama_supplier, c.value_rating, b.feedback FROM pembelian_detail as a, feedback as b, rating as c, bahanbaku as d, supplier as e, pembelian as f WHERE a.no_pembelian = b.no_pembelian AND b.id_rating = c.id_rating AND a.bahanbaku = d.kode_bahanbaku AND a.no_pembelian = f.no_pembelian AND f.supplier = e.kode_supplier ORDER BY d.nama_bahanbaku ASC, c.value_rating DESC
) temp
EOT;

    // primary key tabel
    $primaryKey = 'kode_bahanbaku';

    // membuat array untuk menampilkan isi tabel.
    // Parameter 'db' mewakili nama kolom dalam database.
    // parameter 'dt' mewakili pengenal kolom pada DataTable.
    $columns = array(
        array('db' => 'nama_bahanbaku', 'dt' => 1),
        array('db' => 'nama_supplier', 'dt' => 2),
        array('db' => 'value_rating', 'dt' => 3),
        array('db' => 'feedback', 'dt' => 4)
    );

    // memanggil file "database.php" untuk informasi koneksi ke server SQL
    require_once "../../config/database.php";
    // memanggil file "ssp.class.php" untuk menjalankan datatables server-side processing
    require '../../config/ssp.class.php';


    echo json_encode(
        SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns)
    );
} else {
    // jika tidak ada ajax request, maka alihkan ke halaman "login-error"
    echo '<script>window.location="../../login-error"</script>';
}
