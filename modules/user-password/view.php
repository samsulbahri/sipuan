<?php
// fungsi untuk pengecekan status login user
// jika user belum login, alihkan ke halaman "login-error"
if (empty($_SESSION['username']) && empty($_SESSION['password'])) {
    echo "<meta http-equiv='refresh' content='0; url=../../login-error'>";
}
// jika user sudah login
else { ?>
    <div class="row mb-3">
        <div class="col-md-12">
            <h5>
                <!-- judul halaman tampil data user -->
                <i class="fas fa-user title-icon"></i> Permohonan Pergantian Password
            </h5>
        </div>
    </div>

    <div class="border mb-4"></div>

    <div class="row">
        <div class="col-md-12">
            <!-- Tabel user untuk menampilkan data user dari database -->
            <table id="tabel-user" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <!-- judul kolom pada bagian kepala (atas) tabel -->
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>ID User</th> <!-- kolom "ID User" disembunyikan -->
                        <th>Nama User</th>
                        <th>Username</th>
                        <th>Hak Akses</th>
                        <th>Blokir</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    <!-- Modal form untuk entri dan ubah data user -->
    <div class="modal fade" id="modalUser" tabindex="-1" role="dialog" aria-labelledby="modalUser" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <!-- judul form data user -->
                <div class="modal-header">
                    <h5 class="modal-title"><i class="fas fa-edit title-icon"></i><span id="modalLabel"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!-- isi form data user -->
                <form id="formUser">
                    <div class="modal-body">
                        <input type="hidden" id="id_user" name="id_user">

                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" class="form-control" id="pass" name="pass" autocomplete="off">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success mr-2" id="btnSimpan">Simpan</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#hak_akses').change(function() {
                if (document.getElementById('hak_akses').value === 'Suppliers') {
                    document.getElementById('group-supplier').style.display = 'inline';
                } else {
                    document.getElementById('supplier').value = '';
                    document.getElementById('group-supplier').style.display = 'none';
                }
            });
            // ============================================ View =============================================
            // dataTables plugin untuk membuat nomor urut tabel
            $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
                return {
                    "iStart": oSettings._iDisplayStart,
                    "iEnd": oSettings.fnDisplayEnd(),
                    "iLength": oSettings._iDisplayLength,
                    "iTotal": oSettings.fnRecordsTotal(),
                    "iFilteredTotal": oSettings.fnRecordsDisplay(),
                    "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                    "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                };
            };

            // datatables serverside processing
            var table = $('#tabel-user').DataTable({
                "processing": true, // tampilkan loading saat proses data
                "serverSide": true, // server-side processing
                "ajax": 'modules/user-password/data.php', // panggil file "data.php" untuk menampilkan data user dari database
                // menampilkan data
                "columnDefs": [{
                        "targets": 0,
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "width": '30px',
                        "className": 'center'
                    },
                    {
                        "targets": 1,
                        "visible": false
                    }, // kolom "id_user" disembunyikan
                    {
                        "targets": 2,
                        "width": '180px'
                    },
                    {
                        "responsivePriority": 1,
                        "targets": 3,
                        "width": '180px'
                    },
                    {
                        "targets": 4,
                        "width": '100px',
                        "className": 'center'
                    },
                    {
                        "targets": 5,
                        "width": '80px',
                        "className": 'center'
                    },
                    {
                        "responsivePriority": 2,
                        "targets": 6,
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "width": '70px',
                        "className": 'center',
                        // tombol ubah dan hapus
                        "render": function(data, type, row) {
                            var btn = "<a style=\"margin-right:7px\" title=\"Ubah\" class=\"btn btn-success btn-sm getUbah\" href=\"javascript:void(0);\"><i class=\"fas fa-edit\"></i></a>";
                            return btn;
                        }
                    }
                ],
                "order": [
                    [1, "desc"]
                ], // urutkan data berdasarkan "id_user" secara descending
                "iDisplayLength": 10, // tampilkan 10 data per halaman
                // membuat nomor urut tabel
                "rowCallback": function(row, data, iDisplayIndex) {
                    var info = this.fnPagingInfo();
                    var page = info.iPage;
                    var length = info.iLength;
                    var index = page * length + (iDisplayIndex + 1);
                    $('td:eq(0)', row).html(index);
                }
            });

            // Tampilkan Modal Form Ubah Data
            $('#tabel-user tbody').on('click', '.getUbah', function() {
                // judul form
                $('#modalLabel').text('Ubah Data User');
                // ambil data dari datatables
                var data = table.row($(this).parents('tr')).data();
                // membuat variabel untuk menampung data "id_user"
                var id_user = data[1];

                $.ajax({
                    type: "GET", // mengirim data dengan method GET
                    url: "modules/user/get_data.php", // proses get data user berdasarkan "id_user"
                    data: {
                        id_user: id_user
                    }, // data yang dikirim
                    dataType: "JSON", // tipe data JSON
                    success: function(result) { // ketika proses get data selesai
                        // tampilkan modal ubah data user
                        $('#modalUser').modal('show');
                        // tampilkan data user
                        $('#id_user').val(result.id_user);
                        // tampilkan keterangan pada password
                        $('#pass').attr('placeholder', 'Password');

                    }
                });
            });
            // ===============================================================================================

            // ====================================== Insert dan Update ======================================
            // Proses Simpan Data
            $('#btnSimpan').click(function() {
                // jika form ubah data user yang ditampilkan
                if ($('#modalLabel').text() == "Ubah Data User") {
                    // validasi form input
                    if ($('#pass').val() == "") {
                        // focus ke input pass
                        $("#pass").focus();
                        // tampilkan peringatan data tidak boleh kosong
                        $.notify({
                            title: '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                            message: 'Password tidak boleh kosong.'
                        }, {
                            type: 'warning'
                        });
                    }
                    // jika semua data sudah terisi, jalankan perintah update
                    else {
                        // membuat variabel untuk menampung data dari form ubah data user
                        var data = $('#formUser').serialize();

                        $.ajax({
                            type: "POST", // mengirim data dengan method POST
                            url: "modules/user-password/update.php", // proses update data
                            data: data, // data yang dikirim
                            success: function(result) { // ketika proses update data selesai
                                // jika berhasil
                                if (result === "sukses") {
                                    // reset form
                                    $('#formUser')[0].reset();
                                    // tutup modal ubah data user
                                    $('#modalUser').modal('hide');
                                    // tampilkan pesan sukses update data
                                    $.notify({
                                        title: '<i class="fas fa-check-circle title-icon"></i><strong>Sukses!</strong><br>',
                                        message: 'Password User berhasil diubah.'
                                    }, {
                                        type: 'success'
                                    });
                                    // tampilkan view data user
                                    var table = $('#tabel-user').DataTable();
                                    table.ajax.reload(null, false);
                                }
                                // jika gagal karena script error
                                else {
                                    // tampilkan pesan gagal update data dan error result
                                    $.notify({
                                        title: '<i class="fas fa-times-circle title-icon"></i><strong>Gagal!</strong><br>',
                                        message: 'Data User tidak bisa diubah. Query Error : ' + result
                                    }, {
                                        type: 'danger'
                                    });
                                }
                            }
                        });
                        return false;
                    }
                }
            });
            // ===============================================================================================

            // =========================================== Delete ============================================
            $('#tabel-user tbody').on('click', '.btnHapus', function() {
                // ambil data dari datatables
                var data = table.row($(this).parents('tr')).data();
                // tampilkan notifikasi saat akan menghapus data
                swal({
                        title: "Apakah Anda Yakin?",
                        text: "Anda akan menghapus username : " + data[3],
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ya, Hapus!",
                        closeOnConfirm: false
                    },
                    // jika dipilih ya, maka jalankan perintah delete data
                    function() {
                        // membuat variabel untuk menampung data user
                        var id_user = data[1];
                        var nama_user = data[2];
                        var username = data[3];
                        var hak_akses = data[4];

                        $.ajax({
                            type: "POST", // mengirim data dengan method POST
                            url: "modules/user/delete.php", // proses delete data
                            data: {
                                id_user: id_user,
                                nama_user: nama_user,
                                username: username,
                                hak_akses: hak_akses
                            }, // data yang dikirim
                            success: function(result) { // ketika proses delete data selesai
                                // jika berhasil
                                if (result === "sukses") {
                                    // tutup sweet alert
                                    swal.close();
                                    // tampilkan pesan sukses delete data
                                    $.notify({
                                        title: '<i class="fas fa-check-circle title-icon"></i><strong>Sukses!</strong><br>',
                                        message: 'Data User berhasil dihapus.'
                                    }, {
                                        type: 'success'
                                    });
                                    // tampilkan view data user
                                    var table = $('#tabel-user').DataTable();
                                    table.ajax.reload(null, false);
                                }
                                // jika gagal karena data "user" sudah tercatat di tabel lain
                                else if (result === "gagal") {
                                    // tutup sweet alert
                                    swal.close();
                                    // tampilkan pesan gagal delete data
                                    $.notify({
                                        title: '<i class="fas fa-times-circle title-icon"></i><strong>Gagal!</strong><br>',
                                        message: 'Username <strong>' + data[3] + '</strong> tidak bisa dihapus karena username tersebut sudah tercatat pada data lain.'
                                    }, {
                                        type: 'danger'
                                    });
                                }
                                // jika gagal karena script error
                                else {
                                    // tutup sweet alert
                                    swal.close();
                                    // tampilkan pesan gagal delete data dan error result
                                    $.notify({
                                        title: '<i class="fas fa-times-circle title-icon"></i><strong>Gagal!</strong><br>',
                                        message: 'Data User tidak bisa dihapus. Query Error : ' + result
                                    }, {
                                        type: 'danger'
                                    });
                                }
                            }
                        });
                    });
            });
            // ===============================================================================================
        });
    </script>
<?php } ?>