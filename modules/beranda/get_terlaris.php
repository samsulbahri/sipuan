<?php
// Mengecek AJAX Request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' )) {
    // panggil file "config.php" untuk koneksi ke database
    require_once "../../config/config.php";

    try {
        // variabel untuk nomor urut tabel
        $no = 1;

        // sql statement untuk menampilkan jumlah penjualan bahanbaku dari tabel "penjualan_detail"
        $query = "SELECT a.bahanbaku, SUM(a.jumlah_jual) as jumlah, b.nama_bahanbaku FROM penjualan_detail as a INNER JOIN bahanbaku as b ON a.bahanbaku=b.kode_bahanbaku
                  GROUP BY a.bahanbaku ORDER BY jumlah DESC LIMIT 5";
        // membuat prepared statements
        $stmt = $pdo->prepare($query);

        // eksekusi query
        $stmt->execute();

        // tampilkan hasil query
        while ($data = $stmt->fetch(PDO::FETCH_ASSOC)) {
            // tampilkan data penjualan per bahanbaku
            echo "<tr>
                    <td width='30' class='center'>".$no."</td>
                    <td width='250'>".$data['bahanbaku']." - ".$data['nama_bahanbaku']."</td>
                    <td width='100' class='center'>".number_format($data['jumlah'], 0, ".", ".")."</td>
                </tr>";
        $no++;
        };

        // tutup koneksi
        $pdo = null;
    } catch (PDOException $e) {
        // tampilkan pesan kesalahan
        echo "Query Error : ".$e->getMessage();
    }
} else {
    // jika tidak ada ajax request, maka alihkan ke halaman "login-error"
    echo '<script>window.location="../../login-error"</script>';
}
?>
