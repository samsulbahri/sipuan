<?php
// fungsi untuk pengecekan status login user
// jika user belum login, alihkan ke halaman "login-error"
if (empty($_SESSION['username']) && empty($_SESSION['password'])){
    echo "<meta http-equiv='refresh' content='0; url=../../login-error'>";
}
// jika user sudah login
else {
	// panggil file "fungsi_tanggal.php" untuk format tanggal
    require_once "config/fungsi_tanggal.php";
    // panggil file "fungsi_nama_hari.php" untuk menampilkan nama hari
    require_once "config/fungsi_nama_hari_bulan.php";
?>
	<div class="row">
	    <div class="col-md-12">
			<div class="alert alert-success" role="alert">
				<i class="fas fa-info-circle title-icon"></i> Selamat Datang <strong><?php echo $_SESSION['nama_user']; ?></strong>. Anda login sebagai <strong><?php echo $_SESSION['hak_akses']; ?></strong>.
			</div>
		</div>
	</div>

	<?php
	if ($_SESSION['hak_akses']=='Super Admin') { ?>
		<div class="row mt-2">
			<!-- tampilkan informasi jumlah supplier -->
		    <div class="col-xl-3 col-lg-6 col-xs-12">
		        <div class="card-dashboard">
		            <div class="card-body">
	                    <div class="media">
	                        <div class="media-body">
	                            <h5 class="green" id="loadSupplier"></h5>
	                            <span>Supplier</span>
	                        </div>
	                        <div class="media-right media-middle">
	                            <i class="fas fa-user-friends green icon-dashboard float-xs-right"></i>
	                        </div>
	                    </div>
		            </div>
		        </div>
		    </div>
			<!-- tampilkan informasi jumlah bahanbaku -->
		    <div class="col-xl-3 col-lg-6 col-xs-12">
		        <div class="card-dashboard">
		            <div class="card-body">
	                    <div class="media">
	                        <div class="media-body">
	                            <h5 class="green" id="loadbahanbaku"></h5>
	                            <span>Bahan Baku</span>
	                        </div>
	                        <div class="media-right media-middle">
	                            <i class="fas fa-boxes green icon-dashboard float-xs-right"></i>
	                        </div>
	                    </div>
		            </div>
		        </div>
		    </div>
			<!-- tampilkan informasi jumlah user -->
		    <div class="col-xl-3 col-lg-6 col-xs-12">
		        <div class="card-dashboard">
		            <div class="card-body">
	                    <div class="media">
	                        <div class="media-body">
	                            <h5 class="green" id="loadUser"></h5>
	                            <span>Pengguna Aplikasi</span>
	                        </div>
	                        <div class="media-right media-middle">
	                            <i class="fas fa-user green icon-dashboard float-xs-right"></i>
	                        </div>
	                    </div>
		            </div>
		        </div>
		    </div>
			<!-- tampilkan informasi tanggal terakhir backup database -->
		    <div class="col-xl-3 col-lg-6 col-xs-12">
		        <div class="card-dashboard">
		            <div class="card-body">
	                    <div class="media">
	                        <div class="media-body">
	                            <h5 class="green" id="loadDatabase"></h5>
	                            <span>Backup Database</span>
	                        </div>
	                        <div class="media-right media-middle">
	                            <i class="fas fa-database green icon-dashboard float-xs-right"></i>
	                        </div>
	                    </div>
		            </div>
		        </div>
		    </div>
		</div>

		<div class="border mt-2 mb-3"></div>

		<div class="row mt-2">
			<!-- menampilkan informasi jumlah dan total pembelian -->
			<div class="col-xl-12 col-lg-12 col-xs-12">
		        <div class="card-dashboard">
		            <div class="card-body">
	                    <div class="media">
	                        <div class="media-body">
	                            <div class="row">
								    <div class="col-md-12">
										<div class="alert alert-success" role="alert">
											<i class="fas fa-cart-plus title-icon"></i> <strong>Pembelian Bahan Baku</strong>
										</div>
									</div>
								</div>
					            <table class="table-dashboard" style="width:100%">
					                <tbody>
					                	<tr>
					                		<td style="padding:0.75rem;" colspan="3"><strong>Bulan <?php echo nama_bulan(date("Y-m-d")); ?></strong></td>
					                		<td style="padding:0.75rem;" colspan="3"><strong>Tahun <?php echo date("Y"); ?></strong></td>
					                		<td style="padding:0.75rem;" colspan="3"><strong>Pembelian Seluruh</strong></td>
					                	</tr>
					                	<tr>
					                		<!-- menampilkan informasi jumlah pembelian -->
					                		<td style="padding-top:0.5rem;" width="150">Jumlah Pembelian</td>
					                		<td style="padding-top:0.5rem;" width="5">:</td>
					                		<!-- parameter untuk menampilkan informasi jumlah pembelian per bulan -->
					                		<td class="green" width="250"><h5 id="loadJumlahPembelianBulan"></h5></td>

					                		<td style="padding-top:0.5rem;" width="150">Jumlah Pembelian</td>
					                		<td style="padding-top:0.5rem;" width="5">:</td>
					                		<!-- parameter untuk menampilkan informasi jumlah pembelian per tahun -->
					                		<td class="green" width="250"><h5 id="loadJumlahPembelianTahun"></h5></td>

					                		<td style="padding-top:0.5rem;" width="150">Jumlah Pembelian</td>
					                		<td style="padding-top:0.5rem;" width="5">:</td>
					                		<!-- parameter untuk menampilkan informasi jumlah pembelian seluruh -->
					                		<td class="green" width="250"><h5 id="loadJumlahPembelianSeluruh"></h5></td>
										</tr>
										<tr>
											<!-- menampilkan informasi total pembelian -->
					                		<td style="padding-top:0.5rem;" width="150">Total Pembelian</td>
					                		<td style="padding-top:0.5rem;" width="5">:</td>
					                		<!-- parameter untuk menampilkan informasi total pembelian per bulan -->
					                		<td class="green" width="250"><h5 id="loadTotalPembelianBulan"></h5></td>

					                		<td style="padding-top:0.5rem;" width="150">Total Pembelian</td>
					                		<td style="padding-top:0.5rem;" width="5">:</td>
					                		<!-- parameter untuk menampilkan informasi total pembelian per tahun -->
					                		<td class="green" width="250"><h5 id="loadTotalPembelianTahun"></h5></td>

					                		<td style="padding-top:0.5rem;" width="150">Total Pembelian</td>
					                		<td style="padding-top:0.5rem;" width="5">:</td>
					                		<!-- parameter untuk menampilkan informasi total pembelian seluruh -->
					                		<td class="green" width="250"><h5 id="loadTotalPembelianSeluruh"></h5></td>
					                	</tr>
					                </tbody>
					            </table>
	                        </div>
	                    </div>
		            </div>
		        </div>
		    </div>

		   	<!-- menampilkan informasi jumlah dan Total Pemakaian -->
		    <div class="col-xl-12 col-lg-12 col-xs-12">
		        <div class="card-dashboard">
		            <div class="card-body">
	                    <div class="media">
	                        <div class="media-body">
	                            <div class="row">
								    <div class="col-md-12">
										<div class="alert alert-success" role="alert">
											<i class="fas fa-clipboard title-icon"></i> <strong>Pemakaian Bahan Baku</strong>
										</div>
									</div>
								</div>
					            <table class="table-dashboard" style="width:100%">
					                <tbody>
					                	<tr>
					                		<td style="padding:0.75rem;" colspan="3"><strong>Bulan <?php echo nama_bulan(date("Y-m-d")); ?></strong></td>
					                		<td style="padding:0.75rem;" colspan="3"><strong>Tahun <?php echo date("Y"); ?></strong></td>
					                		<td style="padding:0.75rem;" colspan="3"><strong>Pemakaian Seluruh</strong></td>
					                	</tr>
					                	<tr>
					                		<!-- menampilkan informasi Jumlah Pemakaian -->
					                		<td style="padding-top:0.5rem;" width="150">Jumlah Pemakaian</td>
					                		<td style="padding-top:0.5rem;" width="5">:</td>
					                		<!-- parameter untuk menampilkan informasi Jumlah Pemakaian per bulan -->
					                		<td class="green" width="250"><h5 id="loadJumlahPenjualanBulan"></h5></td>

					                		<td style="padding-top:0.5rem;" width="150">Jumlah Pemakaian</td>
					                		<td style="padding-top:0.5rem;" width="5">:</td>
					                		<!-- parameter untuk menampilkan informasi Jumlah Pemakaian per tahun -->
					                		<td class="green" width="250"><h5 id="loadJumlahPenjualanTahun"></h5></td>

					                		<td style="padding-top:0.5rem;" width="150">Jumlah Pemakaian</td>
					                		<td style="padding-top:0.5rem;" width="5">:</td>
					                		<!-- parameter untuk menampilkan informasi Jumlah Pemakaian seluruh -->
					                		<td class="green" width="250"><h5 id="loadJumlahPenjualanSeluruh"></h5></td>
										</tr>
					                </tbody>
					            </table>
	                        </div>
	                    </div>
		            </div>
		        </div>
		    </div>
		</div>

		<div class="row mt-2">
		    <div class="col-xl-6 col-lg-6 col-xs-12">
		    	<!-- menampilkan informasi bahanbaku yang stok mencapai batas minimum -->
		    	<div class="card-dashboard">
		            <div class="card-body">
	                    <div class="media">
	                        <div class="media-body">
	                            <div class="row">
								    <div class="col-md-12">
										<div class="alert alert-success" role="alert">
											<i class="fas fa-info-circle title-icon"></i> <strong>Stok Bahan Baku Mencapai Batas Minimum</strong>
										</div>
									</div>
								</div>
								<!-- Tabel untuk menampilkan informasi bahanbaku yang stok mencapai batas minimum -->
					            <table id="tabel-stok" class="table table-striped" style="width:100%">
					    			<!-- judul kolom pada bagian kepala (atas) tabel -->
					                <thead>
					                    <tr>
					                        <th>No.</th>
					                        <th style="text-align:left">Bahan Baku</th>
					                        <th>Nama Bahan Baku</th>		<!-- kolom "Nama bahanbaku" disembunyikan, digabung dengan kolom "bahanbaku" -->
					                        <th>Stok</th>
					                    </tr>
					                </thead>
					            </table>
	                        </div>
	                    </div>
		            </div>
		        </div>
		    </div>

		    <!-- menampilkan informasi 5 bahanbaku terlaris -->
		    <div class="col-xl-6 col-lg-6 col-xs-12">
		        <div class="card-dashboard">
		            <div class="card-body">
	                    <div class="media">
	                        <div class="media-body">
	                            <div class="row">
								    <div class="col-md-12">
										<div class="alert alert-success" role="alert">
											<i class="fas fa-info-circle title-icon"></i> <strong>5 Bahan Baku Yang Banyak Digunakan</strong>
										</div>
									</div>
								</div>
								<!-- Tabel untuk menampilkan informasi 5 bahanbaku terlaris -->
					            <table id="tabel-penjualan" class="table table-striped" style="width:100%">
					    			<!-- judul kolom pada bagian kepala (atas) tabel -->
					                <thead>
					                    <tr>
					                        <th>No.</th>
					                        <th style="text-align:left">Bahan Baku</th>
					                        <th>Terpakai</th>
					                    </tr>
					                </thead>
					                <!-- parameter untuk memuat isi tabel bahanbaku terlaris -->
					                <tbody id="loadTerlaris"></tbody>
					            </table>
	                        </div>
	                    </div>
		            </div>
		        </div>
		    </div>
		</div>
	<?php
	} elseif ($_SESSION['hak_akses']=='Purchasing') { ?>
		<div class="row mt-2">
			<!-- menampilkan informasi jumlah dan total pembelian -->
			<div class="col-xl-12 col-lg-12 col-xs-12">
		        <div class="card-dashboard">
		            <div class="card-body">
	                    <div class="media">
	                        <div class="media-body">
	                            <div class="row">
								    <div class="col-md-12">
										<div class="alert alert-success" role="alert">
											<i class="fas fa-cart-plus title-icon"></i> <strong>Pembelian Bahan Baku</strong>
										</div>
									</div>
								</div>
					            <table class="table-dashboard" style="width:100%">
					                <tbody>
					                	<tr>
					                		<td style="padding:0.75rem;" colspan="3"><strong>Bulan <?php echo nama_bulan(date("Y-m-d")); ?></strong></td>
					                		<td style="padding:0.75rem;" colspan="3"><strong>Tahun <?php echo date("Y"); ?></strong></td>
					                		<td style="padding:0.75rem;" colspan="3"><strong>Pembelian Seluruh</strong></td>
					                	</tr>
					                	<tr>
					                		<!-- menampilkan informasi jumlah pembelian -->
					                		<td style="padding-top:0.5rem;" width="150">Jumlah Pembelian</td>
					                		<td style="padding-top:0.5rem;" width="5">:</td>
					                		<!-- parameter untuk menampilkan informasi jumlah pembelian per bulan -->
					                		<td class="green" width="250"><h5 id="loadJumlahPembelianBulan"></h5></td>

					                		<td style="padding-top:0.5rem;" width="150">Jumlah Pembelian</td>
					                		<td style="padding-top:0.5rem;" width="5">:</td>
					                		<!-- parameter untuk menampilkan informasi jumlah pembelian per tahun -->
					                		<td class="green" width="250"><h5 id="loadJumlahPembelianTahun"></h5></td>

					                		<td style="padding-top:0.5rem;" width="150">Jumlah Pembelian</td>
					                		<td style="padding-top:0.5rem;" width="5">:</td>
					                		<!-- parameter untuk menampilkan informasi jumlah pembelian seluruh -->
					                		<td class="green" width="250"><h5 id="loadJumlahPembelianSeluruh"></h5></td>
										</tr>
										<tr>
											<!-- menampilkan informasi total pembelian -->
					                		<td style="padding-top:0.5rem;" width="150">Total Pembelian</td>
					                		<td style="padding-top:0.5rem;" width="5">:</td>
					                		<!-- parameter untuk menampilkan informasi total pembelian per bulan -->
					                		<td class="green" width="250"><h5 id="loadTotalPembelianBulan"></h5></td>

					                		<td style="padding-top:0.5rem;" width="150">Total Pembelian</td>
					                		<td style="padding-top:0.5rem;" width="5">:</td>
					                		<!-- parameter untuk menampilkan informasi total pembelian per tahun -->
					                		<td class="green" width="250"><h5 id="loadTotalPembelianTahun"></h5></td>

					                		<td style="padding-top:0.5rem;" width="150">Total Pembelian</td>
					                		<td style="padding-top:0.5rem;" width="5">:</td>
					                		<!-- parameter untuk menampilkan informasi total pembelian seluruh -->
					                		<td class="green" width="250"><h5 id="loadTotalPembelianSeluruh"></h5></td>
					                	</tr>
					                </tbody>
					            </table>
	                        </div>
	                    </div>
		            </div>
		        </div>
		    </div>
		</div>

		<div class="row mt-2">
		    <div class="col-xl-6 col-lg-6 col-xs-12">
		    	<!-- menampilkan informasi bahanbaku yang stok mencapai batas minimum -->
		    	<div class="card-dashboard">
		            <div class="card-body">
	                    <div class="media">
	                        <div class="media-body">
	                            <div class="row">
								    <div class="col-md-12">
										<div class="alert alert-success" role="alert">
											<i class="fas fa-info-circle title-icon"></i> <strong>Stok Bahan Baku Mencapai Batas Minimum</strong>
										</div>
									</div>
								</div>
								<!-- Tabel untuk menampilkan informasi bahanbaku yang stok mencapai batas minimum -->
					            <table id="tabel-stok" class="table table-striped" style="width:100%">
					    			<!-- judul kolom pada bagian kepala (atas) tabel -->
					                <thead>
					                    <tr>
					                        <th>No.</th>
					                        <th style="text-align:left">Bahan Baku</th>
					                        <th>Nama Bahan Baku</th>		<!-- kolom "Nama bahanbaku" disembunyikan, digabung dengan kolom "bahanbaku" -->
					                        <th>Stok</th>
					                    </tr>
					                </thead>
					            </table>
	                        </div>
	                    </div>
		            </div>
		        </div>
		    </div>

		    <!-- menampilkan informasi 5 bahanbaku terlaris -->
		    <div class="col-xl-6 col-lg-6 col-xs-12">
		        <div class="card-dashboard">
		            <div class="card-body">
	                    <div class="media">
	                        <div class="media-body">
	                            <div class="row">
								    <div class="col-md-12">
										<div class="alert alert-success" role="alert">
											<i class="fas fa-info-circle title-icon"></i> <strong>5 Bahan Baku Yang Banyak Digunakan</strong>
										</div>
									</div>
								</div>
								<!-- Tabel untuk menampilkan informasi 5 bahanbaku terlaris -->
					            <table id="tabel-penjualan" class="table table-striped" style="width:100%">
					    			<!-- judul kolom pada bagian kepala (atas) tabel -->
					                <thead>
					                    <tr>
					                        <th>No.</th>
					                        <th style="text-align:left">Bahan Baku</th>
					                        <th>Terpakai</th>
					                    </tr>
					                </thead>
					                <!-- parameter untuk memuat isi tabel bahanbaku terlaris -->
					                <tbody id="loadTerlaris"></tbody>
					            </table>
	                        </div>
	                    </div>
		            </div>
		        </div>
		    </div>
		</div>
	<?php
	} elseif ($_SESSION['hak_akses']=='Cashier') { ?>
		<div class="row mt-2">
			<!-- menampilkan informasi jumlah dan Total Pemakaian per hari -->
		    <div class="col-xl-6 col-lg-6 col-xs-12">
		        <div class="card-dashboard">
		            <div class="card-body">
	                    <div class="media">
	                        <div class="media-body">
	                            <div class="row">
								    <div class="col-md-12">
										<div class="alert alert-success" role="alert">
											<i class="fas fa-shopping-cart title-icon"></i>
											<strong>Pemakaian Hari <?php echo nama_hari(date("Y-m-d")); ?>, <?php echo tgl_eng_to_ind(date("d-m-Y")); ?></strong>
										</div>
									</div>
								</div>
					            <table class="table-dashboard" style="width:100%">
					                <tbody>
					                	<tr>
					                		<td style="padding-top:0.5rem;" width="170">Jumlah Pemakaian</td>
					                		<td style="padding-top:0.5rem;" width="10">:</td>
					                		<!-- parameter untuk menampilkan informasi Jumlah Pemakaian per hari -->
					                		<td class="green"><h5 id="loadJumlahPenjualanHari"></h5></td>
					                	</tr>
					                </tbody>
					            </table>
	                        </div>
	                    </div>
		            </div>
		        </div>
		    </div>
		    <!-- menampilkan informasi jumlah dan Total Pemakaian per bulan -->
		    <div class="col-xl-6 col-lg-6 col-xs-12">
		        <div class="card-dashboard">
		            <div class="card-body">
	                    <div class="media">
	                        <div class="media-body">
	                            <div class="row">
								    <div class="col-md-12">
										<div class="alert alert-success" role="alert">
											<i class="fas fa-shopping-cart title-icon"></i>
											<strong>Pemakaian Bulan <?php echo nama_bulan(date("Y-m-d")); ?></strong>
										</div>
									</div>
								</div>
					            <table class="table-dashboard" style="width:100%">
					                <tbody>
					                	<tr>
					                		<td style="padding-top:0.5rem;" width="170">Jumlah Pemakaian</td>
					                		<td style="padding-top:0.5rem;" width="10">:</td>
					                		<!-- parameter untuk menampilkan informasi Jumlah Pemakaian per bulan -->
					                		<td class="green"><h5 id="loadJumlahPenjualanBulan"></h5></td>
					                	</tr>
					                </tbody>
					            </table>
	                        </div>
	                    </div>
		            </div>
		        </div>
		    </div>
		    <!-- menampilkan informasi jumlah dan Total Pemakaian per tahun -->
		    <div class="col-xl-6 col-lg-6 col-xs-12">
		        <div class="card-dashboard">
		            <div class="card-body">
	                    <div class="media">
	                        <div class="media-body">
	                            <div class="row">
								    <div class="col-md-12">
										<div class="alert alert-success" role="alert">
											<i class="fas fa-shopping-cart title-icon"></i>
											<strong>Pemakaian Tahun <?php echo date("Y"); ?></strong>
										</div>
									</div>
								</div>
					            <table class="table-dashboard" style="width:100%">
					                <tbody>
					                	<tr>
					                		<td style="padding-top:0.5rem;" width="170">Jumlah Pemakaian</td>
					                		<td style="padding-top:0.5rem;" width="10">:</td>
					                		<!-- parameter untuk menampilkan informasi Jumlah Pemakaian per tahun -->
					                		<td class="green"><h5 id="loadJumlahPenjualanTahun"></h5></td>
					                	</tr>
					                </tbody>
					            </table>
	                        </div>
	                    </div>
		            </div>
		        </div>
		    </div>
		    <!-- menampilkan informasi jumlah dan Total Pemakaian seluruh -->
		    <div class="col-xl-6 col-lg-6 col-xs-12">
		        <div class="card-dashboard">
		            <div class="card-body">
	                    <div class="media">
	                        <div class="media-body">
	                            <div class="row">
								    <div class="col-md-12">
										<div class="alert alert-success" role="alert">
											<i class="fas fa-shopping-cart title-icon"></i>
											<strong>Pemakaian Seluruh</strong>
										</div>
									</div>
								</div>
					            <table class="table-dashboard" style="width:100%">
					                <tbody>
					                	<tr>
					                		<td style="padding-top:0.5rem;" width="170">Jumlah Pemakaian</td>
					                		<td style="padding-top:0.5rem;" width="10">:</td>
					                		<!-- parameter untuk menampilkan informasi Jumlah Pemakaian seluruh -->
					                		<td class="green"><h5 id="loadJumlahPenjualanSeluruh"></h5></td>
					                	</tr>
					                </tbody>
					            </table>
	                        </div>
	                    </div>
		            </div>
		        </div>
		    </div>
		</div>

		<div class="row mt-2">
	    	<!-- menampilkan informasi bahanbaku yang stok mencapai batas minimum -->
		    <div class="col-xl-6 col-lg-6 col-xs-12">
		    	<div class="card-dashboard">
		            <div class="card-body">
	                    <div class="media">
	                        <div class="media-body">
	                            <div class="row">
								    <div class="col-md-12">
										<div class="alert alert-success" role="alert">
											<i class="fas fa-info-circle title-icon"></i> <strong>Stok Bahan Baku Mencapai Batas Minimum</strong>
										</div>
									</div>
								</div>
								<!-- Tabel untuk menampilkan informasi bahanbaku yang stok mencapai batas minimum -->
					            <table id="tabel-stok" class="table table-striped" style="width:100%">
					    			<!-- judul kolom pada bagian kepala (atas) tabel -->
					                <thead>
					                    <tr>
					                        <th>No.</th>
					                        <th style="text-align:left">Bahan Baku</th>
					                        <th>Nama Bahan Baku</th>		<!-- kolom "Nama bahanbaku" disembunyikan, digabung dengan kolom "bahanbaku" -->
					                        <th>Stok</th>
					                    </tr>
					                </thead>
					            </table>
	                        </div>
	                    </div>
		            </div>
		        </div>
		    </div>
		    <!-- menampilkan informasi 5 bahanbaku terlaris -->
		    <div class="col-xl-6 col-lg-6 col-xs-12">
		        <div class="card-dashboard">
		            <div class="card-body">
	                    <div class="media">
	                        <div class="media-body">
	                            <div class="row">
								    <div class="col-md-12">
										<div class="alert alert-success" role="alert">
											<i class="fas fa-info-circle title-icon"></i> <strong>5 Bahan Baku Terlaris.</strong>
										</div>
									</div>
								</div>
								<!-- Tabel untuk menampilkan informasi 5 bahanbaku terlaris -->
					            <table id="tabel-penjualan" class="table table-striped" style="width:100%">
					    			<!-- judul kolom pada bagian kepala (atas) tabel -->
					                <thead>
					                    <tr>
					                        <th>No.</th>
					                        <th style="text-align:left">Bahan Baku</th>
					                        <th>Terpakai</th>
					                    </tr>
					                </thead>
					                <!-- parameter untuk memuat isi tabel bahanbaku terlaris -->
					                <tbody id="loadTerlaris"></tbody>
					            </table>
	                        </div>
	                    </div>
		            </div>
		        </div>
		    </div>
		</div>
	<?php
	}
	?>

	<script type="text/javascript">
	$(document).ready(function(){
	    // =========================================== Onload ============================================
		// load file untuk menampilkan jumlah dan total data pembelian
	    $('#loadJumlahPembelianBulan').load('modules/beranda/get_jumlah_pembelian_bulan.php');
	    $('#loadTotalPembelianBulan').load('modules/beranda/get_total_pembelian_bulan.php');
	    $('#loadJumlahPembelianTahun').load('modules/beranda/get_jumlah_pembelian_tahun.php');
	    $('#loadTotalPembelianTahun').load('modules/beranda/get_total_pembelian_tahun.php');
	    $('#loadJumlahPembelianSeluruh').load('modules/beranda/get_jumlah_pembelian_seluruh.php');
	    $('#loadTotalPembelianSeluruh').load('modules/beranda/get_total_pembelian_seluruh.php');

	    // load file untuk menampilkan jumlah dan total data penjualan
	    $('#loadJumlahPenjualanHari').load('modules/beranda/get_jumlah_penjualan_hari.php');
	    $('#loadTotalPenjualanHari').load('modules/beranda/get_total_penjualan_hari.php');
	    $('#loadJumlahPenjualanBulan').load('modules/beranda/get_jumlah_penjualan_bulan.php');
	    $('#loadTotalPenjualanBulan').load('modules/beranda/get_total_penjualan_bulan.php');
	    $('#loadJumlahPenjualanTahun').load('modules/beranda/get_jumlah_penjualan_tahun.php');
	    $('#loadTotalPenjualanTahun').load('modules/beranda/get_total_penjualan_tahun.php');
	    $('#loadJumlahPenjualanSeluruh').load('modules/beranda/get_jumlah_penjualan_seluruh.php');
	    $('#loadTotalPenjualanSeluruh').load('modules/beranda/get_total_penjualan_seluruh.php');

	    // load file untuk menampilkan jumlah data
	    $('#loadSupplier').load('modules/beranda/get_supplier.php');
	    $('#loadbahanbaku').load('modules/beranda/get_bahanbaku.php');
	    $('#loadUser').load('modules/beranda/get_user.php');
	    $('#loadDatabase').load('modules/beranda/get_database.php');

        // dataTables plugin untuk membuat nomor urut tabel
        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        // menampilkan data bahanbaku yang stok mencapai batas minimum
        var table = $('#tabel-stok').DataTable( {
			"lengthChange": false,						// non-aktifkan lengthChange (fitur untuk menampilkan jumlah data per halaman)
        	"searching": false,							// non-aktifkan form pencarian
            "processing": true,                         // tampilkan loading saat proses data
            "serverSide": true,                         // server-side processing
            "ajax": 'modules/beranda/get_stok.php', 	// panggil file "get_minimum.php" untuk menampilkan data bahanbaku dari database
            // menampilkan data
            "columnDefs": [
                { "targets": 0, "data": null, "orderable": false, "searchable": false, "width": '30px', "className": 'center' },
                {
                    "targets": 1, "width": '250px',
                    "render": function ( data, type, row ) {
                        return  data +' - '+ row[ 2 ];
                    }
                },
                { "targets": 2, "visible": false },        					// kolom "nama_bahanbaku" disembunyikan, digabung dengan kolom "kode_bahanbaku"
                { "targets": 3, "width": '100px', "className": 'center' }
            ],
            "order": [[ 1, "asc" ]],           	// urutkan data berdasarkan "kode_bahanbaku" secara asccending
            "iDisplayLength": 10,               // tampilkan 10 data per halaman
            // membuat nomor urut tabel
            "rowCallback": function (row, data, iDisplayIndex) {
                var info   = this.fnPagingInfo();
                var page   = info.iPage;
                var length = info.iLength;
                var index  = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        } );

		// menampilkan data 5 bahanbaku terlaris
        $.ajax({
			url  : "modules/beranda/get_terlaris.php", 		// proses get data bahanbaku
            success: function(data){                        // ketika proses get data selesai
                // tampilkan data bahanbaku
                $('#loadTerlaris').html(data);
            }
        });
        // ===============================================================================================
	});
	</script>
<?php } ?>
