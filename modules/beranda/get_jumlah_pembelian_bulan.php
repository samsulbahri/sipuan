<?php
// Mengecek AJAX Request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' )) {
	// panggil file "config.php" untuk koneksi ke database
	require_once "../../config/config.php";

    try {
        // ambil bulan dan tahun sekarang
        $bulan = gmdate("Y-m", time()+60*60*7);

    	// sql statement untuk menampilkan jumlah data per bulan dari tabel "pembelian"
        $query = "SELECT count(no_pembelian) as jumlah FROM pembelian WHERE LEFT(tanggal,7)=:bulan";
        // membuat prepared statements
        $stmt = $pdo->prepare($query);

        // hubungkan "data" dengan prepared statements
        $stmt->bindParam(':bulan', $bulan);

        // eksekusi query
        $stmt->execute();

        // ambil data hasil query
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        // tampilkan data
        echo number_format($data['jumlah'], 0, ".", ".");

        // tutup koneksi
        $pdo = null;
    } catch (PDOException $e) {
        // tampilkan pesan kesalahan
        echo "Query Error : ".$e->getMessage();
    }
} else {
    // jika tidak ada ajax request, maka alihkan ke halaman "login-error"
    echo '<script>window.location="../../login-error"</script>';
}
?>