<?php
// Mengecek AJAX Request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' )) {
	// panggil file "config.php" untuk koneksi ke database
	require_once "../../config/config.php";

    try {
    	// sql statement untuk menampilkan tanggal terakhir backup dari tabel "sys_database"
        $query = "SELECT LEFT(created_date,10) as tanggal FROM sys_database ORDER BY id DESC LIMIT 1";
        // membuat prepared statements
        $stmt = $pdo->prepare($query);
        // eksekusi query
        $stmt->execute(); 

        // cek hasil query
        // jika data ada
        if ($stmt->rowCount() <> 0) {
            // ambil data hasil query
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            // tampilkan data
            echo date('d-m-Y', strtotime($data['tanggal']));
        }
        // jika data tidak ada
        else {
            // tampilkan
            echo "-";
        }

        // tutup koneksi
        $pdo = null;
    } catch (PDOException $e) {
        // tampilkan pesan kesalahan
        echo "Query Error : ".$e->getMessage();
    }
} else {
    // jika tidak ada ajax request, maka alihkan ke halaman "login-error"
    echo '<script>window.location="../../login-error"</script>';
}
?>