<?php
session_start();      // memulai session

// fungsi untuk pengecekan status login user
// jika user belum login, alihkan ke halaman "login-error"
if (empty($_SESSION['username']) && empty($_SESSION['password'])){
    echo "<meta http-equiv='refresh' content='0; url=../../login-error'>";
}
// jika user sudah login
else {
    // mengecek data get
    if (isset($_GET['tgl_awal']) && isset($_GET['tgl_akhir'])) {
        // panggil file "config.php" untuk koneksi ke database
        require_once "../../config/config.php";
        // panggil file "fungsi_tanggal.php" untuk format tanggal
        require_once "../../config/fungsi_tanggal.php";

        // fungsi untuk export data
        header("Content-Type: application/force-download");
        header("Cache-Control: no-cache, must-revalidate");
        // cek filter tanggal
        // jika "tgl_awal" sama dengan "tgl_akhir"
        if ($_GET['tgl_awal'] == $_GET['tgl_akhir']) {
            // nama file hasil export
            header("content-disposition: attachment;filename=LAPORAN-PEMBELIAN-PERINCIAN-".$_GET['tgl_awal'].".xls");
        }
        // jika "tgl_awal" tidak sama dengan "tgl_akhir"
        else {
            // nama file hasil export
            header("content-disposition: attachment;filename=LAPORAN-PEMBELIAN-PERINCIAN-".$_GET['tgl_awal']."-sd-".$_GET['tgl_akhir'].".xls");
        }

    ?>
        <!-- Judul laporan -->
        <center>
            <h3>
                LAPORAN PEMBELIAN PERINCIAN <br>
            <?php
            // jika "tgl_awal" sama dengan "tgl_akhir"
            if ($_GET['tgl_awal'] == $_GET['tgl_akhir']) {
                // tampilkan judul laporan "tgl_awal"
                echo "'".tgl_eng_to_ind($_GET['tgl_awal']);
            }
            // jika tgl_awal" tidak sama dengan "tgl_akhir"
            else {
                // tampilkan judul laporan "tgl_awal" s.d. "tgl_akhir"
                echo tgl_eng_to_ind($_GET['tgl_awal']) ." s.d. ". tgl_eng_to_ind($_GET['tgl_akhir']);
            }
            ?>
            </h3>
        </center>
        <!-- Table untuk di Export Ke Excel -->
        <table>
    <?php
    try {
        // ambil "data" get dari ajax
        $tgl_awal  = date('Y-m-d', strtotime($_GET['tgl_awal']));
        $tgl_akhir = date('Y-m-d', strtotime($_GET['tgl_akhir']));
        // variabel untuk nomor urut tabel
        $no = 1;
        // variabel untuk total bayar
        $total = 0;

        // sql statement untuk menampilkan data dari tabel "pembelian" berdasarkan "tanggal"
        $query1 = "SELECT a.no_pembelian,a.tanggal,a.supplier,a.no_nota,a.total_bayar,a.created_user,b.nama_supplier,c.nama_user
                   FROM pembelian as a INNER JOIN supplier as b INNER JOIN sys_users as c ON a.supplier=b.kode_supplier AND a.created_user=c.id_user
                   WHERE a.tanggal BETWEEN :tgl_awal AND :tgl_akhir ORDER BY a.tanggal ASC, a.no_pembelian ASC";
        // membuat prepared statements
        $stmt1 = $pdo->prepare($query1);

        // hubungkan "data" dengan prepared statements
        $stmt1->bindParam(':tgl_awal', $tgl_awal);
        $stmt1->bindParam(':tgl_akhir', $tgl_akhir);

        // eksekusi query
        $stmt1->execute();

        // tampilkan hasil query
        while ($data1 = $stmt1->fetch(PDO::FETCH_ASSOC)) {
            // tampilkan data no_pembelian
            $no_pembelian = $data1['no_pembelian'];
        ?>
            <!-- tampilkan data pembelian bahanbaku -->
            <tr>
                <td align="left" colspan="10">---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</td>
            </tr>
            <tr>
                <td width="100" align="center" valign="middle"><?php echo $no; ?></td>
                <td width="110" valign="middle">Tanggal</td>
                <td width="10" align="center" valign="middle">:</td>
                <td width="180" align="left" valign="middle"><?php echo date('d-m-Y', strtotime($data1['tanggal'])); ?></td>
                <td width="80" valign="middle">Supplier</td>
                <td width="10" align="center" valign="middle">:</td>
                <td width="255" valign="middle"><?php echo $data1['supplier']; ?> - <?php echo $data1['nama_supplier']; ?></td>
                <td width="100" valign="middle">Diinput Oleh</td>
                <td width="10" align="center" valign="middle">:</td>
                <td valign="middle"><?php echo $data1['nama_user']; ?></td>
            </tr>
            <tr>
                <td valign="middle"></td>
                <td width="110" valign="middle">No. Pembelian</td>
                <td width="10" align="center" valign="middle">:</td>
                <td width="180" valign="middle"><?php echo $data1['no_pembelian']; ?></td>
                <td width="80" valign="middle">No. Nota</td>
                <td width="10" align="center" valign="middle">:</td>
                <td valign="middle" colspan="4"><?php echo $data1['no_nota']; ?></td>
            </tr>
            <tr>
                <td align="left" colspan="10">---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</td>
            </tr>
            <tr>
                <td valign="middle"></td>
                <td valign="middle" colspan="9">Daftar Pembelian Bahan Baku :</td>
            </tr>
            <tr>
                <td valign="middle"></td>
                <td valign="middle" colspan="9">
                    <!-- Tabel untuk menampilkan detail data pembelian dari database -->
                    <table>
                        <thead>
                            <tr>
                                <td align="left" colspan="9">-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</td>
                            </tr>
                            <tr>
                                <th align="left" valign="middle">Kode Bahan Baku</th>
                                <th align="left" valign="middle">Nama Bahan Baku</th>
                                <th align="right" valign="middle">Harga</th>
                                <th align="right" valign="middle">Jumlah</th>
                                <th align="right" valign="middle">Total</th>
                            </tr>
                            <tr>
                                <td align="left" colspan="9">-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</td>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        // sql statement untuk menampilkan data dari tabel "pembelian_detail" berdasarkan "no_pembelian"
                        $query2 = "SELECT a.bahanbaku,a.harga_beli,a.jumlah_beli,a.total_harga,b.nama_bahanbaku
                                   FROM pembelian_detail as a INNER JOIN bahanbaku as b ON a.bahanbaku=b.kode_bahanbaku WHERE a.no_pembelian=:no_pembelian";
                        // membuat prepared statements
                        $stmt2 = $pdo->prepare($query2);

                        // hubungkan "data" dengan prepared statements
                        $stmt2->bindParam(':no_pembelian', $no_pembelian);

                        // eksekusi query
                        $stmt2->execute();

                        // tampilkan hasil query
                        while ($data2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                        ?>
                            <!-- tampilkan detail data pembelian bahanbaku -->
                            <tr>
                                <td width="150"><?php echo $data2['bahanbaku']; ?></td>
                                <td width="230"><?php echo $data2['nama_bahanbaku']; ?></td>
                                <td width="180" align="right">Rp. <?php echo number_format($data2['harga_beli'], 0, ".", "."); ?></td>
                                <td width="130" align="right"><?php echo $data2['jumlah_beli']; ?></td>
                                <td width="200" align="right">Rp. <?php echo number_format($data2['total_harga'], 0, ".", "."); ?></td>
                            </tr>
                        <?php
                        };
                        ?>
                            <tr>
                                <td align="left" colspan="9">-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</td>
                            </tr>
                            <tr>
                                <td align="right" colspan="4"><strong>Total :</strong></td>
                                <td align="right"><strong>Rp. <?php echo number_format($data1['total_bayar'], 0, ".", "."); ?></strong></td>
                            </tr>
                            <tr>
                                <td height="10" align="left" colspan="9"></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        <?php
            $no++;
            $total += $data1['total_bayar'];
        };
        ?>
            <tr>
                <td align="left" colspan="10">---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</td>
            </tr>
            <tr>
                <td align="right" colspan="5"><strong>Total Seluruh : </strong></td>
                <td align="right"><strong>Rp. <?php echo number_format($total, 0, ".", "."); ?></strong></td>
            </tr>
            <tr>
                <td align="left" colspan="10">---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</td>
            </tr>
        </table>
        <?php
        // tutup koneksi
        $pdo = null;
    } catch (PDOException $e) {
        // tampilkan pesan kesalahan
        echo "Query Error : ".$e->getMessage();
    }
    ?>

        <div style="text-align:right">
            <h4>Balikpapan, <?php echo date("d/m/Y"); ?></h4> <br><br>
            <h4>.................................................</h4>
        </div>
    <?php
    }
}
?>
