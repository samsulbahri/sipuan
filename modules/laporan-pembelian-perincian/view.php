<?php
// fungsi untuk pengecekan status login user
// jika user belum login, alihkan ke halaman "login-error"
if (empty($_SESSION['username']) && empty($_SESSION['password'])){
    echo "<meta http-equiv='refresh' content='0; url=../../login-error'>";
}
// jika user sudah login
else { ?>
    <div class="row mb-3">
        <div class="col-md-12">
    		<h5>
    			<!-- judul halaman laporan pembelian perincian -->
    			<i class="fas fa-file-invoice title-icon"></i> Riwayat Pembelian Perincian
    		</h5>
    	</div>
    </div>

    <div class="border mb-4"></div>

    <div class="row">
        <div class="col-md-12">
            <!-- form filter periode pembelian -->
            <form id="formFilter">
            	<div class="row">
    				<div class="col">
    					<div class="form-group mb-0">
    						<label>Periode Pembelian : </label>
    					</div>
    				</div>
    			</div>

            	<div class="row">
    				<div class="col">
    		            <div class="form-group">
    		                <input type="text" class="form-control date-picker" data-date-format="dd-mm-yyyy" id="tgl_awal" name="tgl_awal" placeholder="Tanggal Awal" autocomplete="off" required>
    		            </div>
    				</div>

    				<div class="col">
    					<div class="form-group">
    		                <input type="text" class="form-control date-picker" data-date-format="dd-mm-yyyy" id="tgl_akhir" name="tgl_akhir" placeholder="Tanggal Akhir" autocomplete="off" required>
    		            </div>
    				</div>

    				<div class="col">
    					<div class="form-group">
    		                <button type="button" class="btn btn-success" id="btnTampil">Tampilkan</button>
    			  		</div>
    				</div>

    				<div class="col">
    					<div class="form-group right">
    		                <!-- tombol cetak data ke format pdf -->
                            <a style="display:none" class="btn btn-warning mr-2 mb-3" id="btnCetak" href="javascript:void(0);" role="button">
                                <i class="fas fa-print title-icon"></i> Cetak
                            </a>
                            <!-- tombol export data ke format excel -->
                            <a style="display:none" class="btn btn-success mb-3" id="btnExport" href="javascript:void(0);" role="button">
                                <i class="fas fa-file-export title-icon"></i> Export
                            </a>
    			  		</div>
    				</div>
    			</div>
            </form>
        </div>
    </div>

    <div class="border mt-2 mb-4"></div>

    <div class="row">
        <div style="display:none" id="tabelLaporan" class="col-md-12">
            <!-- tampilkan periode pembelian -->
            <div class="alert alert-success" role="alert">
                <i class="fas fa-info-circle title-icon"></i><span id="judulLaporan"></span>
            </div>
            <!-- parameter untuk memuat isi tabel laporan pembelian -->
            <div class="row" id="loadData"></div>
        </div>
    </div>

    <script type="text/javascript">
    $(document).ready(function(){
        // ======================================= initiate plugin =======================================
        // datepicker plugin
        $('.date-picker').datepicker({
            autoclose: true,
            todayHighlight: true
        });
        // ===============================================================================================

        // ============================================ view =============================================
        // Tampilkan tabel laporan pembelian
        $('#btnTampil').click(function(){
            // validasi form input
            // jika tanggal awal kosong
            if ($('#tgl_awal').val()==""){
                // focus ke input tanggal awal
                $( "#tgl_awal" ).focus();
                // tampilkan peringatan data tidak boleh kosong
                $.notify({
                    title   : '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                    message : 'Tanggal awal tidak boleh kosong.'
                },{type: 'warning'});
            }
            // jika tanggal akhir kosong
            else if ($('#tgl_akhir').val()==""){
                // focus ke input tanggal akhir
                $( "#tgl_akhir" ).focus();
                // tampilkan peringatan data tidak boleh kosong
                $.notify({
                    title   : '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                    message : 'Tanggal akhir tidak boleh kosong.'
                },{type: 'warning'});
            }
            // jika semua tanggal sudah diisi, jalankan perintah untuk menampilkan data
            else {
            	// membuat variabel untuk menampung data dari form filter
            	var data = $('#formFilter').serialize();

            	$.ajax({
    				type : "GET",                           	                   // mengirim data dengan method GET
    				url  : "modules/laporan-pembelian-perincian/get_data.php",     // proses get data pembelian berdasarkan tanggal
    				data : data,                 				                   // data yang dikirim
    	            success: function(data){                                       // ketika proses get data selesai
                        // cek value "tgl_awal" dan "tgl_akhir"
                        // jika "tgl_awal" sama dengan "tgl_akhir"
                        if ($('#tgl_awal').val() === $('#tgl_akhir').val()) {
                            // tampilkan judul laporan
                            $('#judulLaporan').html('Laporan Pembelian Perincian, Periode <strong>'+ $('#tgl_awal').val() +'</strong>.');
                        }
                        // jika tgl_awal" tidak sama dengan "tgl_akhir"
                        else {
                            // tampilkan judul laporan
                            $('#judulLaporan').html('Laporan Pembelian Perincian, Periode <strong>'+ $('#tgl_awal').val() +'</strong> s.d. <strong>'+ $('#tgl_akhir').val() +'</strong>.');
                        }

                        // tampilkan tabel laporan pembelian
                        $('#tabelLaporan').removeAttr('style');
                        // tampilkan data laporan pembelian
                        $('#loadData').html(data);
                        // tampilkan tombol cetak
                        $('#btnCetak').removeAttr('style');
                        // tampilkan tombol export
                        $('#btnExport').removeAttr('style');
    	            }
    	        });
            }
        });
        // ===============================================================================================

        // =========================================== Cetak =============================================
        $('#btnCetak').click(function(){
            // validasi form input
            // jika tanggal awal kosong
            if ($('#tgl_awal').val()==""){
                // focus ke input tanggal awal
                $( "#tgl_awal" ).focus();
                // tampilkan peringatan data tidak boleh kosong
                $.notify({
                    title   : '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                    message : 'Tanggal awal tidak boleh kosong.'
                },{type: 'warning'});
            }
            // jika tanggal akhir kosong
            else if ($('#tgl_akhir').val()==""){
                // focus ke input tanggal akhir
                $( "#tgl_akhir" ).focus();
                // tampilkan peringatan data tidak boleh kosong
                $.notify({
                    title   : '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                    message : 'Tanggal akhir tidak boleh kosong.'
                },{type: 'warning'});
            }
            // jika semua tanggal sudah diisi, jalankan perintah untuk cetak laporan
            else {
                // ambil value "tgl_awal" dan "tgl_akhir" dari form filter
                var tgl_awal  = $('#tgl_awal').val();
                var tgl_akhir = $('#tgl_akhir').val();
                // buka file "cetak.php" dan kirim "value"
                window.open('laporan-pembelian-perincian-cetak-'+ tgl_awal +'-sd-'+ tgl_akhir);
            }
        });
        // ===============================================================================================

        // =========================================== Export ============================================
        $('#btnExport').click(function(){
            // validasi form input
            // jika tanggal awal kosong
            if ($('#tgl_awal').val()==""){
                // focus ke input tanggal awal
                $( "#tgl_awal" ).focus();
                // tampilkan peringatan data tidak boleh kosong
                $.notify({
                    title   : '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                    message : 'Tanggal awal tidak boleh kosong.'
                },{type: 'warning'});
            }
            // jika tanggal akhir kosong
            else if ($('#tgl_akhir').val()==""){
                // focus ke input tanggal akhir
                $( "#tgl_akhir" ).focus();
                // tampilkan peringatan data tidak boleh kosong
                $.notify({
                    title   : '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                    message : 'Tanggal akhir tidak boleh kosong.'
                },{type: 'warning'});
            }
            // jika semua tanggal sudah diisi, jalankan perintah untuk export laporan
            else {
                // ambil value "tgl_awal" dan "tgl_akhir" dari form filter
                var tgl_awal  = $('#tgl_awal').val();
                var tgl_akhir = $('#tgl_akhir').val();
                // arahkan ke file "export.php" dan kirim "value"
                location.href = "laporan-pembelian-perincian-export-"+ tgl_awal +"-sd-"+ tgl_akhir;
            }
        });
        // ===============================================================================================
    });
    </script>
<?php } ?>
