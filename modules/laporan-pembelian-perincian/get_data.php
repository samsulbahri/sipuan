<?php
// Mengecek AJAX Request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' )) {
    // panggil file "config.php" untuk koneksi ke database
    require_once "../../config/config.php";

    // mengecek data get dari ajax
    if (isset($_GET['tgl_awal'])) {
        try {
            // ambil "data" get dari ajax
            $tgl_awal  = date('Y-m-d', strtotime($_GET['tgl_awal']));
            $tgl_akhir = date('Y-m-d', strtotime($_GET['tgl_akhir']));
            // variabel untuk nomor urut tabel
            $no = 1;
            // variabel untuk total bayar
            $total = 0;

            // sql statement untuk menampilkan data dari tabel "pembelian" berdasarkan "tanggal"
            $query1 = "SELECT a.no_pembelian,a.tanggal,a.supplier,a.no_nota,a.total_bayar,a.created_user,b.nama_supplier,c.nama_user
                       FROM pembelian as a INNER JOIN supplier as b INNER JOIN sys_users as c ON a.supplier=b.kode_supplier AND a.created_user=c.id_user
                       WHERE a.tanggal BETWEEN :tgl_awal AND :tgl_akhir ORDER BY a.tanggal ASC, a.no_pembelian ASC";
            // membuat prepared statements
            $stmt1 = $pdo->prepare($query1);

            // hubungkan "data" dengan prepared statements
            $stmt1->bindParam(':tgl_awal', $tgl_awal);
            $stmt1->bindParam(':tgl_akhir', $tgl_akhir);

            // eksekusi query
            $stmt1->execute();

            // cek hasil query
            // jika data ada, lakukan perulangan untuk menampilkan data
            if ($stmt1->rowCount() <> 0) {
                    // Tabel untuk menampilkan data pembelian dari database
                    echo "<div class='form-group col-md-12'>
                            <table class='table-report'>";
                // tampilkan hasil query
                while ($data1 = $stmt1->fetch(PDO::FETCH_ASSOC)) {
                    // tampilkan data no_pembelian
                    $no_pembelian = $data1['no_pembelian'];
                    // tampilkan data pembelian
                    echo "      <tr><td style='border-top:1px solid #dee2e6' height='0' colspan='12'></td></tr>
                                <tr>
                                    <td width='50'><strong>".$no.".</strong></td>
                                    <td width='50'>Tanggal</td>
                                    <td width='5'>:</td>
                                    <td width='50'>".date('d-m-Y', strtotime($data1['tanggal']))."</td>
                                    <td width='25'></td>
                                    <td width='50'>Supplier</td>
                                    <td width='5'>:</td>
                                    <td width='170'>".$data1['supplier']." - ".$data1['nama_supplier']."</td>
                                    <td width='25'></td>
                                    <td width='80'>Diinput Oleh</td>
                                    <td width='5'>:</td>
                                    <td width='120'>".$data1['nama_user']."</td>
                                </tr>
                                <tr>
                                    <td width='50'></td>
                                    <td width='50'>No. Pembelian</td>
                                    <td width='5'>:</td>
                                    <td width='50'>".$data1['no_pembelian']."</td>
                                    <td width='25'></td>
                                    <td width='50'>No. Nota</td>
                                    <td width='5'>:</td>
                                    <td width='170' colspan='5'>".$data1['no_nota']."</td>
                                </tr>
                                <tr>
                                    <td width='50'></td>
                                    <td style='border-bottom:1px solid #dee2e6' colspan='10'></td>
                                </tr>
                                <tr>
                                    <td width='50'></td>
                                    <td colspan='12'>Daftar Pembelian Bahan Baku : </td>
                                </tr>
                                <tr>
                                    <td width='50'></td>
                                    <td colspan='12'>
                                        <table class='table-report table-bordered' style='width:100%'>
                                            <thead>
                                                <tr class='table-report-bg-dark'>
                                                    <th>Kode Bahan Baku</th>
                                                    <th>Nama Bahan Baku</th>
                                                    <th>Harga</th>
                                                    <th>Jumlah</th>
                                                    <th>Total</th>
                                                </tr>
                                            </thead>
                                            <tbody>";
                    $no++;

                    // sql statement untuk menampilkan data dari tabel "pembelian_detail" berdasarkan "no_pembelian"
                    $query2 = "SELECT a.bahanbaku,a.harga_beli,a.jumlah_beli,a.total_harga,b.nama_bahanbaku
                               FROM pembelian_detail as a INNER JOIN bahanbaku as b ON a.bahanbaku=b.kode_bahanbaku WHERE a.no_pembelian=:no_pembelian";
                    // membuat prepared statements
                    $stmt2 = $pdo->prepare($query2);

                    // hubungkan "data" dengan prepared statements
                    $stmt2->bindParam(':no_pembelian', $no_pembelian);

                    // eksekusi query
                    $stmt2->execute();

                    // tampilkan hasil query
                    while ($data2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                        // tampilkan detail data pembelian bahanbaku
                        echo "                  <tr>
                                                    <td width='80' class='center'>".$data2['bahanbaku']."</td>
                                                    <td width='180'>".$data2['nama_bahanbaku']."</td>
                                                    <td width='100' class='right'>Rp. ".number_format($data2['harga_beli'], 0, ".", ".")."</td>
                                                    <td width='70' class='right'>".$data2['jumlah_beli']."</td>
                                                    <td width='120' class='right'>Rp. ".number_format($data2['total_harga'], 0, ".", ".")."</td>
                                                </tr>";
                    };
                    echo "                      <tr class='table-report-bg-dark'>
                                                    <td class='center' colspan='4'><strong>Total</strong></td>
                                                    <td class='right'><strong>Rp. ".number_format($data1['total_bayar'], 0, ".", ".")."</strong></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>";

                    $total += $data1['total_bayar'];
                };
                echo "          <tr class='table-report-bg-dark'>
                                    <td style='padding-right: 2.5rem' class='right' colspan='10'><strong>TOTAL SELURUH : </strong></td>
                                    <td style='padding-right: 1.5rem' class='right' colspan='2'><strong>Rp. ".number_format($total, 0, ".", ".")."</strong></td>
                                </tr>
                            </table>
                        </div>";
            }
            // jika data tidak ada
            else {
                echo "<div class='form-group col-md-12'>
                        <div class='alert alert-warning' role='alert'>
                            <i class='fas fa-exclamation-triangle title-icon'></i>Tidak ada transaksi pembelian pada periode ".$_GET['tgl_awal']." s.d. ".$_GET['tgl_akhir']."
                        </div>
                    </div>";
            }

            // tutup koneksi
            $pdo = null;
        } catch (PDOException $e) {
            // tampilkan pesan kesalahan
            echo "Query Error : ".$e->getMessage();
        }
    }
} else {
    // jika tidak ada ajax request, maka alihkan ke halaman "login-error"
    echo '<script>window.location="../../login-error"</script>';
}
?>
