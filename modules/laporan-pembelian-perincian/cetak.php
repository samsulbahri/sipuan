<?php
session_start();      // memulai session
ob_start();

// fungsi untuk pengecekan status login user
// jika user belum login, alihkan ke halaman "login-error"
if (empty($_SESSION['username']) && empty($_SESSION['password'])){
    echo "<meta http-equiv='refresh' content='0; url=../../login-error'>";
}
// jika user sudah login
else {
    // mengecek data get
    if (isset($_GET['tgl_awal']) && isset($_GET['tgl_akhir'])) { ?>
        <!-- Bagian halaman HTML yang akan konvert -->
        <!doctype html>
        <html lang="en">
            <head>
                <!-- Required meta tags -->
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
                <!-- Custom CSS -->
                <link rel="stylesheet" type="text/css" href="../../assets/css/laporan.css" />
                <!-- Title -->
                <title>LAPORAN PEMBELIAN PERINCIAN</title>
            </head>
            <body>
                <?php
                // panggil file "config.php" untuk koneksi ke database
                require_once "../../config/config.php";
                // panggil file "fungsi_tanggal.php" untuk format tanggal
                require_once "../../config/fungsi_tanggal.php";

                try {
                    // siapkan "data"
                    $id = 1;

                    // sql statement untuk menampilkan data dari tabel "sys_config" berdasarkan "id"
                    $query = "SELECT nama, alamat, telepon, logo FROM sys_config WHERE id=:id";
                    // membuat prepared statements
                    $stmt = $pdo->prepare($query);

                    // hubungkan "data" dengan prepared statements
                    $stmt->bindParam(':id', $id);

                    // eksekusi query
                    $stmt->execute();

                    // ambil data hasil query
                    $data = $stmt->fetch(PDO::FETCH_ASSOC);
                    // tampilkan data
                    $nama    = $data['nama'];
                    $alamat  = $data['alamat'];
                    $telepon = $data['telepon'];
                    $logo    = $data['logo'];

                } catch (PDOException $e) {
                    // tampilkan pesan kesalahan
                    echo "Query Error : ".$e->getMessage();
                }
                ?>

                <!-- Kop Laporan -->
                <div class="nama-instansi"><?php echo $nama; ?></div>
                <div class="info-instansi"><?php echo $alamat; ?></div>
                <div class="info-instansi">Telp. <?php echo $telepon; ?></div>
                <div class="logo">
                    <img src="../../assets/img/<?php echo $logo; ?>" alt="Logo">
                </div>

                <hr><br>

                <!-- Judul Laporan -->
                <div class="judul-laporan">
                    LAPORAN PEMBELIAN PERINCIAN
                </div>

                <?php
                // jika "tgl_awal" sama dengan "tgl_akhir"
                if ($_GET['tgl_awal'] == $_GET['tgl_akhir']) { ?>
                    <!-- tampilkan judul -->
                    <div class="judul-tanggal">
                        <?php echo tgl_eng_to_ind($_GET['tgl_awal']); ?>
                    </div>
                <?php
                }
                // jika tgl_awal" tidak sama dengan "tgl_akhir"
                else { ?>
                    <!-- tampilkan judul -->
                    <div class="judul-tanggal">
                        <?php echo tgl_eng_to_ind($_GET['tgl_awal']); ?> s.d. <?php echo tgl_eng_to_ind($_GET['tgl_akhir']); ?>
                    </div>
                <?php
                }
                ?>

                <br>
                <!-- Tabel untuk menampilkan data pembelian dari database -->
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <?php
            try {
                // ambil "data" get dari ajax
                $tgl_awal  = date('Y-m-d', strtotime($_GET['tgl_awal']));
                $tgl_akhir = date('Y-m-d', strtotime($_GET['tgl_akhir']));
                // variabel untuk nomor urut tabel
                $no = 1;
                // variabel untuk total bayar
                $total = 0;

                // sql statement untuk menampilkan data dari tabel "pembelian" berdasarkan "tanggal"
                $query1 = "SELECT a.no_pembelian,a.tanggal,a.supplier,a.no_nota,a.total_bayar,a.created_user,b.nama_supplier,c.nama_user
                           FROM pembelian as a INNER JOIN supplier as b INNER JOIN sys_users as c ON a.supplier=b.kode_supplier AND a.created_user=c.id_user
                           WHERE a.tanggal BETWEEN :tgl_awal AND :tgl_akhir ORDER BY a.tanggal ASC, a.no_pembelian ASC";
                // membuat prepared statements
                $stmt1 = $pdo->prepare($query1);

                // hubungkan "data" dengan prepared statements
                $stmt1->bindParam(':tgl_awal', $tgl_awal);
                $stmt1->bindParam(':tgl_akhir', $tgl_akhir);

                // eksekusi query
                $stmt1->execute();

                // tampilkan hasil query
                while ($data1 = $stmt1->fetch(PDO::FETCH_ASSOC)) {
                    // tampilkan data no_pembelian
                    $no_pembelian = $data1['no_pembelian'];
                ?>
                    <!-- tampilkan data pembelian bahanbaku -->
                    <tr>
                        <td class="border-dotted" height="30" align="right" colspan="10">-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</td>
                    </tr>
                    <tr>
                        <td width="50" height="20" valign="middle"><?php echo $no; ?>.</td>
                        <td width="110" height="20" valign="middle">Tanggal</td>
                        <td width="10" height="20" align="center" valign="middle">:</td>
                        <td width="180" height="20" valign="middle"><?php echo date('d / m / Y', strtotime($data1['tanggal'])); ?></td>
                        <td width="80" height="20" valign="middle">Supplier</td>
                        <td width="10" height="20" align="center" valign="middle">:</td>
                        <td width="255" height="20" valign="middle"><?php echo $data1['supplier']; ?> - <?php echo $data1['nama_supplier']; ?></td>
                        <td width="100" height="20" valign="middle">Diinput Oleh</td>
                        <td width="10" height="20" align="center" valign="middle">:</td>
                        <td width="180" height="20" valign="middle"><?php echo $data1['nama_user']; ?></td>
                    </tr>
                    <tr>
                        <td height="20" valign="middle"></td>
                        <td width="110" height="20" valign="middle">No. Pembelian</td>
                        <td width="10" height="20" align="center" valign="middle">:</td>
                        <td width="180" height="20" valign="middle"><?php echo $data1['no_pembelian']; ?></td>
                        <td width="80" height="20" valign="middle">No. Nota</td>
                        <td width="10" height="20" align="center" valign="middle">:</td>
                        <td height="20" valign="middle" colspan="4"><?php echo $data1['no_nota']; ?></td>
                    </tr>
                    <tr>
                        <td class="border-dotted" height="2" align="right" colspan="10">-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</td>
                    </tr>
                    <tr>
                        <td height="20" valign="middle"></td>
                        <td height="20" valign="middle" colspan="9">Daftar Pembelian Bahan Baku :</td>
                    </tr>
                    <tr>
                        <td height="20" valign="middle"></td>
                        <td height="20" valign="middle" colspan="9">
                            <!-- Tabel untuk menampilkan detail data pembelian dari database -->
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <thead>
                                    <tr class="tr-judul">
                                        <th class="td-left" height="30" align="left" valign="middle">Kode Bahan Baku</th>
                                        <th height="30" align="left" valign="middle">Nama Bahan Baku</th>
                                        <th height="30" align="right" valign="middle">Harga</th>
                                        <th height="30" align="right" valign="middle">Jumlah</th>
                                        <th class="td-right" height="30" align="right" valign="middle">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                // sql statement untuk menampilkan data dari tabel "pembelian_detail" berdasarkan "no_pembelian"
                                $query2 = "SELECT a.bahanbaku,a.harga_beli,a.jumlah_beli,a.total_harga,b.nama_bahanbaku
                                           FROM pembelian_detail as a INNER JOIN bahanbaku as b ON a.bahanbaku=b.kode_bahanbaku WHERE a.no_pembelian=:no_pembelian";
                                // membuat prepared statements
                                $stmt2 = $pdo->prepare($query2);

                                // hubungkan "data" dengan prepared statements
                                $stmt2->bindParam(':no_pembelian', $no_pembelian);

                                // eksekusi query
                                $stmt2->execute();

                                // tampilkan hasil query
                                while ($data2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                                ?>
                                    <!-- tampilkan detail data pembelian bahanbaku -->
                                    <tr>
                                        <td class="td-left" width="150" height="20"><?php echo $data2['bahanbaku']; ?></td>
                                        <td width="230" height="20"><?php echo $data2['nama_bahanbaku']; ?></td>
                                        <td width="180" height="20" align="right">Rp. <?php echo number_format($data2['harga_beli'], 0, ".", "."); ?></td>
                                        <td width="130" height="20" align="right"><?php echo $data2['jumlah_beli']; ?></td>
                                        <td class="td-right" width="200" height="20" align="right">Rp. <?php echo number_format($data2['total_harga'], 0, ".", "."); ?></td>
                                    </tr>
                                <?php
                                };
                                ?>
                                    <tr class="tr-judul">
                                        <td height="30" align="right" colspan="4"><strong>Total :</strong></td>
                                        <td class="td-right" height="30" align="right"><strong>Rp. <?php echo number_format($data1['total_bayar'], 0, ".", "."); ?></strong></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                <?php
                    $no++;
                    $total += $data1['total_bayar'];
                };
                ?>
                    <tr>
                        <td class="border-dotted" height="30" align="right" colspan="10">-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</td>
                    </tr>
                    <tr class="tr-judul">
                        <td class="td-right-total-judul" height="30" align="right" colspan="9"><strong>Total Seluruh : </strong></td>
                        <td class="td-right-total-isi" height="30" align="right"><strong>Rp. <?php echo number_format($total, 0, ".", "."); ?></strong></td>
                    </tr>
                </table>
                <?php
                // tutup koneksi
                $pdo = null;
            } catch (PDOException $e) {
                // tampilkan pesan kesalahan
                echo "Query Error : ".$e->getMessage();
            }
            ?>

                <div class="footer-tanggal">
                    <p>Balikpapan, <?php echo tgl_eng_to_ind(date("d-m-Y")); ?></p> <br><br>
                    <p>.....................................................</p>
                </div>
            </body>
        </html> <!-- Akhir halaman HTML yang akan di konvert -->

        <?php
        // jika "tgl_awal" sama dengan "tgl_akhir"
        if ($_GET['tgl_awal'] == $_GET['tgl_akhir']) {
            // nama file pdf yang dihasilkan
            $filename="LAPORAN-PEMBELIAN-PERINCIAN-".$_GET['tgl_awal'].".pdf";
        }
        // jika tgl_awal" tidak sama dengan "tgl_akhir"
        else {
            // nama file pdf yang dihasilkan
            $filename="LAPORAN-PEMBELIAN-PERINCIAN-".$_GET['tgl_awal']."-sd-".$_GET['tgl_akhir'].".pdf";
        }

        // ====================================== Convert HTML ke PDF ======================================
        $content = ob_get_clean();
        $content = '<page style="font-family: freeserif">'.($content).'</page>';
        // panggil file library html2pdf
        require_once('../../assets/plugins/html2pdf_v4.03/html2pdf.class.php');
        try
        {
            $html2pdf = new HTML2PDF('L','A4','en', false, 'ISO-8859-15',array(10, 8, 10, 8));
            $html2pdf->setDefaultFont('Arial');
            $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
            $html2pdf->Output($filename);
        }
        catch(HTML2PDF_exception $e) { echo $e; }
    }
}
?>
