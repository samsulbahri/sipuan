<?php
// Mengecek AJAX Request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' )) {
    // panggil file "config.php" untuk koneksi ke database
    require_once "../../config/config.php";

    // mengecek data get dari ajax
    if (isset($_GET['tgl_awal'])) {
        try {
            // ambil "data" get dari ajax
            $tgl_awal  = date('Y-m-d', strtotime($_GET['tgl_awal']));
            $tgl_akhir = date('Y-m-d', strtotime($_GET['tgl_akhir']));
            // variabel untuk nomor urut tabel
            $no = 1;
            // variabel untuk total bayar
            $total = 0;

            // sql statement untuk menampilkan data dari tabel "penjualan" berdasarkan "tanggal"
            $query1 = "SELECT a.no_penjualan,a.tanggal,a.total_bayar,a.created_user,b.nama_user, (SELECT SUM(e.jumlah_jual)
                       FROM penjualan_detail as e WHERE e.no_penjualan=a.no_penjualan) as total_pemakaian
                       FROM penjualan as a INNER JOIN sys_users as b ON a.created_user=b.id_user
                       WHERE a.tanggal BETWEEN :tgl_awal AND :tgl_akhir ORDER BY a.tanggal ASC, a.no_penjualan ASC";
            // membuat prepared statements
            $stmt1 = $pdo->prepare($query1);

            // hubungkan "data" dengan prepared statements
            $stmt1->bindParam(':tgl_awal', $tgl_awal);
            $stmt1->bindParam(':tgl_akhir', $tgl_akhir);

            // eksekusi query
            $stmt1->execute();

            // cek hasil query
            // jika data ada, lakukan perulangan untuk menampilkan data
            if ($stmt1->rowCount() <> 0) {
                    // Tabel untuk menampilkan data penjualan dari database
                    echo "<div class='form-group col-md-12'>
                            <table class='table-report'>";
                // tampilkan hasil query
                while ($data1 = $stmt1->fetch(PDO::FETCH_ASSOC)) {
                    // tampilkan data no_penjualan
                    $no_penjualan = $data1['no_penjualan'];
                    // tampilkan data penjualan
                    echo "      <tr><td style='border-top:1px solid #dee2e6' height='0' colspan='6'></td></tr>
                                <tr>
                                    <td width='50'><strong>".$no.".</strong></td>
                                    <td width='117'>Tanggal</td>
                                    <td width='5'>:</td>
                                    <td width='550'>".date('d-m-Y', strtotime($data1['tanggal']))."</td>
                                    <td width='25'></td>
                                </tr>
                                <tr>
                                    <td width='50'></td>
                                    <td width='117'>No. Penjualan</td>
                                    <td width='5'>:</td>
                                    <td width='550' colspan='2'>".$data1['no_penjualan']."</td>
                                </tr>
                                <tr>
                                    <td width='50'></td>
                                    <td style='border-bottom:1px solid #dee2e6' colspan='2'></td>
                                </tr>
                                <tr>
                                    <td width='50'></td>
                                    <td colspan='5'>Daftar Penjualan Bahan Baku : </td>
                                </tr>
                                <tr>
                                    <td width='50'></td>
                                    <td colspan='5'>
                                        <table class='table-report table-bordered' style='width:100%'>
                                            <thead>
                                                <tr class='table-report-bg-dark'>
                                                    <th>Kode bahanbaku</th>
                                                    <th>Nama bahanbaku</th>
                                                    <th>Jumlah</th>
                                                </tr>
                                            </thead>
                                            <tbody>";
                    $no++;

                    // sql statement untuk menampilkan data dari tabel "penjualan_detail" berdasarkan "no_penjualan"
                    $query2 = "SELECT a.bahanbaku,a.harga_jual,a.jumlah_jual,a.total_harga,b.nama_bahanbaku
                               FROM penjualan_detail as a INNER JOIN bahanbaku as b ON a.bahanbaku=b.kode_bahanbaku WHERE a.no_penjualan=:no_penjualan";
                    // membuat prepared statements
                    $stmt2 = $pdo->prepare($query2);

                    // hubungkan "data" dengan prepared statements
                    $stmt2->bindParam(':no_penjualan', $no_penjualan);

                    // eksekusi query
                    $stmt2->execute();

                    // tampilkan hasil query
                    while ($data2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                        // tampilkan detail data penjualan bahanbaku
                        echo "                  <tr>
                                                    <td width='80' class='center'>".$data2['bahanbaku']."</td>
                                                    <td width='180'>".$data2['nama_bahanbaku']."</td>
                                                    <td width='70' class='right'>".$data2['jumlah_jual']."</td>
                                                </tr>";
                    };
                    echo "                      <tr class='table-report-bg-dark'>
                                                    <td class='center' colspan='2'><strong>Total</strong></td>
                                                    <td class='right'><strong>".$data1['total_pemakaian']."</strong></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>";

                    $total += $data1['total_pemakaian'];
                };
                echo "          <tr class='table-report-bg-dark'>
                                    <td style='background-color:white' colspan='1'></td>
                                    <td style='padding-right: 4.5rem' class='right' colspan='2'><strong>TOTAL SELURUH : </strong></td>
                                    <td style='padding-right: 1.5rem' class='right'><strong>".($total)."</strong></td>
                                </tr>
                            </table>
                        </div>";
            }
            // jika data tidak ada
            else {
                echo "<div class='form-group col-md-12'>
                        <div class='alert alert-warning' role='alert'>
                            <i class='fas fa-exclamation-triangle title-icon'></i>Tidak ada transaksi penjualan pada periode ".$_GET['tgl_awal']." s.d. ".$_GET['tgl_akhir']."
                        </div>
                    </div>";
            }

            // tutup koneksi
            $pdo = null;
        } catch (PDOException $e) {
            // tampilkan pesan kesalahan
            echo "Query Error : ".$e->getMessage();
        }
    }
} else {
    // jika tidak ada ajax request, maka alihkan ke halaman "login-error"
    echo '<script>window.location="../../login-error"</script>';
}
?>
