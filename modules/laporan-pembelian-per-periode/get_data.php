<?php
// Mengecek AJAX Request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' )) {
    // panggil file "config.php" untuk koneksi ke database
    require_once "../../config/config.php";

    // mengecek data get dari ajax
    if (isset($_GET['tgl_awal'])) {
        try {
            // ambil "data" get dari ajax
            $tgl_awal  = date('Y-m-d', strtotime($_GET['tgl_awal']));
            $tgl_akhir = date('Y-m-d', strtotime($_GET['tgl_akhir']));
            // variabel untuk nomor urut tabel
            $no = 1;
            // variabel untuk total bayar
            $total = 0;

            // sql statement untuk menampilkan data dari tabel "pembelian" berdasarkan "tanggal"
            $query = "SELECT a.no_pembelian,a.tanggal,a.supplier,a.no_nota,a.total_bayar,b.nama_supplier 
                      FROM pembelian as a INNER JOIN supplier as b ON a.supplier=b.kode_supplier
                      WHERE a.tanggal BETWEEN :tgl_awal AND :tgl_akhir ORDER BY a.tanggal ASC, a.no_pembelian ASC";
            // membuat prepared statements
            $stmt = $pdo->prepare($query);

            // hubungkan "data" dengan prepared statements
            $stmt->bindParam(':tgl_awal', $tgl_awal);
            $stmt->bindParam(':tgl_akhir', $tgl_akhir);

            // eksekusi query
            $stmt->execute();

            // cek hasil query
            // jika data ada, lakukan perulangan untuk menampilkan data
            if ($stmt->rowCount() <> 0) {
                // tampilkan hasil query
                while ($data = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    echo "<tr>
                            <td width='50' class='center'>".$no."</td>
                            <td width='100' class='center'>".date('d-m-Y', strtotime($data['tanggal']))."</td>
                            <td width='120' class='center'>".$data['no_pembelian']."</td>
                            <td width='200'>".$data['supplier']." - ".$data['nama_supplier']."</td>
                            <td width='120' class='center'>".$data['no_nota']."</td>
                            <td width='140' class='right'>Rp. ".number_format($data['total_bayar'], 0, ".", ".")."</td>
                        </tr>";
                    $no++;
                    $total += $data['total_bayar'];
                };
                echo "<tr>
                        <td class='center' colspan='5'><strong>Total</strong></td>
                        <td class='right'><strong>Rp. ".number_format($total, 0, ".", ".")."</strong></td>
                    </tr>";
            }
            // jika data tidak ada
            else {
                echo "<tr>
                        <td class='center' colspan='6'>Tidak ada data yang tersedia pada tabel</td>
                    </tr>";
            }

            // tutup koneksi
            $pdo = null;
        } catch (PDOException $e) {
            // tampilkan pesan kesalahan
            echo "Query Error : ".$e->getMessage();
        }
    }
} else {
    // jika tidak ada ajax request, maka alihkan ke halaman "login-error"
    echo '<script>window.location="../../login-error"</script>';
}
?>