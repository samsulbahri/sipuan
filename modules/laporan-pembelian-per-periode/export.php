<?php
session_start();      // memulai session

// fungsi untuk pengecekan status login user 
// jika user belum login, alihkan ke halaman "login-error"
if (empty($_SESSION['username']) && empty($_SESSION['password'])){
    echo "<meta http-equiv='refresh' content='0; url=../../login-error'>";
}
// jika user sudah login
else {
    // mengecek data get
    if (isset($_GET['tgl_awal']) && isset($_GET['tgl_akhir'])) {
        // panggil file "config.php" untuk koneksi ke database
        require_once "../../config/config.php";
        // panggil file "fungsi_tanggal.php" untuk format tanggal
        require_once "../../config/fungsi_tanggal.php";

        // fungsi untuk export data
        header("Content-Type: application/force-download");
        header("Cache-Control: no-cache, must-revalidate");
        // cek filter tanggal
        // jika "tgl_awal" sama dengan "tgl_akhir"
        if ($_GET['tgl_awal'] == $_GET['tgl_akhir']) {
            // nama file hasil export
            header("content-disposition: attachment;filename=LAPORAN-PEMBELIAN-PER-PERIODE-".$_GET['tgl_awal'].".xls");
        } 
        // jika "tgl_awal" tidak sama dengan "tgl_akhir"
        else {
            // nama file hasil export
            header("content-disposition: attachment;filename=LAPORAN-PEMBELIAN-PER-PERIODE-".$_GET['tgl_awal']."-sd-".$_GET['tgl_akhir'].".xls");
        }
        
    ?>
        <!-- Judul laporan -->
        <center>
            <h3>
                LAPORAN PEMBELIAN PER PERIODE <br>
            <?php 
            // jika "tgl_awal" sama dengan "tgl_akhir"
            if ($_GET['tgl_awal'] == $_GET['tgl_akhir']) {
                // tampilkan judul laporan "tgl_awal"
                echo "'".tgl_eng_to_ind($_GET['tgl_awal']);
            } 
            // jika tgl_awal" tidak sama dengan "tgl_akhir"
            else {
                // tampilkan judul laporan "tgl_awal" s.d. "tgl_akhir" 
                echo tgl_eng_to_ind($_GET['tgl_awal']) ." s.d. ". tgl_eng_to_ind($_GET['tgl_akhir']);
            }
            ?>
            </h3>
        </center>
        <!-- Table untuk di Export Ke Excel -->
        <table border='1'>
            <h3>
                <thead>
                    <tr>
                        <th width='60' align="center" valign="middle">No.</th>
                        <th width='130' align="center" valign="middle">Tanggal</th>
                        <th width='150' align="center" valign="middle">No. Pembelian</th>
                        <th width='290' align="center" valign="middle">Supplier</th>
                        <th width='160' align="center" valign="middle">No. Nota</th>
                        <th width='180' align="center" valign="middle">Total</th>
                    </tr>
                </thead>
            </h3>

            <tbody>
            <?php  
            try {
                // ambil "data" get
                $tgl_awal  = date('Y-m-d', strtotime($_GET['tgl_awal']));
                $tgl_akhir = date('Y-m-d', strtotime($_GET['tgl_akhir']));
                // variabel untuk nomor urut tabel
                $no = 1;
                // variabel untuk total bayar
                $total = 0;

                // sql statement untuk menampilkan data dari tabel "pembelian" berdasarkan "tanggal"
                $query = "SELECT a.no_pembelian,a.tanggal,a.supplier,a.no_nota,a.total_bayar,b.nama_supplier 
                          FROM pembelian as a INNER JOIN supplier as b ON a.supplier=b.kode_supplier
                          WHERE a.tanggal BETWEEN :tgl_awal AND :tgl_akhir ORDER BY a.tanggal ASC, a.no_pembelian ASC";
                // membuat prepared statements
                $stmt = $pdo->prepare($query);

                // hubungkan "data" dengan prepared statements
                $stmt->bindParam(':tgl_awal', $tgl_awal);
                $stmt->bindParam(':tgl_akhir', $tgl_akhir);

                // eksekusi query
                $stmt->execute();

                // cek hasil query
                // jika data ada, lakukan perulangan untuk menampilkan data
                if ($stmt->rowCount() <> 0) {
                    // tampilkan hasil query
                    while ($data = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        echo "<tr>
                                <td width='60' align='center'>".$no."</td>
                                <td width='130' align='center'>".date('d-m-Y', strtotime($data['tanggal']))."</td>
                                <td width='150' align='center'>".$data['no_pembelian']."</td>
                                <td width='290'>".$data['supplier']." - ".$data['nama_supplier']."</td>
                                <td width='160' align='center'>".$data['no_nota']."</td>
                                <td width='180' align='right'>Rp. ".number_format($data['total_bayar'], 0, ".", ".")."</td>
                            </tr>";
                        $no++;
                        $total += $data['total_bayar'];
                    };
                    echo "<tr>
                            <td align='center' colspan='5'><strong>Total</strong></td>
                            <td align='right'><strong>Rp. ".number_format($total, 0, ".", ".")."</strong></td>
                        </tr>";
                }
                // jika data tidak ada
                else {
                    echo "<tr>
                            <td height='20' align='center' colspan='6'>Tidak ada data yang tersedia pada tabel</td>
                        </tr>";
                }

                // tutup koneksi
                $pdo = null;
            } catch (PDOException $e) {
                // tampilkan pesan kesalahan
                echo "Query Error : ".$e->getMessage();
            }
            ?>
            </tbody>
        </table>

        <div style="text-align:right">
            <h4>Bandar Lampung, <?php echo date("d/m/Y"); ?></h4> <br><br>
            <h4>.................................................</h4>
        </div>
    <?php 
    }
}
?>