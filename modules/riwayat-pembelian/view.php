<?php
// fungsi untuk pengecekan status login user
// jika user belum login, alihkan ke halaman "login-error"
if (empty($_SESSION['username']) && empty($_SESSION['password'])) {
    echo "<meta http-equiv='refresh' content='0; url=../../login-error'>";
}
// jika user sudah login
else { ?>
    <div class="row mb-3">
        <div class="col-md-12">
            <h5>
                <!-- judul halaman tampil data bahanbaku -->
                <i class="fas fa-boxes title-icon"></i> Riwayat Pemesanan Bahan Baku
            </h5>
        </div>
    </div>

    <div class="border mb-4"></div>

    <div class="row mt-5">
        <div class="col-md-12">
            <!-- Tabel bahanbaku untuk menampilkan data bahanbaku dari database -->
            <table id="tabel-bahanbaku" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <!-- judul kolom pada bagian kepala (atas) tabel -->
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>No. Pembelian</th>
                        <th>Tanggal</th>
                        <th>Kode Supplier</th> <!-- kolom "Kode Supplier" disembunyikan, digabung dengan kolom "Supplier" -->
                        <th>Supplier</th>
                        <th>No. Nota</th>
                        <th>Total</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    <div id="modal-confirm">

    </div>

    <div id="modal-feedback">

    </div>

    <!-- Modal form untuk menampilkan detail data pembelian -->
    <div class="modal fade" id="modalPembelian" tabindex="-1" role="dialog" aria-labelledby="modalPembelian" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <!-- judul form detail data pembelian -->
                <div class="modal-header">
                    <h5 class="modal-title"><i class="fas fa-search-plus title-icon"></i>Kwitansi Data Pembelian</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <!-- parameter untuk memuat isi detail data pembelian -->
                    <div class="row" id="loadPembelian"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(document).ready(function() {
            // ======================================= initiate plugin =======================================
            // chosen-select plugin
            $('.chosen-select').chosen();
            // ===============================================================================================


            // ===============================================================================================

            // ============================================ View =============================================
            // dataTables plugin untuk membuat nomor urut tabel
            $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
                return {
                    "iStart": oSettings._iDisplayStart,
                    "iEnd": oSettings.fnDisplayEnd(),
                    "iLength": oSettings._iDisplayLength,
                    "iTotal": oSettings.fnRecordsTotal(),
                    "iFilteredTotal": oSettings.fnRecordsDisplay(),
                    "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                    "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                };
            };

            // datatables serverside processing
            var table = $('#tabel-bahanbaku').DataTable({
                "processing": true, // tampilkan loading saat proses data
                "serverSide": true, // server-side processing
                "ajax": 'modules/riwayat-pembelian/data.php', // panggil file "data.php" untuk menampilkan data bahanbaku dari database
                // menampilkan data
                "columnDefs": [{
                        "targets": 0,
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "width": '30px',
                        "className": 'center'
                    },
                    {
                        "responsivePriority": 1,
                        "targets": 1,
                        "width": '100px',
                        "className": 'center'
                    },
                    {
                        "targets": 2,
                        "width": '70px',
                        "className": 'center'
                    },
                    {
                        "targets": 3,
                        "width": '70px',
                        "visible": false
                    }, // kolom "kode_supplier" disembunyikan, digabung dengan kolom "nama_supplier"
                    {
                        "targets": 4,
                        "width": '200px',
                        "render": function(data, type, row) {
                            return row[3] + ' - ' + data;
                        }
                    },
                    {
                        "targets": 5,
                        "width": '100px',
                        "className": 'center'
                    },
                    {
                        "targets": 6,
                        "width": '100px',
                        "className": 'right',
                        render: $.fn.DataTable.render.number('.', ',', 0, 'Rp. ')
                    },
                    {
                        "responsivePriority": 2,
                        "targets": 7,
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "width": '60px',
                        "className": 'center',
                        // tombol detail dan hapus
                        "render": function(data, type, row) {
                            var btn = "<a style=\"margin-right:7px\" data-toggle=\"tooltip\" title=\"Lihat Kwitansi\" class=\"btn btn-primary btn-sm getDetail\" href=\"javascript:void(0);\"><i class=\"fas fa-align-left\"></i></a>";
                            return btn;
                        }
                    }
                ],
                "order": [
                    [1, "desc"]
                ], // urutkan data berdasarkan "kode_bahanbaku" secara descending
                "iDisplayLength": 10, // tampilkan 10 data per halaman
                // membuat nomor urut tabel
                "rowCallback": function(row, data, iDisplayIndex) {
                    var info = this.fnPagingInfo();
                    var page = info.iPage;
                    var length = info.iLength;
                    var index = page * length + (iDisplayIndex + 1);
                    $('td:eq(0)', row).html(index);
                }
            });

            // Tampilkan Detail Data Pembelian
            $('#tabel-bahanbaku tbody').on('click', '.getDetail', function() {
                // ambil data dari datatables 
                var data = table.row($(this).parents('tr')).data();
                // membuat variabel untuk menampung data "no_pembelian"
                var no_pembelian = data[1];

                $.ajax({
                    type: "GET", // mengirim data dengan method GET 
                    url: "modules/pembelian/get_data.php", // proses get data pembelian berdasarkan "no_pembelian"
                    data: {
                        no_pembelian: no_pembelian
                    }, // data yang dikirim
                    success: function(result) { // ketika proses get data selesai
                        // tampilkan modal detail data pembelian
                        $('#modalPembelian').modal('show');
                        // tampilkan detail data pembelian
                        $('#loadPembelian').html(result);
                    }
                });
            });
        });
    </script>
<?php } ?>