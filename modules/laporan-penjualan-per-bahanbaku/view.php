<?php
// fungsi untuk pengecekan status login user
// jika user belum login, alihkan ke halaman "login-error"
if (empty($_SESSION['username']) && empty($_SESSION['password'])){
    echo "<meta http-equiv='refresh' content='0; url=../../login-error'>";
}
// jika user sudah login
else { ?>
    <div class="row mb-3">
        <div class="col-md-12">
    		<h5>
    			<!-- judul halaman laporan penjualan per bahanbaku -->
    			<i class="fas fa-clipboard-list title-icon"></i> Laporan Pemakaian Per Bahan Baku
    		</h5>
    	</div>
    </div>

    <div class="border mb-4"></div>

    <div class="row">
        <div class="col-md-12">
            <!-- form filter bahanbaku dan periode penjualan -->
            <form id="formFilter">
            	<div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Periode Pemakaian : </label>
                            <input type="text" class="form-control date-picker" data-date-format="dd-mm-yyyy" id="tgl_awal" name="tgl_awal" placeholder="Tanggal Awal" autocomplete="off" required>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label style="color:transparent">Periode Pemakaian : </label>
                            <input type="text" class="form-control date-picker" data-date-format="dd-mm-yyyy" id="tgl_akhir" name="tgl_akhir" placeholder="Tanggal Akhir" autocomplete="off" required>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Bahan Baku</label>
                            <select class="chosen-select" id="bahanbaku" name="bahanbaku" autocomplete="off">
                                <option value="">-- Pilih --</option>
                                <option value="Seluruh">Seluruh</option>
                                <?php
                                try {
                                    // sql statement untuk menampilkan data dari tabel "bahanbaku"
                                    $query = "SELECT kode_bahanbaku, nama_bahanbaku FROM bahanbaku ORDER BY kode_bahanbaku ASC";
                                    // membuat prepared statements
                                    $stmt = $pdo->prepare($query);

                                    // eksekusi query
                                    $stmt->execute();

                                    // tampilkan hasil query
                                    while ($data_bahanbaku = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                        echo"<option value='$data_bahanbaku[kode_bahanbaku]'> $data_bahanbaku[kode_bahanbaku] - $data_bahanbaku[nama_bahanbaku] </option>";
                                    }

                                    // tutup koneksi
                                    $pdo = null;
                                } catch (PDOException $e) {
                                    // tampilkan pesan kesalahan
                                    echo $e->getMessage();
                                }
                                ?>
                            </select>

                            <input type="hidden" id="nama_bahanbaku" name="nama_bahanbaku">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <button type="button" class="btn btn-success mt-4 mr-4" id="btnTampil">Tampilkan</button>
                            <!-- tombol export data ke format excel -->
                            <a style="display:none" class="btn btn-success mt-4 float-right" id="btnExport" href="javascript:void(0);" role="button">
                                <i class="fas fa-file-export title-icon"></i> Export
                            </a>
                            <!-- tombol cetak data ke format pdf -->
                            <a style="display:none" class="btn btn-warning mt-4 mr-3 float-right" id="btnCetak" href="javascript:void(0);" role="button">
                                <i class="fas fa-print title-icon"></i> Cetak
                            </a>
                        </div>
                    </div>
    			</div>
            </form>
        </div>
    </div>

    <div class="border mt-2 mb-4"></div>

    <div class="row">
        <div style="display:none" id="tabelLaporan" class="col-md-12">
            <!-- tampilkan informasi bahanbaku dan periode penjualan -->
            <div class="alert alert-success" role="alert">
                <i class="fas fa-info-circle title-icon"></i><span id="judulLaporan"></span>
            </div>
            <!-- parameter untuk memuat isi tabel laporan penjualan -->
            <div class="row" id="loadData"></div>
        </div>
    </div>

    <script type="text/javascript">
    $(document).ready(function(){
        // ======================================= initiate plugin =======================================
        // datepicker plugin
        $('.date-picker').datepicker({
            autoclose: true,
            todayHighlight: true
        });

        // chosen-select plugin
        $('.chosen-select').chosen();
        // ===============================================================================================

        // ============================================ Form =============================================
        // Menampilkan nama bahanbaku dari select box ke textfield
        $('#bahanbaku').change(function(){
            // mengambil value dari "kode_bahanbaku"
            var kode_bahanbaku = $('#bahanbaku').val();

            $.ajax({
                type     : "GET",                                   // mengirim data dengan method GET
                url      : "modules/bahanbaku/get_data.php",             // proses get nama bahanbaku berdasarkan "kode_bahanbaku"
                data     : {kode_bahanbaku:kode_bahanbaku},                   // data yang dikirim
                dataType : "JSON",                                  // tipe data JSON
                success: function(result){                          // ketika proses get nama bahanbaku selesai
                    // tampilkan data
                    $("#nama_bahanbaku").val(result.nama_bahanbaku);
                }
            });
        });
        // ===============================================================================================

        // ============================================ view =============================================
        // Tampilkan tabel laporan penjualan
        $('#btnTampil').click(function(){
            // validasi form input
            // jika tanggal awal kosong
            if ($('#tgl_awal').val()==""){
                // focus ke input tanggal awal
                $( "#tgl_awal" ).focus();
                // tampilkan peringatan data tidak boleh kosong
                $.notify({
                    title   : '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                    message : 'Tanggal awal tidak boleh kosong.'
                },{type: 'warning'});
            }
            // jika tanggal akhir kosong
            else if ($('#tgl_akhir').val()==""){
                // focus ke input tanggal akhir
                $( "#tgl_akhir" ).focus();
                // tampilkan peringatan data tidak boleh kosong
                $.notify({
                    title   : '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                    message : 'Tanggal akhir tidak boleh kosong.'
                },{type: 'warning'});
            }
            // jika bahanbaku kosong
            else if ($('#bahanbaku').val()==""){
                // focus ke input bahanbaku
                $( "#bahanbaku" ).focus();
                // tampilkan peringatan data tidak boleh kosong
                $.notify({
                    title   : '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                    message : 'bahanbaku tidak boleh kosong.'
                },{type: 'warning'});
            }
            // jika semua data sudah diisi, jalankan perintah untuk menampilkan data
            else {
            	// membuat variabel untuk menampung data dari form filter
            	var data = $('#formFilter').serialize();

            	$.ajax({
    				type : "GET",                           	                   // mengirim data dengan method GET
    				url  : "modules/laporan-penjualan-per-bahanbaku/get_data.php",      // proses get data penjualan berdasarkan tanggal
    				data : data,                 				                   // data yang dikirim
    	            success: function(data){                                       // ketika proses get data selesai
                        // cek value bahanbaku
                        // jika bahanbaku dipilih seluruh
                        if ($('#bahanbaku').val() === 'Seluruh') {
                            var bahanbaku = 'Seluruh';
                        }
                        // jika bahanbaku dipilih per bahanbaku
                        else {
                            var bahanbaku = $('#bahanbaku').val() +' - '+ $('#nama_bahanbaku').val();
                        }

                        // cek value "tgl_awal" dan "tgl_akhir"
                        // jika "tgl_awal" sama dengan "tgl_akhir"
                        if ($('#tgl_awal').val() === $('#tgl_akhir').val()) {
                            // tampilkan judul laporan
                            $('#judulLaporan').html('Laporan Penjualan Per bahanbaku <strong>'+ bahanbaku +'</strong>, Periode <strong>'+ $('#tgl_awal').val() +'</strong>.');
                        }
                        // jika tgl_awal" tidak sama dengan "tgl_akhir"
                        else {
                            // tampilkan judul laporan
                            $('#judulLaporan').html('Laporan Penjualan Per bahanbaku <strong>'+ bahanbaku +'</strong>, Periode <strong>'+ $('#tgl_awal').val() +'</strong> s.d. <strong>'+ $('#tgl_akhir').val() +'</strong>.');
                        }

                        // tampilkan tabel laporan penjualan
                        $('#tabelLaporan').removeAttr('style');
                        // tampilkan data laporan penjualan
                        $('#loadData').html(data);
                        // tampilkan tombol cetak
                        $('#btnCetak').removeAttr('style');
                        // tampilkan tombol export
                        $('#btnExport').removeAttr('style');
    	            }
    	        });
            }
        });
        // ===============================================================================================

        // =========================================== Cetak =============================================
        $('#btnCetak').click(function(){
            // validasi form input
            // jika tanggal awal kosong
            if ($('#tgl_awal').val()==""){
                // focus ke input tanggal awal
                $( "#tgl_awal" ).focus();
                // tampilkan peringatan data tidak boleh kosong
                $.notify({
                    title   : '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                    message : 'Tanggal awal tidak boleh kosong.'
                },{type: 'warning'});
            }
            // jika tanggal akhir kosong
            else if ($('#tgl_akhir').val()==""){
                // focus ke input tanggal akhir
                $( "#tgl_akhir" ).focus();
                // tampilkan peringatan data tidak boleh kosong
                $.notify({
                    title   : '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                    message : 'Tanggal akhir tidak boleh kosong.'
                },{type: 'warning'});
            }
            // jika bahanbaku kosong
            else if ($('#bahanbaku').val()==""){
                // focus ke input bahanbaku
                $( "#bahanbaku" ).focus();
                // tampilkan peringatan data tidak boleh kosong
                $.notify({
                    title   : '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                    message : 'bahanbaku tidak boleh kosong.'
                },{type: 'warning'});
            }
            // jika semua data sudah diisi, jalankan perintah untuk cetak laporan
            else {
                // ambil value "tgl_awal" dan "tgl_akhir" dari form filter
                var tgl_awal  = $('#tgl_awal').val();
                var tgl_akhir = $('#tgl_akhir').val();
                var bahanbaku      = $('#bahanbaku').val();

                if ($('#bahanbaku').val() === 'Seluruh') {
                    var nama_bahanbaku = 'Seluruh';
                } else {
                    var nama_bahanbaku = $('#nama_bahanbaku').val();
                }

                // buka file "cetak.php" dan kirim "value"
                window.open('laporan-penjualan-per-bahanbaku-cetak-'+ tgl_awal +'-sd-'+ tgl_akhir +'-'+ bahanbaku +'-'+ nama_bahanbaku);
            }
        });
        // ===============================================================================================

        // =========================================== Export ============================================
        $('#btnExport').click(function(){
            // validasi form input
            // jika tanggal awal kosong
            if ($('#tgl_awal').val()==""){
                // focus ke input tanggal awal
                $( "#tgl_awal" ).focus();
                // tampilkan peringatan data tidak boleh kosong
                $.notify({
                    title   : '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                    message : 'Tanggal awal tidak boleh kosong.'
                },{type: 'warning'});
            }
            // jika tanggal akhir kosong
            else if ($('#tgl_akhir').val()==""){
                // focus ke input tanggal akhir
                $( "#tgl_akhir" ).focus();
                // tampilkan peringatan data tidak boleh kosong
                $.notify({
                    title   : '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                    message : 'Tanggal akhir tidak boleh kosong.'
                },{type: 'warning'});
            }
            // jika bahanbaku kosong
            else if ($('#bahanbaku').val()==""){
                // focus ke input bahanbaku
                $( "#bahanbaku" ).focus();
                // tampilkan peringatan data tidak boleh kosong
                $.notify({
                    title   : '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                    message : 'bahanbaku tidak boleh kosong.'
                },{type: 'warning'});
            }
            // jika semua data sudah diisi, jalankan perintah untuk export laporan
            else {
                // ambil value "tgl_awal" dan "tgl_akhir" dari form filter
                var tgl_awal  = $('#tgl_awal').val();
                var tgl_akhir = $('#tgl_akhir').val();
                var bahanbaku      = $('#bahanbaku').val();

                if ($('#bahanbaku').val() === 'Seluruh') {
                    var nama_bahanbaku = 'Seluruh';
                } else {
                    var nama_bahanbaku = $('#nama_bahanbaku').val();
                }

                // arahkan ke file "export.php" dan kirim "value"
                location.href = "laporan-penjualan-per-bahanbaku-export-"+ tgl_awal +"-sd-"+ tgl_akhir +'-'+ bahanbaku +'-'+ nama_bahanbaku;
            }
        });
        // ===============================================================================================
    });
    </script>
<?php } ?>
