<?php
session_start();      // memulai session

// fungsi untuk pengecekan status login user
// jika user belum login, alihkan ke halaman "login-error"
if (empty($_SESSION['username']) && empty($_SESSION['password'])){
    echo "<meta http-equiv='refresh' content='0; url=../../login-error'>";
}
// jika user sudah login
else {
    // mengecek data get
    if (isset($_GET['tgl_awal']) && isset($_GET['tgl_akhir']) && isset($_GET['bahanbaku'])) {
        // panggil file "config.php" untuk koneksi ke database
        require_once "../../config/config.php";
        // panggil file "fungsi_tanggal.php" untuk format tanggal
        require_once "../../config/fungsi_tanggal.php";

        // fungsi untuk export data
        header("Content-Type: application/force-download");
        header("Cache-Control: no-cache, must-revalidate");
        // cek filter tanggal
        // jika "tgl_awal" sama dengan "tgl_akhir"
        if ($_GET['tgl_awal'] == $_GET['tgl_akhir']) {
            // nama file hasil export
            header("content-disposition: attachment;filename=LAPORAN-PENJUALAN-PER-bahanbaku-".$_GET['bahanbaku']."-".$_GET['tgl_awal'].".xls");
        }
        // jika "tgl_awal" tidak sama dengan "tgl_akhir"
        else {
            // nama file hasil export
            header("content-disposition: attachment;filename=LAPORAN-PENJUALAN-PER-bahanbaku-".$_GET['bahanbaku']."-".$_GET['tgl_awal']."-sd-".$_GET['tgl_akhir'].".xls");
        }

    ?>
        <!-- Judul laporan -->
        <center>
            <h3>
                LAPORAN PENJUALAN PER bahanbaku <br>
            <?php
            // jika bahanbaku = Seluruh (dipilih seluruh)
            if ($_GET['bahanbaku'] == 'Seluruh') {
                // tampilkan judul bahanbaku seluruh
                echo "Seluruh <br>";
            }
            // jika bahanbaku != Seluruh (dipilih per bahanbaku)
            else {
                // tampilkan judul per bahanbaku
                echo $_GET['bahanbaku']." - ".$_GET['nama_bahanbaku']."<br>";
            }

            // jika "tgl_awal" sama dengan "tgl_akhir"
            if ($_GET['tgl_awal'] == $_GET['tgl_akhir']) {
                // tampilkan judul laporan "tgl_awal"
                echo "'".tgl_eng_to_ind($_GET['tgl_awal']);
            }
            // jika tgl_awal" tidak sama dengan "tgl_akhir"
            else {
                // tampilkan judul laporan "tgl_awal" s.d. "tgl_akhir"
                echo tgl_eng_to_ind($_GET['tgl_awal']) ." s.d. ". tgl_eng_to_ind($_GET['tgl_akhir']);
            }
            ?>
            </h3>
        </center>
        <!-- Table untuk di Export Ke Excel -->
        <table>
    <?php
    try {
        // ambil "data" get dari ajax
        $tgl_awal  = date('Y-m-d', strtotime($_GET['tgl_awal']));
        $tgl_akhir = date('Y-m-d', strtotime($_GET['tgl_akhir']));
        $bahanbaku      = $_GET['bahanbaku'];
        // variabel untuk nomor urut tabel
        $no = 1;
        // variabel untuk total jumlah_jual seluruh
        $total_jual_seluruh   = 0;
        // variabel untuk total bayar seluruh
        $total_bayar_seluruh  = 0;
        // variabel untuk total untung seluruh
        $total_untung_seluruh = 0;

        // cek value bahanbaku
        // jika bahanbaku = Seluruh (dipilih seluruh)
        if ($bahanbaku == 'Seluruh') {
            // sql statement untuk menampilkan data bahanbaku dari tabel "penjualan"
            $query1 = "SELECT a.tanggal,b.bahanbaku,c.nama_bahanbaku FROM penjualan as a INNER JOIN penjualan_detail as b INNER JOIN bahanbaku as c
                       ON a.no_penjualan=b.no_penjualan AND b.bahanbaku=c.kode_bahanbaku WHERE a.tanggal BETWEEN :tgl_awal AND :tgl_akhir
                       GROUP BY b.bahanbaku ORDER BY b.bahanbaku ASC";
            // membuat prepared statements
            $stmt1 = $pdo->prepare($query1);

            // hubungkan "data" dengan prepared statements
            $stmt1->bindParam(':tgl_awal', $tgl_awal);
            $stmt1->bindParam(':tgl_akhir', $tgl_akhir);
        }
        // jika bahanbaku != Seluruh (dipilih per bahanbaku)
        else {
            // sql statement untuk menampilkan data bahanbaku dari tabel "penjualan"
            $query1 = "SELECT a.tanggal,b.bahanbaku,c.nama_bahanbaku FROM penjualan as a INNER JOIN penjualan_detail as b INNER JOIN bahanbaku as c
                       ON a.no_penjualan=b.no_penjualan AND b.bahanbaku=c.kode_bahanbaku WHERE b.bahanbaku=:bahanbaku AND a.tanggal BETWEEN :tgl_awal AND :tgl_akhir
                       GROUP BY b.bahanbaku ORDER BY b.bahanbaku ASC";
            // membuat prepared statements
            $stmt1 = $pdo->prepare($query1);

            // hubungkan "data" dengan prepared statements
            $stmt1->bindParam(':bahanbaku', $bahanbaku);
            $stmt1->bindParam(':tgl_awal', $tgl_awal);
            $stmt1->bindParam(':tgl_akhir', $tgl_akhir);
        }

        // eksekusi query
        $stmt1->execute();

        // tampilkan hasil query
        while ($data1 = $stmt1->fetch(PDO::FETCH_ASSOC)) {
            // tampilkan data kode_bahanbaku
            $bahanbaku = $data1['bahanbaku'];
        ?>
            <!-- tampilkan data penjualan bahanbaku -->
            <tr>
                <td align="left" colspan="9">----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</td>
            </tr>
            <tr>
                <td width="100" align="center" valign="middle"><?php echo $no; ?></td>
                <td valign="middle" colspan="8"><?php echo $bahanbaku; ?> - <?php echo $data1['nama_bahanbaku']; ?></td>
            </tr>
            <tr>
                <td align="left" colspan="9">----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</td>
            </tr>
            <tr>
                <td valign="middle"></td>
                <td valign="middle" colspan="8">
                    <!-- Tabel untuk menampilkan data penjualan per bahanbaku dari database -->
                    <table>
                        <thead>
                            <tr>
                                <td align="left" colspan="8">-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</td>
                            </tr>
                            <tr>
                                <th align="left" valign="middle">Tanggal</th>
                                <th align="left" valign="middle">No. Penjualan</th>
                                <th align="right" valign="middle">Harga Beli</th>
                                <th align="right" valign="middle">Harga Jual</th>
                                <th align="right" valign="middle">Jumlah</th>
                                <th align="right" valign="middle">Total</th>
                                <th align="right" valign="middle">Untung</th>
                            </tr>
                            <tr>
                                <td align="left" colspan="8">-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</td>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        // variabel untuk sub total jumlah_jual
                        $sub_total_jual   = 0;
                        // variabel untuk sub total bayar
                        $sub_total_bayar  = 0;
                        // variabel untuk sub total untung
                        $sub_total_untung = 0;

                        // sql statement untuk menampilkan data penjualan per bahanbaku dan tanggal dari tabel "penjualan"
                        $query2 = "SELECT a.no_penjualan,a.tanggal,b.bahanbaku,b.harga_beli,b.harga_jual,b.jumlah_jual,b.total_harga
                                   FROM penjualan as a INNER JOIN penjualan_detail as b ON a.no_penjualan=b.no_penjualan
                                   WHERE b.bahanbaku=:bahanbaku AND a.tanggal BETWEEN :tgl_awal AND :tgl_akhir";
                        // membuat prepared statements
                        $stmt2 = $pdo->prepare($query2);

                        // hubungkan "data" dengan prepared statements
                        $stmt2->bindParam(':bahanbaku', $bahanbaku);
                        $stmt2->bindParam(':tgl_awal', $tgl_awal);
                        $stmt2->bindParam(':tgl_akhir', $tgl_akhir);

                        // eksekusi query
                        $stmt2->execute();

                        // tampilkan hasil query
                        while ($data2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                            // siapkan data untung
                            $untung = $data2['harga_jual'] - $data2['harga_beli'];
                        ?>
                            <!-- tampilkan data penjualan per bahanbaku -->
                            <tr>
                                <td width="80" align="left"><?php echo date('d-m-Y', strtotime($data2['tanggal'])); ?></td>
                                <td width="100"><?php echo $data2['no_penjualan']; ?></td>
                                <td width="120" align="right">Rp. <?php echo number_format($data2['harga_beli'], 0, ".", "."); ?></td>
                                <td width="120" align="right">Rp. <?php echo number_format($data2['harga_jual'], 0, ".", "."); ?></td>
                                <td width="100" align="right"><?php echo $data2['jumlah_jual']; ?></td>
                                <td width="150" align="right">Rp. <?php echo number_format($data2['total_harga'], 0, ".", "."); ?></td>
                                <td width="140" align="right">Rp. <?php echo number_format($untung, 0, ".", "."); ?></td>
                            </tr>
                        <?php
                            // buat sub total
                            $sub_total_jual   += $data2['jumlah_jual'];
                            $sub_total_bayar  += $data2['total_harga'];
                            $sub_total_untung += $untung;
                        };
                        ?>
                            <tr>
                                <td align="left" colspan="8">-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</td>
                            </tr>
                            <tr>
                                <td align="right" colspan="4"><strong>Total :</strong></td>
                                <td align="right"><strong><?php echo number_format($sub_total_jual, 0, ".", "."); ?></strong></td>
                                <td align="right"><strong>Rp. <?php echo number_format($sub_total_bayar, 0, ".", "."); ?></strong></td>
                                <td align="right"><strong>Rp. <?php echo number_format($sub_total_untung, 0, ".", "."); ?></strong></td>
                            </tr>
                            <tr>
                                <td height="10" align="left" colspan="8"></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        <?php
            $no++;
            // buat total seluruh
            $total_jual_seluruh   += $sub_total_jual;
            $total_bayar_seluruh  += $sub_total_bayar;
            $total_untung_seluruh += $sub_total_untung;
        };
        ?>
            <tr>
                <td align="left" colspan="9">----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</td>
            </tr>
            <tr>
                <td align="right" colspan="5"><strong>Total Seluruh : </strong></td>
                <td align="right"><strong><?php echo number_format($total_jual_seluruh, 0, ".", "."); ?></strong></td>
                <td align="right"><strong>Rp. <?php echo number_format($total_bayar_seluruh, 0, ".", "."); ?></strong></td>
                <td align="right"><strong>Rp. <?php echo number_format($total_untung_seluruh, 0, ".", "."); ?></strong></td>
            </tr>
            <tr>
                <td align="left" colspan="9">----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</td>
            </tr>
        </table>
        <?php
        // tutup koneksi
        $pdo = null;
    } catch (PDOException $e) {
        // tampilkan pesan kesalahan
        echo "Query Error : ".$e->getMessage();
    }
    ?>

        <div style="text-align:right">
            <h4>Bandar Lampung, <?php echo date("d/m/Y"); ?></h4> <br><br>
            <h4>.................................................</h4>
        </div>
    <?php
    }
}
?>
