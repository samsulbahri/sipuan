<?php
session_start();      // memulai session
ob_start();

// fungsi untuk pengecekan status login user
// jika user belum login, alihkan ke halaman "login-error"
if (empty($_SESSION['username']) && empty($_SESSION['password'])){
    echo "<meta http-equiv='refresh' content='0; url=../../login-error'>";
}
// jika user sudah login
else {
    // mengecek data get
    if (isset($_GET['tgl_awal']) && isset($_GET['tgl_akhir']) && isset($_GET['bahanbaku'])) { ?>
        <!-- Bagian halaman HTML yang akan konvert -->
        <!doctype html>
        <html lang="en">
            <head>
                <!-- Required meta tags -->
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
                <!-- Custom CSS -->
                <link rel="stylesheet" type="text/css" href="../../assets/css/laporan.css" />
                <!-- Title -->
                <title>LAPORAN PENJUALAN PER bahanbaku</title>
            </head>
            <body>
                <?php
                // panggil file "config.php" untuk koneksi ke database
                require_once "../../config/config.php";
                // panggil file "fungsi_tanggal.php" untuk format tanggal
                require_once "../../config/fungsi_tanggal.php";

                try {
                    // siapkan "data"
                    $id = 1;

                    // sql statement untuk menampilkan data dari tabel "sys_config" berdasarkan "id"
                    $query = "SELECT nama, alamat, telepon, logo FROM sys_config WHERE id=:id";
                    // membuat prepared statements
                    $stmt = $pdo->prepare($query);

                    // hubungkan "data" dengan prepared statements
                    $stmt->bindParam(':id', $id);

                    // eksekusi query
                    $stmt->execute();

                    // ambil data hasil query
                    $data = $stmt->fetch(PDO::FETCH_ASSOC);
                    // tampilkan data
                    $nama    = $data['nama'];
                    $alamat  = $data['alamat'];
                    $telepon = $data['telepon'];
                    $logo    = $data['logo'];

                } catch (PDOException $e) {
                    // tampilkan pesan kesalahan
                    echo "Query Error : ".$e->getMessage();
                }
                ?>

                <!-- Kop Laporan -->
                <div class="nama-instansi"><?php echo $nama; ?></div>
                <div class="info-instansi"><?php echo $alamat; ?></div>
                <div class="info-instansi">Telp. <?php echo $telepon; ?></div>
                <div class="logo">
                    <img src="../../assets/img/<?php echo $logo; ?>" alt="Logo">
                </div>

                <hr><br>

                <!-- Judul Laporan -->
                <div class="judul-laporan">
                    LAPORAN PENJUALAN PER bahanbaku
                </div>

                <?php
                if ($_GET['bahanbaku'] == 'Seluruh') { ?>
                    <!-- tampilkan judul -->
                    <div class="judul-tanggal"> Seluruh </div>
                <?php
                } else { ?>
                    <!-- tampilkan judul -->
                    <div class="judul-tanggal">
                        <?php echo $_GET['bahanbaku']; ?> - <?php echo $_GET['nama_bahanbaku']; ?>
                    </div>
                <?php
                }

                // jika "tgl_awal" sama dengan "tgl_akhir"
                if ($_GET['tgl_awal'] == $_GET['tgl_akhir']) { ?>
                    <!-- tampilkan judul -->
                    <div class="judul-tanggal">
                        <?php echo tgl_eng_to_ind($_GET['tgl_awal']); ?>
                    </div>
                <?php
                }
                // jika tgl_awal" tidak sama dengan "tgl_akhir"
                else { ?>
                    <!-- tampilkan judul -->
                    <div class="judul-tanggal">
                        <?php echo tgl_eng_to_ind($_GET['tgl_awal']); ?> s.d. <?php echo tgl_eng_to_ind($_GET['tgl_akhir']); ?>
                    </div>
                <?php
                }
                ?>

                <br>
                <!-- Tabel untuk menampilkan data penjualan dari database -->
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <?php
            try {
                // ambil "data" get dari ajax
                $tgl_awal  = date('Y-m-d', strtotime($_GET['tgl_awal']));
                $tgl_akhir = date('Y-m-d', strtotime($_GET['tgl_akhir']));
                $bahanbaku      = $_GET['bahanbaku'];
                // variabel untuk nomor urut tabel
                $no = 1;
                // variabel untuk total jumlah_jual seluruh
                $total_jual_seluruh   = 0;
                // variabel untuk total bayar seluruh
                $total_bayar_seluruh  = 0;
                // variabel untuk total untung seluruh
                $total_untung_seluruh = 0;

                // cek value bahanbaku
                // jika bahanbaku = Seluruh (dipilih seluruh)
                if ($bahanbaku == 'Seluruh') {
                    // sql statement untuk menampilkan data bahanbaku dari tabel "penjualan"
                    $query1 = "SELECT a.tanggal,b.bahanbaku,c.nama_bahanbaku FROM penjualan as a INNER JOIN penjualan_detail as b INNER JOIN bahanbaku as c
                               ON a.no_penjualan=b.no_penjualan AND b.bahanbaku=c.kode_bahanbaku WHERE a.tanggal BETWEEN :tgl_awal AND :tgl_akhir
                               GROUP BY b.bahanbaku ORDER BY b.bahanbaku ASC";
                    // membuat prepared statements
                    $stmt1 = $pdo->prepare($query1);

                    // hubungkan "data" dengan prepared statements
                    $stmt1->bindParam(':tgl_awal', $tgl_awal);
                    $stmt1->bindParam(':tgl_akhir', $tgl_akhir);
                }
                // jika bahanbaku != Seluruh (dipilih per bahanbaku)
                else {
                    // sql statement untuk menampilkan data bahanbaku dari tabel "penjualan"
                    $query1 = "SELECT a.tanggal,b.bahanbaku,c.nama_bahanbaku FROM penjualan as a INNER JOIN penjualan_detail as b INNER JOIN bahanbaku as c
                               ON a.no_penjualan=b.no_penjualan AND b.bahanbaku=c.kode_bahanbaku WHERE b.bahanbaku=:bahanbaku AND a.tanggal BETWEEN :tgl_awal AND :tgl_akhir
                               GROUP BY b.bahanbaku ORDER BY b.bahanbaku ASC";
                    // membuat prepared statements
                    $stmt1 = $pdo->prepare($query1);

                    // hubungkan "data" dengan prepared statements
                    $stmt1->bindParam(':bahanbaku', $bahanbaku);
                    $stmt1->bindParam(':tgl_awal', $tgl_awal);
                    $stmt1->bindParam(':tgl_akhir', $tgl_akhir);
                }

                // eksekusi query
                $stmt1->execute();

                // tampilkan hasil query
                while ($data1 = $stmt1->fetch(PDO::FETCH_ASSOC)) {
                    // tampilkan data kode_bahanbaku
                    $bahanbaku = $data1['bahanbaku'];
                ?>
                    <!-- tampilkan data bahanbaku -->
                    <tr>
                        <td class="border-dotted" height="30" align="left" colspan="5">------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</td>
                    </tr>
                    <tr>
                        <td width="4" height="20" valign="middle"><?php echo $no; ?>.</td>
                        <td height="20" valign="middle"><?php echo $bahanbaku; ?> - <?php echo $data1['nama_bahanbaku']; ?></td>
                    </tr>
                    <tr>
                        <td class="border-dotted" height="20" align="right" colspan="5">------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</td>
                    </tr>
                    <tr>
                        <td height="20" valign="middle"></td>
                        <td height="20" valign="middle" colspan="4">
                            <!-- Tabel untuk menampilkan data penjualan per bahanbaku dari database -->
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <thead>
                                    <tr class="tr-judul">
                                        <th class="td-left" height="30" align="left" valign="middle">Tanggal</th>
                                        <th height="30" align="left" valign="middle">No. Penjualan</th>
                                        <th height="30" align="right" valign="middle">Harga Beli</th>
                                        <th height="30" align="right" valign="middle">Harga Jual</th>
                                        <th height="30" align="right" valign="middle">Jumlah</th>
                                        <th height="30" align="right" valign="middle">Total</th>
                                        <th class="td-right" height="30" align="right" valign="middle">Untung</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                // variabel untuk sub total jumlah_jual
                                $sub_total_jual   = 0;
                                // variabel untuk sub total bayar
                                $sub_total_bayar  = 0;
                                // variabel untuk sub total untung
                                $sub_total_untung = 0;

                                // sql statement untuk menampilkan data penjualan per bahanbaku dan tanggal dari tabel "penjualan"
                                $query2 = "SELECT a.no_penjualan,a.tanggal,b.bahanbaku,b.harga_beli,b.harga_jual,b.jumlah_jual,b.total_harga
                                           FROM penjualan as a INNER JOIN penjualan_detail as b ON a.no_penjualan=b.no_penjualan
                                           WHERE b.bahanbaku=:bahanbaku AND a.tanggal BETWEEN :tgl_awal AND :tgl_akhir";
                                // membuat prepared statements
                                $stmt2 = $pdo->prepare($query2);

                                // hubungkan "data" dengan prepared statements
                                $stmt2->bindParam(':bahanbaku', $bahanbaku);
                                $stmt2->bindParam(':tgl_awal', $tgl_awal);
                                $stmt2->bindParam(':tgl_akhir', $tgl_akhir);

                                // eksekusi query
                                $stmt2->execute();

                                // tampilkan hasil query
                                while ($data2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                                    // siapkan data untung
                                    $untung = $data2['harga_jual'] - $data2['harga_beli'];
                                ?>
                                    <!-- tampilkan data penjualan per bahanbaku -->
                                    <tr>
                                        <td class="td-left" width="90" height="20"><?php echo date('d-m-Y', strtotime($data2['tanggal'])); ?></td>
                                        <td width="110" height="20"><?php echo $data2['no_penjualan']; ?></td>
                                        <td width="140" height="20" align="right">Rp. <?php echo number_format($data2['harga_beli'], 0, ".", "."); ?></td>
                                        <td width="140" height="20" align="right">Rp. <?php echo number_format($data2['harga_jual'], 0, ".", "."); ?></td>
                                        <td width="100" height="20" align="right"><?php echo $data2['jumlah_jual']; ?></td>
                                        <td width="160" height="20" align="right">Rp. <?php echo number_format($data2['total_harga'], 0, ".", "."); ?></td>
                                        <td class="td-right" width="140" height="20" align="right">Rp. <?php echo number_format($untung, 0, ".", "."); ?></td>
                                    </tr>
                                <?php
                                    // buat sub total
                                    $sub_total_jual   += $data2['jumlah_jual'];
                                    $sub_total_bayar  += $data2['total_harga'];
                                    $sub_total_untung += $untung;
                                };
                                ?>
                                    <tr class="tr-judul">
                                        <td height="30" align="right" colspan="4"><strong>Total :</strong></td>
                                        <td height="30" align="right"><strong><?php echo number_format($sub_total_jual, 0, ".", "."); ?></strong></td>
                                        <td height="30" align="right"><strong>Rp. <?php echo number_format($sub_total_bayar, 0, ".", "."); ?></strong></td>
                                        <td class="td-right" height="30" align="right"><strong>Rp. <?php echo number_format($sub_total_untung, 0, ".", "."); ?></strong></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                <?php
                    $no++;
                    // buat total seluruh
                    $total_jual_seluruh   += $sub_total_jual;
                    $total_bayar_seluruh  += $sub_total_bayar;
                    $total_untung_seluruh += $sub_total_untung;
                };
                ?>
                    <tr>
                        <td class="border-dotted" height="30" align="left" colspan="5">------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</td>
                    </tr>
                </table>

                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr class="tr-judul">
                        <td width="590" height="30" align="right"><strong>Total Seluruh : </strong></td>
                        <td width="95" height="30" align="right"><strong><?php echo number_format($total_jual_seluruh, 0, ".", "."); ?></strong></td>
                        <td width="160" height="30" align="right"><strong>Rp. <?php echo number_format($total_bayar_seluruh, 0, ".", "."); ?></strong></td>
                        <td style="padding-right: 17px;" width="145" height="30" align="right"><strong>Rp. <?php echo number_format($total_untung_seluruh, 0, ".", "."); ?></strong></td>
                    </tr>
                </table>
                <?php
                // tutup koneksi
                $pdo = null;
            } catch (PDOException $e) {
                // tampilkan pesan kesalahan
                echo "Query Error : ".$e->getMessage();
            }
            ?>

                <div class="footer-tanggal">
                    <p>Bandar Lampung, <?php echo tgl_eng_to_ind(date("d-m-Y")); ?></p> <br><br>
                    <p>.....................................................</p>
                </div>
            </body>
        </html> <!-- Akhir halaman HTML yang akan di konvert -->

        <?php
        // jika "tgl_awal" sama dengan "tgl_akhir"
        if ($_GET['tgl_awal'] == $_GET['tgl_akhir']) {
            // nama file pdf yang dihasilkan
            $filename="LAPORAN-PENJUALAN-PER-bahanbaku-".$_GET['bahanbaku']."-".$_GET['tgl_awal'].".pdf";
        }
        // jika tgl_awal" tidak sama dengan "tgl_akhir"
        else {
            // nama file pdf yang dihasilkan
            $filename="LAPORAN-PENJUALAN-PER-bahanbaku-".$_GET['bahanbaku']."-".$_GET['tgl_awal']."-sd-".$_GET['tgl_akhir'].".pdf";
        }

        // ====================================== Convert HTML ke PDF ======================================
        $content = ob_get_clean();
        $content = '<page style="font-family: freeserif">'.($content).'</page>';
        // panggil file library html2pdf
        require_once('../../assets/plugins/html2pdf_v4.03/html2pdf.class.php');
        try
        {
            $html2pdf = new HTML2PDF('L','A4','en', false, 'ISO-8859-15',array(10, 8, 10, 8));
            $html2pdf->setDefaultFont('Arial');
            $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
            $html2pdf->Output($filename);
        }
        catch(HTML2PDF_exception $e) { echo $e; }
    }
}
?>
