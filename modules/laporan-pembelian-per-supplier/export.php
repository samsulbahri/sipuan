<?php
session_start();      // memulai session

// fungsi untuk pengecekan status login user
// jika user belum login, alihkan ke halaman "login-error"
if (empty($_SESSION['username']) && empty($_SESSION['password'])){
    echo "<meta http-equiv='refresh' content='0; url=../../login-error'>";
}
// jika user sudah login
else {
    // mengecek data get
    if (isset($_GET['tgl_awal']) && isset($_GET['tgl_akhir']) && isset($_GET['supplier'])) {
        // panggil file "config.php" untuk koneksi ke database
        require_once "../../config/config.php";
        // panggil file "fungsi_tanggal.php" untuk format tanggal
        require_once "../../config/fungsi_tanggal.php";

        // fungsi untuk export data
        header("Content-Type: application/force-download");
        header("Cache-Control: no-cache, must-revalidate");
        // cek filter tanggal
        // jika "tgl_awal" sama dengan "tgl_akhir"
        if ($_GET['tgl_awal'] == $_GET['tgl_akhir']) {
            // nama file hasil export
            header("content-disposition: attachment;filename=LAPORAN-PEMBELIAN-PER-SUPPLIER-".$_GET['supplier']."-".$_GET['tgl_awal'].".xls");
        }
        // jika "tgl_awal" tidak sama dengan "tgl_akhir"
        else {
            // nama file hasil export
            header("content-disposition: attachment;filename=LAPORAN-PEMBELIAN-PER-SUPPLIER-".$_GET['supplier']."-".$_GET['tgl_awal']."-sd-".$_GET['tgl_akhir'].".xls");
        }

    ?>
        <!-- Judul laporan -->
        <center>
            <h3>
                LAPORAN PEMBELIAN PER SUPPLIER <br>
            <?php
            // jika supplier = Seluruh (dipilih seluruh)
            if ($_GET['supplier'] == 'Seluruh') {
                // tampilkan judul supplier seluruh
                echo "Seluruh <br>";
            }
            // jika supplier != Seluruh (dipilih per supplier)
            else {
                // tampilkan judul per supplier
                echo $_GET['supplier']." - ".$_GET['nama_supplier']."<br>";
            }

            // jika "tgl_awal" sama dengan "tgl_akhir"
            if ($_GET['tgl_awal'] == $_GET['tgl_akhir']) {
                // tampilkan judul laporan "tgl_awal"
                echo "'".tgl_eng_to_ind($_GET['tgl_awal']);
            }
            // jika tgl_awal" tidak sama dengan "tgl_akhir"
            else {
                // tampilkan judul laporan "tgl_awal" s.d. "tgl_akhir"
                echo tgl_eng_to_ind($_GET['tgl_awal']) ." s.d. ". tgl_eng_to_ind($_GET['tgl_akhir']);
            }
            ?>
            </h3>
        </center>
        <!-- Table untuk di Export Ke Excel -->
        <table>
    <?php
    try {
        // ambil "data" get dari ajax
        $tgl_awal  = date('Y-m-d', strtotime($_GET['tgl_awal']));
        $tgl_akhir = date('Y-m-d', strtotime($_GET['tgl_akhir']));
        $supplier  = $_GET['supplier'];
        // variabel untuk nomor urut tabel
        $no = 1;
        // variabel untuk total jumlah_beli seluruh
        $total_beli_seluruh  = 0;
        // variabel untuk total bayar seluruh
        $total_bayar_seluruh = 0;

        // cek value supplier
        // jika supplier = Seluruh (dipilih seluruh)
        if ($supplier == 'Seluruh') {
            // sql statement untuk menampilkan data supplier dari tabel "pembelian"
            $query1 = "SELECT a.tanggal,a.supplier,b.nama_supplier FROM pembelian as a INNER JOIN supplier as b  ON a.supplier=b.kode_supplier
                       WHERE a.tanggal BETWEEN :tgl_awal AND :tgl_akhir GROUP BY a.supplier ORDER BY a.supplier ASC";
            // membuat prepared statements
            $stmt1 = $pdo->prepare($query1);

            // hubungkan "data" dengan prepared statements
            $stmt1->bindParam(':tgl_awal', $tgl_awal);
            $stmt1->bindParam(':tgl_akhir', $tgl_akhir);
        }
        // jika supplier != Seluruh (dipilih per supplier)
        else {
            // sql statement untuk menampilkan data supplier dari tabel "pembelian"
            $query1 = "SELECT a.tanggal,a.supplier,b.nama_supplier FROM pembelian as a INNER JOIN supplier as b  ON a.supplier=b.kode_supplier
                       WHERE a.supplier=:supplier AND a.tanggal BETWEEN :tgl_awal AND :tgl_akhir GROUP BY a.supplier ORDER BY a.supplier ASC";
            // membuat prepared statements
            $stmt1 = $pdo->prepare($query1);

            // hubungkan "data" dengan prepared statements
            $stmt1->bindParam(':supplier', $supplier);
            $stmt1->bindParam(':tgl_awal', $tgl_awal);
            $stmt1->bindParam(':tgl_akhir', $tgl_akhir);
        }

        // eksekusi query
        $stmt1->execute();

        // tampilkan hasil query
        while ($data1 = $stmt1->fetch(PDO::FETCH_ASSOC)) {
            // tampilkan data kode_supplier
            $supplier = $data1['supplier'];
        ?>
            <!-- tampilkan data pembelian supplier -->
            <tr>
                <td align="left" colspan="8">-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</td>
            </tr>
            <tr>
                <td width="100" align="center" valign="middle"><?php echo $no; ?></td>
                <td valign="middle" colspan="7"><?php echo $supplier; ?> - <?php echo $data1['nama_supplier']; ?></td>
            </tr>
            <tr>
                <td align="left" colspan="8">-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</td>
            </tr>
            <tr>
                <td valign="middle"></td>
                <td valign="middle" colspan="7">
                    <!-- Tabel untuk menampilkan data pembelian per supplier dari database -->
                    <table>
                        <thead>
                            <tr>
                                <td align="left" colspan="7">-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------</td>
                            </tr>
                            <tr>
                                <th align="left" valign="middle">Tanggal</th>
                                <th align="left" valign="middle">No. Pembelian</th>
                                <th align="left" valign="middle">Bahan Baku</th>
                                <th align="right" valign="middle">Harga</th>
                                <th align="right" valign="middle">Jumlah</th>
                                <th align="right" valign="middle">Total</th>
                            </tr>
                            <tr>
                                <td align="left" colspan="7">-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------</td>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        // variabel untuk sub total jumlah_beli
                        $sub_total_beli  = 0;
                        // variabel untuk sub total bayar
                        $sub_total_bayar = 0;

                        // sql statement untuk menampilkan data pembelian per supplier dan tanggal dari tabel "pembelian"
                        $query2 = "SELECT a.no_pembelian,a.tanggal,b.bahanbaku,b.harga_beli,b.jumlah_beli,b.total_harga,c.nama_bahanbaku
                                   FROM pembelian as a INNER JOIN pembelian_detail as b INNER JOIN bahanbaku as c ON a.no_pembelian=b.no_pembelian AND b.bahanbaku=c.kode_bahanbaku
                                   WHERE a.supplier=:supplier AND a.tanggal BETWEEN :tgl_awal AND :tgl_akhir";
                        // membuat prepared statements
                        $stmt2 = $pdo->prepare($query2);

                        // hubungkan "data" dengan prepared statements
                        $stmt2->bindParam(':supplier', $supplier);
                        $stmt2->bindParam(':tgl_awal', $tgl_awal);
                        $stmt2->bindParam(':tgl_akhir', $tgl_akhir);

                        // eksekusi query
                        $stmt2->execute();

                        // tampilkan hasil query
                        while ($data2 = $stmt2->fetch(PDO::FETCH_ASSOC)) { ?>
                            <!-- tampilkan data pembelian per supplier -->
                            <tr>
                                <td width="80" align="left"><?php echo date('d-m-Y', strtotime($data2['tanggal'])); ?></td>
                                <td width="100"><?php echo $data2['no_pembelian']; ?></td>
                                <td width="180"><?php echo $data2['bahanbaku']; ?> - <?php echo $data2['nama_bahanbaku']; ?></td>
                                <td width="120" align="right">Rp. <?php echo number_format($data2['harga_beli'], 0, ".", "."); ?></td>
                                <td width="100" align="right"><?php echo $data2['jumlah_beli']; ?></td>
                                <td width="150" align="right">Rp. <?php echo number_format($data2['total_harga'], 0, ".", "."); ?></td>
                            </tr>
                        <?php
                            // buat sub total
                            $sub_total_beli  += $data2['jumlah_beli'];
                            $sub_total_bayar += $data2['total_harga'];
                        };
                        ?>
                            <tr>
                                <td align="left" colspan="7">-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------</td>
                            </tr>
                            <tr>
                                <td align="right" colspan="4"><strong>Total :</strong></td>
                                <td align="right"><strong><?php echo number_format($sub_total_beli, 0, ".", "."); ?></strong></td>
                                <td align="right"><strong>Rp. <?php echo number_format($sub_total_bayar, 0, ".", "."); ?></strong></td>
                            </tr>
                            <tr>
                                <td height="10" align="left" colspan="7"></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        <?php
            $no++;
            // buat total seluruh
            $total_beli_seluruh  += $sub_total_beli;
            $total_bayar_seluruh += $sub_total_bayar;
        };
        ?>
            <tr>
                <td align="left" colspan="8">-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</td>
            </tr>
            <tr>
                <td align="right" colspan="5"><strong>Total Seluruh : </strong></td>
                <td align="right"><strong><?php echo number_format($total_beli_seluruh, 0, ".", "."); ?></strong></td>
                <td align="right"><strong>Rp. <?php echo number_format($total_bayar_seluruh, 0, ".", "."); ?></strong></td>
            </tr>
            <tr>
                <td align="left" colspan="8">-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</td>
            </tr>
        </table>
        <?php
        // tutup koneksi
        $pdo = null;
    } catch (PDOException $e) {
        // tampilkan pesan kesalahan
        echo "Query Error : ".$e->getMessage();
    }
    ?>

        <div style="text-align:right">
            <h4>Balikpapan, <?php echo date("d/m/Y"); ?></h4> <br><br>
            <h4>.................................................</h4>
        </div>
    <?php
    }
}
?>
