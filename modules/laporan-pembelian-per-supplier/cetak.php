<?php
session_start();      // memulai session
ob_start();

// fungsi untuk pengecekan status login user
// jika user belum login, alihkan ke halaman "login-error"
if (empty($_SESSION['username']) && empty($_SESSION['password'])){
    echo "<meta http-equiv='refresh' content='0; url=../../login-error'>";
}
// jika user sudah login
else {
    // mengecek data get
    if (isset($_GET['tgl_awal']) && isset($_GET['tgl_akhir']) && isset($_GET['supplier'])) { ?>
        <!-- Bagian halaman HTML yang akan konvert -->
        <!doctype html>
        <html lang="en">
            <head>
                <!-- Required meta tags -->
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
                <!-- Custom CSS -->
                <link rel="stylesheet" type="text/css" href="../../assets/css/laporan.css" />
                <!-- Title -->
                <title>LAPORAN PEMBELIAN PER SUPPLIER</title>
            </head>
            <body>
                <?php
                // panggil file "config.php" untuk koneksi ke database
                require_once "../../config/config.php";
                // panggil file "fungsi_tanggal.php" untuk format tanggal
                require_once "../../config/fungsi_tanggal.php";

                try {
                    // siapkan "data"
                    $id = 1;

                    // sql statement untuk menampilkan data dari tabel "sys_config" berdasarkan "id"
                    $query = "SELECT nama, alamat, telepon, logo FROM sys_config WHERE id=:id";
                    // membuat prepared statements
                    $stmt = $pdo->prepare($query);

                    // hubungkan "data" dengan prepared statements
                    $stmt->bindParam(':id', $id);

                    // eksekusi query
                    $stmt->execute();

                    // ambil data hasil query
                    $data = $stmt->fetch(PDO::FETCH_ASSOC);
                    // tampilkan data
                    $nama    = $data['nama'];
                    $alamat  = $data['alamat'];
                    $telepon = $data['telepon'];
                    $logo    = $data['logo'];

                } catch (PDOException $e) {
                    // tampilkan pesan kesalahan
                    echo "Query Error : ".$e->getMessage();
                }
                ?>

                <!-- Kop Laporan -->
                <div class="nama-instansi"><?php echo $nama; ?></div>
                <div class="info-instansi"><?php echo $alamat; ?></div>
                <div class="info-instansi">Telp. <?php echo $telepon; ?></div>
                <div class="logo">
                    <img src="../../assets/img/<?php echo $logo; ?>" alt="Logo">
                </div>

                <hr><br>

                <!-- Judul Laporan -->
                <div class="judul-laporan">
                    LAPORAN PEMBELIAN PER SUPPLIER
                </div>

                <?php
                if ($_GET['supplier'] == 'Seluruh') { ?>
                    <!-- tampilkan judul -->
                    <div class="judul-tanggal"> Seluruh </div>
                <?php
                } else { ?>
                    <!-- tampilkan judul -->
                    <div class="judul-tanggal">
                        <?php echo $_GET['supplier']; ?> - <?php echo $_GET['nama_supplier']; ?>
                    </div>
                <?php
                }

                // jika "tgl_awal" sama dengan "tgl_akhir"
                if ($_GET['tgl_awal'] == $_GET['tgl_akhir']) { ?>
                    <!-- tampilkan judul -->
                    <div class="judul-tanggal">
                        <?php echo tgl_eng_to_ind($_GET['tgl_awal']); ?>
                    </div>
                <?php
                }
                // jika tgl_awal" tidak sama dengan "tgl_akhir"
                else { ?>
                    <!-- tampilkan judul -->
                    <div class="judul-tanggal">
                        <?php echo tgl_eng_to_ind($_GET['tgl_awal']); ?> s.d. <?php echo tgl_eng_to_ind($_GET['tgl_akhir']); ?>
                    </div>
                <?php
                }
                ?>

                <br>
                <!-- Tabel untuk menampilkan data pembelian dari database -->
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <?php
                try {
                    // ambil "data" get dari ajax
                    $tgl_awal  = date('Y-m-d', strtotime($_GET['tgl_awal']));
                    $tgl_akhir = date('Y-m-d', strtotime($_GET['tgl_akhir']));
                    $supplier  = $_GET['supplier'];
                    // variabel untuk nomor urut tabel
                    $no = 1;
                    // variabel untuk total jumlah_beli seluruh
                    $total_beli_seluruh  = 0;
                    // variabel untuk total bayar seluruh
                    $total_bayar_seluruh = 0;

                    // cek value supplier
                // jika supplier = Seluruh (dipilih seluruh)
                if ($supplier == 'Seluruh') {
                    // sql statement untuk menampilkan data supplier dari tabel "pembelian"
                    $query1 = "SELECT a.tanggal,a.supplier,b.nama_supplier FROM pembelian as a INNER JOIN supplier as b  ON a.supplier=b.kode_supplier
                               WHERE a.tanggal BETWEEN :tgl_awal AND :tgl_akhir GROUP BY a.supplier ORDER BY a.supplier ASC";
                    // membuat prepared statements
                    $stmt1 = $pdo->prepare($query1);

                    // hubungkan "data" dengan prepared statements
                    $stmt1->bindParam(':tgl_awal', $tgl_awal);
                    $stmt1->bindParam(':tgl_akhir', $tgl_akhir);
                }
                // jika supplier != Seluruh (dipilih per supplier)
                else {
                    // sql statement untuk menampilkan data supplier dari tabel "pembelian"
                    $query1 = "SELECT a.tanggal,a.supplier,b.nama_supplier FROM pembelian as a INNER JOIN supplier as b  ON a.supplier=b.kode_supplier
                               WHERE a.supplier=:supplier AND a.tanggal BETWEEN :tgl_awal AND :tgl_akhir GROUP BY a.supplier ORDER BY a.supplier ASC";
                    // membuat prepared statements
                    $stmt1 = $pdo->prepare($query1);

                    // hubungkan "data" dengan prepared statements
                    $stmt1->bindParam(':supplier', $supplier);
                    $stmt1->bindParam(':tgl_awal', $tgl_awal);
                    $stmt1->bindParam(':tgl_akhir', $tgl_akhir);
                }

                // eksekusi query
                $stmt1->execute();

                // tampilkan hasil query
                while ($data1 = $stmt1->fetch(PDO::FETCH_ASSOC)) {
                    // tampilkan data kode_supplier
                    $supplier = $data1['supplier'];
                ?>
                    <!-- tampilkan data supplier -->
                    <tr>
                        <td class="border-dotted" height="30" align="left" colspan="4">------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</td>
                    </tr>
                    <tr>
                        <td width="4" height="20" valign="middle"><?php echo $no; ?>.</td>
                        <td height="20" valign="middle"><?php echo $supplier; ?> - <?php echo $data1['nama_supplier']; ?></td>
                    </tr>
                    <tr>
                        <td class="border-dotted" height="20" align="right" colspan="4">------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</td>
                    </tr>
                    <tr>
                        <td height="20" valign="middle"></td>
                        <td height="20" valign="middle" colspan="3">
                            <!-- Tabel untuk menampilkan data pembelian per supplier dari database -->
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <thead>
                                    <tr class="tr-judul">
                                        <th class="td-left" height="30" align="left" valign="middle">Tanggal</th>
                                        <th height="30" align="left" valign="middle">No. Pembelian</th>
                                        <th height="30" align="left" valign="middle">Bahan Baku</th>
                                        <th height="30" align="right" valign="middle">Harga</th>
                                        <th height="30" align="right" valign="middle">Jumlah</th>
                                        <th class="td-right" height="30" align="right" valign="middle">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                // variabel untuk sub total jumlah_beli
                                $sub_total_beli  = 0;
                                // variabel untuk sub total bayar
                                $sub_total_bayar = 0;

                                // sql statement untuk menampilkan data pembelian per supplier dan tanggal dari tabel "pembelian"
                                $query2 = "SELECT a.no_pembelian,a.tanggal,b.bahanbaku,b.harga_beli,b.jumlah_beli,b.total_harga,c.nama_bahanbaku
                                           FROM pembelian as a INNER JOIN pembelian_detail as b INNER JOIN bahanbaku as c ON a.no_pembelian=b.no_pembelian AND b.bahanbaku=c.kode_bahanbaku
                                           WHERE a.supplier=:supplier AND a.tanggal BETWEEN :tgl_awal AND :tgl_akhir";
                                // membuat prepared statements
                                $stmt2 = $pdo->prepare($query2);

                                // hubungkan "data" dengan prepared statements
                                $stmt2->bindParam(':supplier', $supplier);
                                $stmt2->bindParam(':tgl_awal', $tgl_awal);
                                $stmt2->bindParam(':tgl_akhir', $tgl_akhir);

                                // eksekusi query
                                $stmt2->execute();

                                // tampilkan hasil query
                                while ($data2 = $stmt2->fetch(PDO::FETCH_ASSOC)) { ?>
                                    <!-- tampilkan data pembelian per supplier -->
                                    <tr>
                                        <td class="td-left" width="80" height="20"><?php echo date('d-m-Y', strtotime($data2['tanggal'])); ?></td>
                                        <td width="140" height="20"><?php echo $data2['no_pembelian']; ?></td>
                                        <td width="200" height="20"><?php echo $data2['bahanbaku']; ?> - <?php echo $data2['nama_bahanbaku']; ?></td>
                                        <td width="140" height="20" align="right">Rp. <?php echo number_format($data2['harga_beli'], 0, ".", "."); ?></td>
                                        <td width="130" height="20" align="right"><?php echo $data2['jumlah_beli']; ?></td>
                                        <td class="td-right" width="195" height="20" align="right">Rp. <?php echo number_format($data2['total_harga'], 0, ".", "."); ?></td>
                                    </tr>
                                <?php
                                    // buat sub total
                                    $sub_total_beli  += $data2['jumlah_beli'];
                                    $sub_total_bayar += $data2['total_harga'];
                                };
                                ?>
                                    <tr class="tr-judul">
                                        <td height="30" align="right" colspan="4"><strong>Total :</strong></td>
                                        <td height="30" align="right"><strong><?php echo number_format($sub_total_beli, 0, ".", "."); ?></strong></td>
                                        <td class="td-right" height="30" align="right"><strong>Rp. <?php echo number_format($sub_total_bayar, 0, ".", "."); ?></strong></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                <?php
                    $no++;
                    // buat total seluruh
                    $total_beli_seluruh  += $sub_total_beli;
                    $total_bayar_seluruh += $sub_total_bayar;
                };
                ?>
                    <tr>
                        <td class="border-dotted" height="30" align="left" colspan="4">------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</td>
                    </tr>
                </table>

                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr class="tr-judul">
                        <td width="667" height="30" align="right"><strong>Total Seluruh : </strong></td>
                        <td width="127" height="30" align="right"><strong><?php echo number_format($total_beli_seluruh, 0, ".", "."); ?></strong></td>
                        <td class="td-right" width="195" height="30" align="right"><strong>Rp. <?php echo number_format($total_bayar_seluruh, 0, ".", "."); ?></strong></td>
                    </tr>
                </table>
                <?php
                // tutup koneksi
                $pdo = null;
            } catch (PDOException $e) {
                // tampilkan pesan kesalahan
                echo "Query Error : ".$e->getMessage();
            }
            ?>

                <div class="footer-tanggal">
                    <p>Balikpapan, <?php echo tgl_eng_to_ind(date("d-m-Y")); ?></p> <br><br>
                    <p>.....................................................</p>
                </div>
            </body>
        </html> <!-- Akhir halaman HTML yang akan di konvert -->

        <?php
        // jika "tgl_awal" sama dengan "tgl_akhir"
        if ($_GET['tgl_awal'] == $_GET['tgl_akhir']) {
            // nama file pdf yang dihasilkan
            $filename="LAPORAN-PEMBELIAN-PER-SUPPLIER-".$_GET['supplier']."-".$_GET['tgl_awal'].".pdf";
        }
        // jika tgl_awal" tidak sama dengan "tgl_akhir"
        else {
            // nama file pdf yang dihasilkan
            $filename="LAPORAN-PEMBELIAN-PER-SUPPLIER-".$_GET['supplier']."-".$_GET['tgl_awal']."-sd-".$_GET['tgl_akhir'].".pdf";
        }

        // ====================================== Convert HTML ke PDF ======================================
        $content = ob_get_clean();
        $content = '<page style="font-family: freeserif">'.($content).'</page>';
        // panggil file library html2pdf
        require_once('../../assets/plugins/html2pdf_v4.03/html2pdf.class.php');
        try
        {
            $html2pdf = new HTML2PDF('L','A4','en', false, 'ISO-8859-15',array(10, 8, 10, 8));
            $html2pdf->setDefaultFont('Arial');
            $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
            $html2pdf->Output($filename);
        }
        catch(HTML2PDF_exception $e) { echo $e; }
    }
}
?>
