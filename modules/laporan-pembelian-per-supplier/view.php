<?php
// fungsi untuk pengecekan status login user
// jika user belum login, alihkan ke halaman "login-error"
$arr_supplier = [];
if (empty($_SESSION['username']) && empty($_SESSION['password'])){
    echo "<meta http-equiv='refresh' content='0; url=../../login-error'>";
}
// jika user sudah login
else { ?>
    <div class="row mb-3">
        <div class="col-md-12">
    		<h5>
    			<!-- judul halaman laporan pembelian per supplier -->
    			<i class="fas fa-file-invoice title-icon"></i> Riwayat Pembelian Per Supplier
    		</h5>
    	</div>
    </div>

    <div class="border mb-4"></div>

    <div class="row">
        <div class="col-md-12">
            <!-- form filter supplier dan periode pembelian -->
            <form id="formFilter">
            	<div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Periode Pembelian : </label>
                            <input type="text" class="form-control date-picker" data-date-format="dd-mm-yyyy" id="tgl_awal" name="tgl_awal" placeholder="Tanggal Awal" autocomplete="off" required>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label style="color:transparent">Periode Pembelian : </label>
                            <input type="text" class="form-control date-picker" data-date-format="dd-mm-yyyy" id="tgl_akhir" name="tgl_akhir" placeholder="Tanggal Akhir" autocomplete="off" required>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Supplier</label>
                            <select class="chosen-select" id="supplier" name="supplier" autocomplete="off">
                                <option value="">-- Pilih --</option>
                                <option value="Seluruh">Seluruh</option>
                                <?php
                                try {
                                    // sql statement untuk menampilkan data dari tabel "supplier"
                                    $query = "SELECT kode_supplier, nama_supplier FROM supplier ORDER BY kode_supplier ASC";
                                    // membuat prepared statements
                                    $stmt = $pdo->prepare($query);

                                    // eksekusi query
                                    $stmt->execute();
                                    // tampilkan hasil query
                                    while ($data_supplier = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                        $arr_supplier[] = $data_supplier;
                                        echo"<option value='$data_supplier[kode_supplier]'> $data_supplier[kode_supplier] - $data_supplier[nama_supplier] </option>";
                                    }

                                    // tutup koneksi
                                    $pdo = null;
                                } catch (PDOException $e) {
                                    // tampilkan pesan kesalahan
                                    echo $e->getMessage();
                                }
                                ?>
                            </select>

                            <input type="hidden" id="nama_supplier" name="nama_supplier">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <button type="button" class="btn btn-success mt-4 mr-4" id="btnTampil">Tampilkan</button>
                            <!-- tombol export data ke format excel -->
                            <a style="display:none" class="btn btn-success mt-4 float-right" id="btnExport" href="javascript:void(0);" role="button">
                                <i class="fas fa-file-export title-icon"></i> Export
                            </a>
                            <!-- tombol cetak data ke format pdf -->
                            <a style="display:none" class="btn btn-warning mt-4 mr-3 float-right" id="btnCetak" href="javascript:void(0);" role="button">
                                <i class="fas fa-print title-icon"></i> Cetak
                            </a>
                        </div>
                    </div>
    			</div>
            </form>
        </div>
    </div>

    <div class="border mt-2 mb-4"></div>

    <div class="row">
        <div style="display:none" id="tabelLaporan" class="col-md-12">
            <!-- tampilkan informasi supplier dan periode pembelian -->
            <div class="alert alert-success" role="alert">
                <i class="fas fa-info-circle title-icon"></i><span id="judulLaporan"></span>
            </div>
            <!-- parameter untuk memuat isi tabel laporan pembelian -->
            <div class="row" id="loadData"></div>
        </div>
    </div>


    <script type="text/javascript">
    openFeedbackForm = (_this) => {
      const value = $(_this).data('modal-value');
      $('#modalformfeedback').modal('show');
      $('#modalformfeedback > #formfeedback #supplier').val(value).change();
    }
    $(document).ready(function(){
        // ======================================= initiate plugin =======================================
        // datepicker plugin
        $('.date-picker').datepicker({
            autoclose: true,
            todayHighlight: true
        });

        // chosen-select plugin
        $('.chosen-select').chosen();
        // ===============================================================================================

        // ============================================ Form =============================================
        // Menampilkan nama supplier dari select box ke textfield
        $('#supplier').change(function(){
            // mengambil value dari "kode_supplier"
            var kode_supplier = $('#supplier').val();

            $.ajax({
                type     : "GET",                                   // mengirim data dengan method GET
                url      : "modules/supplier/get_data.php",         // proses get nama supplier berdasarkan "kode_supplier"
                data     : {kode_supplier:kode_supplier},           // data yang dikirim
                dataType : "JSON",                                  // tipe data JSON
                success: function(result){                          // ketika proses get nama supplier selesai
                    // tampilkan data
                    $("#nama_supplier").val(result.nama_supplier);
                }
            });
        });
        // ===============================================================================================

        // ============================================ view =============================================
        // Tampilkan tabel laporan pembelian
        $('#btnTampil').click(function(){
            // validasi form input
            // jika tanggal awal kosong
            if ($('#tgl_awal').val()==""){
                // focus ke input tanggal awal
                $( "#tgl_awal" ).focus();
                // tampilkan peringatan data tidak boleh kosong
                $.notify({
                    title   : '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                    message : 'Tanggal awal tidak boleh kosong.'
                },{type: 'warning'});
            }
            // jika tanggal akhir kosong
            else if ($('#tgl_akhir').val()==""){
                // focus ke input tanggal akhir
                $( "#tgl_akhir" ).focus();
                // tampilkan peringatan data tidak boleh kosong
                $.notify({
                    title   : '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                    message : 'Tanggal akhir tidak boleh kosong.'
                },{type: 'warning'});
            }
            // jika supplier kosong
            else if ($('#supplier').val()==""){
                // focus ke input supplier
                $( "#supplier" ).focus();
                // tampilkan peringatan data tidak boleh kosong
                $.notify({
                    title   : '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                    message : 'Supplier tidak boleh kosong.'
                },{type: 'warning'});
            }
            // jika semua data sudah diisi, jalankan perintah untuk menampilkan data
            else {
            	// membuat variabel untuk menampung data dari form filter
            	var data = $('#formFilter').serialize();

            	$.ajax({
    				type : "GET",                           	                   // mengirim data dengan method GET
    				url  : "modules/laporan-pembelian-per-supplier/get_data.php",  // proses get data pembelian berdasarkan tanggal
    				data : data,                 				                   // data yang dikirim
    	            success: function(data){                                       // ketika proses get data selesai
                        // cek value supplier
                        // jika supplier dipilih seluruh
                        if ($('#supplier').val() === 'Seluruh') {
                            var supplier = 'Seluruh';
                        }
                        // jika supplier dipilih per supplier
                        else {
                            var supplier = $('#supplier').val() +' - '+ $('#nama_supplier').val();
                        }

                        // cek value "tgl_awal" dan "tgl_akhir"
                        // jika "tgl_awal" sama dengan "tgl_akhir"
                        if ($('#tgl_awal').val() === $('#tgl_akhir').val()) {
                            // tampilkan judul laporan
                            $('#judulLaporan').html('Laporan Pembelian Per Supplier <strong>'+ supplier +'</strong>, Periode <strong>'+ $('#tgl_awal').val() +'</strong>.');
                        }
                        // jika tgl_awal" tidak sama dengan "tgl_akhir"
                        else {
                            // tampilkan judul laporan
                            $('#judulLaporan').html('Laporan Pembelian Per Supplier <strong>'+ supplier +'</strong>, Periode <strong>'+ $('#tgl_awal').val() +'</strong> s.d. <strong>'+ $('#tgl_akhir').val() +'</strong>.');
                        }

                        // tampilkan tabel laporan pembelian
                        $('#tabelLaporan').removeAttr('style');
                        // tampilkan data laporan pembelian
                        $('#loadData').html(data);
                        // tampilkan tombol cetak
                        $('#btnCetak').removeAttr('style');
                        // tampilkan tombol export
                        $('#btnExport').removeAttr('style');
    	            }
    	        });
            }
        });
        // ===============================================================================================

        // =========================================== Cetak =============================================
        $('#btnCetak').click(function(){
            // validasi form input
            // jika tanggal awal kosong
            if ($('#tgl_awal').val()==""){
                // focus ke input tanggal awal
                $( "#tgl_awal" ).focus();
                // tampilkan peringatan data tidak boleh kosong
                $.notify({
                    title   : '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                    message : 'Tanggal awal tidak boleh kosong.'
                },{type: 'warning'});
            }
            // jika tanggal akhir kosong
            else if ($('#tgl_akhir').val()==""){
                // focus ke input tanggal akhir
                $( "#tgl_akhir" ).focus();
                // tampilkan peringatan data tidak boleh kosong
                $.notify({
                    title   : '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                    message : 'Tanggal akhir tidak boleh kosong.'
                },{type: 'warning'});
            }
            // jika supplier kosong
            else if ($('#supplier').val()==""){
                // focus ke input supplier
                $( "#supplier" ).focus();
                // tampilkan peringatan data tidak boleh kosong
                $.notify({
                    title   : '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                    message : 'Supplier tidak boleh kosong.'
                },{type: 'warning'});
            }
            // jika semua data sudah diisi, jalankan perintah untuk cetak laporan
            else {
                // ambil value "tgl_awal" dan "tgl_akhir" dari form filter
                var tgl_awal  = $('#tgl_awal').val();
                var tgl_akhir = $('#tgl_akhir').val();
                var supplier  = $('#supplier').val();

                if ($('#supplier').val() === 'Seluruh') {
                    var nama_supplier = 'Seluruh';
                } else {
                    var nama_supplier = $('#nama_supplier').val();
                }

                // buka file "cetak.php" dan kirim "value"
                window.open('laporan-pembelian-per-supplier-cetak-'+ tgl_awal +'-sd-'+ tgl_akhir +'-'+ supplier +'-'+ nama_supplier);
            }
        });
        // ===============================================================================================

        // =========================================== Export ============================================
        $('#btnExport').click(function(){
            // validasi form input
            // jika tanggal awal kosong
            if ($('#tgl_awal').val()==""){
                // focus ke input tanggal awal
                $( "#tgl_awal" ).focus();
                // tampilkan peringatan data tidak boleh kosong
                $.notify({
                    title   : '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                    message : 'Tanggal awal tidak boleh kosong.'
                },{type: 'warning'});
            }
            // jika tanggal akhir kosong
            else if ($('#tgl_akhir').val()==""){
                // focus ke input tanggal akhir
                $( "#tgl_akhir" ).focus();
                // tampilkan peringatan data tidak boleh kosong
                $.notify({
                    title   : '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                    message : 'Tanggal akhir tidak boleh kosong.'
                },{type: 'warning'});
            }
            // jika supplier kosong
            else if ($('#supplier').val()==""){
                // focus ke input supplier
                $( "#supplier" ).focus();
                // tampilkan peringatan data tidak boleh kosong
                $.notify({
                    title   : '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                    message : 'Supplier tidak boleh kosong.'
                },{type: 'warning'});
            }
            // jika semua data sudah diisi, jalankan perintah untuk export laporan
            else {
                // ambil value "tgl_awal" dan "tgl_akhir" dari form filter
                var tgl_awal  = $('#tgl_awal').val();
                var tgl_akhir = $('#tgl_akhir').val();
                var supplier  = $('#supplier').val();

                if ($('#supplier').val() === 'Seluruh') {
                    var nama_supplier = 'Seluruh';
                } else {
                    var nama_supplier = $('#nama_supplier').val();
                }

                // arahkan ke file "export.php" dan kirim "value"
                location.href = "laporan-pembelian-per-supplier-export-"+ tgl_awal +"-sd-"+ tgl_akhir +'-'+ supplier +'-'+ nama_supplier;
            }
        });
        // ===============================================================================================
        // Tampilkan Modal Form Ubah Data
        // $('#tabel-bahanbaku tbody').on( 'click', '.getUbah', function (){
        //     // judul form
        //     $('#modalLabel').text('Input Feedback');
        //     // ambil data dari datatables
        //     var data = table.row( $(this).parents('tr') ).data();
        //     // membuat variabel untuk menampung data "kode_bahanbaku"
        //     var kode_bahanbaku = data[ 1 ];
        //
        // });
    });
    </script>

    <!-- Modal form untuk entri -->
    <div class="modal fade" id="modalformfeedback" tabindex="-1" role="dialog" aria-labelledby="modalformfeedback" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
              <!-- judul form data bahanbaku -->
                <div class="modal-header">
                    <h5 class="modal-title"><i class="fas fa-edit title-icon"></i>Form Input Feedback</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
          <!-- isi form data bahanbaku -->
                <form id="formfeedback">
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Supplier</label>
                            <select class="chosen-select" id="supplier" name="supplier" autocomplete="off" required>
                                <option value="">-- Pilih --</option>
                                <?php
                                for ($i=0; $i < count($arr_supplier); $i++) {
                                  echo"<option value='".$arr_supplier[$i]['kode_supplier']."'> ".$arr_supplier[$i]['kode_supplier']." - ".$arr_supplier[$i]['nama_supplier']." </option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                          <label>Feedback</label>
                          <textarea name="feedback" class="form-control" required></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success mr-2" id="btnSimpan">Simpan</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>
