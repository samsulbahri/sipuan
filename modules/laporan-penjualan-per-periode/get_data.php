<?php
// Mengecek AJAX Request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' )) {
    // panggil file "config.php" untuk koneksi ke database
    require_once "../../config/config.php";

    // mengecek data get dari ajax
    if (isset($_GET['tgl_awal'])) {
        try {
            // ambil "data" get dari ajax
            $tgl_awal  = date('Y-m-d', strtotime($_GET['tgl_awal']));
            $tgl_akhir = date('Y-m-d', strtotime($_GET['tgl_akhir']));
            // variabel untuk nomor urut tabel
            $no = 1;
            // variabel untuk total bayar
            $total = 0;

            // sql statement untuk menampilkan data dari tabel "penjualan" berdasarkan "tanggal"
            $query = "SELECT no_penjualan, tanggal, (SELECT SUM(a.jumlah_jual)
                      FROM penjualan_detail as a WHERE a.no_penjualan=penjualan.no_penjualan) as total_pemakaian FROM penjualan
                      WHERE tanggal BETWEEN :tgl_awal AND :tgl_akhir ORDER BY tanggal ASC, no_penjualan ASC";
            // membuat prepared statements
            $stmt = $pdo->prepare($query);

            // hubungkan "data" dengan prepared statements
            $stmt->bindParam(':tgl_awal', $tgl_awal);
            $stmt->bindParam(':tgl_akhir', $tgl_akhir);

            // eksekusi query
            $stmt->execute();

            // cek hasil query
            // jika data ada, lakukan perulangan untuk menampilkan data
            if ($stmt->rowCount() <> 0) {
                // tampilkan hasil query
                while ($data = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    echo "<tr>
                            <td width='50' class='center'>".$no."</td>
                            <td width='150' class='center'>".date('d-m-Y', strtotime($data['tanggal']))."</td>
                            <td width='200' class='center'>".$data['no_penjualan']."</td>
                            <td width='200' class='right'>".$data['total_pemakaian']."</td>
                        </tr>";
                    $no++;
                    $total += $data['total_pemakaian'];
                };
            }
            // jika data tidak ada
            else {
                echo "<tr>
                        <td class='center' colspan='4'>Tidak ada data yang tersedia pada tabel</td>
                    </tr>";
            }

            // tutup koneksi
            $pdo = null;
        } catch (PDOException $e) {
            // tampilkan pesan kesalahan
            echo "Query Error : ".$e->getMessage();
        }
    }
} else {
    // jika tidak ada ajax request, maka alihkan ke halaman "login-error"
    echo '<script>window.location="../../login-error"</script>';
}
?>
