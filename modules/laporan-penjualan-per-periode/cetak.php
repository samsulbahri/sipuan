<?php
session_start();      // memulai session
ob_start();

// fungsi untuk pengecekan status login user
// jika user belum login, alihkan ke halaman "login-error"
if (empty($_SESSION['username']) && empty($_SESSION['password'])){
    echo "<meta http-equiv='refresh' content='0; url=../../login-error'>";
}
// jika user sudah login
else {
    // mengecek data get
    if (isset($_GET['tgl_awal']) && isset($_GET['tgl_akhir'])) { ?>
        <!-- Bagian halaman HTML yang akan konvert -->
        <!doctype html>
        <html lang="en">
            <head>
                <!-- Required meta tags -->
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
                <!-- Custom CSS -->
                <link rel="stylesheet" type="text/css" href="../../assets/css/laporan.css" />
                <!-- Title -->
                <title>LAPORAN PENJUALAN PER PERIODE</title>
            </head>
            <body>
                <?php
                // panggil file "config.php" untuk koneksi ke database
                require_once "../../config/config.php";
                // panggil file "fungsi_tanggal.php" untuk format tanggal
                require_once "../../config/fungsi_tanggal.php";

                try {
                    // siapkan "data"
                    $id = 1;

                    // sql statement untuk menampilkan data dari tabel "sys_config" berdasarkan "id"
                    $query = "SELECT nama, alamat, telepon, logo FROM sys_config WHERE id=:id";
                    // membuat prepared statements
                    $stmt = $pdo->prepare($query);

                    // hubungkan "data" dengan prepared statements
                    $stmt->bindParam(':id', $id);

                    // eksekusi query
                    $stmt->execute();

                    // ambil data hasil query
                    $data = $stmt->fetch(PDO::FETCH_ASSOC);
                    // tampilkan data
                    $nama    = $data['nama'];
                    $alamat  = $data['alamat'];
                    $telepon = $data['telepon'];
                    $logo    = $data['logo'];

                } catch (PDOException $e) {
                    // tampilkan pesan kesalahan
                    echo "Query Error : ".$e->getMessage();
                }
                ?>

                <!-- Kop Laporan -->
                <div class="nama-instansi"><?php echo $nama; ?></div>
                <div class="info-instansi"><?php echo $alamat; ?></div>
                <div class="info-instansi">Telp. <?php echo $telepon; ?></div>
                <div class="logo">
                    <img src="../../assets/img/<?php echo $logo; ?>" alt="Logo">
                </div>

                <hr><br>

                <!-- Judul Laporan -->
                <div class="judul-laporan">
                    LAPORAN PENJUALAN PER PERIODE
                </div>

                <?php
                // jika "tgl_awal" sama dengan "tgl_akhir"
                if ($_GET['tgl_awal'] == $_GET['tgl_akhir']) { ?>
                    <!-- tampilkan judul -->
                    <div class="judul-tanggal">
                        <?php echo tgl_eng_to_ind($_GET['tgl_awal']); ?>
                    </div>
                <?php
                }
                // jika tgl_awal" tidak sama dengan "tgl_akhir"
                else { ?>
                    <!-- tampilkan judul -->
                    <div class="judul-tanggal">
                        <?php echo tgl_eng_to_ind($_GET['tgl_awal']); ?> s.d. <?php echo tgl_eng_to_ind($_GET['tgl_akhir']); ?>
                    </div>
                <?php
                }
                ?>

                <br>
                <!-- Tabel untuk menampilkan laporan penjualan dari database -->
                <table width="100%" border="0.5" cellpadding="0" cellspacing="0">
                    <!-- judul kolom pada bagian kepala (atas) tabel -->
                    <thead style="background:#e8ecee">
                        <tr class="tr-judul">
                            <th width='80' height="30" align="center" valign="middle">No.</th>
                            <th width='255' height="30" align="center" valign="middle">Tanggal</th>
                            <th width='330' height="30" align="center" valign="middle">No. Penjualan</th>
                            <th width='330' height="30" align="center" valign="middle">Total</th>
                        </tr>
                    </thead>
                    <!-- isi tabel -->
                    <tbody>
                    <?php
                    try {
                        // ambil "data" get
                        $tgl_awal  = date('Y-m-d', strtotime($_GET['tgl_awal']));
                        $tgl_akhir = date('Y-m-d', strtotime($_GET['tgl_akhir']));
                        // variabel untuk nomor urut tabel
                        $no = 1;
                        // variabel untuk total bayar
                        $total = 0;

                        // sql statement untuk menampilkan data dari tabel "penjualan" berdasarkan "tanggal"
                        $query = "SELECT no_penjualan, tanggal, total_bayar FROM penjualan
                                  WHERE tanggal BETWEEN :tgl_awal AND :tgl_akhir ORDER BY tanggal ASC, no_penjualan ASC";
                        // membuat prepared statements
                        $stmt = $pdo->prepare($query);

                        // hubungkan "data" dengan prepared statements
                        $stmt->bindParam(':tgl_awal', $tgl_awal);
                        $stmt->bindParam(':tgl_akhir', $tgl_akhir);

                        // eksekusi query
                        $stmt->execute();

                        // cek hasil query
                        // jika data ada, lakukan perulangan untuk menampilkan data
                        if ($stmt->rowCount() <> 0) {
                            // tampilkan hasil query
                            while ($data = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                echo "<tr class='tr-isi'>
                                        <td width='80' height='20' align='center'>".$no."</td>
                                        <td width='255' height='20' align='center'>".date('d-m-Y', strtotime($data['tanggal']))."</td>
                                        <td style='padding-left:7px;' width='330' height='20' align='center'>".$data['no_penjualan']."</td>
                                        <td style='padding-right:7px;' width='330' height='20' align='right'>Rp. ".number_format($data['total_bayar'], 0, ".", ".")."</td>
                                    </tr>";
                                $no++;
                                $total += $data['total_bayar'];
                            };
                            echo "<tr>
                                    <td style='background:#e8ecee;' height='30' align='center' colspan='3'><strong>Total</strong></td>
                                    <td style='padding-right:7px;background:#e8ecee;' height='30' align='right'><strong>Rp. ".number_format($total, 0, ".", ".")."</strong></td>
                                </tr>";
                        }
                        // jika data tidak ada
                        else {
                            echo "<tr class='tr-isi'>
                                    <td height='20' align='center' colspan='4'>Tidak ada data yang tersedia pada tabel</td>
                                </tr>";
                        }

                        // tutup koneksi
                        $pdo = null;
                    } catch (PDOException $e) {
                        // tampilkan pesan kesalahan
                        echo "Query Error : ".$e->getMessage();
                    }
                    ?>
                    </tbody>
                </table>

                <div class="footer-tanggal">
                    <p>Bandar Lampung, <?php echo tgl_eng_to_ind(date("d-m-Y")); ?></p> <br><br>
                    <p>.....................................................</p>
                </div>
            </body>
        </html> <!-- Akhir halaman HTML yang akan di konvert -->

        <?php
        // jika "tgl_awal" sama dengan "tgl_akhir"
        if ($_GET['tgl_awal'] == $_GET['tgl_akhir']) {
            // nama file pdf yang dihasilkan
            $filename="LAPORAN-PENJUALAN-PER-PERIODE-".$_GET['tgl_awal'].".pdf";
        }
        // jika tgl_awal" tidak sama dengan "tgl_akhir"
        else {
            // nama file pdf yang dihasilkan
            $filename="LAPORAN-PENJUALAN-PER-PERIODE-".$_GET['tgl_awal']."-sd-".$_GET['tgl_akhir'].".pdf";
        }

        // ====================================== Convert HTML ke PDF ======================================
        $content = ob_get_clean();
        $content = '<page style="font-family: freeserif">'.($content).'</page>';
        // panggil file library html2pdf
        require_once('../../assets/plugins/html2pdf_v4.03/html2pdf.class.php');
        try
        {
            $html2pdf = new HTML2PDF('L','A4','en', false, 'ISO-8859-15',array(10, 8, 10, 8));
            $html2pdf->setDefaultFont('Arial');
            $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
            $html2pdf->Output($filename);
        }
        catch(HTML2PDF_exception $e) { echo $e; }
    }
}
?>
