<?php
session_start();      // memulai session

// fungsi untuk pengecekan status login user
// jika user belum login, alihkan ke halaman "login-error"
if (empty($_SESSION['username']) && empty($_SESSION['password'])){
    echo "<meta http-equiv='refresh' content='0; url=../../login-error'>";
}
// jika user sudah login
else {
    // mengecek data get
    if (isset($_GET['filter'])) {
        // fungsi untuk export data
        header("Content-Type: application/force-download");
        header("Cache-Control: no-cache, must-revalidate");
        // cek filter stok
        // jika "filter = Seluruh"
        if ($_GET['filter'] == 'Seluruh') {
            // nama file hasil export
            header("content-disposition: attachment;filename=LAPORAN-SELURUH-STOK-BAHAN-BAKU.xls");
        }
        // jika "filter = Minimum"
        elseif ($_GET['filter'] == 'Minimum') {
            // nama file hasil export
            header("content-disposition: attachment;filename=LAPORAN-STOK-BAHAN-BAKU-MINIMUM.xls");
        }
    ?>
        <!-- Judul laporan -->
        <center>
        <?php
        // jika "filter = Seluruh"
        if ($_GET['filter'] == 'Seluruh') {
            // tampilkan judul
            echo "<h3>LAPORAN SELURUH STOK BAHAN BAKU</h3>";
        }
        // jika "filter = Minimum"
        elseif ($_GET['filter'] == 'Minimum') {
            // tampilkan judul
            echo "<h3>LAPORAN STOK BAHAN BAKU YANG MENCAPAI BATAS MINIMUM</h3>";
        }
        ?>
        </center>
        <!-- Table untuk di Export Ke Excel -->
        <table border='1'>
            <h3>
                <thead>
                    <tr>
                        <th width='50' align="center" valign="middle">No.</th>
                        <th width='100' align="center" valign="middle">Kode Bahan Baku</th>
                        <th width='200' align="center" valign="middle">Nama Bahan Baku</th>
                        <th width='120' align="center" valign="middle">Satuan</th>
                        <th width='100' align="center" valign="middle">Min. Stok</th>
                        <th width='100' align="center" valign="middle">Stok</th>
                    </tr>
                </thead>
            </h3>

            <tbody>
            <?php
            // panggil file "config.php" untuk koneksi ke database
            require_once "../../config/config.php";

            try {
                // ambil "data" get
                $filter = $_GET['filter'];
                // variabel untuk nomor urut tabel
                $no = 1;

                // jika "filter = Seluruh"
                if ($filter == 'Seluruh') {
                    // sql statement untuk menampilkan seluruh data dari tabel "bahanbaku"
                    $query = "SELECT a.kode_bahanbaku,a.nama_bahanbaku,a.harga_beli,a.harga_jual,a.satuan,a.min_stok,a.stok,b.nama_satuan
                              FROM bahanbaku as a INNER JOIN satuan as b ON a.satuan=b.kode_satuan";
                }
                // jika "filter = Minimum"
                elseif ($filter == 'Minimum') {
                    // sql statement untuk menampilkan data dari tabel "bahanbaku" berdasarkan "stok <= 2"
                    $query = "SELECT a.kode_bahanbaku,a.nama_bahanbaku,a.harga_beli,a.harga_jual,a.satuan,a.min_stok,a.stok,b.nama_satuan
                              FROM bahanbaku as a INNER JOIN satuan as b ON a.satuan=b.kode_satuan WHERE a.stok <= 2";
                }

                // membuat prepared statements
                $stmt = $pdo->prepare($query);

                // eksekusi query
                $stmt->execute();

                // cek hasil query
                // jika data ada, lakukan perulangan untuk menampilkan data
                if ($stmt->rowCount() <> 0) {
                    // tampilkan hasil query
                    while ($data = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        echo "<tr>
                                <td width='50' align='center'>".$no."</td>
                                <td width='100' align='center'>".$data['kode_bahanbaku']."</td>
                                <td width='200'>".$data['nama_bahanbaku']."</td>
                                <td width='120' align='center'>".$data['nama_satuan']."</td>
                                <td width='100' align='center'>".$data['min_stok']."</td>
                                <td width='100' align='center'>".$data['stok']."</td>
                            </tr>";
                        $no++;
                    };
                }
                // jika data tidak ada
                else {
                    echo "<tr>
                            <td height='20' align='center' colspan='8'>Tidak ada data yang tersedia pada tabel</td>
                        </tr>";
                }

                // tutup koneksi
                $pdo = null;
            } catch (PDOException $e) {
                // tampilkan pesan kesalahan
                echo "Query Error : ".$e->getMessage();
            }
            ?>
            </tbody>
        </table>

        <div style="text-align:right">
            <h4>Balikpapan, <?php echo date("d/m/Y"); ?></h4> <br><br>
            <h4>.................................................</h4>
        </div>
    <?php
    }
}
?>
