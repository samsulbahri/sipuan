<?php
// fungsi untuk pengecekan status login user
// jika user belum login, alihkan ke halaman "login-error"
if (empty($_SESSION['username']) && empty($_SESSION['password'])){
    echo "<meta http-equiv='refresh' content='0; url=../../login-error'>";
}
// jika user sudah login
else { ?>
    <div class="row mb-3">
        <div class="col-md-12">
            <h5>
                <!-- judul halaman laporan stok -->
                <i class="fas fa-file-alt title-icon"></i> Laporan Stok
            </h5>
        </div>
    </div>

    <div class="border mb-4"></div>

    <div class="row">
        <div class="col-md-12">
            <!-- form filter data stok bahanbaku -->
            <form id="formFilter">
                <div class="row">
                    <div class="col">
                        <div class="form-group mb-0">
                            <label>Stok : </label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <select class="form-control" id="stok" name="stok" autocomplete="off">
                                <option value="">-- Pilih --</option>
                                <option value="Seluruh">Seluruh</option> <!-- menampilkan seluruh data stok bahanbaku -->
                                <option value="Minimum">Minimum</option> <!-- menampilkan data stok bahanbaku yang mencapai batas minimum -->
                            </select>
                        </div>
                    </div>

                    <div class="col">
                        <div class="form-group">
                            <button type="button" class="btn btn-success" id="btnTampil">Tampilkan</button>
                        </div>
                    </div>

                    <div class="col">
                        <div class="form-group right">
                            <!-- tombol cetak data ke format pdf -->
                            <a style="display:none" class="btn btn-warning mr-2 mb-3" id="btnCetak" href="javascript:void(0);" role="button">
                                <i class="fas fa-print title-icon"></i> Cetak
                            </a>
                            <!-- tombol export data ke format excel -->
                            <a style="display:none" class="btn btn-success mb-3" id="btnExport" href="javascript:void(0);" role="button">
                                <i class="fas fa-file-export title-icon"></i> Export
                            </a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="border mt-2 mb-4"></div>

    <div class="row">
        <div style="display:none" id="tabelLaporan" class="col-md-12">
            <!-- tampilkan judul laporan -->
            <div class="alert alert-success" role="alert">
                <i class="fas fa-info-circle title-icon"></i><span id="judulLaporan"></span>
            </div>
            <!-- Tabel untuk menampilkan laporan stok dari database -->
            <table class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <!-- judul kolom pada bagian kepala (atas) tabel -->
                <thead>
                    <tr>
                        <th class="center">No.</th>
                        <th class="center">Kode Bahan Baku</th>
                        <th class="center">Nama Bahan Baku</th>
                        <th class="center">Satuan</th>
                        <th class="center">Min. Stok</th>
                        <th class="center">Stok</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    <script type="text/javascript">
    $(document).ready(function(){
        // ============================================ View =============================================
        // Tampilkan tabel laporan stok bahanbaku
        $('#btnTampil').click(function(){
            // validasi form input
            // jika filter stok kosong
            if ($('#stok').val()==""){
                // focus ke input filter stok
                $( "#stok" ).focus();
                // tampilkan peringatan data tidak boleh kosong
                $.notify({
                    title   : '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                    message : 'Filter Stok tidak boleh kosong.'
                },{type: 'warning'});
            }
            // jika filter stok sudah diisi, jalankan perintah untuk menampilkan data
            else {
                // tampilkan tabel laporan
                $('#tabelLaporan').removeAttr('style');
                // tampilkan tombol cetak
                $('#btnCetak').removeAttr('style');
                // tampilkan tombol export
                $('#btnExport').removeAttr('style');

                // mengambil value filter stok
                var filter = $('#stok').val();
                // cek value filter stok
                // jika "filter = Seluruh"
                if (filter === 'Seluruh') {
                    // tampilkan judul laporan
                    $('#judulLaporan').text('Laporan Seluruh Stok Bahan Baku.');
                    // set id tabel = tabel-laporan-stok-seluruh
                    var idTabel = $('table').attr('id', 'tabel-laporan-stok-seluruh');
                    // set url untuk menampilkan seluruh data stok bahanbaku
                    var url = 'modules/laporan-stok/get_seluruh.php';
                }
                // jika "filter = Minimum"
                else if (filter === 'Minimum') {
                    // tampilkan judul laporan
                    $('#judulLaporan').text('Laporan Stok Bahan Baku yang Mencapai Batas Minimum.');
                    // set id tabel = tabel-laporan-stok-minimum
                    var idTabel = $('table').attr('id', 'tabel-laporan-stok-minimum');
                    // set url untuk menampilkan data stok bahanbaku yang mencapai batas minimum
                    var url = 'modules/laporan-stok/get_minimum.php';
                }

                // dataTables plugin untuk membuat nomor urut tabel
                $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
                {
                    return {
                        "iStart": oSettings._iDisplayStart,
                        "iEnd": oSettings.fnDisplayEnd(),
                        "iLength": oSettings._iDisplayLength,
                        "iTotal": oSettings.fnRecordsTotal(),
                        "iFilteredTotal": oSettings.fnRecordsDisplay(),
                        "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                        "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                    };
                };

                // datatables serverside processing
                var table = idTabel.DataTable( {
                    "destroy": true,                        // menghapus "idTabel" yang sudah ada agar bisa diinisialisasi kembali
                    "processing": true,                     // tampilkan loading saat proses data
                    "serverSide": true,                     // server-side processing
                    "ajax": url,                            // proses get data bahanbaku
                    // menampilkan data
                    "columnDefs": [
                        { "targets": 0, "data": null, "orderable": false, "searchable": false, "width": '30px', "className": 'center' },
                        { "responsivePriority": 1, "targets": 1, "width": '70px', "className": 'center' },
                        { "targets": 2, "width": '200px' },
                        { "targets": 3, "width": '80px', "className": 'center' },
                        { "targets": 4, "width": '70px', "className": 'center' },
                        { "targets": 5, "width": '70px', "className": 'center', render: $.fn.DataTable.render.number( '.', ',', 0, '' ) }
                    ],
                    "order": [[ 1, "asc" ]],            // urutkan data berdasarkan "kode_bahanbaku" secara ascending
                    "iDisplayLength": 10,               // tampilkan 10 data per halaman
                    // membuat nomor urut tabel
                    "rowCallback": function (row, data, iDisplayIndex) {
                        var info   = this.fnPagingInfo();
                        var page   = info.iPage;
                        var length = info.iLength;
                        var index  = page * length + (iDisplayIndex + 1);
                        $('td:eq(0)', row).html(index);
                    }
                } );
            }
        });
        // ===============================================================================================

        // =========================================== Cetak =============================================
        $('#btnCetak').click(function(){
            // validasi form input
            // jika filter stok kosong
            if ($('#stok').val()==""){
                // focus ke input filter stok
                $( "#stok" ).focus();
                // tampilkan peringatan data tidak boleh kosong
                $.notify({
                    title   : '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                    message : 'Filter Stok tidak boleh kosong.'
                },{type: 'warning'});
            }
            // jika filter stok sudah diisi, jalankan perintah cetak laporan
            else {
                // mengambil value filter stok
                var filter = $('#stok').val();
                // buka file "cetak.php" dan kirim "value"
                window.open('cetak-laporan-stok-'+ filter);
            }
        });
        // ===============================================================================================

        // =========================================== Export ============================================
        $('#btnExport').click(function(){
            // validasi form input
            // jika filter stok kosong
            if ($('#stok').val()==""){
                // focus ke input filter stok
                $( "#stok" ).focus();
                // tampilkan peringatan data tidak boleh kosong
                $.notify({
                    title   : '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                    message : 'Filter Stok tidak boleh kosong.'
                },{type: 'warning'});
            }
            // jika filter stok sudah diisi, jalankan perintah export laporan
            else {
                // mengambil value filter stok
                var filter = $('#stok').val();
                // arahkan ke file "export.php" dan kirim "value"
                location.href = "export-laporan-stok-"+ filter;
            }
        });
        // ===============================================================================================
    });
    </script>
<?php } ?>
