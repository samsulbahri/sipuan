<?php
session_start();      // memulai session
ob_start();

// fungsi untuk pengecekan status login user
// jika user belum login, alihkan ke halaman "login-error"
if (empty($_SESSION['username']) && empty($_SESSION['password'])){
    echo "<meta http-equiv='refresh' content='0; url=../../login-error'>";
}
// jika user sudah login
else {
    // mengecek data get
    if (isset($_GET['filter'])) { ?>
        <!-- Bagian halaman HTML yang akan konvert -->
        <!doctype html>
        <html lang="en">
            <head>
                <!-- Required meta tags -->
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
                <!-- Custom CSS -->
                <link rel="stylesheet" type="text/css" href="../../assets/css/laporan.css" />
                <!-- Title -->
                <title>LAPORAN STOK BAHAN BAKU</title>
            </head>
            <body>
                <?php
                // panggil file "config.php" untuk koneksi ke database
                require_once "../../config/config.php";
                // panggil file "fungsi_tanggal.php" untuk format tanggal
                require_once "../../config/fungsi_tanggal.php";

                try {
                    // siapkan "data"
                    $id = 1;

                    // sql statement untuk menampilkan data dari tabel "sys_config" berdasarkan "id"
                    $query = "SELECT nama, alamat, telepon, logo FROM sys_config WHERE id=:id";
                    // membuat prepared statements
                    $stmt = $pdo->prepare($query);

                    // hubungkan "data" dengan prepared statements
                    $stmt->bindParam(':id', $id);

                    // eksekusi query
                    $stmt->execute();

                    // ambil data hasil query
                    $data = $stmt->fetch(PDO::FETCH_ASSOC);
                    // tampilkan data
                    $nama    = $data['nama'];
                    $alamat  = $data['alamat'];
                    $telepon = $data['telepon'];
                    $logo    = $data['logo'];

                } catch (PDOException $e) {
                    // tampilkan pesan kesalahan
                    echo "Query Error : ".$e->getMessage();
                }
                ?>

                <!-- Kop Laporan -->
                <div class="nama-instansi"><?php echo $nama; ?></div>
                <div class="info-instansi"><?php echo $alamat; ?></div>
                <div class="info-instansi">Telp. <?php echo $telepon; ?></div>
                <div class="logo">
                    <img src="../../assets/img/<?php echo $logo; ?>" alt="Logo">
                </div>

                <hr><br>

                <?php
                // cek filter stok
                // jika "filter = Seluruh"
                if ($_GET['filter'] == 'Seluruh') {
                    // Judul Laporan
                    echo "<div class='judul-laporan'> LAPORAN SELURUH STOK BAHAN BAKU </div>";
                }
                // jika "filter = Minimum"
                elseif ($_GET['filter'] == 'Minimum') {
                    // Judul Laporan
                    echo "<div class='judul-laporan'> LAPORAN STOK BAHAN BAKU YANG MENCAPAI BATAS MINIMUM </div>";
                }
                ?>

                <br>
                <!-- Tabel untuk menampilkan laporan stok bahanbaku dari database -->
                <table width="100%" border="0.5" cellpadding="0" cellspacing="0" align="center">
                    <!-- judul kolom pada bagian kepala (atas) tabel -->
                    <thead style="background:#e8ecee">
                        <tr class="tr-judul">
                            <th width='60' height="30" align="center" valign="middle">No.</th>
                            <th width='100' height="30" align="center" valign="middle">Kode Bahan Baku</th>
                            <th width='230' height="30" align="center" valign="middle">Nama Bahan Baku</th>
                            <th width='120' height="30" align="center" valign="middle">Satuan</th>
                            <th width='100' height="30" align="center" valign="middle">Min. Stok</th>
                            <th width='100' height="30" align="center" valign="middle">Stok</th>
                        </tr>
                    </thead>
                    <!-- isi tabel -->
                    <tbody>
                    <?php
                    try {
                        // ambil "data" get
                        $filter = $_GET['filter'];
                        // variabel untuk nomor urut tabel
                        $no = 1;

                        // jika "filter = Seluruh"
                        if ($filter == 'Seluruh') {
                            // sql statement untuk menampilkan seluruh data dari tabel "bahanbaku"
                            $query = "SELECT a.kode_bahanbaku,a.nama_bahanbaku,a.harga_beli,a.harga_jual,a.satuan,a.min_stok,a.stok,b.nama_satuan
                                      FROM bahanbaku as a INNER JOIN satuan as b ON a.satuan=b.kode_satuan";
                        }
                        // jika "filter = Minimum"
                        elseif ($filter == 'Minimum') {
                            // sql statement untuk menampilkan data dari tabel "bahanbaku" berdasarkan "stok <= 2"
                            $query = "SELECT a.kode_bahanbaku,a.nama_bahanbaku,a.harga_beli,a.harga_jual,a.satuan,a.min_stok,a.stok,b.nama_satuan
                                      FROM bahanbaku as a INNER JOIN satuan as b ON a.satuan=b.kode_satuan WHERE a.stok <= 2";
                        }

                        // membuat prepared statements
                        $stmt = $pdo->prepare($query);

                        // eksekusi query
                        $stmt->execute();

                        // cek hasil query
                        // jika data ada, lakukan perulangan untuk menampilkan data
                        if ($stmt->rowCount() <> 0) {
                            // tampilkan hasil query
                            while ($data = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                echo "<tr class='tr-isi'>
                                        <td width='60' height='20' align='center'>".$no."</td>
                                        <td width='100' height='20' align='center'>".$data['kode_bahanbaku']."</td>
                                        <td style='padding-left:7px;' width='230' height='20'>".$data['nama_bahanbaku']."</td>
                                        <td style='padding-left:7px;' width='120' height='20' align='center'>".$data['nama_satuan']."</td>
                                        <td width='100' height='20' align='center'>".$data['min_stok']."</td>
                                        <td width='100' height='20' align='center'>".$data['stok']."</td>
                                    </tr>";
                                $no++;
                            };
                        }
                        // jika data tidak ada
                        else {
                            echo "<tr class='tr-isi'>
                                    <td height='20' align='center' colspan='8'>Tidak ada data yang tersedia pada tabel</td>
                                </tr>";
                        }

                        // tutup koneksi
                        $pdo = null;
                    } catch (PDOException $e) {
                        // tampilkan pesan kesalahan
                        echo "Query Error : ".$e->getMessage();
                    }
                    ?>
                    </tbody>
                </table>

                <div class="footer-tanggal">
                    <p>Balikpapan, <?php echo tgl_eng_to_ind(date("d-m-Y")); ?></p> <br><br>
                    <p>................................................</p>
                </div>
            </body>
        </html> <!-- Akhir halaman HTML yang akan di konvert -->

        <?php
        // jika "filter = Seluruh"
        if ($_GET['filter'] == 'Seluruh') {
            // nama file pdf yang dihasilkan
            $filename="LAPORAN SELURUH STOK BAHAN BAKU.pdf";
        }
        // jika "filter = Minimum"
        elseif ($_GET['filter'] == 'Minimum') {
            // nama file pdf yang dihasilkan
            $filename="LAPORAN STOK BAHAN BAKU YANG MENCAPAI BATAS MINIMUM.pdf";
        }
        // ====================================== Convert HTML ke PDF ======================================
        $content = ob_get_clean();
        $content = '<page style="font-family: freeserif">'.($content).'</page>';
        // panggil file library html2pdf
        require_once('../../assets/plugins/html2pdf_v4.03/html2pdf.class.php');
        try
        {
            $html2pdf = new HTML2PDF('L','A4','en', false, 'ISO-8859-15',array(10, 8, 10, 8));
            $html2pdf->setDefaultFont('Arial');
            $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
            $html2pdf->Output($filename);
        }
        catch(HTML2PDF_exception $e) { echo $e; }
    }
}
?>
