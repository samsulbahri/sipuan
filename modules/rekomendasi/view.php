<?php
// fungsi untuk pengecekan status login user
// jika user belum login, alihkan ke halaman "login-error"
if (empty($_SESSION['username']) && empty($_SESSION['password'])) {
    echo "<meta http-equiv='refresh' content='0; url=../../login-error'>";
}
// jika user sudah login
else { ?>
    <div class="row mb-3">
        <div class="col-md-12">
            <h5>
                <!-- judul halaman tampil data bahanbaku -->
                <i class="fas fa-boxes title-icon"></i> Rekomendasi Pembelian Bahan Baku
            </h5>
        </div>
    </div>

    <div class="border mb-4"></div>

    <div class="row">
        <div class="col-md-12">
            <!-- Tabel bahanbaku untuk menampilkan data bahanbaku dari database -->
            <table id="tabel-bahanbaku" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <!-- judul kolom pada bagian kepala (atas) tabel -->
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Supplier</th>
                        <th>Alamat Supplier</th>
                        <th>Rating</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    <!-- Modal form untuk entri dan ubah data bahanbaku -->
    <div class="modal fade" id="modalbahanbaku" tabindex="-1" role="dialog" aria-labelledby="modalbahanbaku" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <!-- judul form data bahanbaku -->
                <div class="modal-header">
                    <h5 class="modal-title"><i class="fas fa-edit title-icon"></i><span id="modalLabel"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>=
            </div>
        </div>
    </div>

    <div id="modal-feedback">

    </div>

    <script type="text/javascript">
        function makeModalFeedback(feedback) {
            document.getElementById("modal-feedback").innerHTML = '<div class="modal fade" id="feedbackModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true"> <div class = "modal-dialog" ><div class = "modal-content" ><div class = "modal-header" ><h5 class = "modal-title" id ="exampleModalLabel" > Feedback </h5> <button type = "button" class = "close" data-dismiss = "modal" aria-label = "Close" ><span aria-hidden = "true" > &times; </span> </button> </div> <div class = "modal-body" >' + feedback + '</div> <div class = "modal-footer" ><button type = "button" class = "btn btn-primary" data-dismiss = "modal" > Tutup </button></div> </div> </div> </div>';
            $("#feedbackModal").modal('show');
        }
        $(document).ready(function() {
            // ======================================= initiate plugin =======================================
            // chosen-select plugin
            $('.chosen-select').chosen();
            // ===============================================================================================


            // ===============================================================================================

            // ============================================ View =============================================
            // dataTables plugin untuk membuat nomor urut tabel
            $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
                return {
                    "iStart": oSettings._iDisplayStart,
                    "iEnd": oSettings.fnDisplayEnd(),
                    "iLength": oSettings._iDisplayLength,
                    "iTotal": oSettings.fnRecordsTotal(),
                    "iFilteredTotal": oSettings.fnRecordsDisplay(),
                    "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                    "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                };
            };

            // datatables serverside processing
            var table = $('#tabel-bahanbaku').DataTable({
                "processing": true, // tampilkan loading saat proses data
                "serverSide": true, // server-side processing
                "ajax": 'modules/rekomendasi/data.php', // panggil file "data.php" untuk menampilkan data bahanbaku dari database
                // menampilkan data
                "columnDefs": [{
                        "targets": 0,
                        "data": null,
                        "width": '30px',
                        "className": 'center'
                    },
                    {
                        "targets": 1,
                        "width": '50px'
                    },
                    {
                        "targets": 2,
                        "width": '150px'
                    },
                    {
                        "targets": 3,
                        "data": null,
                        "width": '100px',
                        "render": function(data, type, row) {
                            var star = '';

                            if (data[3] == 1) {
                                star = '<i class="fas fa-star" style="color:orange"></i><i class="far fa-star" style="color:orange"></i><i class="far fa-star" style="color:orange"></i><i class="far fa-star" style="color:orange"></i><i class="far fa-star" style="color:orange"></i>';
                            } else if (data[3] == 2) {
                                star = '<i class="fas fa-star" style="color:orange"></i><i class="fas fa-star" style="color:orange"></i><i class="far fa-star" style="color:orange"></i><i class="far fa-star" style="color:orange"></i><i class="far fa-star" style="color:orange"></i>';
                            } else if (data[3] == 3) {
                                star = '<i class="fas fa-star" style="color:orange"></i><i class="fas fa-star" style="color:orange"></i><i class="fas fa-star" style="color:orange"></i><i class="far fa-star" style="color:orange"></i><i class="far fa-star" style="color:orange"></i>';
                            } else if (data[3] == 4) {
                                star = '<i class="fas fa-star" style="color:orange"></i><i class="fas fa-star" style="color:orange"></i><i class="fas fa-star" style="color:orange"></i><i class="fas fa-star" style="color:orange"></i><i class="far fa-star" style="color:orange"></i>';
                            } else if (data[3] == 5) {
                                star = '<i class="fas fa-star" style="color:orange"></i><i class="fas fa-star" style="color:orange"></i><i class="fas fa-star" style="color:orange"></i><i class="fas fa-star" style="color:orange"></i><i class="fas fa-star" style="color:orange"></i>';
                            }
                            return star;
                        }
                    }

                ],
                "order": [
                    [3, "asc"]
                ], // urutkan data berdasarkan "kode_bahanbaku" secara descending
                // membuat nomor urut tabel
                "rowCallback": function(row, data, iDisplayIndex) {
                    var info = this.fnPagingInfo();
                    var page = info.iPage;
                    var length = info.iLength;
                    var index = page * length + (iDisplayIndex + 1);
                    $('td:eq(0)', row).html(index);
                }
            });
            // ===============================================================================================
        });
    </script>
<?php } ?>