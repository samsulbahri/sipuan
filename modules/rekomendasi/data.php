<?php
// Mengecek AJAX Request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')) {

    // sql statement untuk join table
    $table = <<<EOT
(
    SELECT a.id_feedback, d.nama_supplier, d.alamat, c.value_rating FROM feedback as a, pembelian as b, rating as c, supplier as d WHERE a.no_pembelian = b.no_pembelian AND a.id_rating = c.id_rating AND b.supplier = d.kode_supplier AND c.value_rating > 2 GROUP BY d.nama_supplier
) temp
EOT;

    // primary key tabel
    $primaryKey = 'id_feedback';

    // membuat array untuk menampilkan isi tabel.
    // Parameter 'db' mewakili nama kolom dalam database.
    // parameter 'dt' mewakili pengenal kolom pada DataTable.
    $columns = array(
        array('db' => 'nama_supplier', 'dt' => 1),
        array('db' => 'alamat', 'dt' => 2),
        array('db' => 'value_rating', 'dt' => 3)
    );

    // memanggil file "database.php" untuk informasi koneksi ke server SQL
    require_once "../../config/database.php";
    // memanggil file "ssp.class.php" untuk menjalankan datatables server-side processing
    require '../../config/ssp.class.php';


    echo json_encode(
        SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns)
    );
} else {
    // jika tidak ada ajax request, maka alihkan ke halaman "login-error"
    echo '<script>window.location="../../login-error"</script>';
}
