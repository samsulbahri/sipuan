<?php
require_once "../../config/config.php";
try {
    // sql statement untuk menampilkan data dari tabel "satuan"
    if (isset($_GET['q'])) {
        $id = $_GET['q'];
        $query = "SELECT * FROM pemesanan as a, bahanbaku as b, supplier as c, pemesanan_detail as d WHERE a.id_pemesanan = d.id_pemesanan AND d.kode_bahanbaku = b.kode_bahanbaku AND a.kode_supplier = c.kode_supplier AND a.id_pemesanan = '$id'";
        // membuat prepared statements
        $stmt = $pdo->prepare($query);

        // eksekusi query
        $stmt->execute();

        // tampilkan hasil query
        while ($data_pemesanan = $stmt->fetch(PDO::FETCH_ASSOC)) {
            echo '<div class="modal fade" id="feedbackConfirm" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"> Konfirmasi Pemesanan ' . $data_pemesanan['nama_bahanbaku'] . ' </h5> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> &times; </span> </button>
                    </div>
                    <form method="POST" action="modules/manajemen-pemesanan/konfirmasi.php">
                        <div class="modal-body">
                            <div class="form group mb-4">
                                <label>Nota</label>
                                <input type="hidden" name="id_pemesanan" value="' . $data_pemesanan['id_pemesanan'] . '">
                                <input type="text" name="nota" class="form-control" required>
                            </div>
                            <div class="form group">
                                <label>Jumlah Uang Masuk</label>
                                <input type="text" name="uang" class="form-control" required>
                            </div>
                        </div>
                        <div class="modal-footer"> 
                            <button type="button" class="btn btn-secondary" data-dismiss="modal"> Tutup </button> 
                            <button type="submit" class="btn btn-primary">Kirim <i>invoice</i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>';
        }
        // tutup koneksi
        $pdo = null;
    } else if (isset($_GET['i'])) {
        $id = $_GET['i'];
        $query = "SELECT * FROM pemesanan as a, bahanbaku as b, supplier as c, pemesanan_detail as d WHERE a.id_pemesanan = d.id_pemesanan AND d.kode_bahanbaku = b.kode_bahanbaku AND a.kode_supplier = c.kode_supplier AND a.id_pemesanan = '$id'";
        // membuat prepared statements
        $stmt = $pdo->prepare($query);

        // eksekusi query
        $stmt->execute();

        // tampilkan hasil query
        while ($data_pemesanan = $stmt->fetch(PDO::FETCH_ASSOC)) {
            echo '<div class="modal fade" id="ModalImage" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"> Bukti Pembayaran ' . $data_pemesanan['nama_bahanbaku'] . ' </h5> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> &times; </span> </button>
                    </div>
                        <div class="modal-body">
                            <img src="assets/img/bukti/' . $data_pemesanan['bukti_pembayaran'] . '" class="img-fluid">
                        </div>
                        <div class="modal-footer"> <button type="button" class="btn btn-primary" data-dismiss="modal"> Tutup </button></div>
                </div>
            </div>
        </div>';
        }
        // tutup koneksi
        $pdo = null;
    }
} catch (PDOException $e) {
    // tampilkan pesan kesalahan
    echo $e->getMessage();
}
