<?php
// fungsi untuk pengecekan status login user
// jika user belum login, alihkan ke halaman "login-error"
if (empty($_SESSION['username']) && empty($_SESSION['password'])) {
    echo "<meta http-equiv='refresh' content='0; url=../../login-error'>";
}
// jika user sudah login
else { ?>
    <div class="row mb-3">
        <div class="col-md-12">
            <h5>
                <!-- judul halaman tampil data bahanbaku -->
                <i class="fas fa-boxes title-icon"></i> Manajemen Pemesanan
            </h5>
        </div>
    </div>

    <div class="border mb-4"></div>

    <?php
    $kode_supplier = $_SESSION['kode_supplier'];
    $qp = "SELECT * FROM pemesanan as a, supplier as b WHERE a.kode_supplier = b.kode_supplier AND status='diproses' AND a.kode_supplier = '$kode_supplier';";
    $sp = $pdo->prepare($qp);
    $sp->execute();
    if ($sp->rowCount() === 0) { ?>
        <div class="row">
            <div class="col text-center">
                <p class="text-muted">Tidak ada Pesanan</p>
            </div>
        </div>
    <?php }
    while ($pemesanan = $sp->fetch(PDO::FETCH_ASSOC)) { ?>
        <div class="row mb-3">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h6><?= $pemesanan['nama_supplier']; ?></h6>
                        <p class="text-muted"><?= $pemesanan['tanggal_pemesanan']; ?></p>
                    </div>
                    <div class="card-body">
                        <?php

                        $qd = "SELECT * FROM pemesanan_detail as a, bahanbaku as b, jenis_bahanbaku as c, satuan as d WHERE a.kode_bahanbaku = b.kode_bahanbaku AND b.id_jenis_bahanbaku = c.id_jenis_bahanbaku AND b.satuan = d.kode_satuan AND a.id_pemesanan = :id_pemesanan";

                        $sd = $pdo->prepare($qd);
                        $sd->bindParam(':id_pemesanan', $pemesanan['id_pemesanan']);
                        $sd->execute();
                        $totalPesanan = 0;

                        while ($detail = $sd->fetch(PDO::FETCH_ASSOC)) {
                            $totalPesanan += $detail['jumlah'] * $detail['harga_beli']; ?>
                            <div class="row">
                                <!-- <div class="col-1">
                                    <span onclick="deleteItem($detail['id_pemesanan_detail']; ?>)"><i class="fas fa-trash text-danger"></i></span>
                                </div> -->
                                <div class="col">
                                    <span><?= $detail['nama_bahanbaku']; ?></span>
                                    <br>
                                    <span class="text-muted"><?= $detail['jenis_bahanbaku']; ?></span>
                                </div>
                                <div class="col text-right">
                                    <p class="mb-1">
                                        <?= $detail['jumlah']; ?> x Rp. <?= number_format($detail['harga_beli'], 0, ',', '.'); ?> / <?= $detail['nama_satuan']; ?>
                                    </p>
                                    <br>
                                    <p class="text-muted">
                                        Rp. <?= number_format(($detail['jumlah'] * $detail['harga_beli']), 0, ',', '.'); ?>
                                    </p>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col">
                                Total Pembayaran: Rp. <?= number_format($totalPesanan, 0, ',', '.'); ?>
                            </div>
                            <div class="col">
                                <div class="float-right">
                                    <form action="" method="POST">
                                        <div class="form-inline">
                                            <a href="javascript:void(0);" class="btn btn-success mr-2" onclick="makeModalSeeImages(<?= $pemesanan['id_pemesanan']; ?>)">Lihat Bukti Pembayaran</a>
                                            <a href="javascript:void(0);" class="btn btn-primary" onClick="makeModalConfirm(<?= $pemesanan['id_pemesanan']; ?>)">Konfirmasi</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

    <!-- Modal form untuk entri dan ubah data bahanbaku -->
    <div class="modal fade" id="modalbahanbaku" tabindex="-1" role="dialog" aria-labelledby="modalbahanbaku" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <!-- judul form data bahanbaku -->
                <div class="modal-header">
                    <h5 class="modal-title"><i class="fas fa-edit title-icon"></i><span id="modalLabel"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-confirm">

    </div>

    <div id="modal-image">

    </div>


    <script type="text/javascript">
        function makeModalSeeImages(id) {
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("modal-image").innerHTML = this.responseText;
                }
            };

            xmlhttp.open("GET", "modules/manajemen-pemesanan/get_pemesanan.php?i=" + id, true);
            xmlhttp.send();

            $("#ModalImage").modal('show');
        }

        function makeModalConfirm(id) {
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("modal-confirm").innerHTML = this.responseText;
                }
            };

            xmlhttp.open("GET", "modules/manajemen-pemesanan/get_pemesanan.php?q=" + id, true);
            xmlhttp.send();

            $("#feedbackConfirm").modal('show');

        }

        $(document).ready(function() {
            // ======================================= initiate plugin =======================================
            // chosen-select plugin
            $('.chosen-select').chosen();
            // ===============================================================================================

            // ===============================================================================================

            // ============================================ Form =============================================
            // Tampilkan Modal Form Entri Data
            $('#btnTambah').click(function() {
                // reset form
                $('#formbahanbaku')[0].reset();
                $('#satuan').val('').trigger('chosen:updated');
                // judul form
                $('#modalLabel').text('Entri Data Pemesanan Bahan Baku');
            });
            // ===============================================================================================

            // ====================================== Insert dan Update ======================================
            // Proses Simpan Data
            $('#btnSimpan').click(function() {
                // validasi form input
                // jika nama bahanbaku kosong
                if ($('#bahan_baku').val() == "") {
                    // focus ke input nama bahanbaku
                    $("#bahan_baku").focus();
                    // tampilkan peringatan data tidak boleh kosong
                    $.notify({
                        title: '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                        message: 'Nama bahan baku tidak boleh kosong.'
                    }, {
                        type: 'warning'
                    });
                }
                // jika satuan kosong
                else if ($('#supplier').val() == "") {
                    // focus ke input supplier
                    $("#supplier").focus();
                    // tampilkan peringatan data tidak boleh kosong
                    $.notify({
                        title: '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                        message: 'Supplier tidak boleh kosong.'
                    }, {
                        type: 'warning'
                    });
                }
                // jika minimum stok kosong atau 0 (nol)
                else if ($('#banyak_pesanan').val() == "" || $('#banyak_pesanan').val() == 0) {
                    // focus ke input minimum stok
                    $("#banyak_pesanan").focus();
                    // tampilkan peringatan data tidak boleh kosong atau 0 (nol)
                    $.notify({
                        title: '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                        message: 'Minimum Stok tidak boleh kosong atau 0 (nol).'
                    }, {
                        type: 'warning'
                    });
                }
                // jika semua data sudah terisi, jalankan perintah insert / update data
                else {
                    // jika form entri data bahanbaku yang ditampilkan, jalankan perintah insert
                    if ($('#modalLabel').text() == "Entri Data Pemesanan Bahan Baku") {
                        // membuat variabel untuk menampung data dari form entri data bahanbaku
                        var data = $('#formbahanbaku').serialize();
                        $.ajax({
                            type: "POST", // mengirim data dengan method POST
                            url: "modules/pemesanan/insert.php", // proses insert data
                            data: data,
                            //data : data,                                    // data yang dikirim
                            success: function(result) { // ketika proses insert data selesai
                                // jika berhasil
                                if (result === "sukses") {
                                    // reset form
                                    $('#formbahanbaku')[0].reset();
                                    $('#satuan').val('').trigger('chosen:updated');
                                    // tutup modal entri data bahanbaku
                                    $('#modalbahanbaku').modal('hide');
                                    // tampilkan pesan sukses insert data
                                    $.notify({
                                        title: '<i class="fas fa-check-circle title-icon"></i><strong>Sukses!</strong><br>',
                                        message: 'Data Pemesanan Bahan Baku berhasil disimpan.'
                                    }, {
                                        type: 'success'
                                    });
                                    // tampilkan view data bahanbaku
                                    var table = $('#tabel-bahanbaku').DataTable();
                                    table.ajax.reload(null, false);
                                }
                                // jika gagal
                                else {
                                    // tampilkan pesan gagal insert data dan error result
                                    $.notify({
                                        title: '<i class="fas fa-times-circle title-icon"></i><strong>Gagal!</strong><br>',
                                        message: 'Data Pemesanan Bahan Baku tidak bisa disimpan. Query Error : ' + result
                                    }, {
                                        type: 'danger'
                                    });
                                }
                            }
                        });
                        return false;
                    }
                }
            });
            // ===============================================================================================

            // =========================================== Delete ============================================
            $('#tabel-bahanbaku tbody').on('click', '.btnHapus', function() {
                // ambil data dari datatables
                var data = table.row($(this).parents('tr')).data();
                // tampilkan notifikasi saat akan menghapus data
                swal({
                        title: "Apakah Anda Yakin?",
                        text: "Anda akan menghapus data Pemesanan Bahan Baku : " + data[2] + "-" + data[7],
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ya, Hapus!",
                        closeOnConfirm: false
                    },
                    // jika dipilih ya, maka jalankan perintah delete data
                    function() {
                        // membuat variabel untuk menampung data bahanbaku
                        var kode_bahanbaku = data[1];

                        $.ajax({
                            type: "POST", // mengirim data dengan method POST
                            url: "modules/pemesanan/delete.php", // proses delete data
                            data: {
                                kode_bahanbaku: kode_bahanbaku
                            }, // data yang dikirim
                            success: function(result) { // ketika proses delete data selesai
                                // jika berhasil
                                if (result === "sukses") {
                                    // tutup sweet alert
                                    swal.close();
                                    // tampilkan pesan sukses delete data
                                    $.notify({
                                        title: '<i class="fas fa-check-circle title-icon"></i><strong>Sukses!</strong><br>',
                                        message: 'Data Pemesanan Bahan Baku berhasil dihapus.'
                                    }, {
                                        type: 'success'
                                    });
                                    // tampilkan view data bahanbaku
                                    var table = $('#tabel-bahanbaku').DataTable();
                                    table.ajax.reload(null, false);
                                }
                                // jika gagal karena data "bahanbaku" sudah tercatat di tabel pembelian / penjualan
                                else if (result === "gagal") {
                                    // tutup sweet alert
                                    swal.close();
                                    // tampilkan pesan gagal delete data
                                    $.notify({
                                        title: '<i class="fas fa-times-circle title-icon"></i><strong>Gagal!</strong><br>',
                                        message: 'Data Pemesanan Bahan Baku gagal dihapus.'
                                    }, {
                                        type: 'danger'
                                    });
                                }
                                // jika gagal karena script error
                                else {
                                    // tutup sweet alert
                                    swal.close();
                                    // tampilkan pesan gagal delete data dan error result
                                    $.notify({
                                        title: '<i class="fas fa-times-circle title-icon"></i><strong>Gagal!</strong><br>',
                                        message: 'Data Pemesanan Bahan Baku tidak bisa dihapus. Query Error : ' + result
                                    }, {
                                        type: 'danger'
                                    });
                                }
                            }
                        });
                    });
            });
            // ===============================================================================================
        });
    </script>
<?php } ?>