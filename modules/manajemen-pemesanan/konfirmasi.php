<?php
session_start();      // memulai session

// panggil file "config.php" untuk koneksi ke database
require_once "../../config/config.php";

try {

    // sql statement untuk menampilkan 5 digit terakhir "no_pembelian" dari tabel "pembelian"
    $query = "SELECT RIGHT(no_pembelian,5) as kode FROM pembelian ORDER BY no_pembelian DESC LIMIT 1";
    // membuat prepared statements
    $stmt = $pdo->prepare($query);

    // eksekusi query
    $stmt->execute();

    // cek hasil query
    // jika no. pembelian sudah ada
    if ($stmt->rowCount() <> 0) {
        // ambil data hasil query
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        // no. pembelian + 1
        $kode = $data['kode'] + 1;
    }
    // jika no. pembelian belum ada
    else {
        // no. pembelian = 1
        $kode = 1;
    }

    // ambil bulan dan tahun sekarang
    $bulan        = date("m");
    $tahun        = date("Y");
    // membuat no. pembelian
    $buat_kode    = str_pad($kode, 5, "0", STR_PAD_LEFT);
    $no_pembelian = "BL" . $bulan . $tahun . $buat_kode;

    // ambil "data" hasil post dari ajax
    $id_pemesanan = trim($_POST['id_pemesanan']);
    $id_user = $_SESSION['id_user'];
    $id_supplier = $_SESSION['kode_supplier'];
    $nota = trim($_POST['nota']);
    $uang = trim($_POST['uang']);

    $q = "SELECT * FROM pemesanan_detail WHERE id_pemesanan = :id_pemesanan";
    $s = $pdo->prepare($q);
    $s->bindParam(':id_pemesanan', $id_pemesanan);
    $s->execute();

    while ($detail = $s->fetch(PDO::FETCH_ASSOC)) {
        $qb = "SELECT * FROM bahanbaku WHERE kode_bahanbaku = :kode_bahanbaku";
        $sb = $pdo->prepare($qb);
        $sb->bindParam(':kode_bahanbaku', $detail['kode_bahanbaku']);
        $sb->execute();

        while ($bahanbaku = $sb->fetch(PDO::FETCH_ASSOC)) {
            $total_harga = $bahanbaku['harga_beli'] * $detail['jumlah'];
            $qp = "INSERT INTO pembelian_detail(no_pembelian, bahanbaku, harga_beli, jumlah_beli, total_harga, created_user) VALUES (:no_pembelian, :bahanbaku, :harga_beli, :jumlah_beli, :total_harga, :created_user)";
            $sp = $pdo->prepare($qp);
            $sp->bindParam(':no_pembelian', $no_pembelian);
            $sp->bindParam(':bahanbaku', $detail['kode_bahanbaku']);
            $sp->bindParam(':harga_beli', $bahanbaku['harga_beli']);
            $sp->bindParam(':jumlah_beli', $detail['jumlah']);
            $sp->bindParam(':total_harga', $total_harga);
            $sp->bindParam(':created_user', $_SESSION['id_user']);
            $sp->execute();
        }
    }

    // sql statement untuk insert data ke tabel "bahanbaku"
    $query = "INSERT INTO pembelian(no_pembelian, tanggal, supplier, no_nota, total_bayar, created_user, is_feedback) VALUES (:no_pembelian, CURRENT_DATE(), :supplier, :no_nota, :total_bayar, :created_user, 'belum'); UPDATE pemesanan SET status = 'diverifikasi' WHERE id_pemesanan = :id_pemesanan";
    // membuat prepared statements
    $stmt = $pdo->prepare($query);

    // hubungkan "data" dengan prepared statements
    $stmt->bindParam(':no_pembelian', $no_pembelian);
    $stmt->bindParam(':supplier', $id_supplier);
    $stmt->bindParam(':no_nota', $nota);
    $stmt->bindParam(':total_bayar', $uang);
    $stmt->bindParam(':created_user', $id_user);
    $stmt->bindParam(':id_pemesanan', $id_pemesanan);

    // eksekusi query
    $stmt->execute();

    // cek query
    if ($stmt) {
        // jika berhasil tampilkan pesan "sukses"
        header('location: ../../manajemen-pemesanan');
    }

    // tutup koneksi
    $pdo = null;
} catch (PDOException $e) {
    // tampilkan pesan kesalahan
    echo $e->getMessage();
}
