<?php
session_start();      // memulai session

// Mengecek AJAX Request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' )) {
    // panggil file "config.php" untuk koneksi ke database
    require_once "../../config/config.php";

    // mengecek data post dari ajax
    if (isset($_POST['no_penjualan'])) {
        try {
            // ambil "data" post dari ajax
            $no_penjualan  = $_POST['no_penjualan'];
            $tanggal       = $_POST['tanggal'];
            $total_bayar   = $_POST['total_bayar'];

            // sql statement untuk delete data dari tabel "penjualan" dan "penjualan_detail"
            $query = "DELETE penjualan, penjualan_detail FROM penjualan INNER JOIN penjualan_detail ON penjualan.no_penjualan=penjualan_detail.no_penjualan
                      WHERE penjualan.no_penjualan=:no_penjualan";
            // membuat prepared statements
            $stmt = $pdo->prepare($query);

            // hubungkan "data" dengan prepared statements
            $stmt->bindParam(':no_penjualan', $no_penjualan);

            // eksekusi query
            $stmt->execute();

            // cek hasil query
            // jika data "penjualan" berhasil dihapus, jalankan perintah untuk insert data audit
            if ($stmt) {
                // siapkan "data"
                $username   = $_SESSION['id_user'];
                $aksi       = "Delete";
                $keterangan = "<b>Delete</b> data penjualan pada tabel <b>penjualan</b>. <br> <b>[No. Penjualan : </b>".$no_penjualan."<b>][Tanggal : </b>".$tanggal."<b>][Total Bayar : </b>".$total_bayar."<b>]";

                // sql statement untuk insert data ke tabel "sys_audit_trail"
                $query = "INSERT INTO sys_audit_trail(username, aksi, keterangan) VALUES (:username, :aksi, :keterangan)";
                // membuat prepared statements
                $stmt = $pdo->prepare($query);

                // hubungkan "data" dengan prepared statements
                $stmt->bindParam(':username', $username);
                $stmt->bindParam(':aksi', $aksi);
                $stmt->bindParam(':keterangan', $keterangan);

                // eksekusi query
                $stmt->execute();

                // cek query
                if ($stmt) {
                    // jika insert data audit berhasil tampilkan pesan "sukses"
                    echo "sukses";
                }
            }

            // tutup koneksi
            $pdo = null;
        } catch (PDOException $e) {
            // tampilkan pesan kesalahan
            echo $e->getMessage();
        }
    }
} else {
    // jika tidak ada ajax request, maka alihkan ke halaman "login-error"
    echo '<script>window.location="../../login-error"</script>';
}
?>
