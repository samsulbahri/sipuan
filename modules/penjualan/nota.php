<?php
session_start();      // memulai session

// fungsi untuk pengecekan status login user
// jika user belum login, alihkan ke halaman "login-error"
if (empty($_SESSION['username']) && empty($_SESSION['password'])){
    echo "<meta http-equiv='refresh' content='0; url=../../login-error'>";
}
// jika user sudah login
else { ?>
    <!doctype html>
    <html lang="en">
        <head>
            <!-- Required meta tags -->
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <!-- Title -->
            <title> Nota Púan Kopi </title>
        </head>
        <!-- Tampilkan "windows print" saat file "nota.php" dijalankan -->
        <body style="font-size:10px" onload="window.print()">
            <table>
                <?php
                // panggil file "config.php" untuk koneksi ke database
                require_once "../../config/config.php";

                try {
                    // siapkan "data" query
                    $id = 1;

                    // sql statement untuk menampilkan data dari tabel "sys_config" berdasarkan "id"
                    $query = "SELECT nama, alamat, telepon FROM sys_config WHERE id=:id";
                    // membuat prepared statements
                    $stmt = $pdo->prepare($query);

                    // hubungkan "data" dengan prepared statements
                    $stmt->bindParam(':id', $id);

                    // eksekusi query
                    $stmt->execute();

                    // ambil data hasil query
                    $data = $stmt->fetch(PDO::FETCH_ASSOC);
                } catch (PDOException $e) {
                    // tampilkan pesan kesalahan
                    echo "Query Error : ".$e->getMessage();
                }
                ?>
                <tr>
                    <td align="center" colspan="7"><strong><?php echo $data['nama']; ?></strong></td>
                </tr>
                <tr>
                    <td align="center" colspan="7"><?php echo $data['alamat']; ?></td>
                </tr>
                <tr>
                    <td align="center" colspan="7"><?php echo $data['telepon']; ?></td>
                </tr>

                <tr>
                    <td align="center" colspan="7">============================================</td>
                </tr>

                <?php
                try {
                    // ambil "data" get dari ajax
                    $no_penjualan = $_GET['no_penjualan'];

                    // sql statement untuk menampilkan data dari tabel "penjualan" berdasarkan "no_penjualan"
                    $query = "SELECT a.no_penjualan,a.tanggal,a.total_bayar,a.uang_bayar,a.created_user,b.nama_user
                              FROM penjualan as a INNER JOIN sys_users as b ON a.created_user=b.id_user WHERE a.no_penjualan=:no_penjualan";
                    // membuat prepared statements
                    $stmt = $pdo->prepare($query);

                    // hubungkan "data" dengan prepared statements
                    $stmt->bindParam(':no_penjualan', $no_penjualan);

                    // eksekusi query
                    $stmt->execute();

                    // ambil data hasil query
                    $data = $stmt->fetch(PDO::FETCH_ASSOC);
                    // tampilkan data
                    $total_bayar  = $data['total_bayar'];
                    $uang_bayar   = $data['uang_bayar'];
                    $uang_kembali = $data['uang_bayar'] - $data['total_bayar'];
                    $kasir        = $data['nama_user'];
                } catch (PDOException $e) {
                    // tampilkan pesan kesalahan
                    echo "Query Error : ".$e->getMessage();
                }
                ?>
                <tr>
                    <td width="45"> No. </td>
                    <td width="10"> : </td>
                    <td> <?php echo $data['no_penjualan']; ?> </td>
                </tr>
                <tr>
                    <td width="45"> Tanggal </td>
                    <td width="10"> : </td>
                    <td> <?php echo date('d-m-Y', strtotime($data['tanggal'])); ?> </td>
                </tr>
            </table>

            <table>
                <tr>
                    <td align="center" colspan="4">--------------------------------------------------------------------------</td>
                </tr>

                <tr>
                    <td align="left" colspan="4">Daftar Bahan Baku :</td>
                </tr>

                <tr>
                    <td align="center" colspan="4">--------------------------------------------------------------------------</td>
                </tr>

                <tr>
                    <td align="center">Bahan Baku</td>
                    <td align="center">Harga</td>
                    <td align="center">Jumlah</td>
                    <td align="center">Total</td>
                </tr>

                <tr>
                    <td align="center" colspan="4">--------------------------------------------------------------------------</td>
                </tr>

                <?php
                try {
                    // sql statement untuk menampilkan data dari tabel "penjualan_detail" berdasarkan "no_penjualan"
                    $query = "SELECT a.bahanbaku,a.harga_jual,a.jumlah_jual,a.total_harga,b.nama_bahanbaku
                              FROM penjualan_detail as a INNER JOIN bahanbaku as b ON a.bahanbaku=b.kode_bahanbaku WHERE a.no_penjualan=:no_penjualan";
                    // membuat prepared statements
                    $stmt = $pdo->prepare($query);

                    // hubungkan "data" dengan prepared statements
                    $stmt->bindParam(':no_penjualan', $no_penjualan);

                    // eksekusi query
                    $stmt->execute();

                    // tampilkan hasil query
                    while ($data = $stmt->fetch(PDO::FETCH_ASSOC)) { ?>
                        <tr>
                            <td width="100" align="left"><?php echo $data['bahanbaku']; ?> - <?php echo $data['nama_bahanbaku']; ?></td>
                            <td width="50" align="right">Rp. <?php echo number_format($data['harga_jual'], 0, ".", "."); ?></td>
                            <td width="20" width="100" align="right"><?php echo $data['jumlah_jual']; ?></td>
                            <td width="60" align="right">Rp. <?php echo number_format($data['total_harga'], 0, ".", "."); ?></td>
                        </tr>
                    <?php
                    };
                    // tutup koneksi
                    $pdo = null;
                } catch (PDOException $e) {
                    // tampilkan pesan kesalahan
                    echo "Query Error : ".$e->getMessage();
                }
                ?>

                <tr>
                    <td align="center" colspan="4">--------------------------------------------------------------------------</td>
                </tr>

                <tr>
                    <td align="right" colspan="2">Total</td>
                    <td align="right">:</td>
                    <td align="right">Rp. <?php echo number_format($total_bayar, 0, ".", "."); ?></td>
                </tr>

                <tr>
                    <td align="right" colspan="2">Bayar (Tunai)</td>
                    <td align="right">:</td>
                    <td align="right">Rp. <?php echo number_format($uang_bayar, 0, ".", "."); ?></td>
                </tr>

                <tr>
                    <td align="right" colspan="2">Kembali</td>
                    <td align="right">:</td>
                    <td align="right">Rp. <?php echo number_format($uang_kembali, 0, ".", "."); ?></td>
                </tr>

                <tr>
                    <td align="center" colspan="4">--------------------------------------------------------------------------</td>
                </tr>
            </table>

            <table>
                <tr>
                    <td width="45"> Kasir </td>
                    <td width="10"> : </td>
                    <td> <?php echo $kasir; ?> </td>
                </tr>

                <tr>
                    <td align="center" colspan="4">--------------------------------------------------------------------------</td>
                </tr>

                <tr>
                    <td align="center" colspan="3"></td>
                </tr>

                <tr>
                    <td align="center" colspan="3">Terima Kasih Atas Kunjungan Anda</td>
                </tr>
            </table>
        </body>
    </html>
<?php } ?>
