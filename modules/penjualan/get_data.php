<?php
// Mengecek AJAX Request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' )) {
	// panggil file "config.php" untuk koneksi ke database
	require_once "../../config/config.php";

	// mengecek data get dari ajax
    if (isset($_GET['no_penjualan'])) {
        try {
            // ambil "data" get dari ajax
            $no_penjualan = $_GET['no_penjualan'];

        	// sql statement untuk menampilkan data dari tabel "penjualan" berdasarkan "no_penjualan"
            $query = "SELECT no_penjualan, tanggal, total_bayar, uang_bayar, (SELECT SUM(e.jumlah_jual)
                       FROM penjualan_detail as e WHERE e.no_penjualan=:no_penjualan) as total_pemakaian
											 FROM penjualan WHERE no_penjualan=:no_penjualan";
            // membuat prepared statements
            $stmt = $pdo->prepare($query);

        	// hubungkan "data" dengan prepared statements
            $stmt->bindParam(':no_penjualan', $no_penjualan);

            // eksekusi query
            $stmt->execute();

            // ambil data hasil query
            $data = $stmt->fetch(PDO::FETCH_ASSOC);

						$total_pemakaian = $data['total_pemakaian'];
            // tampilkan data penjualan
            echo "<div class='form-group col-md-12'>
                    <table class='table'>
                        <tr>
                            <td style='border-top:none' width='150'>No. Pemakaian</td>
                            <td style='border-top:none' width='10'>:</td>
                            <td style='border-top:none'>".$data['no_penjualan']."</td>
                        </tr>
                        <tr>
                            <td>Tanggal</td>
                            <td>:</td>
                            <td>".date('d-m-Y', strtotime($data['tanggal']))."</td>
                        </tr>
                    </table>
                </div>
                <div class='form-group col-md-12'>
                    <div class='border mb-2'></div>
                </div>
                <div class='form-group col-md-12'>
                    <label class='mb-3'><strong>Daftar Pemakaian Bahan Baku</strong></label>
                    <table id='tabel-penjualan-detail' class='table table-striped table-bordered' style='width:100%'>
                        <thead>
                            <tr>
                                <th>Kode Bahan Baku</th>
                                <th>Nama Bahan Baku</th>
                                <th>Jumlah</th>
                            </tr>
                        </thead>
                        <tbody>";

            // sql statement untuk menampilkan data dari tabel "penjualan_detail" berdasarkan "no_penjualan"
            $query = "SELECT a.bahanbaku,a.harga_jual,a.jumlah_jual,a.total_harga,b.nama_bahanbaku
                      FROM penjualan_detail as a INNER JOIN bahanbaku as b ON a.bahanbaku=b.kode_bahanbaku WHERE a.no_penjualan=:no_penjualan";
            // membuat prepared statements
            $stmt = $pdo->prepare($query);

            // hubungkan "data" dengan prepared statements
            $stmt->bindParam(':no_penjualan', $no_penjualan);

            // eksekusi query
            $stmt->execute();

            // tampilkan hasil query
            while ($data = $stmt->fetch(PDO::FETCH_ASSOC)) {
                echo "      <tr>
                                <td width='80' class='center'>".$data['bahanbaku']."</td>
                                <td width='180'>".$data['nama_bahanbaku']."</td>
                                <td width='70' class='right'>".$data['jumlah_jual']."</td>
                            </tr>";
            };
            echo "          <tr>
                                <td class='right' colspan='2'><strong>Total</strong></td>
                                <td class='right'><strong>".number_format($total_pemakaian, 0, ".", ".")."</strong></td>
                            </tr>
                        </tbody>
                    </table>
                </div>";

            // tutup koneksi
            $pdo = null;
        } catch (PDOException $e) {
            // tampilkan pesan kesalahan
            echo "Query Error : ".$e->getMessage();
        }
	}
} else {
    // jika tidak ada ajax request, maka alihkan ke halaman "login-error"
    echo '<script>window.location="../../login-error"</script>';
}
?>
