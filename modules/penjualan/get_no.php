<?php
// Mengecek AJAX Request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' )) {
	// panggil file "config.php" untuk koneksi ke database
	require_once "../../config/config.php";

    try {
    	// sql statement untuk menampilkan 5 digit terakhir "no_penjualan" dari tabel "penjualan"
        $query = "SELECT RIGHT(no_penjualan,5) as kode FROM penjualan ORDER BY no_penjualan DESC LIMIT 1";
        // membuat prepared statements
        $stmt = $pdo->prepare($query);

        // eksekusi query
        $stmt->execute();

        // cek hasil query
        // jika no. penjualan sudah ada
        if ($stmt->rowCount() <> 0) {
            // ambil data hasil query
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            // no. penjualan + 1
            $kode = $data['kode']+1;
        }
        // jika no. penjualan belum ada
        else {
            // no. penjualan = 1
            $kode = 1;
        }

        // ambil bulan dan tahun sekarang
        $bulan        = date("m");
        $tahun        = date("Y");
        // membuat no. penjualan
        $buat_kode    = str_pad($kode, 5, "0", STR_PAD_LEFT);
        $no_penjualan = "JL".$bulan.$tahun.$buat_kode;

        // tampilkan data
        echo $no_penjualan;

        // tutup koneksi
        $pdo = null;
    } catch (PDOException $e) {
        // tampilkan pesan kesalahan
        echo "Query Error : ".$e->getMessage();
    }
} else {
    // jika tidak ada ajax request, maka alihkan ke halaman "login-error"
    echo '<script>window.location="../../login-error"</script>';
}
?>
