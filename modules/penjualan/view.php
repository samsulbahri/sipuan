<?php
// fungsi untuk pengecekan status login user
// jika user belum login, alihkan ke halaman "login-error"
if (empty($_SESSION['username']) && empty($_SESSION['password'])){
    echo "<meta http-equiv='refresh' content='0; url=../../login-error'>";
}
// jika user sudah login
else { ?>
    <div class="row mb-3">
        <div class="col-md-12">
            <h5>
                <!-- judul halaman data penjualan -->
                <span id="judulHalaman"><i class="fas fa-box-open title-icon"></i> Data Pemakaian</span>
                <!-- tombol tambah data penjualan -->
                <a class="btn btn-success float-right" id="btnTambah" href="javascript:void(0);" role="button">
                    <i class="fas fa-plus"></i> Tambah
                </a>
            </h5>
        </div>
    </div>

    <div class="border mb-4"></div>

    <!-- view data penjualan -->
    <div id="viewPenjualan">
        <div class="row">
            <div class="col-md-12">
                <!-- Tabel penjualan untuk menampilkan data penjualan dari database -->
                <table id="tabel-penjualan" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <!-- judul kolom pada bagian kepala (atas) tabel -->
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>No. Pemakaian</th>
                            <th>Tanggal</th>
                            <th>Total</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <!-- parameter untuk menampilkan form entri data penjualan -->
    <div id="formTambah"></div>

    <!-- Modal form untuk menampilkan detail data penjualan -->
    <div class="modal fade" id="modalPenjualan" tabindex="-1" role="dialog" aria-labelledby="modalPenjualan" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <!-- judul form detail data penjualan -->
                <div class="modal-header">
                    <h5 class="modal-title"><i class="fas fa-search-plus title-icon"></i>Detail Data Pemakaian</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <!-- parameter untuk memuat isi detail data penjualan -->
                    <div class="row" id="loadPenjualan"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
    $(document).ready(function(){
        // ============================================ View =============================================
        // dataTables plugin untuk membuat nomor urut tabel
        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        // datatables serverside processing
        var table = $('#tabel-penjualan').DataTable( {
            "processing": true,                         // tampilkan loading saat proses data
            "serverSide": true,                         // server-side processing
            "ajax": 'modules/penjualan/data.php',       // panggil file "data.php" untuk menampilkan data penjualan dari database
            // menampilkan data
            "columnDefs": [
                { "targets": 0, "data": null, "orderable": false, "searchable": false, "width": '30px', "className": 'center' },
                { "responsivePriority": 1, "targets": 1, "width": '150px', "className": 'center' },
                { "targets": 2, "width": '100px', "className": 'center' },
                { "targets": 3, "width": '150px', "className": 'right', render: $.fn.DataTable.render.number( '.', ',', 0, '' ) },
                {
                  "responsivePriority": 2, "targets": 4, "data": null, "orderable": false, "searchable": false, "width": '100px', "className": 'center',
                  // tombol cetak, detail, dan hapus
                  "render": function(data, type, row) {
                      var btn = "<a data-toggle=\"tooltip\" title=\"Detail\" class=\"btn btn-success btn-sm mr-2 getDetail\" href=\"javascript:void(0);\"><i class=\"fas fa-search-plus\"></i></a><a data-toggle=\"tooltip\" title=\"Hapus\" class=\"btn btn-danger btn-sm btnHapus\" href=\"javascript:void(0);\"><i class=\"fas fa-trash\"></i></a>";
                      return btn;
                  }
                }
            ],
            "order": [[ 1, "desc" ]],           // urutkan data berdasarkan "no_penjualan" secara descending
            "iDisplayLength": 10,               // tampilkan 10 data per halaman
            // membuat nomor urut tabel
            "rowCallback": function (row, data, iDisplayIndex) {
                var info   = this.fnPagingInfo();
                var page   = info.iPage;
                var length = info.iLength;
                var index  = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        } );
        // ===============================================================================================

        // ============================================ Form =============================================
        // Tampilkan Form Entri Data Penjualan
        $('#btnTambah').click(function(){
            // judul form
            $('#judulHalaman').html('<i class="fas fa-edit title-icon"></i> Entri Data Pemakaian');
            // sembunyikan tombol tambah
            $('#btnTambah').hide();
            // sembunyikan view data penjualan
            $('#viewPenjualan').hide();
            // tampilkan form entri data penjualan
            $('#formTambah').load('modules/penjualan/form.php');
        });

        // Tampilkan Detail Data Penjualan
        $('#tabel-penjualan tbody').on( 'click', '.getDetail', function (){
            // ambil data dari datatables
            var data = table.row( $(this).parents('tr') ).data();
            // membuat variabel untuk menampung data "no_penjualan"
            var no_penjualan = data[ 1 ];

            $.ajax({
                type     : "GET",                                       // mengirim data dengan method GET
                url      : "modules/penjualan/get_data.php",            // proses get data penjualan berdasarkan "no_penjualan"
                data     : {no_penjualan:no_penjualan},                 // data yang dikirim
                success: function(result){                              // ketika proses get data selesai
                    // tampilkan modal detail data penjualan
                    $('#modalPenjualan').modal('show');
                    // tampilkan detail data penjualan
                    $('#loadPenjualan').html(result);
                }
            });
        });
        // ===============================================================================================

        // =========================================== Delete ============================================
        $('#tabel-penjualan tbody').on( 'click', '.btnHapus', function (){
            // ambil data dari datatables
            var data = table.row( $(this).parents('tr') ).data();
            // tampilkan notifikasi saat akan menghapus data
            swal({
                title: "Apakah Anda Yakin?",
                text: "Anda akan menghapus data Pemakaian : "+ data[ 1 ],
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya, Hapus!",
                closeOnConfirm: false
            },
            // jika dipilih ya, maka jalankan perintah delete data
            function () {
                // membuat variabel untuk menampung data penjualan
                var no_penjualan  = data[ 1 ];
                var tanggal       = data[ 2 ];
                var total_bayar   = data[ 3 ];

                $.ajax({
                    type : "POST",                                                                  // mengirim data dengan method POST
                    url  : "modules/penjualan/delete.php",                                          // proses delete data
                    data : {no_penjualan:no_penjualan, tanggal:tanggal, total_bayar:total_bayar},   // data yang dikirim
                    success: function(result){                                                      // ketika proses delete data selesai
                        // jika berhasil
                        if (result==="sukses") {
                            // tutup sweet alert
                            swal.close();
                            // tampilkan pesan sukses delete data
                            $.notify({
                                title   : '<i class="fas fa-check-circle title-icon"></i><strong>Sukses!</strong><br>',
                                message : 'Data Penjualan berhasil dihapus.'
                            },{type: 'success'});
                            // tampilkan view data penjualan
                            var table = $('#tabel-penjualan').DataTable();
                            table.ajax.reload( null, false );
                        }
                        // jika gagal
                        else {
                            // tutup sweet alert
                            swal.close();
                            // tampilkan pesan gagal delete data dan error result
                            $.notify({
                                title   : '<i class="fas fa-times-circle title-icon"></i><strong>Gagal!</strong><br>',
                                message : 'Data Penjualan tidak bisa dihapus. Query Error : '+ result
                            },{type: 'danger'});
                        }
                    }
                });
            });
        });
        // ===============================================================================================
    });
    </script>
<?php } ?>
