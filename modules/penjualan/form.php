<!-- Form Entri Data penjualan -->
<form id="formPenjualan">
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <div class="form-group col-md-12">
                        <label>Bahan Baku</label>
                        <select class="chosen-select" id="bahanbaku" name="bahanbaku" autocomplete="off">
                            <option value="">-- Pilih --</option>
                            <?php
                            // panggil file "config.php" untuk koneksi ke database
                            require_once "../../config/config.php";

                            try {
                                // sql statement untuk menampilkan data dari tabel "bahanbaku"
                                $query = "SELECT kode_bahanbaku, nama_bahanbaku FROM bahanbaku ORDER BY kode_bahanbaku ASC";
                                // membuat prepared statements
                                $stmt = $pdo->prepare($query);

                                // eksekusi query
                                $stmt->execute();

                                // tampilkan hasil query
                                while ($data_bahanbaku = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                    echo"<option value='$data_bahanbaku[kode_bahanbaku]'> $data_bahanbaku[kode_bahanbaku] - $data_bahanbaku[nama_bahanbaku] </option>";
                                }

                                // tutup koneksi
                                $pdo = null;
                            } catch (PDOException $e) {
                                // tampilkan pesan kesalahan
                                echo $e->getMessage();
                            }
                            ?>
                        </select>
                        <input type="hidden" id="nama_bahanbaku" name="nama_bahanbaku">
                    </div>

                    <input type="hidden" id="harga_beli" name="harga_beli">

                    <div class="form-group col-md-12">
                        <label>Stok</label>
                        <input type="text" class="form-control" id="stok" name="stok" readonly>
                    </div>

                    <div class="form-group col-md-12">
                        <label>Satuan</label>
                        <input type="text" class="form-control" id="satuan" name="satuan" readonly>
                    </div>

                    <div class="form-group col-md-12">
                        <label>Jumlah</label>
                        <input type="text" class="form-control" id="jumlah_jual" name="jumlah_jual" onKeyPress="return goodchars(event,'0123456789',this)" autocomplete="off">
                    </div>
                </div>

                <div class="card-footer">
                    <div class="form-group col-md-12 mt-3">
                        <button type="button" class="btn btn-success btn-block" id="btnTambahbahanbaku">Tambah</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group col-md-12">
                                <label>No. Pemakaian</label>
                                <input type="text" class="form-control" id="no_penjualan" name="no_penjualan" autocomplete="off" readonly>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group col-md-12">
                                <label>Tanggal</label>
                                <input type="text" class="form-control date-picker" data-date-format="dd-mm-yyyy" id="tanggal" name="tanggal" value="<?php echo date("d-m-Y"); ?>" autocomplete="off">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group col-md-12">
                                <input type="hidden" class="form-control right" id="uang_bayar" name="uang_bayar" autocomplete="off" value="0">
                                <div class="border mt-2 mb-2"></div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group col-md-12">
                                <label class="mb-3"><strong>Daftar Pemakaian Bahan Baku</strong></label>
                                <!-- Tabel daftar penjualan bahanbaku untuk menampung sementara data penjualan bahanbaku sebelum disimpan ke database -->
                                <table id="tabel-penjualan-detail" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Kode Bahan Baku</th>
                                            <th>Nama Bahan Baku</th>
                                            <th>Jumlah</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                    <div class="form-group col-md-12 mt-2">
                        <div class="float-right">
                            <button type="button" class="btn btn-success mr-2" id="btnSimpan">Simpan</button>
                            <button type="button" class="btn btn-secondary" id="btnBatal">Batal</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
$(document).ready(function() {
    // ======================================= initiate plugin =======================================
    // datepicker plugin
    $('.date-picker').datepicker({
        autoclose: true,
        todayHighlight: true
    });

    // chosen-select plugin
    $('.chosen-select').chosen();
    // ===============================================================================================

    // =========================================== Onload ============================================
    // variabel untuk membuat "row id"
    var count = 0;
    // variabel untuk membuat "total_bayar"
    var total_bayar = 0;

    // reset form
    $('#formPenjualan')[0].reset();
    $('#bahanbaku').val('').trigger('chosen:updated');

    // membuat "no_penjualan"
    $.ajax({
        url  : "modules/penjualan/get_no.php",              // proses get "no_penjualan"
        success: function(result){                          // ketika proses get "no_penjualan" selesai
            // tampilkan data "no_penjualan"
            $('#no_penjualan').val(result);
        }
    });
    // ===============================================================================================

    // ============================================ Form =============================================
    // Menampilkan data bahanbaku dari select box ke textfield
    $('#bahanbaku').change(function(){
        // mengambil value dari "kode_bahanbaku"
        var kode_bahanbaku = $('#bahanbaku').val();

        $.ajax({
            type     : "GET",                                   // mengirim data dengan method GET
            url      : "modules/bahanbaku/get_data.php",             // proses get data bahanbaku berdasarkan "kode_bahanbaku"
            data     : {kode_bahanbaku:kode_bahanbaku},                   // data yang dikirim
            dataType : "JSON",                                  // tipe data JSON
            success: function(result){                          // ketika proses get data bahanbaku selesai
                // tampilkan data
                $("#nama_bahanbaku").val(result.nama_bahanbaku);
                $("#stok").val(result.stok);
                $("#satuan").val(result.nama_satuan);
                // fokus ke input jumlah jual
                $("#jumlah_jual").focus();

                // cek stok
                // jika stok mencapai batas minimum
                if (eval(result.stok) === 0) {
                    // tampilkan pesan peringatan stok mencapai batas minimum
                    $.notify({
                        title   : '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                        message : 'Stok bahanbaku <strong>'+ result.nama_bahanbaku +'</strong> telah habis. Segera lakukan pembelian bahanbaku.'
                    },{type: 'warning'});
                }
                // jika stok mencapai batas minimum
                if (eval(result.stok) > 0 && eval(result.stok) <= eval(result.min_stok)) {
                    // tampilkan pesan peringatan stok mencapai batas minimum
                    $.notify({
                        title   : '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                        message : 'Stok bahanbaku <strong>'+ result.nama_bahanbaku +'</strong> telah mencapai batas minimum. Segera lakukan pembelian bahanbaku.'
                    },{type: 'warning'});
                }
            }
        });
    });

    // Menambahkan daftar penjualan bahanbaku ke tabel sementara
    $('#btnTambahbahanbaku').click(function(){
        // validasi form input
        // jika bahanbaku kosong
        if ($('#bahanbaku').val()==""){
            // focus ke input bahanbaku
            $( "#bahanbaku" ).focus();
            // tampilkan peringatan data tidak boleh kosong
            $.notify({
                title   : '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                message : 'bahanbaku tidak boleh kosong.'
            },{type: 'warning'});
        }
        // jika jumlah jual kosong atau 0 (nol)
        else if ($('#jumlah_jual').val()=="" || $('#jumlah_jual').val()==0){
            // focus ke input jumlah jual
            $( "#jumlah_jual" ).focus();
            // tampilkan peringatan data tidak boleh kosong atau 0 (nol)
            $.notify({
                title   : '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                message : 'Jumlah tidak boleh kosong atau 0 (nol).'
            },{type: 'warning'});
        }
        // jika jumlah jual lebih besar dari stok
        else if (eval($('#jumlah_jual').val()) > eval($('#stok').val())){
            // focus ke input jumlah jual
            $( "#jumlah_jual" ).focus();
            // tampilkan peringatan data stok tidak memenuhi
            $.notify({
                title   : '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                message : 'Stok tidak memenuhi, kurangi jumlah penjualan bahanbaku.'
            },{type: 'warning'});
        }
        // jika semua data sudah terisi, jalankan perintah untuk menambahkan data
        else {
            // mengambil value data bahanbaku
            var kode_bahanbaku   = $('#bahanbaku').val();
            var nama_bahanbaku   = $('#nama_bahanbaku').val();
            var jumlah_jual = $('#jumlah_jual').val();

            // buat "row id"
            count = count + 1;
            // siapkan data untuk ditambahkan ke daftar penjualan bahanbaku
            output = '<tr id="row_'+count+'">';
            output += '<td width="90" class="center">'+kode_bahanbaku+' <input type="hidden" name="kode_bahanbaku_detail[]" id="kode_bahanbaku_detail'+count+'" value="'+kode_bahanbaku+'" /></td>';
            output += '<td width="170">'+nama_bahanbaku+'</td>';
            output += '<td width="30" class="right">'+jumlah_jual+' <input type="hidden" name="jumlah_jual_detail[]" id="jumlah_jual_detail'+count+'" value="'+jumlah_jual+'" /></td>';
            output += '<td width="20" class="center"><button type="button" name="btnHapusbahanbaku" class="btn btn-danger btn-sm btnHapusbahanbaku" id="'+count+'"><i class="fas fa-trash"></i></button></td>';
            output += '</tr>';

            // tambahkan data ke daftar penjualan bahanbaku
            $('#tabel-penjualan-detail').prepend(output);
            // siapkan data total bayar

            // reset form input bahanbaku
            $('#bahanbaku').val('').trigger('chosen:updated');
            $('#nama_bahanbaku').val("");
            $('#stok').val("");
            $('#satuan').val("");
            $('#jumlah_jual').val("");
        }
    });

    // Menghapus daftar penjualan bahanbaku dari tabel sementara
    $("#tabel-penjualan-detail").on('click', '.btnHapusbahanbaku', function(){
        // mengambil row berdasarkan atribut id
        var row_id = $(this).attr("id");
        // mengambil value total harga
        // hapus row
        $('#row_'+row_id).remove();
    });

    // ===============================================================================================

    // =========================================== Insert ============================================
    // Proses Simpan Data
    $('#btnSimpan').click(function(){
        // validasi form input
        // jika tanggal kosong
        if ($('#tanggal').val()==""){
            // focus ke input tanggal
            $( "#tanggal" ).focus();
            // tampilkan peringatan data tidak boleh kosong
            $.notify({
                title   : '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                message : 'Tanggal Penjualan tidak boleh kosong.'
            },{type: 'warning'});
        }
        // jika semua data sudah terisi, jalankan perintah insert data
        else {
            // membuat variabel untuk menampung data dari form entri data penjualan
            var data = $('#formPenjualan').serialize();

            $.ajax({
                type : "POST",                                  // mengirim data dengan method POST
                url  : "modules/penjualan/insert.php",          // proses insert data
                data : data,                                    // data yang dikirim
                success: function(result){                      // ketika proses insert data selesai
                    // jika berhasil
                    if (result==="sukses") {
                        // tutup form entri data penjualan
                        tutup_form();
                        // tampilkan pesan sukses insert data
                        $.notify({
                            title   : '<i class="fas fa-check-circle title-icon"></i><strong>Sukses!</strong><br>',
                            message : 'Data Penjualan berhasil disimpan.'
                        },{type: 'success'});
                        // tampilkan view data penjualan
                        var table = $('#tabel-penjualan').DataTable();
                        table.ajax.reload( null, false );
                    }
                    // jika gagal
                    else {
                        // tampilkan pesan gagal insert data dan error result
                        $.notify({
                            title   : '<i class="fas fa-times-circle title-icon"></i><strong>Gagal!</strong><br>',
                            message : 'Data Penjualan tidak bisa disimpan. Query Error : '+ result
                        },{type: 'danger'});
                    }
                }
            });
            return false;
        }
    });
    // ===============================================================================================

    // ========================================= Reset Form ==========================================
    // Batal Entri Data Penjualan
    $('#btnBatal').click(function(){
        // tutup form entri data penjualan
        tutup_form();
    });

    // fungsi untuk menutup form entri data penjualan
    function tutup_form() {
        // judul form
        $('#judulHalaman').html('<i class="fas fa-box-open title-icon"></i> Data Pemakaian');
        // tampilkan tombol tambah
        $('#btnTambah').show();
        // tampilkan view data penjualan
        $('#viewPenjualan').show();
        // sembunyikan form entri data penjualan
        $('#formTambah').empty();
    }
    // ===============================================================================================
} );
</script>
