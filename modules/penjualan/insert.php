<?php
session_start();      // memulai session

// Mengecek AJAX Request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' )) {
	// panggil file "config.php" untuk koneksi ke database
	require_once "../../config/config.php";

	try {
		// ambil "data" hasil post dari ajax
		$no_penjualan = trim($_POST['no_penjualan']);
		$tanggal      = trim(date('Y-m-d', strtotime($_POST['tanggal'])));
		$total_bayar  = 0;
		// preg_replace untuk menghilangkan Rp. dan titik
		$uang_bayar   = 0;
		// ambil "data" dari session
		$created_user = $_SESSION['id_user'];

		// sql statement untuk insert data ke tabel "penjualan"
		$query = "INSERT INTO penjualan(no_penjualan, tanggal, total_bayar, uang_bayar, created_user)
				  VALUES (:no_penjualan, :tanggal, :total_bayar, :uang_bayar, :created_user)";
		// membuat prepared statements
		$stmt = $pdo->prepare($query);

		// hubungkan "data" dengan prepared statements
		$stmt->bindParam(':no_penjualan', $no_penjualan);
		$stmt->bindParam(':tanggal', $tanggal);
		$stmt->bindParam(':total_bayar', $total_bayar);
		$stmt->bindParam(':uang_bayar', $uang_bayar);
		$stmt->bindParam(':created_user', $created_user);

		// eksekusi query
		$stmt->execute();

		// cek query
		// jika insert data penjualan berhasil, lanjutkan insert data detail penjualan
		if ($stmt) {
			// sql statement untuk insert data ke tabel "penjualan_detail"
			$query = "INSERT INTO penjualan_detail(no_penjualan, bahanbaku, harga_beli, harga_jual, jumlah_jual, total_harga, created_user)
					  VALUES (:no_penjualan, :bahanbaku, :harga_beli, :harga_jual, :jumlah_jual, :total_harga, :created_user)";
			// membuat prepared statements
			$stmt = $pdo->prepare($query);

			// lakukan perulangan
			for($count = 0; $count < count($_POST['kode_bahanbaku_detail']); $count++)
			{
				// siapkan data
				$data = array(
					':no_penjualan'	=> $no_penjualan,
					':bahanbaku'			=> $_POST['kode_bahanbaku_detail'][$count],
					':harga_beli'	=> 0,
					':harga_jual'	=> 0,
					':jumlah_jual'	=> $_POST['jumlah_jual_detail'][$count],
					':total_harga'	=> 0,
					':created_user'	=> $created_user
				);

				// eksekusi query
				$stmt->execute($data);
			}

			// cek query
			if ($stmt) {
			    // jika berhasil tampilkan pesan "sukses"
			    echo "sukses";
			}
		}

		// tutup koneksi
        $pdo = null;
    } catch (PDOException $e) {
        // tampilkan pesan kesalahan
        echo $e->getMessage();
    }
} else {
    // jika tidak ada ajax request, maka alihkan ke halaman "login-error"
    echo '<script>window.location="../../login-error"</script>';
}
?>
