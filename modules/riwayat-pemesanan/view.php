<?php
// fungsi untuk pengecekan status login user
// jika user belum login, alihkan ke halaman "login-error"
if (empty($_SESSION['username']) && empty($_SESSION['password'])) {
    echo "<meta http-equiv='refresh' content='0; url=../../login-error'>";
}
// jika user sudah login
else { ?>
    <div class="row mb-3">
        <div class="col-md-12">
            <h5>
                <!-- judul halaman tampil data bahanbaku -->
                <i class="fas fa-boxes title-icon"></i> Riwayat Pemesanan Bahan Baku
            </h5>
        </div>
    </div>

    <div class="border mb-4"></div>

    <div class="row mt-5">
        <div class="col-md-12">
            <!-- Tabel bahanbaku untuk menampilkan data bahanbaku dari database -->
            <table id="tabel-bahanbaku" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <!-- judul kolom pada bagian kepala (atas) tabel -->
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>No. Pembelian</th>
                        <th>Tanggal</th>
                        <th>Kode Supplier</th> <!-- kolom "Kode Supplier" disembunyikan, digabung dengan kolom "Supplier" -->
                        <th>Supplier</th>
                        <th>No. Nota</th>
                        <th>Total</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    <div id="modal-confirm">

    </div>

    <div id="modal-feedback">

    </div>

    <!-- Modal form untuk menampilkan detail data pembelian -->
    <div class="modal fade" id="modalPembelian" tabindex="-1" role="dialog" aria-labelledby="modalPembelian" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <!-- judul form detail data pembelian -->
                <div class="modal-header">
                    <h5 class="modal-title"><i class="fas fa-search-plus title-icon"></i>Kwitansi Data Pembelian</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <!-- parameter untuk memuat isi detail data pembelian -->
                    <div class="row" id="loadPembelian"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        function makeModalConfirm(id) {
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("modal-confirm").innerHTML = this.responseText;
                }
            };

            xmlhttp.open("GET", "modules/riwayat-pemesanan/get_pemesanan.php?q=" + id, true);
            xmlhttp.send();

            $("#feedbackConfirm").modal('show');

        }

        function makeModalFeedback(id) {
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("modal-feedback").innerHTML = this.responseText;
                }
            };

            xmlhttp.open("GET", "modules/riwayat-pemesanan/get_pemesanan.php?f=" + id, true);
            xmlhttp.send();

            $("#giveFeedback").modal('show');

        }

        $(document).ready(function() {
            // ======================================= initiate plugin =======================================
            // chosen-select plugin
            $('.chosen-select').chosen();
            // ===============================================================================================


            // ===============================================================================================

            // ============================================ View =============================================
            // dataTables plugin untuk membuat nomor urut tabel
            $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
                return {
                    "iStart": oSettings._iDisplayStart,
                    "iEnd": oSettings.fnDisplayEnd(),
                    "iLength": oSettings._iDisplayLength,
                    "iTotal": oSettings.fnRecordsTotal(),
                    "iFilteredTotal": oSettings.fnRecordsDisplay(),
                    "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                    "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                };
            };

            // datatables serverside processing
            var table = $('#tabel-bahanbaku').DataTable({
                "processing": true, // tampilkan loading saat proses data
                "serverSide": true, // server-side processing
                "ajax": 'modules/riwayat-pemesanan/data.php', // panggil file "data.php" untuk menampilkan data bahanbaku dari database
                // menampilkan data
                "columnDefs": [{
                        "targets": 0,
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "width": '30px',
                        "className": 'center'
                    },
                    {
                        "responsivePriority": 1,
                        "targets": 1,
                        "width": '100px',
                        "className": 'center'
                    },
                    {
                        "targets": 2,
                        "width": '70px',
                        "className": 'center'
                    },
                    {
                        "targets": 3,
                        "width": '70px',
                        "visible": false
                    }, // kolom "kode_supplier" disembunyikan, digabung dengan kolom "nama_supplier"
                    {
                        "targets": 4,
                        "width": '200px',
                        "render": function(data, type, row) {
                            return row[3] + ' - ' + data;
                        }
                    },
                    {
                        "targets": 5,
                        "width": '100px',
                        "className": 'center'
                    },
                    {
                        "targets": 6,
                        "width": '100px',
                        "className": 'right',
                        render: $.fn.DataTable.render.number('.', ',', 0, 'Rp. ')
                    },
                    {
                        "responsivePriority": 2,
                        "targets": 7,
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "width": '60px',
                        "className": 'center',
                        // tombol detail dan hapus
                        "render": function(data, type, row) {
                            var btn = '';
                            if (data[8] === 'belum') {
                                var btn = "<a style=\"margin-right:7px\" data-toggle=\"tooltip\" title=\"Detail\" class=\"btn btn-success btn-sm\" href=\"javascript:void(0);\" onclick=\"makeModalFeedback('" + data[7] + "')\"><i class=\"fas fa-thumbs-up\"></i></a>";
                            } else {
                                var btn = "<a style=\"margin-right:7px\" data-toggle=\"tooltip\" title=\"Lihat Kwitansi\" class=\"btn btn-primary btn-sm getDetail\" href=\"javascript:void(0);\"><i class=\"fas fa-align-left\"></i></a>";
                            }
                            return btn;
                        }
                    }
                ],
                "order": [
                    [1, "desc"]
                ], // urutkan data berdasarkan "kode_bahanbaku" secara descending
                "iDisplayLength": 10, // tampilkan 10 data per halaman
                // membuat nomor urut tabel
                "rowCallback": function(row, data, iDisplayIndex) {
                    var info = this.fnPagingInfo();
                    var page = info.iPage;
                    var length = info.iLength;
                    var index = page * length + (iDisplayIndex + 1);
                    $('td:eq(0)', row).html(index);
                }
            });

            // Tampilkan Detail Data Pembelian
            $('#tabel-bahanbaku tbody').on('click', '.getDetail', function() {
                // ambil data dari datatables 
                var data = table.row($(this).parents('tr')).data();
                // membuat variabel untuk menampung data "no_pembelian"
                var no_pembelian = data[1];

                $.ajax({
                    type: "GET", // mengirim data dengan method GET 
                    url: "modules/pembelian/get_data.php", // proses get data pembelian berdasarkan "no_pembelian"
                    data: {
                        no_pembelian: no_pembelian
                    }, // data yang dikirim
                    success: function(result) { // ketika proses get data selesai
                        // tampilkan modal detail data pembelian
                        $('#modalPembelian').modal('show');
                        // tampilkan detail data pembelian
                        $('#loadPembelian').html(result);
                    }
                });
            });
            // ===============================================================================================

            // ============================================ Form =============================================
            // Tampilkan Modal Form Entri Data
            $('#btnTambah').click(function() {
                // reset form
                $('#formbahanbaku')[0].reset();
                $('#satuan').val('').trigger('chosen:updated');
                // judul form
                $('#modalLabel').text('Entri Data Pemesanan Bahan Baku');
            });
            // ===============================================================================================

            // ====================================== Insert dan Update ======================================
            // Proses Simpan Data
            $('#btnSimpan').click(function() {
                // validasi form input
                // jika nama bahanbaku kosong
                if ($('#bahan_baku').val() == "") {
                    // focus ke input nama bahanbaku
                    $("#bahan_baku").focus();
                    // tampilkan peringatan data tidak boleh kosong
                    $.notify({
                        title: '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                        message: 'Nama bahan baku tidak boleh kosong.'
                    }, {
                        type: 'warning'
                    });
                }
                // jika satuan kosong
                else if ($('#supplier').val() == "") {
                    // focus ke input supplier
                    $("#supplier").focus();
                    // tampilkan peringatan data tidak boleh kosong
                    $.notify({
                        title: '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                        message: 'Supplier tidak boleh kosong.'
                    }, {
                        type: 'warning'
                    });
                }
                // jika minimum stok kosong atau 0 (nol)
                else if ($('#banyak_pesanan').val() == "" || $('#banyak_pesanan').val() == 0) {
                    // focus ke input minimum stok
                    $("#banyak_pesanan").focus();
                    // tampilkan peringatan data tidak boleh kosong atau 0 (nol)
                    $.notify({
                        title: '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                        message: 'Minimum Stok tidak boleh kosong atau 0 (nol).'
                    }, {
                        type: 'warning'
                    });
                }
                // jika semua data sudah terisi, jalankan perintah insert / update data
                else {
                    // jika form entri data bahanbaku yang ditampilkan, jalankan perintah insert
                    if ($('#modalLabel').text() == "Entri Data Pemesanan Bahan Baku") {
                        // membuat variabel untuk menampung data dari form entri data bahanbaku
                        var data = $('#formbahanbaku').serialize();
                        $.ajax({
                            type: "POST", // mengirim data dengan method POST
                            url: "modules/riwayat-pemesanan/insert.php", // proses insert data
                            data: data,
                            //data : data,                                    // data yang dikirim
                            success: function(result) { // ketika proses insert data selesai
                                // jika berhasil
                                if (result === "sukses") {
                                    // reset form
                                    $('#formbahanbaku')[0].reset();
                                    $('#satuan').val('').trigger('chosen:updated');
                                    // tutup modal entri data bahanbaku
                                    $('#modalbahanbaku').modal('hide');
                                    // tampilkan pesan sukses insert data
                                    $.notify({
                                        title: '<i class="fas fa-check-circle title-icon"></i><strong>Sukses!</strong><br>',
                                        message: 'Data Pemesanan Bahan Baku berhasil disimpan.'
                                    }, {
                                        type: 'success'
                                    });
                                    // tampilkan view data bahanbaku
                                    var table = $('#tabel-bahanbaku').DataTable();
                                    table.ajax.reload(null, false);
                                }
                                // jika gagal
                                else {
                                    // tampilkan pesan gagal insert data dan error result
                                    $.notify({
                                        title: '<i class="fas fa-times-circle title-icon"></i><strong>Gagal!</strong><br>',
                                        message: 'Data Pemesanan Bahan Baku tidak bisa disimpan. Query Error : ' + result
                                    }, {
                                        type: 'danger'
                                    });
                                }
                            }
                        });
                        return false;
                    }
                }
            });
            // ===============================================================================================

            // =========================================== Delete ============================================
            $('#tabel-bahanbaku tbody').on('click', '.btnHapus', function() {
                // ambil data dari datatables
                var data = table.row($(this).parents('tr')).data();
                // tampilkan notifikasi saat akan menghapus data
                swal({
                        title: "Apakah Anda Yakin?",
                        text: "Anda akan menghapus data Pemesanan Bahan Baku : " + data[2] + "-" + data[7],
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ya, Hapus!",
                        closeOnConfirm: false
                    },
                    // jika dipilih ya, maka jalankan perintah delete data
                    function() {
                        // membuat variabel untuk menampung data bahanbaku
                        var kode_bahanbaku = data[1];

                        $.ajax({
                            type: "POST", // mengirim data dengan method POST
                            url: "modules/riwayat-pemesanan/delete.php", // proses delete data
                            data: {
                                kode_bahanbaku: kode_bahanbaku
                            }, // data yang dikirim
                            success: function(result) { // ketika proses delete data selesai
                                // jika berhasil
                                if (result === "sukses") {
                                    // tutup sweet alert
                                    swal.close();
                                    // tampilkan pesan sukses delete data
                                    $.notify({
                                        title: '<i class="fas fa-check-circle title-icon"></i><strong>Sukses!</strong><br>',
                                        message: 'Data Pemesanan Bahan Baku berhasil dihapus.'
                                    }, {
                                        type: 'success'
                                    });
                                    // tampilkan view data bahanbaku
                                    var table = $('#tabel-bahanbaku').DataTable();
                                    table.ajax.reload(null, false);
                                }
                                // jika gagal karena data "bahanbaku" sudah tercatat di tabel pembelian / penjualan
                                else if (result === "gagal") {
                                    // tutup sweet alert
                                    swal.close();
                                    // tampilkan pesan gagal delete data
                                    $.notify({
                                        title: '<i class="fas fa-times-circle title-icon"></i><strong>Gagal!</strong><br>',
                                        message: 'Data Pemesanan Bahan Baku gagal dihapus.'
                                    }, {
                                        type: 'danger'
                                    });
                                }
                                // jika gagal karena script error
                                else {
                                    // tutup sweet alert
                                    swal.close();
                                    // tampilkan pesan gagal delete data dan error result
                                    $.notify({
                                        title: '<i class="fas fa-times-circle title-icon"></i><strong>Gagal!</strong><br>',
                                        message: 'Data Pemesanan Bahan Baku tidak bisa dihapus. Query Error : ' + result
                                    }, {
                                        type: 'danger'
                                    });
                                }
                            }
                        });
                    });
            });
            // ===============================================================================================
        });

        var rating = 1;

        function setStar(star, that) {
            if (star === 1) {
                poin = '<input id="rating" name="rating" type="hidden" value="1" required><i id="star-one" class="fas fa-star mr-2 orange" onclick="setStar(1, this.parentElement);"></i> <i id = "star-two" class = "fas fa-star mr-2" onclick = "setStar(2, this.parentElement);"></i> <i id = "star-three" class = "fas fa-star mr-2" onclick = "setStar(3, this.parentElement);"></i> <i id = "star-four" class = "fas fa-star mr-2" onclick = "setStar(4, this.parentElement);"></i> <i id = "star-five" class = "fas fa-star mr-2"onclick = "setStar(5, this.parentElement);"></i>';
            } else if (star === 2) {
                poin = '<input id="rating" name="rating" type="hidden" value="2" required><i id="star-one" class="fas fa-star mr-2 orange" onclick="setStar(1, this.parentElement);"></i> <i id = "star-two" class = "fas fa-star mr-2 orange" onclick = "setStar(2, this.parentElement);"></i> <i id = "star-three" class = "fas fa-star mr-2" onclick = "setStar(3, this.parentElement);"></i> <i id = "star-four" class = "fas fa-star mr-2" onclick = "setStar(4, this.parentElement);"></i> <i id = "star-five" class = "fas fa-star mr-2"onclick = "setStar(5, this.parentElement);"></i>';
            } else if (star === 3) {
                poin = '<input id="rating" name="rating" type="hidden" value="3" required><i id="star-one" class="fas fa-star mr-2 orange" onclick="setStar(1, this.parentElement);"></i> <i id = "star-two" class = "fas fa-star mr-2 orange" onclick = "setStar(2, this.parentElement);"></i> <i id = "star-three" class = "fas fa-star mr-2 orange" onclick = "setStar(3, this.parentElement);"></i> <i id = "star-four" class = "fas fa-star mr-2" onclick = "setStar(4, this.parentElement);"></i> <i id = "star-five" class = "fas fa-star mr-2"onclick = "setStar(5, this.parentElement);"></i>';
            } else if (star === 4) {
                poin = '<input id="rating" name="rating" type="hidden" value="4" required><i id="star-one" class="fas fa-star mr-2 orange" onclick="setStar(1, this.parentElement);"></i> <i id = "star-two" class = "fas fa-star mr-2 orange" onclick = "setStar(2, this.parentElement);"></i> <i id = "star-three" class = "fas fa-star mr-2 orange" onclick = "setStar(3, this.parentElement);"></i> <i id = "star-four" class = "fas fa-star mr-2 orange" onclick = "setStar(4, this.parentElement);"></i> <i id = "star-five" class = "fas fa-star mr-2"onclick = "setStar(5, this.parentElement);"></i>';
            } else if (star === 5) {
                poin = '<input id="rating" name="rating" type="hidden" value="5" required><i id="star-one" class="fas fa-star mr-2 orange" onclick="setStar(1, this.parentElement);"></i> <i id = "star-two" class = "fas fa-star mr-2 orange" onclick = "setStar(2, this.parentElement);"></i> <i id = "star-three" class = "fas fa-star mr-2 orange" onclick = "setStar(3, this.parentElement);"></i> <i id = "star-four" class = "fas fa-star mr-2 orange" onclick = "setStar(4, this.parentElement);"></i> <i id = "star-five" class = "fas fa-star mr-2 orange"onclick = "setStar(5, this.parentElement);"></i>';
            }
            that.innerHTML = poin;
        }
    </script>
<?php } ?>