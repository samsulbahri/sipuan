<?php
session_start();      // memulai session

require_once "../../config/config.php";

// mengecek data post dari ajax
if (isset($_POST['id_pemesanan'])) {
    try {
        $stmtUpdate = FALSE;

        $id_pemesanan = $_POST['id_pemesanan'];
        // sql statement untuk delete data dari tabel "bahanbaku"
        $query = "SELECT * FROM pemesanan WHERE id_pemesanan=:id_pemesanan";
        // membuat prepared statements
        $stmt = $pdo->prepare($query);

        // hubungkan "data" dengan prepared statements
        $stmt->bindParam(':id_pemesanan', $id_pemesanan);

        // eksekusi query
        $stmt->execute();

        while ($res = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $kode_bahanbaku = $res['kode_bahanbaku'];
            $queryGetBahan = "SELECT stok FROM bahanbaku WHERE kode_bahanbaku=:kode_bahanbaku";
            $stmtGetBahan = $pdo->prepare($queryGetBahan);
            $stmtGetBahan->bindParam(':kode_bahanbaku', $kode_bahanbaku);
            $stmtGetBahan->execute();

            while ($resGetBahan = $stmtGetBahan->fetch(PDO::FETCH_ASSOC)) {
                $stok = $res['quantity'] + $resGetBahan['stok'];

                $eksAllow = array('jpg', 'jpeg', 'png');
                $nama = $_FILES['bukti']['name'];
                $x = explode('.', $nama);
                $eks = strtolower(end($x));
                $ukuran = $_FILES['bukti']['size'];
                $file_tmp = $_FILES['bukti']['tmp_name'];

                if (in_array($eks, $eksAllow) === TRUE) {
                    move_uploaded_file($file_tmp, '../../assets/img/bukti/' . $nama);
                }


                $queryUpdate = "UPDATE bahanbaku SET stok=:stok WHERE kode_bahanbaku=:kode_bahanbaku; UPDATE pemesanan SET bukti_pembayaran=:bukti_pembayaran, status='diproses' WHERE id_pemesanan=:id_pemesanan;";

                $stmtUpdate = $pdo->prepare($queryUpdate);

                $stmtUpdate->bindParam(':kode_bahanbaku', $kode_bahanbaku);
                $stmtUpdate->bindParam(':stok', $stok);
                $stmtUpdate->bindParam(':bukti_pembayaran', $nama);
                $stmtUpdate->bindParam(':id_pemesanan', $id_pemesanan);

                $stmtUpdate->execute();

                $stmtUpdate = TRUE;
            }
        }

        // cek hasil query
        // jika data "bahanbaku" berhasil dihapus, jalankan perintah untuk insert data audit
        if ($stmtUpdate) {
            // siapkan "data"
            $username   = $_SESSION['id_user'];
            $aksi       = "Confirm";
            $keterangan = "<b>Confirm</b> data pemesanan pada tabel <b>pemesanan</b> dan tabel <b>bahanbaku</b>. <br> <b>[Kode bahanbaku : </b>" . $kode_bahanbaku . "]";

            // sql statement untuk insert data ke tabel "sys_audit_trail"
            $query = "INSERT INTO sys_audit_trail(username, aksi, keterangan) VALUES (:username, :aksi, :keterangan)";
            // membuat prepared statements
            $stmt = $pdo->prepare($query);

            // hubungkan "data" dengan prepared statements
            $stmt->bindParam(':username', $username);
            $stmt->bindParam(':aksi', $aksi);
            $stmt->bindParam(':keterangan', $keterangan);

            // eksekusi query
            $stmt->execute();

            // cek query
            if ($stmtUpdate) {
                // jika insert data audit berhasil tampilkan pesan "sukses"
                header('Location: ../../main.php?module=pemesanan');
            }
        }

        // tutup koneksi
        $pdo = null;
    } catch (PDOException $e) {
        // tampilkan pesan kesalahan
        echo $e->getMessage();
    }
}
