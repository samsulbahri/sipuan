<?php
// Mengecek AJAX Request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')) {

    // sql statement untuk join table
    $table = <<<EOT
(
    SELECT a.is_feedback, a.no_pembelian,a.tanggal,a.supplier,a.no_nota,a.total_bayar,b.nama_supplier FROM pembelian as a, supplier as b WHERE a.supplier=b.kode_supplier
) temp
EOT;

    // primary key tabel
    $primaryKey = 'no_pembelian';

    // membuat array untuk menampilkan isi tabel.
    // Parameter 'db' mewakili nama kolom dalam database.
    // parameter 'dt' mewakili pengenal kolom pada DataTable.
    $columns = array(
        array('db' => 'no_pembelian', 'dt' => 1),
        array(
            'db' => 'tanggal',
            'dt' => 2,
            'formatter' => function ($d, $row) {
                return date('d-m-Y', strtotime($d));
            }
        ),
        array('db' => 'supplier', 'dt' => 3),
        array('db' => 'nama_supplier', 'dt' => 4),
        array('db' => 'no_nota', 'dt' => 5),
        array('db' => 'total_bayar', 'dt' => 6),
        array('db' => 'no_pembelian', 'dt' => 7),
        array('db' => 'is_feedback', 'dt' => 8),
    );

    // memanggil file "database.php" untuk informasi koneksi ke server SQL
    require_once "../../config/database.php";
    // memanggil file "ssp.class.php" untuk menjalankan datatables server-side processing
    require '../../config/ssp.class.php';

    echo json_encode(
        SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns)
    );
} else {
    // jika tidak ada ajax request, maka alihkan ke halaman "login-error"
    echo '<script>window.location="../../login-error"</script>';
}
