<?php
session_start();      // memulai session

// Mengecek AJAX Request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')) {
    // panggil file "config.php" untuk koneksi ke database
    require_once "../../config/config.php";

    try {
        // ambil "data" hasil post dari ajax
        $bahan_baku    = trim($_POST['bahan_baku']);
        $supplier    = trim($_POST['supplier']);
        $banyak_pesanan     = trim($_POST['banyak_pesanan']);
        // ambil "data" dari session
        $created_user = $_SESSION['id_user'];

        // sql statement untuk insert data ke tabel "bahanbaku"
        $query = "INSERT INTO pemesanan(kode_bahanbaku, kode_supplier, quantity, status)
				  VALUES (:kode_bahanbaku, :kode_supplier, :banyak_pesanan, 'dipesan')";
        // membuat prepared statements
        $stmt = $pdo->prepare($query);

        // hubungkan "data" dengan prepared statements
        $stmt->bindParam(':kode_bahanbaku', $bahan_baku);
        $stmt->bindParam(':kode_supplier', $supplier);
        $stmt->bindParam(':banyak_pesanan', $banyak_pesanan);

        // eksekusi query
        $stmt->execute();

        // cek query
        if ($stmt) {
            // jika berhasil tampilkan pesan "sukses"
            echo "sukses";
        }

        // tutup koneksi
        $pdo = null;
    } catch (PDOException $e) {
        // tampilkan pesan kesalahan
        echo $e->getMessage();
    }
} else {
    // jika tidak ada ajax request, maka alihkan ke halaman "login-error"
    echo '<script>window.location="../../login-error"</script>';
}
