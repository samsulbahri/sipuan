<?php
require_once "../../config/config.php";
try {
    // sql statement untuk menampilkan data dari tabel "satuan"
    if (isset($_GET['q'])) {
        $id = $_GET['q'];
        $query = "SELECT * FROM pemesanan as a, bahanbaku as b, supplier as c WHERE a.kode_bahanbaku = b.kode_bahanbaku AND a.kode_supplier = c.kode_supplier AND id_pemesanan = '$id'";
        // membuat prepared statements
        $stmt = $pdo->prepare($query);

        // eksekusi query
        $stmt->execute();

        // tampilkan hasil query
        while ($data_pemesanan = $stmt->fetch(PDO::FETCH_ASSOC)) {
            echo '<div class="modal fade" id="feedbackConfirm" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"> Bukti Pembayaran ' . $data_pemesanan['nama_bahanbaku'] . ' </h5> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> &times; </span> </button>
                    </div>
                    <form action="modules/pemesanan/confirm.php" method="POST" enctype="multipart/form-data">
                        <div class="modal-body">
                            <div class="jumbotron jumbotron-fluid bg-primary text-white">
                                <div class="container">
                                    <h3 class="display-4 text-center font-weight-normal">' . $data_pemesanan['nama_bank'] . '</h3>
                                    <p class="lead text-center font-weight-light">A.N. ' . $data_pemesanan['nama_pemilik_bank'] . '</p>
                                    <p class="lead text-center font-weight-light">' . $data_pemesanan['nomor_rekening'] . '</p>
                                </div>
                            </div>
                            <div class="form-group"><label>Bukti Pembayaran </label> <input type="hidden" name="id_pemesanan" value="' . $data_pemesanan['id_pemesanan'] . '"><input type="file" name="bukti" id="bukti" class="form-control" required><small class="form-text text-muted">Bukti Pembayaran berupa gambar dengan ekstensi .jpg, .jpeg atau .png</small></div>
                        </div>
                        <div class="modal-footer"> <button type="button" class="btn btn-secondary" data-dismiss="modal"> Tutup </button><input type="submit" value="Konfirmasi" class="btn btn-primary"></div>
                    </form>
                </div>
            </div>
        </div>';
        }
        // tutup koneksi
        $pdo = null;
    } else if (isset($_GET['f'])) {
        $id = $_GET['f'];
        $query = "SELECT * FROM pembelian as a, supplier as b WHERE a.supplier=b.kode_supplier AND no_pembelian = '$id'";
        // membuat prepared statements
        $stmt = $pdo->prepare($query);

        // eksekusi query
        $stmt->execute();

        // tampilkan hasil query
        while ($data_pemesanan = $stmt->fetch(PDO::FETCH_ASSOC)) {
            echo '<div class="modal fade" id="giveFeedback" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"> Beri Feedback ' . $data_pemesanan['no_pembelian'] . ' - ' . $data_pemesanan['nama_supplier'] . ' </h5> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> &times; </span> </button>
                    </div>
                    <form action="modules/riwayat-pemesanan/feedback.php" method="POST">
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Rating</label>
                                <input name="no_pembelian" type="hidden" value="' . $data_pemesanan['no_pembelian'] . '">
                                <div id="star">
                                    <input id="rating" name="rating" type="hidden" value="0" required>
                                    <i id="star-one" class="fas fa-star mr-2" onclick="setStar(1, this.parentElement);"></i>
                                    <i id="star-two" class="fas fa-star mr-2" onclick="setStar(2, this.parentElement);"></i>
                                    <i id="star-three" class="fas fa-star mr-2" onclick="setStar(3, this.parentElement);"></i>
                                    <i id="star-four" class="fas fa-star mr-2" onclick="setStar(4, this.parentElement);"></i>
                                    <i id="star-five" class="fas fa-star mr-2" onclick="setStar(5, this.parentElement);"></i>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Feedback</label>
                                <textarea class="form-control" name="feedback" cols="30" rows="4" required></textarea>
                            </div>
                        </div>
                        <div class="modal-footer"> <button type="button" class="btn btn-secondary" data-dismiss="modal"> Tutup </button><input type="submit" value="Konfirmasi" class="btn btn-primary"></div>
                    </form>
                </div>
            </div>
        </div>';
        }
        // tutup koneksi
        $pdo = null;
    }
} catch (PDOException $e) {
    // tampilkan pesan kesalahan
    echo $e->getMessage();
}
