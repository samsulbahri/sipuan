<?php
session_start();      // memulai session

// panggil file "config.php" untuk koneksi ke database
require_once "../../config/config.php";

try {

    $no_pembelian = trim($_POST['no_pembelian']);
    $rating = trim($_POST['rating']);
    $feedback = trim($_POST['feedback']);

    if ($rating == 0) {
        echo '<script>alert("Rating Bintang Tidak Boleh Kosong"); window.location.href = "../../riwayat-pemesanan";</script>';
    } else {
        // sql statement untuk insert data ke tabel "bahanbaku"
        $query = "INSERT INTO feedback(id_rating, no_pembelian, feedback) VALUES (:id_rating, :no_pembelian, :feedback); UPDATE pembelian SET is_feedback = 'sudah' WHERE no_pembelian = :no_pembelian; UPDATE supplier SET rating_keseluruhan = :id_rating";
        // membuat prepared statements
        $stmt = $pdo->prepare($query);

        // hubungkan "data" dengan prepared statements
        $stmt->bindParam(':id_rating', $rating);
        $stmt->bindParam(':no_pembelian', $no_pembelian);
        $stmt->bindParam(':feedback', $feedback);

        // eksekusi query
        $stmt->execute();

        // cek query
        if ($stmt) {
            // jika berhasil tampilkan pesan "sukses"
            header('location: ../../riwayat-pemesanan');
        }

        // tutup koneksi
        $pdo = null;
    }
} catch (PDOException $e) {
    // tampilkan pesan kesalahan
    echo $e->getMessage();
}
