<?php
session_start();      // memulai session

// Mengecek AJAX Request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' )) {
	// panggil file "config.php" untuk koneksi ke database
	require_once "../../config/config.php";

	try {
		// ambil "data" hasil post dari ajax
		$no_pembelian = trim($_POST['no_pembelian']);
		$tanggal      = trim(date('Y-m-d', strtotime($_POST['tanggal'])));
		$supplier     = trim($_POST['supplier']);
		$no_nota      = trim($_POST['no_nota']);
		$total_bayar  = trim($_POST['total_bayar']);
		// ambil "data" dari session
		$created_user = $_SESSION['id_user'];

		// sql statement untuk insert data ke tabel "pembelian"
		$query = "INSERT INTO pembelian(no_pembelian, tanggal, supplier, no_nota, total_bayar, created_user)
				  VALUES (:no_pembelian, :tanggal, :supplier, :no_nota, :total_bayar, :created_user)";
		// membuat prepared statements
		$stmt = $pdo->prepare($query);

		// hubungkan "data" dengan prepared statements
		$stmt->bindParam(':no_pembelian', $no_pembelian);
		$stmt->bindParam(':tanggal', $tanggal);
		$stmt->bindParam(':supplier', $supplier);
		$stmt->bindParam(':no_nota', $no_nota);
		$stmt->bindParam(':total_bayar', $total_bayar);
		$stmt->bindParam(':created_user', $created_user);

		// eksekusi query
		$stmt->execute();

		// cek query
		// jika insert data pembelian berhasil, lanjutkan insert data detail pembelian
		if ($stmt) {
			// sql statement untuk insert data ke tabel "pembelian_detail"
			$query = "INSERT INTO pembelian_detail(no_pembelian, bahanbaku, harga_beli, jumlah_beli, total_harga, created_user)
					  VALUES (:no_pembelian, :bahanbaku, :harga_beli, :jumlah_beli, :total_harga, :created_user)";
			// membuat prepared statements
			$stmt = $pdo->prepare($query);

			// lakukan perulangan
			for($count = 0; $count < count($_POST['kode_bahanbaku_detail']); $count++)
			{
				// siapkan data
				$data = array(
					':no_pembelian'	=> $no_pembelian,
					':bahanbaku'		=> $_POST['kode_bahanbaku_detail'][$count],
					':harga_beli'	  => $_POST['harga_beli_detail'][$count],
					':jumlah_beli'	=> $_POST['jumlah_beli_detail'][$count],
					':total_harga'	=> $_POST['total_harga'][$count],
					':created_user'	=> $created_user
				);

				// eksekusi query
				$stmt->execute($data);
			}

			// cek query
			if ($stmt) {
			    // jika berhasil tampilkan pesan "sukses"
			    echo "sukses";
			}
		}

		// tutup koneksi
        $pdo = null;
    } catch (PDOException $e) {
        // tampilkan pesan kesalahan
        echo $e->getMessage();
    }
} else {
    // jika tidak ada ajax request, maka alihkan ke halaman "login-error"
    echo '<script>window.location="../../login-error"</script>';
}
?>
