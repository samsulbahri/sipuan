<!-- Form Entri Data Pembelian -->
<form id="formPembelian">
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <div class="form-group col-md-12">
                        <label>Supplier</label>
                        <select class="chosen-select" id="supplier" name="supplier" autocomplete="off">
                            <option value="">-- Pilih --</option>
                            <?php
                            // panggil file "config.php" untuk koneksi ke database
                            require_once "../../config/config.php";

                            try {
                                // sql statement untuk menampilkan data dari tabel "supplier"
                                $query = "SELECT kode_supplier, nama_supplier FROM supplier ORDER BY kode_supplier ASC";
                                // membuat prepared statements
                                $stmt = $pdo->prepare($query);

                                // eksekusi query
                                $stmt->execute();

                                // tampilkan hasil query
                                while ($data_supplier = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                    echo"<option value='$data_supplier[kode_supplier]'> $data_supplier[kode_supplier] - $data_supplier[nama_supplier] </option>";
                                }
                            } catch (PDOException $e) {
                                // tampilkan pesan kesalahan
                                echo $e->getMessage();
                            }
                            ?>
                        </select>
                    </div>

                    <div class="form-group col-md-12">
                        <label>No. Nota</label>
                        <input type="text" class="form-control" id="no_nota" name="no_nota" autocomplete="off">
                    </div>

                    <div class="form-group col-md-12">
                        <div class="border mt-4 mb-4"></div>
                    </div>

                    <div class="form-group col-md-12">
                        <label>Bahan Baku</label>
                        <select class="chosen-select" id="bahanbaku" name="bahanbaku" autocomplete="off">
                            <option value="">-- Pilih --</option>
                            <?php
                            try {
                                // sql statement untuk menampilkan data dari tabel "bahanbaku"
                                $query = "SELECT kode_bahanbaku, nama_bahanbaku FROM bahanbaku ORDER BY kode_bahanbaku ASC";
                                // membuat prepared statements
                                $stmt = $pdo->prepare($query);

                                // eksekusi query
                                $stmt->execute();

                                // tampilkan hasil query
                                while ($data_bahanbaku = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                    echo"<option value='$data_bahanbaku[kode_bahanbaku]'> $data_bahanbaku[kode_bahanbaku] - $data_bahanbaku[nama_bahanbaku] </option>";
                                }

                                // tutup koneksi
                                $pdo = null;
                            } catch (PDOException $e) {
                                // tampilkan pesan kesalahan
                                echo $e->getMessage();
                            }
                            ?>
                        </select>
                        <input type="hidden" id="nama_bahanbaku" name="nama_bahanbaku">
                    </div>

                    <div class="form-group col-md-12">
                        <label>Harga</label>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend"><div class="input-group-text">Rp.</div></div>
                            <input type="text" class="form-control" id="harga_beli" name="harga_beli" readonly>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <label>Satuan</label>
                        <input type="text" class="form-control" id="satuan" name="satuan" readonly>
                    </div>

                    <div class="form-group col-md-12">
                        <label>Jumlah</label>
                        <input type="text" class="form-control" id="jumlah_beli" name="jumlah_beli" onKeyPress="return goodchars(event,'0123456789',this)" autocomplete="off">
                    </div>
                </div>

                <div class="card-footer">
                    <div class="form-group col-md-12 mt-3">
                        <button type="button" class="btn btn-success btn-block" id="btnTambahbahanbaku">Tambah</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group col-md-12">
                                <label>No. Pembelian</label>
                                <input type="text" class="form-control" id="no_pembelian" name="no_pembelian" autocomplete="off" readonly>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group col-md-12">
                                <label>Tanggal</label>
                                <input type="text" class="form-control date-picker" data-date-format="dd-mm-yyyy" id="tanggal" name="tanggal" value="<?php echo date("d-m-Y"); ?>" autocomplete="off">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group col-md-12">
                                <div class="border mt-2 mb-2"></div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group col-md-12">
                                <label class="mb-3"><strong>Daftar Pembelian Bahan Baku</strong></label>
                                <!-- Tabel daftar pembelian bahanbaku untuk menampung sementara data pembelian bahanbaku sebelum disimpan ke database -->
                                <table id="tabel-pembelian-detail" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Kode Bahan Baku</th>
                                            <th>Nama Bahan Baku</th>
                                            <th>Harga</th>
                                            <th>Jumlah</th>
                                            <th>Total</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="center" colspan="4"><strong>Total</strong></td>
                                            <td class="right">
                                                <strong>Rp. <span class="total_bayar">0</span></strong>
                                                <input type="hidden" name="total_bayar" id="total_bayar">
                                            </td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                    <div class="form-group col-md-12 mt-2">
                        <div class="float-right">
                            <button type="button" class="btn btn-success mr-2" id="btnSimpan">Simpan</button>
                            <button type="button" class="btn btn-secondary" id="btnBatal">Batal</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
$(document).ready(function() {
    // ======================================= initiate plugin =======================================
    // datepicker plugin
    $('.date-picker').datepicker({
        autoclose: true,
        todayHighlight: true
    });

    // chosen-select plugin
    $('.chosen-select').chosen();
    // ===============================================================================================

    // =========================================== Onload ============================================
    // variabel untuk membuat "row id"
    var count = 0;
    // variabel untuk membuat "total_bayar"
    var total_bayar = 0;

    // reset form
    $('#formPembelian')[0].reset();
    $('#supplier').val('').trigger('chosen:updated');
    $('#bahanbaku').val('').trigger('chosen:updated');

    // membuat "no_pembelian"
    $.ajax({
        url  : "modules/pembelian/get_no.php",              // proses get "no_pembelian"
        success: function(result){                          // ketika proses get "no_pembelian" selesai
            // tampilkan data "no_pembelian"
            $('#no_pembelian').val(result);
        }
    });
    // ===============================================================================================

    // ============================================ Form =============================================
    // Menampilkan data bahanbaku dari select box ke textfield
    $('#bahanbaku').change(function(){
        // mengambil value dari "kode_bahanbaku"
        var kode_bahanbaku = $('#bahanbaku').val();

        $.ajax({
            type     : "GET",                                   // mengirim data dengan method GET
            url      : "modules/bahanbaku/get_data.php",             // proses get data bahanbaku berdasarkan "kode_bahanbaku"
            data     : {kode_bahanbaku:kode_bahanbaku},                   // data yang dikirim
            dataType : "JSON",                                  // tipe data JSON
            success: function(result){                          // ketika proses get data bahanbaku selesai
                // tampilkan data
                $("#nama_bahanbaku").val(result.nama_bahanbaku);
                $("#harga_beli").val(convertToRupiah(result.harga_beli));
                $("#satuan").val(result.nama_satuan);
                // fokus ke input jumlah beli
                $("#jumlah_beli").focus();
            }
        });
    });

    // Menambahkan daftar pembelian bahanbaku ke tabel sementara
    $('#btnTambahbahanbaku').click(function(){
        // validasi form input
        // jika bahanbaku kosong
        if ($('#bahanbaku').val()==""){
            // focus ke input bahanbaku
            $( "#bahanbaku" ).focus();
            // tampilkan peringatan data tidak boleh kosong
            $.notify({
                title   : '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                message : 'Bahan Baku tidak boleh kosong.'
            },{type: 'warning'});
        }
        // jika jumlah beli kosong atau 0 (nol)
        else if ($('#jumlah_beli').val()=="" || $('#jumlah_beli').val()==0){
            // focus ke input jumlah beli
            $( "#jumlah_beli" ).focus();
            // tampilkan peringatan data tidak boleh kosong atau 0 (nol)
            $.notify({
                title   : '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                message : 'Jumlah tidak boleh kosong atau 0 (nol).'
            },{type: 'warning'});
        }
        // jika semua data sudah terisi, jalankan perintah untuk menambahkan data
        else {
            // mengambil value data bahanbaku
            var kode_bahanbaku   = $('#bahanbaku').val();
            var nama_bahanbaku   = $('#nama_bahanbaku').val();
            var harga_beli  = convertToAngka($('#harga_beli').val());
            var jumlah_beli = $('#jumlah_beli').val();
            var total_harga = harga_beli * jumlah_beli;

            // buat "row id"
            count = count + 1;
            // siapkan data untuk ditambahkan ke daftar pembelian bahanbaku
            output = '<tr id="row_'+count+'">';
            output += '<td width="50" class="center">'+kode_bahanbaku+' <input type="hidden" name="kode_bahanbaku_detail[]" id="kode_bahanbaku_detail'+count+'" value="'+kode_bahanbaku+'" /></td>';
            output += '<td width="170">'+nama_bahanbaku+'</td>';
            output += '<td width="100" class="right"> Rp. '+convertToRupiah(harga_beli)+' <input type="hidden" name="harga_beli_detail[]" id="harga_beli_detail'+count+'" value="'+harga_beli+'" /></td>';
            output += '<td width="30" class="right">'+jumlah_beli+' <input type="hidden" name="jumlah_beli_detail[]" id="jumlah_beli_detail'+count+'" value="'+jumlah_beli+'" /></td>';
            output += '<td width="140" class="right"> Rp. '+convertToRupiah(total_harga)+' <input type="hidden" name="total_harga[]" id="total_harga'+count+'" value="'+total_harga+'" /></td>';
            output += '<td width="20" class="center"><button type="button" name="btnHapusbahanbaku" class="btn btn-danger btn-sm btnHapusbahanbaku" id="'+count+'"><i class="fas fa-trash"></i></button></td>';
            output += '</tr>';

            // tambahkan data ke daftar pembelian bahanbaku
            $('#tabel-pembelian-detail').prepend(output);
            // siapkan data total bayar
            total_bayar += total_harga;
            // tampilkan total bayar
            $('.total_bayar').text(convertToRupiah(total_bayar));
            $('#total_bayar').val(total_bayar);

            // reset form input bahanbaku
            $('#bahanbaku').val('').trigger('chosen:updated');
            $('#nama_bahanbaku').val("");
            $('#harga_beli').val("");
            $('#satuan').val("");
            $('#jumlah_beli').val("");
        }
    });

    // Menghapus daftar pembelian bahanbaku dari tabel sementara
    $("#tabel-pembelian-detail").on('click', '.btnHapusbahanbaku', function(){
        // mengambil row berdasarkan atribut id
        var row_id = $(this).attr("id");
        // mengambil value total harga
        var total_harga = $('#total_harga'+row_id).val();

        // kurangi total bayar
        total_bayar -= total_harga;

        // hapus row
        $('#row_'+row_id).remove();
        // tampilkan total bayar
        $('.total_bayar').text(convertToRupiah(total_bayar));
        $('#total_bayar').val(total_bayar);
    });
    // ===============================================================================================

    // =========================================== Insert ============================================
    // Proses Simpan Data
    $('#btnSimpan').click(function(){
        // validasi form input
        // jika supplier kosong
        if ($('#supplier').val()==""){
            // focus ke input supplier
            $( "#supplier" ).focus();
            // tampilkan peringatan data tidak boleh kosong
            $.notify({
                title   : '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                message : 'Supplier tidak boleh kosong.'
            },{type: 'warning'});
        }
        // jika no. nota kosong
        else if ($('#no_nota').val()==""){
            // focus ke input no. nota
            $( "#no_nota" ).focus();
            // tampilkan peringatan data tidak boleh kosong
            $.notify({
                title   : '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                message : 'No. Nota tidak boleh kosong.'
            },{type: 'warning'});
        }
        // jika tanggal kosong
        else if ($('#tanggal').val()==""){
            // focus ke input tanggal
            $( "#tanggal" ).focus();
            // tampilkan peringatan data tidak boleh kosong
            $.notify({
                title   : '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                message : 'Tanggal Pembelian tidak boleh kosong.'
            },{type: 'warning'});
        }
        // jika daftar pembelian bahanbaku kosong
        else if ($('.total_bayar').text()==0){
            // focus ke input bahanbaku
            $( "#bahanbaku" ).focus();
            // tampilkan peringatan data tidak boleh kosong
            $.notify({
                title   : '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                message : 'Daftar pembelian bahan baku tidak boleh kosong.'
            },{type: 'warning'});
        }
        // jika semua data sudah terisi, jalankan perintah insert data
        else {
            // membuat variabel untuk menampung data dari form entri data pembelian
            var data = $('#formPembelian').serialize();

            $.ajax({
                type : "POST",                                  // mengirim data dengan method POST
                url  : "modules/pembelian/insert.php",          // proses insert data
                data : data,                                    // data yang dikirim
                success: function(result){                      // ketika proses insert data selesai
                    // jika berhasil
                    if (result==="sukses") {
                        // tutup form entri data pembelian
                        tutup_form();
                        // tampilkan pesan sukses insert data
                        $.notify({
                            title   : '<i class="fas fa-check-circle title-icon"></i><strong>Sukses!</strong><br>',
                            message : 'Data Pembelian berhasil disimpan.'
                        },{type: 'success'});
                        // tampilkan view data pembelian
                        var table = $('#tabel-pembelian').DataTable();
                        table.ajax.reload( null, false );
                    }
                    // jika gagal
                    else {
                        // tampilkan pesan gagal insert data dan error result
                        $.notify({
                            title   : '<i class="fas fa-times-circle title-icon"></i><strong>Gagal!</strong><br>',
                            message : 'Data Pembelian tidak bisa disimpan. Query Error : '+ result
                        },{type: 'danger'});
                    }
                }
            });
            return false;
        }
    });
    // ===============================================================================================

    // ========================================= Reset Form ==========================================
    // Batal Entri Data Pembelian
    $('#btnBatal').click(function(){
        // tutup form entri data pembelian
        tutup_form();
    });

    // fungsi untuk menutup form entri data pembelian
    function tutup_form() {
        // judul form
        $('#judulHalaman').html('<i class="fas fa-medkit title-icon"></i> Data Pembelian');
        // tampilkan tombol tambah
        $('#btnTambah').show();
        // tampilkan view data pembelian
        $('#viewPembelian').show();
        // sembunyikan form entri data pembelian
        $('#formTambah').empty();
    }
    // ===============================================================================================
} );
</script>
