<?php
// Mengecek AJAX Request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' )) {
	// panggil file "config.php" untuk koneksi ke database
	require_once "../../config/config.php";

	// mengecek data get dari ajax
    if (isset($_GET['no_pembelian'])) {
        try {
            // ambil "data" get dari ajax
            $no_pembelian = $_GET['no_pembelian'];

        	// sql statement untuk menampilkan data dari tabel "pembelian" berdasarkan "no_pembelian"
            $query = "SELECT a.no_pembelian,a.tanggal,a.supplier,a.no_nota,a.total_bayar,b.nama_supplier
                      FROM pembelian as a INNER JOIN supplier as b ON a.supplier=b.kode_supplier WHERE a.no_pembelian=:no_pembelian";
            // membuat prepared statements
            $stmt = $pdo->prepare($query);

        	// hubungkan "data" dengan prepared statements
            $stmt->bindParam(':no_pembelian', $no_pembelian);

            // eksekusi query
            $stmt->execute();

            // ambil data hasil query
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            // tampilkan data total bayar
            $total_bayar = $data['total_bayar'];
            // tampilkan data pembelian
            echo "<div class='form-group col-md-12'>
                    <table class='table'>
                        <tr>
                            <td style='border-top:none' width='150'>No. Pembelian</td>
                            <td style='border-top:none' width='10'>:</td>
                            <td style='border-top:none'>".$data['no_pembelian']."</td>
                        </tr>
                        <tr>
                            <td>Tanggal</td>
                            <td>:</td>
                            <td>".date('d-m-Y', strtotime($data['tanggal']))."</td>
                        </tr>
                        <tr>
                            <td>Supplier</td>
                            <td>:</td>
                            <td>".$data['supplier']." - ".$data['nama_supplier']."</td>
                        </tr>
                        <tr>
                            <td>No. Nota</td>
                            <td>:</td>
                            <td>".$data['no_nota']."</td>
                        </tr>
                    </table>
                </div>
                <div class='form-group col-md-12'>
                    <div class='border mb-2'></div>
                </div>
                <div class='form-group col-md-12'>
                    <label class='mb-3'><strong>Daftar Pembelian bahanbaku</strong></label>
                    <table id='tabel-pembelian-detail' class='table table-striped table-bordered' style='width:100%'>
                        <thead>
                            <tr>
                                <th>Kode Bahan Baku</th>
                                <th>Nama Bahan Baku</th>
                                <th>Harga</th>
                                <th>Jumlah</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>";

            // sql statement untuk menampilkan data dari tabel "pembelian_detail" berdasarkan "no_pembelian"
            $query = "SELECT a.bahanbaku,a.harga_beli,a.jumlah_beli,a.total_harga,b.nama_bahanbaku
                      FROM pembelian_detail as a INNER JOIN bahanbaku as b ON a.bahanbaku=b.kode_bahanbaku WHERE a.no_pembelian=:no_pembelian";
            // membuat prepared statements
            $stmt = $pdo->prepare($query);

            // hubungkan "data" dengan prepared statements
            $stmt->bindParam(':no_pembelian', $no_pembelian);

            // eksekusi query
            $stmt->execute();

            // tampilkan hasil query
            while ($data = $stmt->fetch(PDO::FETCH_ASSOC)) {
                echo "      <tr>
                                <td width='80' class='center'>".$data['bahanbaku']."</td>
                                <td width='180'>".$data['nama_bahanbaku']."</td>
                                <td width='100' class='right'>Rp. ".number_format($data['harga_beli'], 0, ".", ".")."</td>
                                <td width='70' class='right'>".$data['jumlah_beli']."</td>
                                <td width='120' class='right'>Rp. ".number_format($data['total_harga'], 0, ".", ".")."</td>
                            </tr>";
            };
            echo "          <tr>
                                <td class='center' colspan='4'><strong>Total</strong></td>
                                <td class='right'><strong>Rp. ".number_format($total_bayar, 0, ".", ".")."</strong></td>
                            </tr>
                        </tbody>
                    </table>
                </div>";

            // tutup koneksi
            $pdo = null;
        } catch (PDOException $e) {
            // tampilkan pesan kesalahan
            echo "Query Error : ".$e->getMessage();
        }
	}
} else {
    // jika tidak ada ajax request, maka alihkan ke halaman "login-error"
    echo '<script>window.location="../../login-error"</script>';
}
?>
