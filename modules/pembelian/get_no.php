<?php
// Mengecek AJAX Request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' )) {
	// panggil file "config.php" untuk koneksi ke database
	require_once "../../config/config.php";

    try {
    	// sql statement untuk menampilkan 5 digit terakhir "no_pembelian" dari tabel "pembelian"
        $query = "SELECT RIGHT(no_pembelian,5) as kode FROM pembelian ORDER BY no_pembelian DESC LIMIT 1";
        // membuat prepared statements
        $stmt = $pdo->prepare($query);

        // eksekusi query
        $stmt->execute(); 

        // cek hasil query
        // jika no. pembelian sudah ada
        if ($stmt->rowCount() <> 0) {
            // ambil data hasil query
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            // no. pembelian + 1
            $kode = $data['kode']+1;
        }
        // jika no. pembelian belum ada
        else {
            // no. pembelian = 1
            $kode = 1;
        }
        
        // ambil bulan dan tahun sekarang
        $bulan        = date("m");
        $tahun        = date("Y");
        // membuat no. pembelian
        $buat_kode    = str_pad($kode, 5, "0", STR_PAD_LEFT);
        $no_pembelian = "BL".$bulan.$tahun.$buat_kode;
        
        // tampilkan data
        echo $no_pembelian;

        // tutup koneksi
        $pdo = null;
    } catch (PDOException $e) {
        // tampilkan pesan kesalahan
        echo "Query Error : ".$e->getMessage();
    }
} else {
    // jika tidak ada ajax request, maka alihkan ke halaman "login-error"
    echo '<script>window.location="../../login-error"</script>';
}
?>