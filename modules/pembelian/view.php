<?php
// fungsi untuk pengecekan status login user 
// jika user belum login, alihkan ke halaman "login-error"
if (empty($_SESSION['username']) && empty($_SESSION['password'])) {
    echo "<meta http-equiv='refresh' content='0; url=../../login-error'>";
}
// jika user sudah login
else { ?>
    <div class="row mb-3">
        <div class="col-md-12">
            <h5>
                <!-- judul halaman data pembelian -->
                <span id="judulHalaman"><i class="fas fa-cart-plus title-icon"></i> Data Pembelian</span>
                <!-- tombol tambah data pembelian -->
                <a class="btn btn-success float-right" id="btnTambah" href="javascript:void(0);" role="button">
                    <i class="fas fa-plus"></i> Tambah
                </a>
            </h5>
        </div>
    </div>

    <div class="border mb-4"></div>

    <!-- view data pembelian -->
    <div id="viewPembelian">
        <div class="row">
            <div class="col-md-12">
                <!-- Tabel pembelian untuk menampilkan data pembelian dari database -->
                <table id="tabel-pembelian" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <!-- judul kolom pada bagian kepala (atas) tabel -->
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>No. Pembelian</th>
                            <th>Tanggal</th>
                            <th>Kode Supplier</th> <!-- kolom "Kode Supplier" disembunyikan, digabung dengan kolom "Supplier" -->
                            <th>Supplier</th>
                            <th>No. Nota</th>
                            <th>Total</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <!-- parameter untuk menampilkan form entri data pembelian -->
    <div id="formTambah"></div>

    <!-- Modal form untuk menampilkan detail data pembelian -->
    <div class="modal fade" id="modalPembelian" tabindex="-1" role="dialog" aria-labelledby="modalPembelian" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <!-- judul form detail data pembelian -->
                <div class="modal-header">
                    <h5 class="modal-title"><i class="fas fa-search-plus title-icon"></i>Detail Data Pembelian</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <!-- parameter untuk memuat isi detail data pembelian -->
                    <div class="row" id="loadPembelian"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            // ============================================ View =============================================
            // dataTables plugin untuk membuat nomor urut tabel
            $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
                return {
                    "iStart": oSettings._iDisplayStart,
                    "iEnd": oSettings.fnDisplayEnd(),
                    "iLength": oSettings._iDisplayLength,
                    "iTotal": oSettings.fnRecordsTotal(),
                    "iFilteredTotal": oSettings.fnRecordsDisplay(),
                    "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                    "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                };
            };

            // datatables serverside processing
            var table = $('#tabel-pembelian').DataTable({
                "processing": true, // tampilkan loading saat proses data
                "serverSide": true, // server-side processing
                "ajax": 'modules/pembelian/data.php', // panggil file "data.php" untuk menampilkan data pembelian dari database
                // menampilkan data
                "columnDefs": [{
                        "targets": 0,
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "width": '30px',
                        "className": 'center'
                    },
                    {
                        "responsivePriority": 1,
                        "targets": 1,
                        "width": '100px',
                        "className": 'center'
                    },
                    {
                        "targets": 2,
                        "width": '70px',
                        "className": 'center'
                    },
                    {
                        "targets": 3,
                        "width": '70px',
                        "visible": false
                    }, // kolom "kode_supplier" disembunyikan, digabung dengan kolom "nama_supplier"
                    {
                        "targets": 4,
                        "width": '200px',
                        "render": function(data, type, row) {
                            return row[3] + ' - ' + data;
                        }
                    },
                    {
                        "targets": 5,
                        "width": '100px',
                        "className": 'center'
                    },
                    {
                        "targets": 6,
                        "width": '100px',
                        "className": 'right',
                        render: $.fn.DataTable.render.number('.', ',', 0, 'Rp. ')
                    },
                    {
                        "responsivePriority": 2,
                        "targets": 7,
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "width": '60px',
                        "className": 'center',
                        // tombol detail dan hapus
                        "render": function(data, type, row) {
                            var btn = "<a style=\"margin-right:7px\" data-toggle=\"tooltip\" title=\"Detail\" class=\"btn btn-success btn-sm getDetail\" href=\"javascript:void(0);\"><i class=\"fas fa-search-plus\"></i></a><a data-toggle=\"tooltip\" title=\"Hapus\" class=\"btn btn-danger btn-sm btnHapus\" href=\"javascript:void(0);\"><i class=\"fas fa-trash\"></i></a>";
                            return btn;
                        }
                    }
                ],
                "order": [
                    [1, "desc"]
                ], // urutkan data berdasarkan "no_pembelian" secara descending
                "iDisplayLength": 10, // tampilkan 10 data per halaman
                // membuat nomor urut tabel
                "rowCallback": function(row, data, iDisplayIndex) {
                    var info = this.fnPagingInfo();
                    var page = info.iPage;
                    var length = info.iLength;
                    var index = page * length + (iDisplayIndex + 1);
                    $('td:eq(0)', row).html(index);
                }
            });
            // ===============================================================================================

            // ============================================ Form =============================================
            // Tampilkan Form Entri Data Pembelian
            $('#btnTambah').click(function() {
                // judul form
                $('#judulHalaman').html('<i class="fas fa-edit title-icon"></i> Entri Data Pembelian');
                // sembunyikan tombol tambah
                $('#btnTambah').hide();
                // sembunyikan view data pembelian
                $('#viewPembelian').hide();
                // tampilkan form entri data pembelian
                $('#formTambah').load('modules/pembelian/form.php');
            });

            // Tampilkan Detail Data Pembelian
            $('#tabel-pembelian tbody').on('click', '.getDetail', function() {
                // ambil data dari datatables 
                var data = table.row($(this).parents('tr')).data();
                // membuat variabel untuk menampung data "no_pembelian"
                var no_pembelian = data[1];

                $.ajax({
                    type: "GET", // mengirim data dengan method GET 
                    url: "modules/pembelian/get_data.php", // proses get data pembelian berdasarkan "no_pembelian"
                    data: {
                        no_pembelian: no_pembelian
                    }, // data yang dikirim
                    success: function(result) { // ketika proses get data selesai
                        // tampilkan modal detail data pembelian
                        $('#modalPembelian').modal('show');
                        // tampilkan detail data pembelian
                        $('#loadPembelian').html(result);
                    }
                });
            });
            // ===============================================================================================

            // =========================================== Delete ============================================
            $('#tabel-pembelian tbody').on('click', '.btnHapus', function() {
                // ambil data dari datatables 
                var data = table.row($(this).parents('tr')).data();
                // tampilkan notifikasi saat akan menghapus data
                swal({
                        title: "Apakah Anda Yakin?",
                        text: "Anda akan menghapus data Pembelian : " + data[1],
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ya, Hapus!",
                        closeOnConfirm: false
                    },
                    // jika dipilih ya, maka jalankan perintah delete data
                    function() {
                        // membuat variabel untuk menampung data pembelian
                        var no_pembelian = data[1];
                        var tanggal = data[2];
                        var kode_supplier = data[3];
                        var no_nota = data[5];
                        var total_bayar = data[6];

                        $.ajax({
                            type: "POST", // mengirim data dengan method POST 
                            url: "modules/pembelian/delete.php", // proses delete data 
                            data: { // data yang dikirim
                                no_pembelian: no_pembelian,
                                tanggal: tanggal,
                                kode_supplier: kode_supplier,
                                no_nota: no_nota,
                                total_bayar: total_bayar
                            },
                            success: function(result) { // ketika proses delete data selesai
                                // jika berhasil
                                if (result === "sukses") {
                                    // tutup sweet alert
                                    swal.close();
                                    // tampilkan pesan sukses delete data
                                    $.notify({
                                        title: '<i class="fas fa-check-circle title-icon"></i><strong>Sukses!</strong><br>',
                                        message: 'Data Pembelian berhasil dihapus.'
                                    }, {
                                        type: 'success'
                                    });
                                    // tampilkan view data pembelian
                                    var table = $('#tabel-pembelian').DataTable();
                                    table.ajax.reload(null, false);
                                }
                                // jika gagal
                                else {
                                    // tutup sweet alert
                                    swal.close();
                                    // tampilkan pesan gagal delete data dan error result
                                    $.notify({
                                        title: '<i class="fas fa-times-circle title-icon"></i><strong>Gagal!</strong><br>',
                                        message: 'Data Pembelian tidak bisa dihapus. Query Error : ' + result
                                    }, {
                                        type: 'danger'
                                    });
                                }
                            }
                        });
                    });
            });
            // ===============================================================================================
        });
    </script>
<?php } ?>