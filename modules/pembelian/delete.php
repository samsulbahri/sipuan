<?php
session_start();      // memulai session

// Mengecek AJAX Request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' )) {
    // panggil file "config.php" untuk koneksi ke database
    require_once "../../config/config.php";

    // mengecek data post dari ajax
    if (isset($_POST['no_pembelian'])) {
        try {
            // ambil "data" post dari ajax
            $no_pembelian  = $_POST['no_pembelian'];
            $tanggal       = $_POST['tanggal'];
            $kode_supplier = $_POST['kode_supplier'];
            $no_nota       = $_POST['no_nota'];
            $total_bayar   = $_POST['total_bayar'];

            // sql statement untuk delete data dari tabel "pembelian" dan "pembelian_detail"
            $query = "DELETE FROM pembelian_detail WHERE pembelian_detail.no_pembelian=:no_pembelian";
            // membuat prepared statements
            $stmt = $pdo->prepare($query);

            // hubungkan "data" dengan prepared statements
            $stmt->bindParam(':no_pembelian', $no_pembelian);

            // eksekusi query
            $stmt->execute();

            $query = "DELETE FROM pembelian WHERE pembelian.no_pembelian=:no_pembelian";
            // membuat prepared statements
            $stmt = $pdo->prepare($query);

            // hubungkan "data" dengan prepared statements
            $stmt->bindParam(':no_pembelian', $no_pembelian);

            // eksekusi query
            $stmt->execute();

            // cek hasil query
            // jika data "pembelian" berhasil dihapus, jalankan perintah untuk insert data audit
            if ($stmt) {
                // siapkan "data"
                $username   = $_SESSION['id_user'];
                $aksi       = "Delete";
                $keterangan = "<b>Delete</b> data pembelian pada tabel <b>pembelian</b>. <br> <b>[No. Pembelian : </b>".$no_pembelian."<b>][Tanggal : </b>".$tanggal."<b>][Kode Supplier : </b>".$kode_supplier."<b>][No. Nota : </b>".$no_nota."<b>][Total Bayar : </b>".$total_bayar."<b>]";

                // sql statement untuk insert data ke tabel "sys_audit_trail"
                $query = "INSERT INTO sys_audit_trail(username, aksi, keterangan) VALUES (:username, :aksi, :keterangan)";
                // membuat prepared statements
                $stmt = $pdo->prepare($query);

                // hubungkan "data" dengan prepared statements
                $stmt->bindParam(':username', $username);
                $stmt->bindParam(':aksi', $aksi);
                $stmt->bindParam(':keterangan', $keterangan);

                // eksekusi query
                $stmt->execute();

                // cek query
                if ($stmt) {
                    // jika insert data audit berhasil tampilkan pesan "sukses"
                    echo "sukses";
                }
            }

            // tutup koneksi
            $pdo = null;
        } catch (PDOException $e) {
            // tampilkan pesan kesalahan
            echo $e->getMessage();
        }
    }
} else {
    // jika tidak ada ajax request, maka alihkan ke halaman "login-error"
    echo '<script>window.location="../../login-error"</script>';
}
?>
