<?php
// Mengecek AJAX Request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' )) {
    // panggil file "config.php" untuk koneksi ke database
    require_once "../../config/config.php";

    // mengecek data get dari ajax
    if (isset($_GET['tgl_awal'])) {
        try {
            // ambil "data" get dari ajax
            $tgl_awal  = date('Y-m-d', strtotime($_GET['tgl_awal']));
            $tgl_akhir = date('Y-m-d', strtotime($_GET['tgl_akhir']));
            $bahanbaku      = $_GET['bahanbaku'];
            // variabel untuk nomor urut tabel
            $no = 1;
            // variabel untuk total jumlah_beli seluruh
            $total_beli_seluruh  = 0;
            // variabel untuk total bayar seluruh
            $total_bayar_seluruh = 0;

            // cek value bahanbaku
            // jika bahanbaku = Seluruh (dipilih seluruh)
            if ($bahanbaku == 'Seluruh') {
                // sql statement untuk menampilkan data bahanbaku dari tabel "pembelian"
                $query1 = "SELECT a.tanggal,b.bahanbaku,c.nama_bahanbaku FROM pembelian as a INNER JOIN pembelian_detail as b INNER JOIN bahanbaku as c
                           ON a.no_pembelian=b.no_pembelian AND b.bahanbaku=c.kode_bahanbaku WHERE a.tanggal BETWEEN :tgl_awal AND :tgl_akhir
                           GROUP BY b.bahanbaku ORDER BY b.bahanbaku ASC";
                // membuat prepared statements
                $stmt1 = $pdo->prepare($query1);

                // hubungkan "data" dengan prepared statements
                $stmt1->bindParam(':tgl_awal', $tgl_awal);
                $stmt1->bindParam(':tgl_akhir', $tgl_akhir);
            }
            // jika bahanbaku != Seluruh (dipilih per bahanbaku)
            else {
                // sql statement untuk menampilkan data bahanbaku dari tabel "pembelian"
                $query1 = "SELECT a.tanggal,b.bahanbaku,c.nama_bahanbaku FROM pembelian as a INNER JOIN pembelian_detail as b INNER JOIN bahanbaku as c
                           ON a.no_pembelian=b.no_pembelian AND b.bahanbaku=c.kode_bahanbaku WHERE b.bahanbaku=:bahanbaku AND a.tanggal BETWEEN :tgl_awal AND :tgl_akhir
                           GROUP BY b.bahanbaku ORDER BY b.bahanbaku ASC";
                // membuat prepared statements
                $stmt1 = $pdo->prepare($query1);

                // hubungkan "data" dengan prepared statements
                $stmt1->bindParam(':bahanbaku', $bahanbaku);
                $stmt1->bindParam(':tgl_awal', $tgl_awal);
                $stmt1->bindParam(':tgl_akhir', $tgl_akhir);
            }

            // eksekusi query
            $stmt1->execute();

            // cek hasil query
            // jika data ada, lakukan perulangan untuk menampilkan data
            if ($stmt1->rowCount() <> 0) {
                    // Tabel untuk menampilkan data bahanbaku dari database
                    echo "<div class='form-group col-md-12'>
                            <table class='table-report'>";
                // tampilkan hasil query
                while ($data1 = $stmt1->fetch(PDO::FETCH_ASSOC)) {
                    // tampilkan data kode_bahanbaku
                    $bahanbaku = $data1['bahanbaku'];
                    // tampilkan data bahanbaku
                    echo "      <tr><td style='border-top:1px solid #dee2e6' height='0' colspan='4'></td></tr>
                                <tr>
                                    <td width='50'><strong>".$no.".</strong></td>
                                    <td style='padding: .35rem 0 0 0' colspan='3'>".$bahanbaku." - ".$data1['nama_bahanbaku']."</td>
                                </tr>
                                <tr>
                                    <td width='50'></td>
                                    <td style='border-bottom:1px solid #dee2e6' colspan='4'></td>
                                </tr>
                                <tr>
                                    <td width='50'></td>
                                    <td style='padding: .75rem 0 .75rem 0' colspan='3'>
                                        <table class='table-report table-bordered' style='width:100%'>
                                            <thead>
                                                <tr class='table-report-bg-dark'>
                                                    <th>Tanggal</th>
                                                    <th>No. Pembelian</th>
                                                    <th>Harga</th>
                                                    <th>Jumlah</th>
                                                    <th>Total</th>
                                                </tr>
                                            </thead>
                                            <tbody>";
                    $no++;

                    // variabel untuk sub total jumlah_beli
                    $sub_total_beli  = 0;
                    // variabel untuk sub total bayar
                    $sub_total_bayar = 0;

                    // sql statement untuk menampilkan data pembelian per bahanbaku dan tanggal dari tabel "pembelian"
                    $query2 = "SELECT a.no_pembelian,a.tanggal,b.bahanbaku,b.harga_beli,b.jumlah_beli,b.total_harga
                               FROM pembelian as a INNER JOIN pembelian_detail as b ON a.no_pembelian=b.no_pembelian
                               WHERE b.bahanbaku=:bahanbaku AND a.tanggal BETWEEN :tgl_awal AND :tgl_akhir";
                    // membuat prepared statements
                    $stmt2 = $pdo->prepare($query2);

                    // hubungkan "data" dengan prepared statements
                    $stmt2->bindParam(':bahanbaku', $bahanbaku);
                    $stmt2->bindParam(':tgl_awal', $tgl_awal);
                    $stmt2->bindParam(':tgl_akhir', $tgl_akhir);

                    // eksekusi query
                    $stmt2->execute();

                    // tampilkan hasil query
                    while ($data2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                        // tampilkan data pembelian per bahanbaku
                        echo "                  <tr>
                                                    <td width='80' class='center'>".date('d-m-Y', strtotime($data2['tanggal']))."</td>
                                                    <td width='100' class='center'>".$data2['no_pembelian']."</td>
                                                    <td width='100' class='right'>Rp. ".number_format($data2['harga_beli'], 0, ".", ".")."</td>
                                                    <td width='70' class='right'>".$data2['jumlah_beli']."</td>
                                                    <td width='140' class='right'>Rp. ".number_format($data2['total_harga'], 0, ".", ".")."</td>
                                                </tr>";
                        // buat sub total
                        $sub_total_beli  += $data2['jumlah_beli'];
                        $sub_total_bayar += $data2['total_harga'];
                    };
                    echo "                      <tr class='table-report-bg-dark'>
                                                    <td class='center' colspan='3'><strong>Total</strong></td>
                                                    <td class='right'><strong>".number_format($sub_total_beli, 0, ".", ".")."</strong></td>
                                                    <td class='right'><strong>Rp. ".number_format($sub_total_bayar, 0, ".", ".")."</strong></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>";
                    // buat total seluruh
                    $total_beli_seluruh  += $sub_total_beli;
                    $total_bayar_seluruh += $sub_total_bayar;
                };
                echo "          <tr class='table-report-bg-dark'>
                                    <td style='padding-right: 0' class='right' colspan='2'><strong>TOTAL SELURUH : </strong></td>
                                    <td style='padding-right: 0.7rem' width='178' class='right'><strong>".number_format($total_beli_seluruh, 0, ".", ".")."</strong></td>
                                    <td style='padding-right: 0.7rem' width='331' class='right'><strong>Rp. ".number_format($total_bayar_seluruh, 0, ".", ".")."</strong></td>
                                </tr>
                            </table>
                        </div>";
            }
            // jika data tidak ada
            else {
                echo "<div class='form-group col-md-12'>
                        <div class='alert alert-warning' role='alert'>
                            <i class='fas fa-exclamation-triangle title-icon'></i>Tidak ada transaksi pembelian.
                        </div>
                    </div>";
            }

            // tutup koneksi
            $pdo = null;
        } catch (PDOException $e) {
            // tampilkan pesan kesalahan
            echo "Query Error : ".$e->getMessage();
        }
    }
} else {
    // jika tidak ada ajax request, maka alihkan ke halaman "login-error"
    echo '<script>window.location="../../login-error"</script>';
}
?>
