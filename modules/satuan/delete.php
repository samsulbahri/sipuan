<?php
session_start();      // memulai session

// Mengecek AJAX Request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' )) {
    // panggil file "config.php" untuk koneksi ke database
    require_once "../../config/config.php";

    // mengecek data post dari ajax
    if (isset($_POST['kode_satuan'])) {
        try {
            // ambil "data" post dari ajax
            $kode_satuan = $_POST['kode_satuan'];
            $nama_satuan = $_POST['nama_satuan'];

            // sql statement untuk menampilkan data "satuan" dari tabel "bahanbaku" berdasarkan "kode_satuan"
            $query = "SELECT satuan FROM bahanbaku WHERE satuan=:kode_satuan";
            // membuat prepared statements
            $stmt = $pdo->prepare($query);

            // hubungkan "data" dengan prepared statements
            $stmt->bindParam(':kode_satuan', $kode_satuan);

            // eksekusi query
            $stmt->execute();

            // cek hasil query
            // jika data "satuan" sudah ada di tabel "bahanbaku"
            if ($stmt->rowCount() <> 0) {
                // tampilkan pesan gagal delete data
                echo "gagal";
            }
            // jika data tidak ada, jalankan perintah delete
            else {
                // sql statement untuk delete data dari tabel "satuan"
                $query = "DELETE FROM satuan WHERE kode_satuan=:kode_satuan";
                // membuat prepared statements
                $stmt = $pdo->prepare($query);

                // hubungkan "data" dengan prepared statements
                $stmt->bindParam(':kode_satuan', $kode_satuan);

                // eksekusi query
                $stmt->execute();

                // cek hasil query
                // jika data "satuan" berhasil dihapus, jalankan perintah untuk insert data audit
                if ($stmt) {
                    // siapkan "data"
                    $username   = $_SESSION['id_user'];
                    $aksi       = "Delete";
                    $keterangan = "<b>Delete</b> data satuan pada tabel <b>satuan</b>. <br> <b>[Kode Satuan : </b>".$kode_satuan."<b>][Nama Satuan : </b>".$nama_satuan."<b>]";

                    // sql statement untuk insert data ke tabel "sys_audit_trail"
                    $query = "INSERT INTO sys_audit_trail(username, aksi, keterangan) VALUES (:username, :aksi, :keterangan)";
                    // membuat prepared statements
                    $stmt = $pdo->prepare($query);

                    // hubungkan "data" dengan prepared statements
                    $stmt->bindParam(':username', $username);
                    $stmt->bindParam(':aksi', $aksi);
                    $stmt->bindParam(':keterangan', $keterangan);

                    // eksekusi query
                    $stmt->execute();

                    // cek query
                    if ($stmt) {
                        // jika insert data audit berhasil tampilkan pesan "sukses"
                        echo "sukses";
                    }
                }
            }

            // tutup koneksi
            $pdo = null;
        } catch (PDOException $e) {
            // tampilkan pesan kesalahan
            echo $e->getMessage();
        }
    }
} else {
    // jika tidak ada ajax request, maka alihkan ke halaman "login-error"
    echo '<script>window.location="../../login-error"</script>';
}
?>
