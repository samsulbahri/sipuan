<?php
// fungsi untuk pengecekan status login user
// jika user belum login, alihkan ke halaman "login-error"
if (empty($_SESSION['username']) && empty($_SESSION['password'])){
    echo "<meta http-equiv='refresh' content='0; url=../../login-error'>";
}
// jika user sudah login
else { ?>
    <div class="row mb-3">
        <div class="col-md-12">
    		<h5>
    			<!-- judul halaman tampil data satuan -->
    			<i class="fas fa-boxes title-icon"></i> Data Satuan
    			<!-- tombol tambah, dan export data satuan -->
                <div class="float-right">
                    <a class="btn btn-success mr-2" id="btnTambah" href="javascript:void(0);" data-toggle="modal" data-target="#modalSatuan" role="button">
                        <i class="fas fa-plus"></i> Tambah
                    </a>
                </div>
    		</h5>
    	</div>
    </div>

    <div class="border mb-4"></div>

    <div class="row">
        <div class="col-md-12">
            <!-- Tabel satuan untuk menampilkan data satuan dari database -->
            <table id="tabel-satuan" class="table table-striped table-bordered" style="width:100%">
    			<!-- judul kolom pada bagian kepala (atas) tabel -->
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Satuan</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    <!-- Modal form untuk entri dan ubah data satuan -->
    <div class="modal fade" id="modalSatuan" tabindex="-1" role="dialog" aria-labelledby="modalSatuan" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            	<!-- judul form data satuan -->
                <div class="modal-header">
                    <h5 class="modal-title"><i class="fas fa-edit title-icon"></i><span id="modalLabel"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
    			<!-- isi form data satuan -->
                <form id="formSatuan">
                    <div class="modal-body">
                        <input type="hidden" id="kode_satuan" name="kode_satuan">

                        <div class="form-group">
                            <label>Satuan</label>
                            <input type="text" class="form-control" id="nama_satuan" name="nama_satuan" autocomplete="off">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success mr-2" id="btnSimpan">Simpan</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
    $(document).ready(function(){
        // ============================================ View =============================================
        // dataTables plugin untuk membuat nomor urut tabel
        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        // datatables serverside processing
        var table = $('#tabel-satuan').DataTable( {
            "processing": true,                         // tampilkan loading saat proses data
            "serverSide": true,                         // server-side processing
            "ajax": 'modules/satuan/data.php',        // panggil file "data.php" untuk menampilkan data satuan dari database
            // menampilkan data
            "columnDefs": [
                { "targets": 0, "data": null, "orderable": false, "searchable": false, "width": '30px', "className": 'center' },
                { "targets": 1, "width": '300px' },
                {
                  "targets": 2, "data": null, "orderable": false, "searchable": false, "width": '70px', "className": 'center',
                  // tombol ubah dan hapus
                  "render": function(data, type, row) {
                      var btn = "<a style=\"margin-right:7px\" data-toggle=\"tooltip\" title=\"Ubah\" class=\"btn btn-success btn-sm getUbah\" href=\"javascript:void(0);\"><i class=\"fas fa-edit\"></i></a><a data-toggle=\"tooltip\" title=\"Hapus\" class=\"btn btn-danger btn-sm btnHapus\" href=\"javascript:void(0);\"><i class=\"fas fa-trash\"></i></a>";
                      return btn;
                  }
                }
            ],
            "order": [[ 1, "asc" ]],            // urutkan data berdasarkan "nama_satuan" secara ascending
            "iDisplayLength": 10,               // tampilkan 10 data per halaman
            // membuat nomor urut tabel
            "rowCallback": function (row, data, iDisplayIndex) {
                var info   = this.fnPagingInfo();
                var page   = info.iPage;
                var length = info.iLength;
                var index  = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        } );
        // ===============================================================================================

        // ============================================ Form =============================================
        // Tampilkan Modal Form Entri Data
        $('#btnTambah').click(function(){
            // reset form
            $('#formSatuan')[0].reset();
            // judul form
            $('#modalLabel').text('Entri Data Satuan');
        });

        // Tampilkan Modal Form Ubah Data
        $('#tabel-satuan tbody').on( 'click', '.getUbah', function (){
            // judul form
            $('#modalLabel').text('Ubah Data Satuan');
            // ambil data dari datatables
            var data = table.row( $(this).parents('tr') ).data();
            // membuat variabel untuk menampung data "kode_satuan"
            var kode_satuan = data[ 2 ];

            $.ajax({
                type     : "GET",                                       // mengirim data dengan method GET
                url      : "modules/satuan/get_data.php",               // proses get data satuan berdasarkan "kode_satuan"
                data     : {kode_satuan:kode_satuan},                   // data yang dikirim
                dataType : "JSON",                                      // tipe data JSON
                success: function(result){                              // ketika proses get data selesai
                    // tampilkan modal ubah data satuan
                    $('#modalSatuan').modal('show');
                    // tampilkan data satuan
                    $('#kode_satuan').val(result.kode_satuan);
                    $('#nama_satuan').val(result.nama_satuan);
                }
            });
        });
        // ===============================================================================================

        // ====================================== Insert dan Update ======================================
        // Proses Simpan Data
        $('#btnSimpan').click(function(){
            // validasi form input
            // jika nama satuan kosong
            if ($('#nama_satuan').val()==""){
                // focus ke input nama satuan
                $( "#nama_satuan" ).focus();
                // tampilkan peringatan data tidak boleh kosong
                $.notify({
                    title   : '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                    message : 'Satuan tidak boleh kosong.'
                },{type: 'warning'});
            }
            // jika semua data sudah terisi, jalankan perintah insert / update data
            else {
                // jika form entri data satuan yang ditampilkan, jalankan perintah insert
                if ($('#modalLabel').text()=="Entri Data Satuan") {
                    // membuat variabel untuk menampung data dari form entri data satuan
                    var data = $('#formSatuan').serialize();

                    $.ajax({
                        type : "POST",                                  // mengirim data dengan method POST
                        url  : "modules/satuan/insert.php",             // proses insert data
                        data : data,                                    // data yang dikirim
                        success: function(result){                      // ketika proses insert data selesai
                            // jika berhasil
                            if (result==="sukses") {
                                // reset form
                                $('#formSatuan')[0].reset();
                                // tutup modal entri data satuan
                                $('#modalSatuan').modal('hide');
                                // tampilkan pesan sukses insert data
                                $.notify({
                                    title   : '<i class="fas fa-check-circle title-icon"></i><strong>Sukses!</strong><br>',
                                    message : 'Data Satuan berhasil disimpan.'
                                },{type: 'success'});
                                // tampilkan view data satuan
                                var table = $('#tabel-satuan').DataTable();
                                table.ajax.reload( null, false );
                            }
                            // jika gagal
                            else {
                                // tampilkan pesan gagal insert data dan error result
                                $.notify({
                                    title   : '<i class="fas fa-times-circle title-icon"></i><strong>Gagal!</strong><br>',
                                    message : 'Data Satuan tidak bisa disimpan. Query Error : '+ result
                                },{type: 'danger'});
                            }
                        }
                    });
                    return false;
                }
                // jika form ubah data satuan yang ditampilkan, jalankan perintah update
                else if ($('#modalLabel').text()=="Ubah Data Satuan") {
                    // membuat variabel untuk menampung data dari form ubah data satuan
                    var data = $('#formSatuan').serialize();

                    $.ajax({
                        type : "POST",                                  // mengirim data dengan method POST
                        url  : "modules/satuan/update.php",             // proses update data
                        data : data,                                    // data yang dikirim
                        success: function(result){                      // ketika proses update data selesai
                            // jika berhasil
                            if (result==="sukses") {
                                // reset form
                                $('#formSatuan')[0].reset();
                                // tutup modal ubah data satuan
                                $('#modalSatuan').modal('hide');
                                // tampilkan pesan sukses update data
                                $.notify({
                                    title   : '<i class="fas fa-check-circle title-icon"></i><strong>Sukses!</strong><br>',
                                    message : 'Data Satuan berhasil diubah.'
                                },{type: 'success'});
                                // tampilkan view data satuan
                                var table = $('#tabel-satuan').DataTable();
                                table.ajax.reload( null, false );
                            }
                            // jika gagal
                            else {
                                // tampilkan pesan gagal update data dan error result
                                $.notify({
                                    title   : '<i class="fas fa-times-circle title-icon"></i><strong>Gagal!</strong><br>',
                                    message : 'Data Satuan tidak bisa diubah. Query Error : '+ result
                                },{type: 'danger'});
                            }
                        }
                    });
                    return false;
                }
            }
        });
        // ===============================================================================================

        // =========================================== Delete ============================================
        $('#tabel-satuan tbody').on( 'click', '.btnHapus', function (){
            // ambil data dari datatables
            var data = table.row( $(this).parents('tr') ).data();
            // tampilkan notifikasi saat akan menghapus data
            swal({
                title: "Apakah Anda Yakin?",
                text: "Anda akan menghapus data satuan : "+ data[ 1 ],
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya, Hapus!",
                closeOnConfirm: false
            },
            // jika dipilih ya, maka jalankan perintah delete data
            function () {
                // membuat variabel untuk menampung data satuan
                var kode_satuan = data[ 2 ];
                var nama_satuan = data[ 1 ];

                $.ajax({
                    type : "POST",                                              // mengirim data dengan method POST
                    url  : "modules/satuan/delete.php",                         // proses delete data
                    data : {kode_satuan:kode_satuan, nama_satuan:nama_satuan},  // data yang dikirim
                    success: function(result){                                  // ketika proses delete data selesai
                        // jika berhasil
                        if (result==="sukses") {
                            // tutup sweet alert
                            swal.close();
                            // tampilkan pesan sukses delete data
                            $.notify({
                                title   : '<i class="fas fa-check-circle title-icon"></i><strong>Sukses!</strong><br>',
                                message : 'Data Satuan berhasil dihapus.'
                            },{type: 'success'});
                            // tampilkan view data satuan
                            var table = $('#tabel-satuan').DataTable();
                            table.ajax.reload( null, false );
                        }
                        // jika gagal karena data "satuan" sudah ada di tabel "bahanbaku"
                        else if (result==="gagal") {
                            // tutup sweet alert
                            swal.close();
                            // tampilkan pesan gagal delete data
                            $.notify({
                                title   : '<i class="fas fa-times-circle title-icon"></i><strong>Gagal!</strong><br>',
                                message : 'Data Satuan <strong>'+ data[ 1 ] +'</strong> tidak bisa dihapus karena data satuan tersebut sudah tercatat pada data <strong>Bahan Baku</strong>.'
                            },{type: 'danger'});
                        }
                        // jika gagal karena script error
                        else {
                            // tutup sweet alert
                            swal.close();
                            // tampilkan pesan gagal delete data dan error result
                            $.notify({
                                title   : '<i class="fas fa-times-circle title-icon"></i><strong>Gagal!</strong><br>',
                                message : 'Data Satuan tidak bisa dihapus. Query Error : '+ result
                            },{type: 'danger'});
                        }
                    }
                });
            });
        });
        // ===============================================================================================
    });
    </script>
<?php } ?>
