<?php
session_start();      // memulai session

// Mengecek AJAX Request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')) {
    // panggil file "config.php" untuk koneksi ke database
    require_once "../../config/config.php";

    $stmt = FALSE;

    try {
        // ambil "data" hasil post dari ajax
        $id    = trim($_POST['id']);
        $pengiriman = trim($_POST['pengiriman']);
        $id_pemesanan = '';
        $query = "UPDATE pemesanan SET metode_pengiriman = :pengiriman WHERE id_pemesanan = :id_pemesanan";
        $stmt = $pdo->prepare($query);
        $stmt->bindParam(':id_pemesanan', $id);
        $stmt->bindParam(':pengiriman', $pengiriman);
        $stmt->execute();

        if ($stmt) {
            echo "sukses";
        }
        // tutup koneksi
        $pdo = null;
    } catch (PDOException $e) {
        // tampilkan pesan kesalahan
        echo $e->getMessage();
    }
} else {
    // jika tidak ada ajax request, maka alihkan ke halaman "login-error"
    echo '<script>window.location="../../login-error"</script>';
}
