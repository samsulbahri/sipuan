<?php
session_start();      // memulai session

// panggil file "config.php" untuk koneksi ke database
require_once "../../config/config.php";

try {
    // ambil "data" hasil post dari ajax
    $id_pemesanan = trim($_POST['id_pemesanan']);
    $kode_bahanbaku = trim($_POST['kode_bahanbaku']);
    $kode_supplier = trim($_POST['kode_supplier']);
    $rating = trim($_POST['rating']);
    $feedback = trim($_POST['feedback']);

    var_dump('Id pemesanan: ' . $id_pemesanan . 'Id bahan baku: ' . $kode_bahanbaku . 'Id supplier: ' . $kode_supplier . 'rating: ' . $rating . 'feedback: ' . $feedback);

    // sql statement untuk insert data ke tabel "bahanbaku"
    $query = "INSERT INTO feedback(id_rating, kode_bahanbaku, kode_supplier, feedback) VALUES (:id_rating, :kode_bahanbaku, :kode_supplier, :feedback); UPDATE pemesanan SET status = 'selesai' WHERE id_pemesanan = :id_pemesanan";
    // membuat prepared statements
    $stmt = $pdo->prepare($query);

    // hubungkan "data" dengan prepared statements
    $stmt->bindParam(':id_rating', $rating);
    $stmt->bindParam(':kode_bahanbaku', $kode_bahanbaku);
    $stmt->bindParam(':kode_supplier', $kode_supplier);
    $stmt->bindParam(':feedback', $feedback);
    $stmt->bindParam(':id_pemesanan', $id_pemesanan);

    // eksekusi query
    $stmt->execute();

    // cek query
    if ($stmt) {
        // jika berhasil tampilkan pesan "sukses"
        header('location: ../../pemesanan');
    }

    // tutup koneksi
    $pdo = null;
} catch (PDOException $e) {
    // tampilkan pesan kesalahan
    echo $e->getMessage();
}
