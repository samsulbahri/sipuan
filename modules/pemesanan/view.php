<?php
// fungsi untuk pengecekan status login user
// jika user belum login, alihkan ke halaman "login-error"
if (empty($_SESSION['username']) && empty($_SESSION['password'])) {
    echo "<meta http-equiv='refresh' content='0; url=../../login-error'>";
}
// jika user sudah login
else { ?>
    <div class="row mb-3">
        <div class="col-md-12">
            <h5>
                <!-- judul halaman tampil data bahanbaku -->
                <i class="fas fa-boxes title-icon"></i> Pemesanan Bahan Baku
                <a class="btn btn-success float-right" id="btnTambah" href="javascript:void(0);" data-toggle="modal" data-target="#modalbahanbaku" role="button">
                    <i class="fas fa-plus"></i> Tambah
                </a>
            </h5>
        </div>
    </div>

    <div class="border mb-4"></div>

    <?php
    $qp = "SELECT * FROM pemesanan as a, supplier as b WHERE a.kode_supplier = b.kode_supplier AND status='dipesan';";
    $sp = $pdo->prepare($qp);
    $sp->execute();
    if ($sp->rowCount() === 0) { ?>
        <div class="row">
            <div class="col text-center">
                <p class="text-muted">Tidak ada Pesanan</p>
            </div>
        </div>
    <?php }
    while ($pemesanan = $sp->fetch(PDO::FETCH_ASSOC)) { ?>
        <div class="row mb-3">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h6><?= $pemesanan['nama_supplier']; ?></h6>
                        <p class="text-muted"><?= $pemesanan['tanggal_pemesanan']; ?></p>
                    </div>
                    <div class="card-body">
                        <?php

                        $qd = "SELECT * FROM pemesanan_detail as a, bahanbaku as b, jenis_bahanbaku as c, satuan as d WHERE a.kode_bahanbaku = b.kode_bahanbaku AND b.id_jenis_bahanbaku = c.id_jenis_bahanbaku AND b.satuan = d.kode_satuan AND a.id_pemesanan = :id_pemesanan";

                        $sd = $pdo->prepare($qd);
                        $sd->bindParam(':id_pemesanan', $pemesanan['id_pemesanan']);
                        $sd->execute();
                        $totalPesanan = 0;

                        while ($detail = $sd->fetch(PDO::FETCH_ASSOC)) {
                            $totalPesanan += $detail['jumlah'] * $detail['harga_beli']; ?>
                            <div class="row">
                                <div class="col-1">
                                    <span onclick="deleteItem(<?= $detail['id_pemesanan_detail']; ?>)"><i class="fas fa-trash text-danger"></i></span>
                                </div>
                                <div class="col">
                                    <span><?= $detail['nama_bahanbaku']; ?></span>
                                    <br>
                                    <span class="text-muted"><?= $detail['jenis_bahanbaku']; ?></span>
                                </div>
                                <div class="col text-right">
                                    <p class="mb-1">
                                        <?= $detail['jumlah']; ?> x Rp. <?= number_format($detail['harga_beli'], 0, ',', '.'); ?> / <?= $detail['nama_satuan']; ?>
                                    </p>
                                    <br>
                                    <p class="text-muted">
                                        Rp. <?= number_format(($detail['jumlah'] * $detail['harga_beli']), 0, ',', '.'); ?>
                                    </p>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col">
                                Total Pembayaran: Rp. <?= number_format($totalPesanan, 0, ',', '.'); ?>
                            </div>
                            <div class="col">
                                <div class="float-right">
                                    <form action="" method="POST">
                                        <div class="form-inline">
                                            <select id="pengiriman" class="form-control mr-3" onchange="ubahMetodePengiriman(<?= $pemesanan['id_pemesanan']; ?>)">
                                                <option value="<?= $pemesanan['metode_pengiriman']; ?>">Selected: <?= $pemesanan['metode_pengiriman']; ?></option>
                                                <option value="JNE">JNE</option>
                                                <option value="JNT">JNT</option>
                                                <option value="SiCepat">SiCepat</option>
                                            </select>
                                            <a href="javascript:void(0);" class="btn btn-primary" onClick="makeModalConfirm(<?= $pemesanan['id_pemesanan']; ?>)">Pembayaran</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

    <!-- Modal form untuk entri dan ubah data bahanbaku -->
    <div class="modal fade" id="modalbahanbaku" tabindex="-1" role="dialog" aria-labelledby="modalbahanbaku" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <!-- judul form data bahanbaku -->
                <div class="modal-header">
                    <h5 class="modal-title"><i class="fas fa-edit title-icon"></i><span id="modalLabel"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!-- isi form data bahanbaku -->
                <form id="formbahanbaku">
                    <div class="modal-body">

                        <div class="form-group">
                            <label>Bahan Baku</label>
                            <select name="bahan_baku" id="bahan_baku" autocomplete="off" class="chosen-select">
                                <option value="">-- Pilih --</option>
                                <?php

                                try {
                                    $q = "SELECT * FROM bahanbaku";
                                    $stmt = $pdo->prepare($q);
                                    $stmt->execute();

                                    while ($b = $stmt->fetch(PDO::FETCH_ASSOC)) { ?>
                                        <option value="<?= $b['kode_bahanbaku']; ?>"><?= $b['nama_bahanbaku']; ?></option>
                                <?php }
                                } catch (PDOException $e) {
                                    echo $e->getMessage();
                                }

                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Supplier</label>
                            <select name="supplier" id="supplier" autocomplete="off" class="chosen-select">
                                <option value="">-- Pilih --</option>
                                <?php

                                try {
                                    $q = "SELECT * FROM supplier";
                                    $stmt = $pdo->prepare($q);
                                    $stmt->execute();

                                    while ($b = $stmt->fetch(PDO::FETCH_ASSOC)) { ?>
                                        <option value="<?= $b['kode_supplier']; ?>"><?= $b['nama_supplier']; ?></option>
                                <?php }
                                } catch (PDOException $e) {
                                    echo $e->getMessage();
                                }

                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Banyak Pesanan</label>
                            <input type="number" class="form-control" id="banyak_pesanan" name="banyak_pesanan" onKeyPress="return goodchars(event,'0123456789',this)" autocomplete="off">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success mr-2" id="btnSimpan">Simpan</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="modal-confirm">

    </div>

    <div id="modal-feedback">

    </div>


    <script type="text/javascript">
        function makeModalConfirm(id) {
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("modal-confirm").innerHTML = this.responseText;
                }
            };

            xmlhttp.open("GET", "modules/pemesanan/get_pemesanan.php?q=" + id, true);
            xmlhttp.send();

            $("#feedbackConfirm").modal('show');

        }

        function makeModalFeedback(id) {
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("modal-feedback").innerHTML = this.responseText;
                }
            };

            xmlhttp.open("GET", "modules/pemesanan/get_pemesanan.php?f=" + id, true);
            xmlhttp.send();

            $("#giveFeedback").modal('show');

        }

        function ubahMetodePengiriman(id) {
            var pengiriman = document.getElementById("pengiriman").value;
            $.ajax({
                type: "POST",
                url: "modules/pemesanan/update_expedition.php",
                data: {
                    pengiriman: pengiriman,
                    id: id
                },
                beforeSend: function() { // proses sebelum data dikirim
                    // tampilkan pesan pengecekan login
                    document.getElementById('btnSimpan').disabled = true;
                    $.notify('<i class="fas fa-sync title-icon"></i> Mengubah Ekspedisi ... <br><br>', {
                        allow_dismiss: false,
                    });
                },
                //data : data,                                    // data yang dikirim
                success: function(result) { // ketika proses insert data selesai
                    // jika berhasil
                    if (result === "sukses") {
                        setTimeout(function() {
                            // tampilkan pesan berhasil login
                            $.notify({
                                title: '<i class="fas fa-check-circle title-icon"></i><strong>Ekspedisi Berhasil Diubah!</strong><br>',
                                message: ' '
                            }, {
                                type: 'success',
                                allow_dismiss: false,
                                showProgressbar: false
                            });

                            // alihkan ke halaman admin
                            setTimeout('window.location = "keranjang";', 3000);
                        }, 3000);
                    }
                    // jika gagal
                    else {
                        // tampilkan pesan gagal insert data dan error result
                        $.notify({
                            title: '<i class="fas fa-times-circle title-icon"></i><strong>Gagal!</strong><br>',
                            message: 'Data Pemesanan Bahan Baku tidak bisa disimpan. Query Error : ' + result
                        }, {
                            type: 'danger'
                        });
                    }
                }
            });
        }

        function deleteItem(id) {
            $.ajax({
                type: "POST", // mengirim data dengan method POST
                url: "modules/pemesanan/delete_item.php",
                data: {
                    id: id
                },
                beforeSend: function() { // proses sebelum data dikirim
                    // tampilkan pesan pengecekan login
                    document.getElementById('btnSimpan').disabled = true;
                    $.notify('<i class="fas fa-sync title-icon"></i> Menghapus Pesanan ... <br><br>', {
                        allow_dismiss: false,
                    });
                },
                //data : data,                                    // data yang dikirim
                success: function(result) { // ketika proses insert data selesai
                    // jika berhasil
                    if (result === "sukses") {
                        setTimeout(function() {
                            // tampilkan pesan berhasil login
                            $.notify({
                                title: '<i class="fas fa-check-circle title-icon"></i><strong>Hapus Pemesanan Bahan Baku Berhasil!</strong><br>',
                                message: ' '
                            }, {
                                type: 'success',
                                allow_dismiss: false,
                                showProgressbar: false
                            });

                            // alihkan ke halaman admin
                            setTimeout('window.location = "keranjang";', 3000);
                        }, 3000);
                    }
                    // jika gagal
                    else {
                        // tampilkan pesan gagal insert data dan error result
                        $.notify({
                            title: '<i class="fas fa-times-circle title-icon"></i><strong>Gagal!</strong><br>',
                            message: 'Data Pemesanan Bahan Baku tidak bisa disimpan. Query Error : ' + result
                        }, {
                            type: 'danger'
                        });
                    }
                }
            });
            return false;
        }

        $(document).ready(function() {
            <?php if (isset($_SESSION['pesan'])) {
                if ($_SESSION['pesan'] == 'sukses-pembayaran') { ?>
                    $.notify({
                        title: '<i class="fas fa-times-circle title-icon"></i><strong>Berhasil!</strong><br>',
                        message: 'Pembayaran untuk Pemesanan Bahan Baku Berhasil dilakukan.'
                    }, {
                        type: 'success'
                    });
            <?php
                    unset($_SESSION['pesan']);
                }
            } ?>
            // ======================================= initiate plugin =======================================
            // chosen-select plugin
            $('.chosen-select').chosen();
            // ===============================================================================================
            // ===============================================================================================

            // ============================================ Form =============================================
            // Tampilkan Modal Form Entri Data
            $('#btnTambah').click(function() {
                // reset form
                $('#formbahanbaku')[0].reset();
                $('#satuan').val('').trigger('chosen:updated');
                // judul form
                $('#modalLabel').text('Entri Data Pemesanan Bahan Baku');
            });
            // ===============================================================================================

            // ====================================== Insert dan Update ======================================
            // Proses Simpan Data
            $('#btnSimpan').click(function() {
                // validasi form input
                // jika nama bahanbaku kosong
                if ($('#bahan_baku').val() == "") {
                    // focus ke input nama bahanbaku
                    $("#bahan_baku").focus();
                    // tampilkan peringatan data tidak boleh kosong
                    $.notify({
                        title: '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                        message: 'Nama bahan baku tidak boleh kosong.'
                    }, {
                        type: 'warning'
                    });
                }
                // jika satuan kosong
                else if ($('#supplier').val() == "") {
                    // focus ke input supplier
                    $("#supplier").focus();
                    // tampilkan peringatan data tidak boleh kosong
                    $.notify({
                        title: '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                        message: 'Supplier tidak boleh kosong.'
                    }, {
                        type: 'warning'
                    });
                }
                // jika minimum stok kosong atau 0 (nol)
                else if ($('#banyak_pesanan').val() == "" || $('#banyak_pesanan').val() == 0) {
                    // focus ke input minimum stok
                    $("#banyak_pesanan").focus();
                    // tampilkan peringatan data tidak boleh kosong atau 0 (nol)
                    $.notify({
                        title: '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                        message: 'Minimum Stok tidak boleh kosong atau 0 (nol).'
                    }, {
                        type: 'warning'
                    });
                }
                // jika semua data sudah terisi, jalankan perintah insert / update data
                else {
                    // jika form entri data bahanbaku yang ditampilkan, jalankan perintah insert
                    if ($('#modalLabel').text() == "Entri Data Pemesanan Bahan Baku") {
                        // membuat variabel untuk menampung data dari form entri data bahanbaku
                        var data = $('#formbahanbaku').serialize();
                        $.ajax({
                            type: "POST", // mengirim data dengan method POST
                            url: "modules/pemesanan/insert.php", // proses insert data
                            data: data,
                            beforeSend: function() { // proses sebelum data dikirim
                                // tampilkan pesan pengecekan login
                                document.getElementById('btnSimpan').disabled = true;
                                $.notify('<i class="fas fa-sync title-icon"></i> Menambahkan Data Pemesanan ... <br><br>', {
                                    allow_dismiss: false,
                                });
                            },
                            //data : data,                                    // data yang dikirim
                            success: function(result) { // ketika proses insert data selesai
                                // jika berhasil
                                if (result === "sukses") {
                                    setTimeout(function() {
                                        // tampilkan pesan berhasil login
                                        $.notify({
                                            title: '<i class="fas fa-check-circle title-icon"></i><strong>Pemesanan Berhasil!</strong><br>',
                                            message: 'Segera lakukan pembayaran agar pesanan anda dapat diproses.. <br><br>'
                                        }, {
                                            type: 'success',
                                            allow_dismiss: false,
                                            showProgressbar: false
                                        });

                                        // alihkan ke halaman admin
                                        setTimeout('window.location = "keranjang";', 3000);
                                    }, 3000);
                                }
                                // jika gagal
                                else {
                                    // tampilkan pesan gagal insert data dan error result
                                    $.notify({
                                        title: '<i class="fas fa-times-circle title-icon"></i><strong>Gagal!</strong><br>',
                                        message: 'Data Pemesanan Bahan Baku tidak bisa disimpan. Query Error : ' + result
                                    }, {
                                        type: 'danger'
                                    });
                                }
                            }
                        });
                        return false;
                    }
                }
            });
            // ===============================================================================================

            // =========================================== Delete ============================================
            $('#tabel-bahanbaku tbody').on('click', '.btnHapus', function() {
                // ambil data dari datatables
                var data = table.row($(this).parents('tr')).data();
                // tampilkan notifikasi saat akan menghapus data
                swal({
                        title: "Apakah Anda Yakin?",
                        text: "Anda akan menghapus data Pemesanan Bahan Baku : " + data[2] + "-" + data[7],
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ya, Hapus!",
                        closeOnConfirm: false
                    },
                    // jika dipilih ya, maka jalankan perintah delete data
                    function() {
                        // membuat variabel untuk menampung data bahanbaku
                        var kode_bahanbaku = data[1];

                        $.ajax({
                            type: "POST", // mengirim data dengan method POST
                            url: "modules/pemesanan/delete.php", // proses delete data
                            data: {
                                kode_bahanbaku: kode_bahanbaku
                            }, // data yang dikirim
                            success: function(result) { // ketika proses delete data selesai
                                // jika berhasil
                                if (result === "sukses") {
                                    // tutup sweet alert
                                    swal.close();
                                    // tampilkan pesan sukses delete data
                                    $.notify({
                                        title: '<i class="fas fa-check-circle title-icon"></i><strong>Sukses!</strong><br>',
                                        message: 'Data Pemesanan Bahan Baku berhasil dihapus.'
                                    }, {
                                        type: 'success'
                                    });
                                    // tampilkan view data bahanbaku
                                    var table = $('#tabel-bahanbaku').DataTable();
                                    table.ajax.reload(null, false);
                                }
                                // jika gagal karena data "bahanbaku" sudah tercatat di tabel pembelian / penjualan
                                else if (result === "gagal") {
                                    // tutup sweet alert
                                    swal.close();
                                    // tampilkan pesan gagal delete data
                                    $.notify({
                                        title: '<i class="fas fa-times-circle title-icon"></i><strong>Gagal!</strong><br>',
                                        message: 'Data Pemesanan Bahan Baku gagal dihapus.'
                                    }, {
                                        type: 'danger'
                                    });
                                }
                                // jika gagal karena script error
                                else {
                                    // tutup sweet alert
                                    swal.close();
                                    // tampilkan pesan gagal delete data dan error result
                                    $.notify({
                                        title: '<i class="fas fa-times-circle title-icon"></i><strong>Gagal!</strong><br>',
                                        message: 'Data Pemesanan Bahan Baku tidak bisa dihapus. Query Error : ' + result
                                    }, {
                                        type: 'danger'
                                    });
                                }
                            }
                        });
                    });
            });
            // ===============================================================================================
        });

        var rating = 1;

        function setStar(star, that) {
            if (star === 1) {
                poin = '<input id="rating" name="rating" type="hidden" value="1"><i id="star-one" class="fas fa-star mr-2 orange" onclick="setStar(1, this.parentElement);"></i> <i id = "star-two" class = "fas fa-star mr-2" onclick = "setStar(2, this.parentElement);"></i> <i id = "star-three" class = "fas fa-star mr-2" onclick = "setStar(3, this.parentElement);"></i> <i id = "star-four" class = "fas fa-star mr-2" onclick = "setStar(4, this.parentElement);"></i> <i id = "star-five" class = "fas fa-star mr-2"onclick = "setStar(5, this.parentElement);"></i>';
            } else if (star === 2) {
                poin = '<input id="rating" name="rating" type="hidden" value="2"><i id="star-one" class="fas fa-star mr-2 orange" onclick="setStar(1, this.parentElement);"></i> <i id = "star-two" class = "fas fa-star mr-2 orange" onclick = "setStar(2, this.parentElement);"></i> <i id = "star-three" class = "fas fa-star mr-2" onclick = "setStar(3, this.parentElement);"></i> <i id = "star-four" class = "fas fa-star mr-2" onclick = "setStar(4, this.parentElement);"></i> <i id = "star-five" class = "fas fa-star mr-2"onclick = "setStar(5, this.parentElement);"></i>';
            } else if (star === 3) {
                poin = '<input id="rating" name="rating" type="hidden" value="3"><i id="star-one" class="fas fa-star mr-2 orange" onclick="setStar(1, this.parentElement);"></i> <i id = "star-two" class = "fas fa-star mr-2 orange" onclick = "setStar(2, this.parentElement);"></i> <i id = "star-three" class = "fas fa-star mr-2 orange" onclick = "setStar(3, this.parentElement);"></i> <i id = "star-four" class = "fas fa-star mr-2" onclick = "setStar(4, this.parentElement);"></i> <i id = "star-five" class = "fas fa-star mr-2"onclick = "setStar(5, this.parentElement);"></i>';
            } else if (star === 4) {
                poin = '<input id="rating" name="rating" type="hidden" value="4"><i id="star-one" class="fas fa-star mr-2 orange" onclick="setStar(1, this.parentElement);"></i> <i id = "star-two" class = "fas fa-star mr-2 orange" onclick = "setStar(2, this.parentElement);"></i> <i id = "star-three" class = "fas fa-star mr-2 orange" onclick = "setStar(3, this.parentElement);"></i> <i id = "star-four" class = "fas fa-star mr-2 orange" onclick = "setStar(4, this.parentElement);"></i> <i id = "star-five" class = "fas fa-star mr-2"onclick = "setStar(5, this.parentElement);"></i>';
            } else if (star === 5) {
                poin = '<input id="rating" name="rating" type="hidden" value="5"><i id="star-one" class="fas fa-star mr-2 orange" onclick="setStar(1, this.parentElement);"></i> <i id = "star-two" class = "fas fa-star mr-2 orange" onclick = "setStar(2, this.parentElement);"></i> <i id = "star-three" class = "fas fa-star mr-2 orange" onclick = "setStar(3, this.parentElement);"></i> <i id = "star-four" class = "fas fa-star mr-2 orange" onclick = "setStar(4, this.parentElement);"></i> <i id = "star-five" class = "fas fa-star mr-2 orange"onclick = "setStar(5, this.parentElement);"></i>';
            }
            that.innerHTML = poin;
        }
    </script>
<?php } ?>