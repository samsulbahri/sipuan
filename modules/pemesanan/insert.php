<?php
session_start();      // memulai session

// Mengecek AJAX Request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')) {
    // panggil file "config.php" untuk koneksi ke database
    require_once "../../config/config.php";

    $stmt = FALSE;

    try {
        // ambil "data" hasil post dari ajax
        $bahan_baku    = trim($_POST['bahan_baku']);
        $supplier    = trim($_POST['supplier']);
        $banyak_pesanan     = trim($_POST['banyak_pesanan']);
        // ambil "data" dari session
        $created_user = $_SESSION['id_user'];

        $queryCek = "SELECT * FROM pemesanan WHERE kode_supplier = :kode_supplier AND status = 'dipesan'";

        $stmtCek = $pdo->prepare($queryCek);

        $stmtCek->bindParam(':kode_supplier', $supplier);

        $stmtCek->execute();

        if ($stmtCek->rowCount() === 0) {
            $queryInsertBase = "INSERT INTO pemesanan(kode_supplier, no_pembelian, metode_pengiriman, status) VALUES (:kode_supplier, 0, 'JNE', 'dipesan')";

            $stmtInsertBase = $pdo->prepare($queryInsertBase);
            $stmtInsertBase->bindParam(':kode_supplier', $supplier);
            $stmtInsertBase->execute();
        }

        $queryGet = "SELECT * FROM pemesanan WHERE kode_supplier = :kode_supplier AND status = 'dipesan'";
        $stmtGet = $pdo->prepare($queryGet);
        $stmtGet->bindParam(':kode_supplier', $supplier);
        $stmtGet->execute();

        while ($pemesanan = $stmtGet->fetch(PDO::FETCH_ASSOC)) {
            // sql statement untuk insert data ke tabel "bahanbaku"
            $query = "INSERT INTO pemesanan_detail(id_pemesanan, kode_bahanbaku, jumlah)
				  VALUES (:id_pemesanan, :kode_bahanbaku, :banyak_pesanan)";
            // membuat prepared statements
            $stmt = $pdo->prepare($query);

            // hubungkan "data" dengan prepared statements
            $stmt->bindParam(':kode_bahanbaku', $bahan_baku);
            $stmt->bindParam(':id_pemesanan', $pemesanan['id_pemesanan']);
            $stmt->bindParam(':banyak_pesanan', $banyak_pesanan);

            // eksekusi query
            $stmt->execute();
        }

        // cek query
        if ($stmt) {
            // jika berhasil tampilkan pesan "sukses"
            echo "sukses";
        }

        // tutup koneksi
        $pdo = null;
    } catch (PDOException $e) {
        // tampilkan pesan kesalahan
        echo $e->getMessage();
    }
} else {
    // jika tidak ada ajax request, maka alihkan ke halaman "login-error"
    echo '<script>window.location="../../login-error"</script>';
}
