<?php
// Mengecek AJAX Request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')) {

    // sql statement untuk join table
    $table = <<<EOT
(
    SELECT c.status, c.id_pemesanan, a.kode_bahanbaku,a.nama_bahanbaku,a.harga_beli,a.harga_jual,a.satuan,a.min_stok,a.stok,b.nama_satuan, d.kode_supplier, d.nama_supplier, c.quantity
    FROM bahanbaku as a, satuan as b, pemesanan as c, supplier as d WHERE a.satuan=b.kode_satuan AND c.kode_bahanbaku=a.kode_bahanbaku AND c.kode_supplier=d.kode_supplier
) temp
EOT;

    // primary key tabel
    $primaryKey = 'kode_bahanbaku';

    // membuat array untuk menampilkan isi tabel.
    // Parameter 'db' mewakili nama kolom dalam database.
    // parameter 'dt' mewakili pengenal kolom pada DataTable.
    $columns = array(
        array('db' => 'kode_bahanbaku', 'dt' => 1),
        array('db' => 'nama_bahanbaku', 'dt' => 2),
        array('db' => 'harga_beli', 'dt' => 3),
        array('db' => 'nama_satuan', 'dt' => 4),
        array('db' => 'stok', 'dt' => 5),
        array('db' => 'quantity', 'dt' => 6),
        array('db' => 'nama_supplier', 'dt' => 7),
        array('db' => 'id_pemesanan', 'dt' => 8),
        array('db' => 'status', 'dt' => 9)
    );

    // memanggil file "database.php" untuk informasi koneksi ke server SQL
    require_once "../../config/database.php";
    // memanggil file "ssp.class.php" untuk menjalankan datatables server-side processing
    require '../../config/ssp.class.php';

    echo json_encode(
        SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns)
    );
} else {
    // jika tidak ada ajax request, maka alihkan ke halaman "login-error"
    echo '<script>window.location="../../login-error"</script>';
}
