<?php
// Mengecek AJAX Request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')) {
    // panggil file "config.php" untuk koneksi ke database
    require_once "config/config.php";

    // mengecek data post dari ajax
    if (isset($_POST['username'])) {
        try {
            // ambil "data" post dari ajax
            $username = trim($_POST['username']);

            // sql statement untuk menampilkan data dari tabel "sys_users" berdasarkan username, password, dan blokir
            $query = "SELECT * FROM sys_users WHERE username=:username";
            // membuat prepared statements
            $stmt = $pdo->prepare($query);

            // hubungkan "data" dengan prepared statements
            $stmt->bindParam(':username', $username);

            // eksekusi query
            $stmt->execute();

            // cek hasil query
            // jika data ada, jalankan perintah untuk membuat session
            if ($stmt->rowCount() <> 0) {
                $queryInsert = "UPDATE sys_users SET isForgetPassword = 1, updated_user = 1 WHERE username = :username";

                $stmtInsert = $pdo->prepare($queryInsert);

                // hubungkan "data" dengan prepared statements
                $stmtInsert->bindParam(':username', $username);

                // eksekusi query
                $stmtInsert->execute();

                echo 'sukses';
            }
            // jika data tidak ada
            else {
                echo 'tidak ditemukan';
            }
        } catch (Exception $e) {
            // tampilkan pesan kesalahan
            echo $e->getMessage();
        }
    }
    // tutup koneksi
    $pdo = null;
} else {
    // jika tidak ada ajax request, maka alihkan ke halaman "login-error"
    echo '<script>window.location="login-error"</script>';
}
