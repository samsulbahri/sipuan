<style>
	a {
		font-size: 13px;
	}
</style>
<?php
// fungsi untuk pengecekan status login user
// jika user belum login, alihkan ke halaman "login-error"
if (empty($_SESSION['username']) && empty($_SESSION['password'])) {
	echo "<meta http-equiv='refresh' content='0; url=login-error'>";
}
// jika user sudah login
else {
	// mengecek hak akses user untuk menampilkan menu sesuai dengan hak akses
	// jika "hak_akses = Super Admin", tampilkan menu
	if ($_SESSION['hak_akses'] == 'Super Admin') {
		// pengecekan menu aktif
		// jika menu beranda dipilih, menu beranda aktif
		if ($_GET["module"] == "beranda") { ?>
			<li class="nav-item active">
				<a class="nav-link mr-2 menu" href="beranda">
					<i class="fas fa-home title-icon"></i> Beranda
				</a>
			</li>
		<?php
		}
		// jika tidak, menu beranda tidak aktif
		else { ?>
			<li class="nav-item">
				<a class="nav-link mr-2 menu" href="beranda">
					<i class="fas fa-home title-icon"></i> Beranda
				</a>
			</li>
		<?php
		}

		// jika menu supplier dipilih, menu supplier aktif
		if ($_GET["module"] == "supplier") { ?>
			<li class="nav-item active">
				<a class="nav-link mr-2 menu" href="supplier">
					<i class="fas fa-user-friends title-icon"></i> Supplier
				</a>
			</li>
		<?php
		}
		// jika tidak, menu supplier tidak aktif
		else { ?>
			<li class="nav-item">
				<a class="nav-link mr-2 menu" href="supplier">
					<i class="fas fa-user-friends title-icon"></i> Supplier
				</a>
			</li>
		<?php } ?>

		<?php if ($_GET["module"] == "rekomendasi") { ?>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle mr-2" href="javascript:void(0);" id="navbarbahanbaku" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-box title-icon"></i> Penilaian
				</a>
				<!-- Dropdown Menu -->
				<div class="dropdown-menu" aria-labelledby="navbarbahanbaku">
					<a class="dropdown-item menu active" href="rekomendasi">
						<i class="fas fa-angle-right title-icon"></i> Rekomendasi
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="rating">
						<i class="fas fa-angle-right title-icon"></i> Rating Keseluruhan
					</a>
				</div>
			</li>
		<?php } else if ($_GET['module'] == "rating") { ?>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle mr-2" href="javascript:void(0);" id="navbarbahanbaku" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-box title-icon"></i> Penilaian
				</a>
				<!-- Dropdown Menu -->
				<div class="dropdown-menu" aria-labelledby="navbarbahanbaku">
					<a class="dropdown-item menu" href="rekomendasi">
						<i class="fas fa-angle-right title-icon"></i> Rekomendasi
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu active" href="rating">
						<i class="fas fa-angle-right title-icon"></i> Rating Keseluruhan
					</a>
				</div>
			</li>
		<?php } else { ?>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle mr-2" href="javascript:void(0);" id="navbarbahanbaku" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-box title-icon"></i> Penilaian
				</a>
				<!-- Dropdown Menu -->
				<div class="dropdown-menu" aria-labelledby="navbarbahanbaku">
					<a class="dropdown-item menu" href="rekomendasi">
						<i class="fas fa-angle-right title-icon"></i> Rekomendasi
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="rating">
						<i class="fas fa-angle-right title-icon"></i> Rating Keseluruhan
					</a>
				</div>
			</li>
		<?php } ?>

		<!-- // jika menu bahanbaku dipilih, menu bahanbaku aktif -->
		<?php if ($_GET["module"] == "bahanbaku") { ?>
			<li class="nav-item active dropdown">
				<a class="nav-link dropdown-toggle mr-2" href="javascript:void(0);" id="navbarbahanbaku" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-box title-icon"></i> Bahan Baku
				</a>
				<!-- Dropdown Menu -->
				<div class="dropdown-menu" aria-labelledby="navbarbahanbaku">
					<a class="dropdown-item menu active" href="bahanbaku">
						<i class="fas fa-angle-right title-icon"></i> Bahan Baku
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="satuan">
						<i class="fas fa-angle-right title-icon"></i> Satuan
					</a>

					<div class="dropdown-divider"></div>

					<a href="jenis-bahan-baku" class="dropdown-item menu">
						<i class="fas fa-angle-right title-icon"></i> Jenis Bahan Baku
					</a>
				</div>
			</li>
		<?php
		}
		// jika menu satuan dipilih, menu satuan aktif
		elseif ($_GET["module"] == "satuan") { ?>
			<li class="nav-item active dropdown">
				<a class="nav-link dropdown-toggle mr-2" href="javascript:void(0);" id="navbarbahanbaku" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-box title-icon"></i> Bahan Baku
				</a>
				<!-- Dropdown Menu -->
				<div class="dropdown-menu" aria-labelledby="navbarbahanbaku">
					<a class="dropdown-item menu" href="bahanbaku">
						<i class="fas fa-angle-right title-icon"></i> Bahan Baku
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu active" href="satuan">
						<i class="fas fa-angle-right title-icon"></i> Satuan
					</a>

					<div class="dropdown-divider"></div>

					<a href="jenis-bahan-baku" class="dropdown-item menu">
						<i class="fas fa-angle-right title-icon"></i> Jenis Bahan Baku
					</a>
				</div>
			</li>
		<?php

		} elseif ($_GET["module"] == "jenis-bahan-baku") { ?>
			<li class="nav-item active dropdown">
				<a class="nav-link dropdown-toggle mr-2" href="javascript:void(0);" id="navbarbahanbaku" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-box title-icon"></i> Bahan Baku
				</a>
				<!-- Dropdown Menu -->
				<div class="dropdown-menu" aria-labelledby="navbarbahanbaku">
					<a class="dropdown-item menu" href="bahanbaku">
						<i class="fas fa-angle-right title-icon"></i> Bahan Baku
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="satuan">
						<i class="fas fa-angle-right title-icon"></i> Satuan
					</a>

					<div class="dropdown-divider"></div>

					<a href="jenis-bahan-baku" class="dropdown-item menu active">
						<i class="fas fa-angle-right title-icon"></i> Jenis Bahan Baku
					</a>
				</div>
			</li>
		<?php
		} elseif (strpos($_GET['module'], 'bahanbaku-by-jenis') !== false) { ?>
			<li class="nav-item active dropdown">
				<a class="nav-link dropdown-toggle mr-2" href="javascript:void(0);" id="navbarbahanbaku" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-box title-icon"></i> Bahan Baku
				</a>
				<!-- Dropdown Menu -->
				<div class="dropdown-menu" aria-labelledby="navbarbahanbaku">
					<a class="dropdown-item menu active" href="bahanbaku">
						<i class="fas fa-angle-right title-icon"></i> Bahan Baku
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="satuan">
						<i class="fas fa-angle-right title-icon"></i> Satuan
					</a>

					<div class="dropdown-divider"></div>

					<a href="jenis-bahan-baku" class="dropdown-item menu">
						<i class="fas fa-angle-right title-icon"></i> Jenis Bahan Baku
					</a>
				</div>
			</li>
		<?php
		}
		// jika tidak, menu bahanbaku tidak aktif
		else { ?>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle mr-2" href="javascript:void(0);" id="navbarbahanbaku" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-box title-icon"></i> Bahan Baku
				</a>
				<!-- Dropdown Menu -->
				<div class="dropdown-menu" aria-labelledby="navbarbahanbaku">
					<a class="dropdown-item menu" href="bahanbaku">
						<i class="fas fa-angle-right title-icon"></i> Bahan Baku
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="satuan">
						<i class="fas fa-angle-right title-icon"></i> Satuan
					</a>

					<div class="dropdown-divider"></div>

					<a href="jenis-bahan-baku" class="dropdown-item menu">
						<i class="fas fa-angle-right title-icon"></i> Jenis Bahan Baku
					</a>
				</div>
			</li>
		<?php
		}

		// jika menu pembelian dipilih, menu pembelian aktif
		if ($_GET["module"] == "pembelian") { ?>
			<li class="nav-item active">
				<a class="nav-link mr-2 menu" href="pembelian">
					<i class="fas fa-cart-plus title-icon"></i> Pembelian
				</a>
			</li>
		<?php
		}
		// jika tidak, menu pembelian tidak aktif
		else { ?>
			<li class="nav-item">
				<a class="nav-link mr-2 menu" href="pembelian">
					<i class="fas fa-cart-plus title-icon"></i> Pembelian
				</a>
			</li>
		<?php
		}

		// jika menu penjualan dipilih, menu penjualan aktif
		if ($_GET["module"] == "penjualan") { ?>
			<li class="nav-item active">
				<a class="nav-link mr-2 menu" href="penjualan">
					<i class="fas fa-people-carry title-icon"></i> Pemakaian
				</a>
			</li>
		<?php
		}
		// jika tidak, menu penjualan tidak aktif
		else { ?>
			<li class="nav-item">
				<a class="nav-link mr-2 menu" href="penjualan">
					<i class="fas fa-people-carry title-icon"></i> Pemakaian
				</a>
			</li>
		<?php
		}

		// jika menu laporan stok dipilih, menu laporan stok aktif
		if ($_GET["module"] == "lap_stok") { ?>
			<li class="nav-item active">
				<a class="nav-link mr-2 menu" href="laporan-stok">
					<i class="fas fa-file-alt title-icon"></i> Laporan Stok
				</a>
			</li>
		<?php
		}
		// jika tidak, menu laporan stok tidak aktif
		else { ?>
			<li class="nav-item">
				<a class="nav-link mr-2 menu" href="laporan-stok">
					<i class="fas fa-file-alt title-icon"></i> Laporan Stok
				</a>
			</li>
		<?php
		}

		// jika menu Riwayat Pembelian per periode dipilih, menu Riwayat Pembelian per periode aktif
		if ($_GET["module"] == "lap_pembelian_per_periode") { ?>
			<li class="nav-item active dropdown">
				<a class="nav-link dropdown-toggle mr-2" href="javascript:void(0);" id="navbarLaporanPembelian" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-file-invoice title-icon"></i> Riwayat Pembelian
				</a>
				<!-- Dropdown Menu -->
				<div class="dropdown-menu" aria-labelledby="navbarLaporanPembelian">
					<a class="dropdown-item menu active" href="laporan-pembelian-per-periode">
						<i class="fas fa-angle-right title-icon"></i> Per Periode
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="laporan-pembelian-perincian">
						<i class="fas fa-angle-right title-icon"></i> Perincian
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="laporan-pembelian-per-bahanbaku">
						<i class="fas fa-angle-right title-icon"></i> Per Bahan Baku
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="laporan-pembelian-per-supplier">
						<i class="fas fa-angle-right title-icon"></i> Per Supplier
					</a>
				</div>
			</li>
		<?php
		}
		// jika menu Riwayat Pembelian perincian dipilih, menu Riwayat Pembelian perincian aktif
		elseif ($_GET["module"] == "lap_pembelian_perincian") { ?>
			<li class="nav-item active dropdown">
				<a class="nav-link dropdown-toggle mr-2" href="javascript:void(0);" id="navbarLaporanPembelian" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-file-invoice title-icon"></i> Riwayat Pembelian
				</a>
				<!-- Dropdown Menu -->
				<div class="dropdown-menu" aria-labelledby="navbarLaporanPembelian">
					<a class="dropdown-item menu" href="laporan-pembelian-per-periode">
						<i class="fas fa-angle-right title-icon"></i> Per Periode
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu active" href="laporan-pembelian-perincian">
						<i class="fas fa-angle-right title-icon"></i> Perincian
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="laporan-pembelian-per-bahanbaku">
						<i class="fas fa-angle-right title-icon"></i> Per Bahan Baku
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="laporan-pembelian-per-supplier">
						<i class="fas fa-angle-right title-icon"></i> Per Supplier
					</a>
				</div>
			</li>
		<?php
		}
		// jika menu Riwayat Pembelian Per Bahan Baku dipilih, menu Riwayat Pembelian Per Bahan Baku aktif
		elseif ($_GET["module"] == "lap_pembelian_per_barang") { ?>
			<li class="nav-item active dropdown">
				<a class="nav-link dropdown-toggle mr-2" href="javascript:void(0);" id="navbarLaporanPembelian" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-file-invoice title-icon"></i> Riwayat Pembelian
				</a>
				<!-- Dropdown Menu -->
				<div class="dropdown-menu" aria-labelledby="navbarLaporanPembelian">
					<a class="dropdown-item menu" href="laporan-pembelian-per-periode">
						<i class="fas fa-angle-right title-icon"></i> Per Periode
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="laporan-pembelian-perincian">
						<i class="fas fa-angle-right title-icon"></i> Perincian
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu active" href="laporan-pembelian-per-bahanbaku">
						<i class="fas fa-angle-right title-icon"></i> Per Bahan Baku
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="laporan-pembelian-per-supplier">
						<i class="fas fa-angle-right title-icon"></i> Per Supplier
					</a>
				</div>
			</li>
		<?php
		}
		// jika menu Riwayat Pembelian per supplier dipilih, menu Riwayat Pembelian per supplier aktif
		elseif ($_GET["module"] == "lap_pembelian_per_supplier") { ?>
			<li class="nav-item active dropdown">
				<a class="nav-link dropdown-toggle mr-2" href="javascript:void(0);" id="navbarLaporanPembelian" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-file-invoice title-icon"></i> Riwayat Pembelian
				</a>
				<!-- Dropdown Menu -->
				<div class="dropdown-menu" aria-labelledby="navbarLaporanPembelian">
					<a class="dropdown-item menu" href="laporan-pembelian-per-periode">
						<i class="fas fa-angle-right title-icon"></i> Per Periode
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="laporan-pembelian-perincian">
						<i class="fas fa-angle-right title-icon"></i> Perincian
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="laporan-pembelian-per-bahanbaku">
						<i class="fas fa-angle-right title-icon"></i> Per Bahan Baku
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu active" href="laporan-pembelian-per-supplier">
						<i class="fas fa-angle-right title-icon"></i> Per Supplier
					</a>
				</div>
			</li>
		<?php
		}
		// jika tidak, menu Riwayat Pembelian tidak aktif
		else { ?>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle mr-2" href="javascript:void(0);" id="navbarLaporanPembelian" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-file-invoice title-icon"></i> Riwayat Pembelian
				</a>
				<!-- Dropdown Menu -->
				<div class="dropdown-menu" aria-labelledby="navbarLaporanPembelian">
					<a class="dropdown-item menu" href="laporan-pembelian-per-periode">
						<i class="fas fa-angle-right title-icon"></i> Per Periode
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="laporan-pembelian-perincian">
						<i class="fas fa-angle-right title-icon"></i> Perincian
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="laporan-pembelian-per-bahanbaku">
						<i class="fas fa-angle-right title-icon"></i> Per Bahan Baku
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="laporan-pembelian-per-supplier">
						<i class="fas fa-angle-right title-icon"></i> Per Supplier
					</a>
				</div>
			</li>
		<?php
		}

		// jika menu laporan penjualan per periode dipilih, menu laporan penjualan per periode aktif
		if ($_GET["module"] == "lap_penjualan_per_periode") { ?>
			<li class="nav-item active dropdown">
				<a class="nav-link dropdown-toggle mr-2" href="javascript:void(0);" id="navbarLaporanpenjualan" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-clipboard title-icon"></i> Laporan Pemakaian
				</a>
				<!-- Dropdown Menu -->
				<div class="dropdown-menu" aria-labelledby="navbarLaporanpenjualan">
					<a class="dropdown-item menu active" href="laporan-penjualan-per-periode">
						<i class="fas fa-angle-right title-icon"></i> Per Periode
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="laporan-penjualan-perincian">
						<i class="fas fa-angle-right title-icon"></i> Perincian
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="laporan-penjualan-per-bahanbaku">
						<i class="fas fa-angle-right title-icon"></i> Per Bahan Baku
					</a>
				</div>
			</li>
		<?php
		}
		// jika menu laporan penjualan perincian dipilih, menu laporan penjualan perincian aktif
		elseif ($_GET["module"] == "lap_penjualan_perincian") { ?>
			<li class="nav-item active dropdown">
				<a class="nav-link dropdown-toggle mr-2" href="javascript:void(0);" id="navbarLaporanpenjualan" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-clipboard title-icon"></i> Laporan Pemakaian
				</a>
				<!-- Dropdown Menu -->
				<div class="dropdown-menu" aria-labelledby="navbarLaporanpenjualan">
					<a class="dropdown-item menu" href="laporan-penjualan-per-periode">
						<i class="fas fa-angle-right title-icon"></i> Per Periode
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu active" href="laporan-penjualan-perincian">
						<i class="fas fa-angle-right title-icon"></i> Perincian
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="laporan-penjualan-per-bahanbaku">
						<i class="fas fa-angle-right title-icon"></i> Per Bahan Baku
					</a>
				</div>
			</li>
		<?php
		}
		// jika menu laporan penjualan Per Bahan Baku dipilih, menu laporan penjualan Per Bahan Baku aktif
		elseif ($_GET["module"] == "lap_penjualan_per_barang") { ?>
			<li class="nav-item active dropdown">
				<a class="nav-link dropdown-toggle mr-2" href="javascript:void(0);" id="navbarLaporanpenjualan" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-clipboard title-icon"></i> Laporan Pemakaian
				</a>
				<!-- Dropdown Menu -->
				<div class="dropdown-menu" aria-labelledby="navbarLaporanpenjualan">
					<a class="dropdown-item menu" href="laporan-penjualan-per-periode">
						<i class="fas fa-angle-right title-icon"></i> Per Periode
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="laporan-penjualan-perincian">
						<i class="fas fa-angle-right title-icon"></i> Perincian
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu active" href="laporan-penjualan-per-bahanbaku">
						<i class="fas fa-angle-right title-icon"></i> Per Bahan Baku
					</a>
				</div>
			</li>
		<?php
		}
		// jika tidak, menu laporan penjualan tidak aktif
		else { ?>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle mr-2" href="javascript:void(0);" id="navbarLaporanpenjualan" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-clipboard title-icon"></i> Laporan Pemakaian
				</a>
				<!-- Dropdown Menu -->
				<div class="dropdown-menu" aria-labelledby="navbarLaporanpenjualan">
					<a class="dropdown-item menu" href="laporan-penjualan-per-periode">
						<i class="fas fa-angle-right title-icon"></i> Per Periode
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="laporan-penjualan-perincian">
						<i class="fas fa-angle-right title-icon"></i> Perincian
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="laporan-penjualan-per-bahanbaku">
						<i class="fas fa-angle-right title-icon"></i> Per Bahan Baku
					</a>
				</div>
			</li>
		<?php
		}

		// jika menu konfigurasi aplikasi dipilih, menu konfigurasi aplikasi aktif
		if ($_GET["module"] == "config") { ?>
			<li class="nav-item active dropdown">
				<a class="nav-link dropdown-toggle mr-2" href="javascript:void(0);" id="navbarUtility" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-user-cog title-icon"></i> Utility
				</a>
				<!-- Dropdown Menu -->
				<div class="dropdown-menu" aria-labelledby="navbarUtility">
					<a class="dropdown-item menu active" href="konfigurasi-aplikasi">
						<i class="fas fa-angle-right title-icon"></i> Konfigurasi Aplikasi
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="manajemen-user">
						<i class="fas fa-angle-right title-icon"></i> Manajemen User
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="backup-database">
						<i class="fas fa-angle-right title-icon"></i> Backup Database
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="audit-trail">
						<i class="fas fa-angle-right title-icon"></i> Audit Trail
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="user-password">
						<i class="fas fa-angle-right title-icon"></i> Permohonan Pergantian Password
					</a>
				</div>
			</li>
		<?php
		}
		// jika menu manajemen user dipilih, menu manajemen user aktif
		elseif ($_GET["module"] == "user") { ?>
			<li class="nav-item active dropdown">
				<a class="nav-link dropdown-toggle mr-2" href="javascript:void(0);" id="navbarUtility" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-user-cog title-icon"></i> Utility
				</a>
				<!-- Dropdown Menu -->
				<div class="dropdown-menu" aria-labelledby="navbarUtility">
					<a class="dropdown-item menu" href="konfigurasi-aplikasi">
						<i class="fas fa-angle-right title-icon"></i> Konfigurasi Aplikasi
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu active" href="manajemen-user">
						<i class="fas fa-angle-right title-icon"></i> Manajemen User
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="backup-database">
						<i class="fas fa-angle-right title-icon"></i> Backup Database
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="audit-trail">
						<i class="fas fa-angle-right title-icon"></i> Audit Trail
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="user-password">
						<i class="fas fa-angle-right title-icon"></i> Permohonan Pergantian Password
					</a>
				</div>
			</li>
		<?php
		} elseif ($_GET["module"] == "user-password") { ?>
			<li class="nav-item active dropdown">
				<a class="nav-link dropdown-toggle mr-2" href="javascript:void(0);" id="navbarUtility" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-user-cog title-icon"></i> Utility
				</a>
				<!-- Dropdown Menu -->
				<div class="dropdown-menu" aria-labelledby="navbarUtility">
					<a class="dropdown-item menu" href="konfigurasi-aplikasi">
						<i class="fas fa-angle-right title-icon"></i> Konfigurasi Aplikasi
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="manajemen-user">
						<i class="fas fa-angle-right title-icon"></i> Manajemen User
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="backup-database">
						<i class="fas fa-angle-right title-icon"></i> Backup Database
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="audit-trail">
						<i class="fas fa-angle-right title-icon"></i> Audit Trail
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu active" href="user-password">
						<i class="fas fa-angle-right title-icon"></i> Permohonan Pergantian Password
					</a>
				</div>
			</li>
		<?php
		}
		// jika menu backup database dipilih, menu backup database aktif
		elseif ($_GET["module"] == "backupdb") { ?>
			<li class="nav-item active dropdown">
				<a class="nav-link dropdown-toggle mr-2" href="javascript:void(0);" id="navbarUtility" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-user-cog title-icon"></i> Utility
				</a>
				<!-- Dropdown Menu -->
				<div class="dropdown-menu" aria-labelledby="navbarUtility">
					<a class="dropdown-item menu" href="konfigurasi-aplikasi">
						<i class="fas fa-angle-right title-icon"></i> Konfigurasi Aplikasi
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="manajemen-user">
						<i class="fas fa-angle-right title-icon"></i> Manajemen User
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu active" href="backup-database">
						<i class="fas fa-angle-right title-icon"></i> Backup Database
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="audit-trail">
						<i class="fas fa-angle-right title-icon"></i> Audit Trail
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="user-password">
						<i class="fas fa-angle-right title-icon"></i> Permohonan Pergantian Password
					</a>
				</div>
			</li>
		<?php
		}
		// jika menu audit trail dipilih, menu audit trail aktif
		elseif ($_GET["module"] == "audit_trail") { ?>
			<li class="nav-item active dropdown">
				<a class="nav-link dropdown-toggle mr-2" href="javascript:void(0);" id="navbarUtility" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-user-cog title-icon"></i> Utility
				</a>
				<!-- Dropdown Menu -->
				<div class="dropdown-menu" aria-labelledby="navbarUtility">
					<a class="dropdown-item menu" href="konfigurasi-aplikasi">
						<i class="fas fa-angle-right title-icon"></i> Konfigurasi Aplikasi
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="manajemen-user">
						<i class="fas fa-angle-right title-icon"></i> Manajemen User
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="backup-database">
						<i class="fas fa-angle-right title-icon"></i> Backup Database
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu active" href="audit-trail">
						<i class="fas fa-angle-right title-icon"></i> Audit Trail
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="user-password">
						<i class="fas fa-angle-right title-icon"></i> Permohonan Pergantian Password
					</a>
				</div>
			</li>
		<?php
		}
		// jika tidak, menu utility tidak aktif
		else { ?>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle mr-2" href="javascript:void(0);" id="navbarUtility" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-user-cog title-icon"></i> Utility
				</a>
				<!-- Dropdown Menu -->
				<div class="dropdown-menu" aria-labelledby="navbarUtility">
					<a class="dropdown-item menu" href="konfigurasi-aplikasi">
						<i class="fas fa-angle-right title-icon"></i> Konfigurasi Aplikasi
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="manajemen-user">
						<i class="fas fa-angle-right title-icon"></i> Manajemen User
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="backup-database">
						<i class="fas fa-angle-right title-icon"></i> Backup Database
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="audit-trail">
						<i class="fas fa-angle-right title-icon"></i> Audit Trail
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="user-password">
						<i class="fas fa-angle-right title-icon"></i> Permohonan Pergantian Password
					</a>
				</div>
			</li>
		<?php
		}
	}
	// jika "hak_akses = Purchasing", tampilkan menu
	elseif ($_SESSION['hak_akses'] == 'Purchasing') {
		// pengecekan menu aktif
		// jika menu beranda dipilih, menu beranda aktif
		if ($_GET["module"] == "beranda") { ?>
			<li class="nav-item active">
				<a class="nav-link mr-2 menu" href="beranda">
					<i class="fas fa-home title-icon"></i> Beranda
				</a>
			</li>
		<?php
		}
		// jika tidak, menu beranda tidak aktif
		else { ?>
			<li class="nav-item">
				<a class="nav-link mr-2 menu" href="beranda">
					<i class="fas fa-home title-icon"></i> Beranda
				</a>
			</li>
		<?php
		}

		if ($_GET["module"] == "keranjang") { ?>
			<li class="nav-item active dropdown">
				<a class="nav-link dropdown-toggle mr-2" href="javascript:void(0);" id="navbarbahanbaku" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-box title-icon"></i> Pemesanan
				</a>
				<!-- Dropdown Menu -->
				<div class="dropdown-menu" aria-labelledby="navbarbahanbaku">
					<a class="dropdown-item menu active" href="keranjang">
						<i class="fas fa-angle-right title-icon"></i> Keranjang
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="riwayat-pemesanan">
						<i class="fas fa-angle-right title-icon"></i> Riwayat Pemesanan
					</a>
				</div>
			</li>
		<?php } else if ($_GET["module"] == "riwayat-pemesanan") { ?>
			<li class="nav-item active dropdown">
				<a class="nav-link dropdown-toggle mr-2" href="javascript:void(0);" id="navbarbahanbaku" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-box title-icon"></i> Pemesanan
				</a>
				<!-- Dropdown Menu -->
				<div class="dropdown-menu" aria-labelledby="navbarbahanbaku">
					<a class="dropdown-item menu" href="keranjang">
						<i class="fas fa-angle-right title-icon"></i> Keranjang
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu active" href="riwayat-pemesanan">
						<i class="fas fa-angle-right title-icon"></i> Riwayat Pemesanan
					</a>
				</div>
			</li>
		<?php } else { ?>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle mr-2" href="javascript:void(0);" id="navbarbahanbaku" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-box title-icon"></i> Pemesanan
				</a>
				<!-- Dropdown Menu -->
				<div class="dropdown-menu" aria-labelledby="navbarbahanbaku">
					<a class="dropdown-item menu" href="keranjang">
						<i class="fas fa-angle-right title-icon"></i> Keranjang
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="riwayat-pemesanan">
						<i class="fas fa-angle-right title-icon"></i> Riwayat Pemesanan
					</a>
				</div>
			</li>
		<?php }

		// jika menu supplier dipilih, menu supplier aktif
		if ($_GET["module"] == "supplier") { ?>
			<li class="nav-item active">
				<a class="nav-link mr-2 menu" href="supplier">
					<i class="fas fa-user-friends title-icon"></i> Supplier
				</a>
			</li>
		<?php
		}
		// jika tidak, menu supplier tidak aktif
		else { ?>
			<li class="nav-item">
				<a class="nav-link mr-2 menu" href="supplier">
					<i class="fas fa-user-friends title-icon"></i> Supplier
				</a>
			</li>
		<?php
		}

		// jika menu bahanbaku dipilih, menu bahanbaku aktif
		if ($_GET["module"] == "bahanbaku") { ?>
			<li class="nav-item active dropdown">
				<a class="nav-link dropdown-toggle mr-2" href="javascript:void(0);" id="navbarbahanbaku" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-box title-icon"></i> Bahan Baku
				</a>
				<!-- Dropdown Menu -->
				<div class="dropdown-menu" aria-labelledby="navbarbahanbaku">
					<a class="dropdown-item menu active" href="bahanbaku">
						<i class="fas fa-angle-right title-icon"></i> Bahan Baku
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="satuan">
						<i class="fas fa-angle-right title-icon"></i> Satuan
					</a>

					<div class="dropdown-divider"></div>

					<a href="jenis-bahan-baku" class="dropdown-item menu">
						<i class="fas fa-angle-right title-icon"></i> Jenis Bahan Baku
					</a>
				</div>
			</li>
		<?php
		}
		// jika menu satuan dipilih, menu satuan aktif
		elseif ($_GET["module"] == "satuan") { ?>
			<li class="nav-item active dropdown">
				<a class="nav-link dropdown-toggle mr-2" href="javascript:void(0);" id="navbarbahanbaku" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-box title-icon"></i> Bahan Baku
				</a>
				<!-- Dropdown Menu -->
				<div class="dropdown-menu" aria-labelledby="navbarbahanbaku">
					<a class="dropdown-item menu" href="bahanbaku">
						<i class="fas fa-angle-right title-icon"></i> Bahan Baku
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu active" href="satuan">
						<i class="fas fa-angle-right title-icon"></i> Satuan
					</a>

					<div class="dropdown-divider"></div>

					<a href="jenis-bahan-baku" class="dropdown-item menu">
						<i class="fas fa-angle-right title-icon"></i> Jenis Bahan Baku
					</a>
				</div>
			</li>
		<?php
		} elseif ($_GET["module"] == "jenis-bahan-baku") { ?>
			<li class="nav-item active dropdown">
				<a class="nav-link dropdown-toggle mr-2" href="javascript:void(0);" id="navbarbahanbaku" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-box title-icon"></i> Bahan Baku
				</a>
				<!-- Dropdown Menu -->
				<div class="dropdown-menu" aria-labelledby="navbarbahanbaku">
					<a class="dropdown-item menu" href="bahanbaku">
						<i class="fas fa-angle-right title-icon"></i> Bahan Baku
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="satuan">
						<i class="fas fa-angle-right title-icon"></i> Satuan
					</a>

					<div class="dropdown-divider"></div>

					<a href="jenis-bahan-baku" class="dropdown-item menu active">
						<i class="fas fa-angle-right title-icon"></i> Jenis Bahan Baku
					</a>
				</div>
			</li>
		<?php
		}
		// jika tidak, menu bahanbaku tidak aktif
		else { ?>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle mr-2" href="javascript:void(0);" id="navbarbahanbaku" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-box title-icon"></i> Bahan Baku
				</a>
				<!-- Dropdown Menu -->
				<div class="dropdown-menu" aria-labelledby="navbarbahanbaku">
					<a class="dropdown-item menu" href="bahanbaku">
						<i class="fas fa-angle-right title-icon"></i> Bahan Baku
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="satuan">
						<i class="fas fa-angle-right title-icon"></i> Satuan
					</a>

					<div class="dropdown-divider"></div>

					<a href="jenis-bahan-baku" class="dropdown-item menu">
						<i class="fas fa-angle-right title-icon"></i> Jenis Bahan Baku
					</a>
				</div>
			</li>
		<?php
		}

		// jika menu pembelian dipilih, menu pembelian aktif
		if ($_GET["module"] == "pembelian") { ?>
			<li class="nav-item active">
				<a class="nav-link mr-2 menu" href="pembelian">
					<i class="fas fa-cart-plus title-icon"></i> Pembelian
				</a>
			</li>
		<?php
		}
		// jika tidak, menu pembelian tidak aktif
		else { ?>
			<li class="nav-item">
				<a class="nav-link mr-2 menu" href="pembelian">
					<i class="fas fa-cart-plus title-icon"></i> Pembelian
				</a>
			</li>
		<?php
		}

		// jika menu laporan stok dipilih, menu laporan stok aktif
		if ($_GET["module"] == "lap_stok") { ?>
			<li class="nav-item active">
				<a class="nav-link mr-2 menu" href="laporan-stok">
					<i class="fas fa-file-alt title-icon"></i> Laporan Stok
				</a>
			</li>
		<?php
		}
		// jika tidak, menu laporan stok tidak aktif
		else { ?>
			<li class="nav-item">
				<a class="nav-link mr-2 menu" href="laporan-stok">
					<i class="fas fa-file-alt title-icon"></i> Laporan Stok
				</a>
			</li>
		<?php
		}

		// jika menu Riwayat Pembelian per periode dipilih, menu Riwayat Pembelian per periode aktif
		if ($_GET["module"] == "lap_pembelian_per_periode") { ?>
			<li class="nav-item active dropdown">
				<a class="nav-link dropdown-toggle mr-2" href="javascript:void(0);" id="navbarLaporanPembelian" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-file-invoice title-icon"></i> Riwayat Pembelian
				</a>
				<!-- Dropdown Menu -->
				<div class="dropdown-menu" aria-labelledby="navbarLaporanPembelian">
					<a class="dropdown-item menu active" href="laporan-pembelian-per-periode">
						<i class="fas fa-angle-right title-icon"></i> Per Periode
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="laporan-pembelian-perincian">
						<i class="fas fa-angle-right title-icon"></i> Perincian
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="laporan-pembelian-per-bahanbaku">
						<i class="fas fa-angle-right title-icon"></i> Per Bahan Baku
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="laporan-pembelian-per-supplier">
						<i class="fas fa-angle-right title-icon"></i> Per Supplier
					</a>
				</div>
			</li>
		<?php
		}
		// jika menu Riwayat Pembelian perincian dipilih, menu Riwayat Pembelian perincian aktif
		elseif ($_GET["module"] == "lap_pembelian_perincian") { ?>
			<li class="nav-item active dropdown">
				<a class="nav-link dropdown-toggle mr-2" href="javascript:void(0);" id="navbarLaporanPembelian" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-file-invoice title-icon"></i> Riwayat Pembelian
				</a>
				<!-- Dropdown Menu -->
				<div class="dropdown-menu" aria-labelledby="navbarLaporanPembelian">
					<a class="dropdown-item menu" href="laporan-pembelian-per-periode">
						<i class="fas fa-angle-right title-icon"></i> Per Periode
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu active" href="laporan-pembelian-perincian">
						<i class="fas fa-angle-right title-icon"></i> Perincian
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="laporan-pembelian-per-bahanbaku">
						<i class="fas fa-angle-right title-icon"></i> Per Bahan Baku
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="laporan-pembelian-per-supplier">
						<i class="fas fa-angle-right title-icon"></i> Per Supplier
					</a>
				</div>
			</li>
		<?php
		}
		// jika menu Riwayat Pembelian Per Bahan Baku dipilih, menu Riwayat Pembelian Per Bahan Baku aktif
		elseif ($_GET["module"] == "lap_pembelian_per_barang") { ?>
			<li class="nav-item active dropdown">
				<a class="nav-link dropdown-toggle mr-2" href="javascript:void(0);" id="navbarLaporanPembelian" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-file-invoice title-icon"></i> Riwayat Pembelian
				</a>
				<!-- Dropdown Menu -->
				<div class="dropdown-menu" aria-labelledby="navbarLaporanPembelian">
					<a class="dropdown-item menu" href="laporan-pembelian-per-periode">
						<i class="fas fa-angle-right title-icon"></i> Per Periode
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="laporan-pembelian-perincian">
						<i class="fas fa-angle-right title-icon"></i> Perincian
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu active" href="laporan-pembelian-per-bahanbaku">
						<i class="fas fa-angle-right title-icon"></i> Per Bahan Baku
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="laporan-pembelian-per-supplier">
						<i class="fas fa-angle-right title-icon"></i> Per Supplier
					</a>
				</div>
			</li>
		<?php
		}
		// jika menu Riwayat Pembelian per supplier dipilih, menu Riwayat Pembelian per supplier aktif
		elseif ($_GET["module"] == "lap_pembelian_per_supplier") { ?>
			<li class="nav-item active dropdown">
				<a class="nav-link dropdown-toggle mr-2" href="javascript:void(0);" id="navbarLaporanPembelian" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-file-invoice title-icon"></i> Riwayat Pembelian
				</a>
				<!-- Dropdown Menu -->
				<div class="dropdown-menu" aria-labelledby="navbarLaporanPembelian">
					<a class="dropdown-item menu" href="laporan-pembelian-per-periode">
						<i class="fas fa-angle-right title-icon"></i> Per Periode
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="laporan-pembelian-perincian">
						<i class="fas fa-angle-right title-icon"></i> Perincian
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="laporan-pembelian-per-bahanbaku">
						<i class="fas fa-angle-right title-icon"></i> Per Bahan Baku
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu active" href="laporan-pembelian-per-supplier">
						<i class="fas fa-angle-right title-icon"></i> Per Supplier
					</a>
				</div>
			</li>
		<?php
		}
		// jika tidak, menu Riwayat Pembelian tidak aktif
		else { ?>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle mr-2" href="javascript:void(0);" id="navbarLaporanPembelian" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-file-invoice title-icon"></i> Riwayat Pembelian
				</a>
				<!-- Dropdown Menu -->
				<div class="dropdown-menu" aria-labelledby="navbarLaporanPembelian">
					<a class="dropdown-item menu" href="laporan-pembelian-per-periode">
						<i class="fas fa-angle-right title-icon"></i> Per Periode
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="laporan-pembelian-perincian">
						<i class="fas fa-angle-right title-icon"></i> Perincian
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="laporan-pembelian-per-bahanbaku">
						<i class="fas fa-angle-right title-icon"></i> Per Bahan Baku
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="laporan-pembelian-per-supplier">
						<i class="fas fa-angle-right title-icon"></i> Per Supplier
					</a>
				</div>
			</li>
		<?php
		}
	}
	// jika "hak_akses = Cashier", tampilkan menu
	elseif ($_SESSION['hak_akses'] == 'Cashier') {
		// pengecekan menu aktif
		// jika menu beranda dipilih, menu beranda aktif
		if ($_GET["module"] == "beranda") { ?>
			<li class="nav-item active">
				<a class="nav-link mr-2 menu" href="beranda">
					<i class="fas fa-home title-icon"></i> Beranda
				</a>
			</li>
		<?php
		}
		// jika tidak, menu beranda tidak aktif
		else { ?>
			<li class="nav-item">
				<a class="nav-link mr-2 menu" href="beranda">
					<i class="fas fa-home title-icon"></i> Beranda
				</a>
			</li>
		<?php
		}

		// jika menu penjualan dipilih, menu penjualan aktif
		if ($_GET["module"] == "penjualan") { ?>
			<li class="nav-item active">
				<a class="nav-link mr-2 menu" href="penjualan">
					<i class="fas fa-people-carry title-icon"></i> Pemakaian
				</a>
			</li>
		<?php
		}
		// jika tidak, menu penjualan tidak aktif
		else { ?>
			<li class="nav-item">
				<a class="nav-link mr-2 menu" href="penjualan">
					<i class="fas fa-people-carry title-icon"></i> Pemakaian
				</a>
			</li>
		<?php
		}

		// jika menu laporan stok dipilih, menu laporan stok aktif
		if ($_GET["module"] == "lap_stok") { ?>
			<li class="nav-item active">
				<a class="nav-link mr-2 menu" href="laporan-stok">
					<i class="fas fa-file-alt title-icon"></i> Laporan Stok
				</a>
			</li>
		<?php
		}
		// jika tidak, menu laporan stok tidak aktif
		else { ?>
			<li class="nav-item">
				<a class="nav-link mr-2 menu" href="laporan-stok">
					<i class="fas fa-file-alt title-icon"></i> Laporan Stok
				</a>
			</li>
		<?php
		}

		// jika menu laporan penjualan per periode dipilih, menu laporan penjualan per periode aktif
		if ($_GET["module"] == "lap_penjualan_per_periode") { ?>
			<li class="nav-item active dropdown">
				<a class="nav-link dropdown-toggle mr-2" href="javascript:void(0);" id="navbarLaporanpenjualan" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-clipboard title-icon"></i> Laporan Pemakaian
				</a>
				<!-- Dropdown Menu -->
				<div class="dropdown-menu" aria-labelledby="navbarLaporanpenjualan">
					<a class="dropdown-item menu active" href="laporan-penjualan-per-periode">
						<i class="fas fa-angle-right title-icon"></i> Per Periode
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="laporan-penjualan-perincian">
						<i class="fas fa-angle-right title-icon"></i> Perincian
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="laporan-penjualan-per-bahanbaku">
						<i class="fas fa-angle-right title-icon"></i> Per Bahan Baku
					</a>
				</div>
			</li>
		<?php
		}
		// jika menu laporan penjualan perincian dipilih, menu laporan penjualan perincian aktif
		elseif ($_GET["module"] == "lap_penjualan_perincian") { ?>
			<li class="nav-item active dropdown">
				<a class="nav-link dropdown-toggle mr-2" href="javascript:void(0);" id="navbarLaporanpenjualan" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-clipboard title-icon"></i> Laporan Pemakaian
				</a>
				<!-- Dropdown Menu -->
				<div class="dropdown-menu" aria-labelledby="navbarLaporanpenjualan">
					<a class="dropdown-item menu" href="laporan-penjualan-per-periode">
						<i class="fas fa-angle-right title-icon"></i> Per Periode
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu active" href="laporan-penjualan-perincian">
						<i class="fas fa-angle-right title-icon"></i> Perincian
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="laporan-penjualan-per-bahanbaku">
						<i class="fas fa-angle-right title-icon"></i> Per Bahan Baku
					</a>
				</div>
			</li>
		<?php
		}
		// jika menu laporan penjualan Per Bahan Baku dipilih, menu laporan penjualan Per Bahan Baku aktif
		elseif ($_GET["module"] == "lap_penjualan_per_barang") { ?>
			<li class="nav-item active dropdown">
				<a class="nav-link dropdown-toggle mr-2" href="javascript:void(0);" id="navbarLaporanpenjualan" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-clipboard title-icon"></i> Laporan Pemakaian
				</a>
				<!-- Dropdown Menu -->
				<div class="dropdown-menu" aria-labelledby="navbarLaporanpenjualan">
					<a class="dropdown-item menu" href="laporan-penjualan-per-periode">
						<i class="fas fa-angle-right title-icon"></i> Per Periode
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="laporan-penjualan-perincian">
						<i class="fas fa-angle-right title-icon"></i> Perincian
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu active" href="laporan-penjualan-per-bahanbaku">
						<i class="fas fa-angle-right title-icon"></i> Per Bahan Baku
					</a>
				</div>
			</li>
		<?php
		}
		// jika tidak, menu laporan penjualan tidak aktif
		else { ?>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle mr-2" href="javascript:void(0);" id="navbarLaporanpenjualan" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-clipboard title-icon"></i> Laporan Pemakaian
				</a>
				<!-- Dropdown Menu -->
				<div class="dropdown-menu" aria-labelledby="navbarLaporanpenjualan">
					<a class="dropdown-item menu" href="laporan-penjualan-per-periode">
						<i class="fas fa-angle-right title-icon"></i> Per Periode
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="laporan-penjualan-perincian">
						<i class="fas fa-angle-right title-icon"></i> Perincian
					</a>

					<div class="dropdown-divider"></div>

					<a class="dropdown-item menu" href="laporan-penjualan-per-bahanbaku">
						<i class="fas fa-angle-right title-icon"></i> Per Bahan Baku
					</a>
				</div>
			</li>
		<?php
		}
	} elseif ($_SESSION['hak_akses'] == 'Suppliers') {
		if ($_GET["module"] == "beranda") { ?>
			<li class="nav-item active">
				<a class="nav-link mr-2 menu" href="beranda">
					<i class="fas fa-home title-icon"></i> Beranda
				</a>
			</li>
		<?php
		}
		// jika tidak, menu beranda tidak aktif
		else { ?>
			<li class="nav-item">
				<a class="nav-link mr-2 menu" href="beranda">
					<i class="fas fa-home title-icon"></i> Beranda
				</a>
			</li>
		<?php
		} ?>

		<?php if ($_GET['module'] == 'manajemen-pemesanan') { ?>
			<li class="nav-item active">
				<a class="nav-link mr-2 menu" href="manajemen-pemesanan">
					<i class="fas fa-home title-icon"></i> Manajemen Pemesanan
				</a>
			</li>
		<?php } else { ?>
			<li class="nav-item">
				<a class="nav-link mr-2 menu" href="manajemen-pemesanan">
					<i class="fas fa-home title-icon"></i> Manajemen Pemesanan
				</a>
			</li>
		<?php } ?>

		<?php if ($_GET['module'] == 'riwayat-pembelian') { ?>
			<li class="nav-item active">
				<a class="nav-link mr-2 menu" href="riwayat-pembelian">
					<i class="fas fa-shopping-cart title-icon"></i> Riwayat Pembelian
				</a>
			</li>
		<?php } else { ?>
			<li class="nav-item">
				<a class="nav-link mr-2 menu" href="riwayat-pembelian">
					<i class="fas fa-shopping-cart title-icon"></i> Riwayat Pembelian
				</a>
			</li>
		<?php } ?>

		<?php if ($_GET["module"] == "rating-saya") { ?>
			<li class="nav-item active">
				<a class="nav-link mr-2 menu" href="beranda">
					<i class="fas fa-star title-icon"></i> Rating Saya
				</a>
			</li>
		<?php
		}
		// jika tidak, menu beranda tidak aktif
		else { ?>
			<li class="nav-item">
				<a class="nav-link mr-2 menu" href="rating-saya">
					<i class="fas fa-star title-icon"></i> Rating Saya
				</a>
			</li>
		<?php
		} ?>

<?php }
}
?>