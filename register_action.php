<?php
// Mengecek AJAX Request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')) {
    // panggil file "config.php" untuk koneksi ke database
    require_once "config/config.php";

    // mengecek data post dari ajax
    if (isset($_POST['username']) && isset($_POST['password']) && isset($_POST['nama'])) {
        try {
            // ambil "data" post dari ajax
            $nama = trim($_POST['nama']);
            $username = trim($_POST['username']);
            $password = sha1(md5(trim($_POST['password'])));
            // siapkan "data" blokir
            $blokir   = "Tidak";

            // sql statement untuk menampilkan data dari tabel "sys_users" berdasarkan username, password, dan blokir
            $query = "SELECT * FROM sys_users WHERE username=:username";
            // membuat prepared statements
            $stmt = $pdo->prepare($query);

            // hubungkan "data" dengan prepared statements
            $stmt->bindParam(':username', $username);

            // eksekusi query
            $stmt->execute();

            // cek hasil query
            // jika data ada, jalankan perintah untuk membuat session
            if ($stmt->rowCount() <> 0) {
                echo 'sudah ada';
            }
            // jika data tidak ada
            else {
                $queryInsert = "INSERT INTO sys_users(nama_user, username, password, hak_akses, blokir, created_user) VALUES (:nama, :username, :password, 'Visitors', 'Tidak', '1')";

                $stmtInsert = $pdo->prepare($queryInsert);

                // hubungkan "data" dengan prepared statements
                $stmtInsert->bindParam(':nama', $nama);
                $stmtInsert->bindParam(':username', $username);
                $stmtInsert->bindParam(':password', $password);

                // eksekusi query
                $stmtInsert->execute();

                session_start();

                $queryGet = "SELECT * FROM sys_users ORDER BY id_user DESC LIMIT 1";
                $stmtGet = $pdo->prepare($queryGet);
                $stmtGet->execute();

                while ($register = $stmtGet->fetch(PDO::FETCH_ASSOC)) {
                    $_SESSION['id_user']   = $register['id_user'];
                    $_SESSION['nama_user'] = $nama;
                    $_SESSION['username']  = $username;
                    $_SESSION['password']  = $password;
                    $_SESSION['hak_akses'] = 'Visitors';
                }

                echo 'sukses';
            }
        } catch (Exception $e) {
            // tampilkan pesan kesalahan
            echo $e->getMessage();
        }
    }
    // tutup koneksi
    $pdo = null;
} else {
    // jika tidak ada ajax request, maka alihkan ke halaman "login-error"
    echo '<script>window.location="login-error"</script>';
}
