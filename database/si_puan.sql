-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 01 Des 2020 pada 04.50
-- Versi Server: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `si_puan`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `bahanbaku`
--

CREATE TABLE `bahanbaku` (
  `kode_bahanbaku` varchar(6) NOT NULL,
  `nama_bahanbaku` varchar(30) NOT NULL,
  `harga_beli` int(11) NOT NULL,
  `harga_jual` int(11) NOT NULL,
  `satuan` int(11) NOT NULL,
  `min_stok` int(11) NOT NULL,
  `stok` int(11) NOT NULL DEFAULT '0',
  `created_user` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_user` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `bahanbaku`
--

INSERT INTO `bahanbaku` (`kode_bahanbaku`, `nama_bahanbaku`, `harga_beli`, `harga_jual`, `satuan`, `min_stok`, `stok`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
('OB0001', 'Sirup Caramel', 35000, 16000, 5, 2, 12, 2, '2019-02-06 03:03:03', 1, '2020-11-14 16:55:36'),
('OB0002', 'Sirup Mint', 35000, 16000, 5, 2, 37, 2, '2019-02-06 03:03:20', 1, '2020-11-14 16:57:48'),
('OB0003', 'Sirup Strawberry', 35000, 27500, 5, 2, 49, 2, '2019-02-06 03:04:35', 1, '2020-11-14 16:59:03'),
('OB0004', 'Sirup Aren', 35000, 12000, 5, 2, 23, 2, '2019-02-06 03:05:12', 1, '2020-11-14 17:01:19'),
('OB0005', 'Sirup Hazelnut', 35000, 16000, 5, 2, 7, 2, '2019-02-06 03:05:39', 1, '2020-11-14 17:03:05'),
('OB0006', 'Sirup Coklat', 35000, 10000, 5, 2, 4, 2, '2019-02-06 03:06:18', 1, '2020-11-14 17:03:43'),
('OB0007', 'Sirup Greentea', 18000, 24000, 5, 2, 1, 2, '2019-02-06 03:06:45', 1, '2020-11-14 17:03:58'),
('OB0008', 'Sirup Bubble Gum', 35000, 69000, 5, 2, 2, 2, '2019-02-06 03:07:07', 1, '2020-11-14 17:04:40'),
('OB0009', 'Susu Kental Manis', 112000, 85000, 4, 2, 1, 2, '2019-02-06 03:08:02', 1, '2020-11-14 17:05:47'),
('OB0010', 'Susu Original', 95600, 150000, 4, 2, 54, 2, '2019-02-06 03:08:27', 1, '2020-11-14 17:06:31'),
('OB0011', 'Susu Coconut', 100500, 65000, 4, 2, 42, 2, '2019-02-06 03:08:50', 1, '2020-11-14 17:06:53'),
('OB0012', 'Gelas Plastik', 50000, 29000, 1, 2, 12, 2, '2019-02-06 03:10:07', 1, '2020-11-14 17:12:28'),
('OB0013', 'Cup Sealer', 75000, 37500, 2, 2, 15, 2, '2019-02-06 03:10:37', 1, '2020-11-14 17:13:15'),
('OB0014', 'Botol 500ml', 15000, 11500, 5, 2, 30, 2, '2019-02-06 03:11:21', 1, '2020-11-14 17:16:14'),
('OB0015', 'Botol 1000ml', 25000, 11500, 5, 2, 0, 2, '2019-02-06 03:11:41', 1, '2020-11-14 17:16:46'),
('OB0016', 'Biji Kopi', 250000, 0, 4, 2, 0, 1, '2020-11-15 07:04:20', NULL, NULL);

--
-- Trigger `bahanbaku`
--
DELIMITER $$
CREATE TRIGGER `bahanbaku_insert` AFTER INSERT ON `bahanbaku` FOR EACH ROW BEGIN
INSERT INTO sys_audit_trail (username,aksi,keterangan) VALUES (NEW.created_user,'Insert',CONCAT('<b>Insert</b> data bahan baku pada tabel <b>bahan baku</b>.<br><b>[Kode Bahan Baku : </b>',NEW.kode_bahanbaku,'<b>][Nama Bahan Baku : </b>',NEW.nama_bahanbaku,'<b>][Harga Beli : </b>',NEW.harga_beli,'<b>][Harga Jual : </b>',NEW.harga_jual,'<b>][Kode Satuan : </b>',NEW.satuan,'<b>][Minimum Stok : </b>',NEW.min_stok,'<b>]' ));
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `bahanbaku_update` AFTER UPDATE ON `bahanbaku` FOR EACH ROW BEGIN
IF OLD.stok=NEW.stok THEN
INSERT INTO sys_audit_trail (username,aksi,keterangan) VALUES (NEW.updated_user,'Update',CONCAT('<b>Update</b> data bahan baku pada tabel <b>bahan baku</b>.<br><b>Data Lama = [Kode Bahan Baku : </b>',OLD.kode_bahanbaku,'<b>][Nama Bahan Baku : </b>',OLD.nama_bahanbaku,'<b>][Harga Beli : </b>',OLD.harga_beli,'<b>][Harga Jual : </b>',OLD.harga_jual,'<b>][Kode Satuan : </b>',OLD.satuan,'<b>][Minimum Stok : </b>',OLD.min_stok,'<b>][Stok : </b>',OLD.stok,'<b>],<br> Data Baru = [Kode Bahan Baku : </b>',NEW.kode_bahanbaku,'<b>][Nama Bahan Baku : </b>',NEW.nama_bahanbaku,'<b>][Harga Beli : </b>',NEW.harga_beli,'<b>][Harga Jual : </b>',NEW.harga_jual,'<b>][Kode Satuan : </b>',NEW.satuan,'<b>][Minimum Stok : </b>',NEW.min_stok,'<b>][Stok : </b>',NEW.stok,'<b>]</b>' ));
END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `feedback`
--

CREATE TABLE `feedback` (
  `id_feedback` int(5) NOT NULL,
  `kode_supplier` varchar(5) NOT NULL,
  `feedback` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `jumlah_pemakaian_bahan_baku`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `jumlah_pemakaian_bahan_baku` (
`no_penjualan` varchar(13)
,`tanggal` date
,`total_pemakaian` decimal(32,0)
);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembelian`
--

CREATE TABLE `pembelian` (
  `no_pembelian` varchar(13) NOT NULL,
  `tanggal` date NOT NULL,
  `tanggal_kadaluarsa` date NOT NULL,
  `supplier` varchar(5) NOT NULL,
  `no_nota` varchar(15) NOT NULL,
  `total_bayar` int(11) NOT NULL,
  `created_user` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pembelian`
--

INSERT INTO `pembelian` (`no_pembelian`, `tanggal`, `tanggal_kadaluarsa`, `supplier`, `no_nota`, `total_bayar`, `created_user`, `created_date`) VALUES
('BL11202000001', '2020-11-15', '0000-00-00', 'SP005', '123456', 210000, 1, '2020-11-14 22:07:26'),
('BL11202000002', '2020-11-15', '0000-00-00', 'SP001', '234567', 1750000, 1, '2020-11-14 22:08:37'),
('BL11202000003', '2020-11-15', '0000-00-00', 'SP001', '123032', 125000, 1, '2020-11-14 23:28:06'),
('BL11202000004', '2020-11-29', '0000-00-00', 'SP003', '423512', 210000, 1, '2020-11-29 01:33:32');

--
-- Trigger `pembelian`
--
DELIMITER $$
CREATE TRIGGER `pembelian_insert` AFTER INSERT ON `pembelian` FOR EACH ROW BEGIN
INSERT INTO sys_audit_trail (username,aksi,keterangan) VALUES (NEW.created_user,'Insert',CONCAT('<b>Insert</b> data pembelian pada tabel <b>pembelian</b>.<br><b>[No. Pembelian : </b>',NEW.no_pembelian,'<b>][Tanggal : </b>',NEW.tanggal,'<b>][Kode Supplier : </b>',NEW.supplier,'<b>][No. Nota : </b>',NEW.no_nota,'<b>][Total Bayar : </b>',NEW.total_bayar,'<b>]' ));
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembelian_detail`
--

CREATE TABLE `pembelian_detail` (
  `id` int(5) NOT NULL,
  `no_pembelian` varchar(13) NOT NULL,
  `bahanbaku` varchar(6) NOT NULL,
  `harga_beli` int(11) NOT NULL,
  `jumlah_beli` int(11) NOT NULL,
  `total_harga` int(11) NOT NULL,
  `created_user` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pembelian_detail`
--

INSERT INTO `pembelian_detail` (`id`, `no_pembelian`, `bahanbaku`, `harga_beli`, `jumlah_beli`, `total_harga`, `created_user`, `created_date`) VALUES
(1, 'BL11202000001', 'OB0001', 35000, 6, 210000, 1, '2020-11-14 22:07:26'),
(2, 'BL11202000002', 'OB0005', 35000, 50, 1750000, 1, '2020-11-14 22:08:37'),
(3, 'BL11202000003', 'OB0015', 25000, 5, 125000, 1, '2020-11-14 23:28:06'),
(4, 'BL11202000004', 'OB0006', 35000, 4, 140000, 1, '2020-11-29 01:33:32'),
(5, 'BL11202000004', 'OB0001', 35000, 2, 70000, 1, '2020-11-29 01:33:32');

--
-- Trigger `pembelian_detail`
--
DELIMITER $$
CREATE TRIGGER `pembelian_batal` AFTER DELETE ON `pembelian_detail` FOR EACH ROW BEGIN
	UPDATE bahanbaku SET stok=stok-OLD.jumlah_beli
	WHERE kode_bahanbaku=OLD.bahanbaku;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `pembelian_tambah_stok` AFTER INSERT ON `pembelian_detail` FOR EACH ROW BEGIN
INSERT INTO sys_audit_trail (username,aksi,keterangan) VALUES (NEW.created_user,'Insert',CONCAT('<b>Insert</b> detail data pembelian bahan baku pada tabel <b>pembelian_detail</b>.<br><b>[No. Pembelian : </b>',NEW.no_pembelian,'<b>][Kode Bahan Baku : </b>',NEW.bahanbaku,'<b>][Harga Beli : </b>',NEW.harga_beli,'<b>][Jumlah Beli : </b>',NEW.jumlah_beli,'<b>][Total Harga : </b>',NEW.total_harga,'<b>]' ));

UPDATE bahanbaku SET stok=stok+NEW.jumlah_beli
WHERE kode_bahanbaku=NEW.bahanbaku;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `penjualan`
--

CREATE TABLE `penjualan` (
  `no_penjualan` varchar(13) NOT NULL,
  `tanggal` date NOT NULL,
  `total_bayar` int(11) NOT NULL,
  `uang_bayar` int(11) NOT NULL,
  `created_user` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `penjualan`
--

INSERT INTO `penjualan` (`no_penjualan`, `tanggal`, `total_bayar`, `uang_bayar`, `created_user`, `created_date`) VALUES
('JL08201900001', '2019-08-22', 1680000, 50000000, 3, '2019-08-22 14:51:02'),
('JL11202000002', '2020-11-07', 172500, 200000, 1, '2020-11-07 08:13:28'),
('JL11202000003', '2020-11-15', 0, 0, 1, '2020-11-15 08:32:09'),
('JL11202000004', '2020-11-15', 0, 0, 1, '2020-11-15 08:32:59'),
('JL11202000005', '2020-11-15', 0, 0, 1, '2020-11-15 08:37:13');

--
-- Trigger `penjualan`
--
DELIMITER $$
CREATE TRIGGER `penjualan_insert` AFTER INSERT ON `penjualan` FOR EACH ROW BEGIN
INSERT INTO sys_audit_trail (username,aksi,keterangan) VALUES (NEW.created_user,'Insert',CONCAT('<b>Insert</b> data penjualan pada tabel <b>penjualan</b>.<br><b>[No. Penjualan : </b>',NEW.no_penjualan,'<b>][Tanggal : </b>',NEW.tanggal,'<b>]' ));
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `penjualan_detail`
--

CREATE TABLE `penjualan_detail` (
  `no_penjualan` varchar(13) NOT NULL,
  `bahanbaku` varchar(6) NOT NULL,
  `harga_beli` int(11) DEFAULT NULL,
  `harga_jual` int(11) DEFAULT NULL,
  `jumlah_jual` int(11) DEFAULT NULL,
  `total_harga` int(11) DEFAULT NULL,
  `created_user` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `penjualan_detail`
--

INSERT INTO `penjualan_detail` (`no_penjualan`, `bahanbaku`, `harga_beli`, `harga_jual`, `jumlah_jual`, `total_harga`, `created_user`, `created_date`) VALUES
('JL08201900001', 'OB0005', 31500, 42000, 40, 1680000, 3, '2019-08-22 14:51:02'),
('JL11202000002', 'OB0015', 8625, 11500, 15, 172500, 1, '2020-11-07 08:13:28'),
('JL11202000005', 'OB0001', 0, 0, 0, 0, 1, '2020-11-15 08:24:10'),
('JL11202000006', 'OB0004', 0, 0, 0, 0, 1, '2020-11-15 08:27:51'),
('JL11202000006', 'OB0003', 0, 0, 0, 0, 1, '2020-11-15 08:27:51'),
('JL11202000007', 'OB0005', 0, 0, 3, 0, 1, '2020-11-15 08:30:18'),
('JL11202000003', 'OB0003', 0, 0, 5, 0, 1, '2020-11-15 08:32:09'),
('JL11202000003', 'OB0001', 0, 0, 5, 0, 1, '2020-11-15 08:32:09'),
('JL11202000004', 'OB0001', 0, 0, 7, 0, 1, '2020-11-15 08:32:59'),
('JL11202000005', 'OB0002', 0, 0, 5, 0, 1, '2020-11-15 08:37:13');

--
-- Trigger `penjualan_detail`
--
DELIMITER $$
CREATE TRIGGER `penjualan_batal` AFTER DELETE ON `penjualan_detail` FOR EACH ROW BEGIN
	UPDATE bahanbaku SET stok=stok+OLD.jumlah_jual
	WHERE kode_bahanbaku=OLD.bahanbaku;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `penjualan_kurang_stok` AFTER INSERT ON `penjualan_detail` FOR EACH ROW BEGIN
INSERT INTO sys_audit_trail (username,aksi,keterangan) VALUES (NEW.created_user,'Insert',CONCAT('<b>Insert</b> detail data penjualan bahan baku pada tabel <b>penjualan_detail</b>.<br><b>[No. Penjualan : </b>',NEW.no_penjualan,'<b>][Kode Bahan Baku : </b>',NEW.bahanbaku,'<b>][Harga Beli : </b>',NEW.harga_beli,'<b>][Harga Jual : </b>',NEW.harga_jual,'<b>][Jumlah Jual : </b>',NEW.jumlah_jual,'<b>][Total Harga : </b>',NEW.total_harga,'<b>]' ));

UPDATE bahanbaku SET stok=stok-NEW.jumlah_jual
WHERE kode_bahanbaku=NEW.bahanbaku;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rating`
--

CREATE TABLE `rating` (
  `id_rating` int(5) NOT NULL,
  `value_rating` int(5) NOT NULL,
  `deskripsi` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `rating`
--

INSERT INTO `rating` (`id_rating`, `value_rating`, `deskripsi`) VALUES
(1, 1, 'Sangat Buruk'),
(2, 2, 'Buruk'),
(3, 3, 'Biasa'),
(4, 4, 'Baik'),
(5, 5, 'Sempurna');

-- --------------------------------------------------------

--
-- Struktur dari tabel `satuan`
--

CREATE TABLE `satuan` (
  `kode_satuan` int(11) NOT NULL,
  `nama_satuan` varchar(10) NOT NULL,
  `created_user` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_user` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `satuan`
--

INSERT INTO `satuan` (`kode_satuan`, `nama_satuan`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(1, 'Slop', 2, '2019-02-06 03:01:56', 1, '2020-11-14 17:11:15'),
(2, 'Roll', 2, '2019-02-06 03:02:02', 1, '2020-11-14 17:09:20'),
(3, 'Kotak', 2, '2019-02-06 03:02:09', NULL, NULL),
(4, 'Box', 2, '2019-02-06 03:02:15', NULL, NULL),
(5, 'Botol', 2, '2019-02-06 03:02:20', NULL, NULL);

--
-- Trigger `satuan`
--
DELIMITER $$
CREATE TRIGGER `satuan_insert` AFTER INSERT ON `satuan` FOR EACH ROW BEGIN
INSERT INTO sys_audit_trail (username,aksi,keterangan) VALUES (NEW.created_user,'Insert',CONCAT('<b>Insert</b> data satuan pada tabel <b>satuan</b>.<br><b>[Kode Satuan : </b>',NEW.kode_satuan,'<b>][Nama Satuan : </b>',NEW.nama_satuan,'<b>]' ));
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `satuan_update` AFTER UPDATE ON `satuan` FOR EACH ROW BEGIN
INSERT INTO sys_audit_trail (username,aksi,keterangan) VALUES (NEW.updated_user,'Update',CONCAT('<b>Update</b> data satuan pada tabel <b>satuan</b>.<br><b>Data Lama = [Kode Satuan : </b>',OLD.kode_satuan,'<b>][Nama Satuan : </b>',OLD.nama_satuan,'<b>],<br> Data Baru = [Kode Satuan : </b>',NEW.kode_satuan,'<b>][Nama Satuan : </b>',NEW.nama_satuan,'<b>]</b>' ));
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `supplier`
--

CREATE TABLE `supplier` (
  `kode_supplier` varchar(5) NOT NULL,
  `nama_supplier` varchar(30) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `telepon` varchar(13) NOT NULL,
  `created_user` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_user` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `supplier`
--

INSERT INTO `supplier` (`kode_supplier`, `nama_supplier`, `alamat`, `telepon`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
('SP001', 'AAM', 'Jln. Kenanga No. 26 Rawa Laut Tanjung Karang', '0721262639', 2, '2019-02-01 01:15:01', 1, '2020-11-20 19:35:20'),
('SP002', 'AMS', 'Jln. Dr. Cipto M. No.3 Sumur Batu Teluk Betung', '0721484707', 2, '2019-02-01 01:15:15', NULL, NULL),
('SP003', 'APL', 'Jln. Tembesu No.18 Campang Raya', '0721785912', 2, '2019-02-01 01:15:29', NULL, NULL),
('SP004', 'Aria Jiwa', 'Jln. Alam Hijau No. 16 Way Halim', '0721703500', 2, '2019-02-01 01:15:43', 1, '2020-11-14 16:52:29'),
('SP005', 'Bina San Prima', 'Jln. Tembesu No. 16A Campang Raya', '0721783559', 2, '2019-02-01 01:15:57', NULL, NULL),
('SP006', 'Dos Ni Roha', 'Jln. Panjang Wisma Indovision Kebun Jeruk Jakarta', '0721591359', 2, '2019-02-01 01:16:11', NULL, NULL),
('SP007', 'Elkaka', 'Jln. Pulau Batam Raya No. 34 Bandar Lampung', '0721293258', 2, '2019-02-01 01:16:25', NULL, NULL),
('SP008', 'Enseval', 'Jln. Tembesu No. 20 Campang Raya', '07218012266', 2, '2019-02-01 01:16:39', NULL, NULL),
('SP009', 'IGM', 'Jln. KH. Ah. Dahlan No.68 Bandar Lampung', '0721487131', 2, '2019-02-01 01:16:53', NULL, NULL),
('SP010', 'Darma Refill', 'Jln. Tembesu 2 No. 3B Campang Raya', '0721789242', 2, '2019-02-01 01:17:07', 1, '2020-11-14 17:17:41'),
('SP011', 'MPI', 'Jln. P. Antasari No. 7B Kedamaian', '0721263191', 2, '2019-02-01 01:17:21', NULL, NULL),
('SP012', 'Merapi Utama', 'Jln. Cilosari 23 Cikini Menteng Jakarta', '0213141906', 2, '2019-02-01 01:17:35', 1, '2020-11-14 16:53:30'),
('SP013', 'Penta Valent', 'Jln. ZA Pagar Alam Gg PU Bandar Lampung', '0721359923', 2, '2019-02-01 01:17:49', NULL, NULL),
('SP014', 'PPG', 'Jln. Tembesu No. 15A Campang Raya', '0721781135', 2, '2019-02-01 01:18:03', NULL, NULL),
('SP015', 'Rajawali Nusindo', 'Jln. Denpasar Raya, Kuningan Jakarta', '0721261373', 2, '2019-02-01 01:18:17', NULL, NULL),
('SP016', 'Setya Besar', 'Jln. Mawar No. 2 Pahoman Bandar Lampung', '0721269896', 2, '2019-02-01 01:18:31', 1, '2020-11-14 16:53:18'),
('SP017', 'Tri Sapta Jaya', 'Perum Korpri Blok B9 Sukarame', '0721890511', 2, '2019-02-01 01:18:45', NULL, NULL),
('SP018', 'United Dico Citas', 'Jln. P. Emir Minoor 91', '0721251252', 2, '2019-02-01 01:18:59', NULL, NULL);

--
-- Trigger `supplier`
--
DELIMITER $$
CREATE TRIGGER `supplier_insert` AFTER INSERT ON `supplier` FOR EACH ROW BEGIN
INSERT INTO sys_audit_trail (username,aksi,keterangan) VALUES (NEW.created_user,'Insert',CONCAT('<b>Insert</b> data supplier pada tabel <b>supplier</b>.<br><b>[Kode Supplier : </b>',NEW.kode_supplier,'<b>][Nama Supplier : </b>',NEW.nama_supplier,'<b>][Alamat : </b>',NEW.alamat,'<b>]</b>[Telepon : </b>',NEW.telepon,'<b>]' ));
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `supplier_update` AFTER UPDATE ON `supplier` FOR EACH ROW BEGIN
INSERT INTO sys_audit_trail (username,aksi,keterangan) VALUES (NEW.updated_user,'Update',CONCAT('<b>Update</b> data supplier pada tabel <b>supplier</b>.<br><b>Data Lama = [Kode Supplier : </b>',OLD.kode_supplier,'<b>][Nama Supplier : </b>',OLD.nama_supplier,'<b>][Alamat : </b>',OLD.alamat,'<b>]</b>[Telepon : </b>',OLD.telepon,'<b>],<br> Data Baru = [Kode Supplier : </b>',NEW.kode_supplier,'<b>][Nama Supplier : </b>',NEW.nama_supplier,'<b>][Alamat : </b>',NEW.alamat,'<b>]</b>[Telepon : </b>',NEW.telepon,'<b>]</b>' ));
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `sys_audit_trail`
--

CREATE TABLE `sys_audit_trail` (
  `id` bigint(20) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `username` varchar(30) NOT NULL,
  `aksi` enum('Insert','Update','Delete') NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sys_audit_trail`
--

INSERT INTO `sys_audit_trail` (`id`, `tanggal`, `username`, `aksi`, `keterangan`) VALUES
(1, '2019-02-06 02:55:53', '1', 'Insert', '<b>Insert</b> data pengguna pada tabel <b>sys_users</b>.<br><b>[ID User : </b>1<b>][Nama User : </b>Indra Styawantoro<b>][Username : </b>indrasatya<b>][Password : </b>2437018d9a925c9fce796b99bfb9591728c5f208<b>][Hak Akses : </b>Super Admin<b>][Blokir : </b>Tidak<b>]</b>'),
(2, '2019-02-06 02:56:09', '1', 'Insert', '<b>Insert</b> data pengguna pada tabel <b>sys_users</b>.<br><b>[ID User : </b>2<b>][Nama User : </b>Danang Kesuma<b>][Username : </b>danang<b>][Password : </b>f8966cc671220b4858818afca4a8c9eedbeb6a5d<b>][Hak Akses : </b>Purchasing<b>][Blokir : </b>Tidak<b>]</b>'),
(3, '2019-02-06 02:56:24', '1', 'Insert', '<b>Insert</b> data pengguna pada tabel <b>sys_users</b>.<br><b>[ID User : </b>3<b>][Nama User : </b>Kadina Putri<b>][Username : </b>kadina<b>][Password : </b>b09f2e9039a74ed9b05e3275c21d2bafd9778f8d<b>][Hak Akses : </b>Cashier<b>][Blokir : </b>Tidak<b>]</b>'),
(4, '2019-02-06 02:57:16', '1', 'Update', '<b>Update</b> data konfigurasi aplikasi pada tabel <b>sys_config</b>.<br><b>Data Lama = [Nama Apotek : </b><b>][Alamat : </b><b>][Telepon : </b><b>][Email : </b><b>][Website : </b><b>][Logo : </b><b>],<br> Data Baru = [Nama Apotek : </b>APOTEK NUSANTARA<b>][Alamat : </b>Rajabasa, Bandar Lampung, Lampung<b>][Telepon : </b>081377783334<b>][Email : </b>apoteknusantara@gmail.com<b>][Website : </b>www.apoteknusantara.com<b>][Logo : </b>logo.png<b>]</b>'),
(5, '2019-02-06 03:00:38', '2', 'Insert', '<b>Insert</b> data supplier pada tabel <b>supplier</b>.<br><b>[Kode Supplier : </b>SP001<b>][Nama Supplier : </b>AAM<b>][Alamat : </b>Jln. Kenanga No. 26 Rawa Laut Tanjung Karang<b>]</b>[Telepon : </b>0721262639<b>]'),
(6, '2019-02-06 03:00:38', '2', 'Insert', '<b>Insert</b> data supplier pada tabel <b>supplier</b>.<br><b>[Kode Supplier : </b>SP002<b>][Nama Supplier : </b>AMS<b>][Alamat : </b>Jln. Dr. Cipto M. No.3 Sumur Batu Teluk Betung<b>]</b>[Telepon : </b>0721484707<b>]'),
(7, '2019-02-06 03:00:38', '2', 'Insert', '<b>Insert</b> data supplier pada tabel <b>supplier</b>.<br><b>[Kode Supplier : </b>SP003<b>][Nama Supplier : </b>APL<b>][Alamat : </b>Jln. Tembesu No.18 Campang Raya<b>]</b>[Telepon : </b>0721785912<b>]'),
(8, '2019-02-06 03:00:38', '2', 'Insert', '<b>Insert</b> data supplier pada tabel <b>supplier</b>.<br><b>[Kode Supplier : </b>SP004<b>][Nama Supplier : </b>Aria Jiwa Farma<b>][Alamat : </b>Jln. Alam Hijau No. 16 Way Halim<b>]</b>[Telepon : </b>0721703500<b>]'),
(9, '2019-02-06 03:00:38', '2', 'Insert', '<b>Insert</b> data supplier pada tabel <b>supplier</b>.<br><b>[Kode Supplier : </b>SP005<b>][Nama Supplier : </b>Bina San Prima<b>][Alamat : </b>Jln. Tembesu No. 16A Campang Raya<b>]</b>[Telepon : </b>0721783559<b>]'),
(10, '2019-02-06 03:00:38', '2', 'Insert', '<b>Insert</b> data supplier pada tabel <b>supplier</b>.<br><b>[Kode Supplier : </b>SP006<b>][Nama Supplier : </b>Dos Ni Roha<b>][Alamat : </b>Jln. Panjang Wisma Indovision Kebun Jeruk Jakarta<b>]</b>[Telepon : </b>0721591359<b>]'),
(11, '2019-02-06 03:00:38', '2', 'Insert', '<b>Insert</b> data supplier pada tabel <b>supplier</b>.<br><b>[Kode Supplier : </b>SP007<b>][Nama Supplier : </b>Elkaka<b>][Alamat : </b>Jln. Pulau Batam Raya No. 34 Bandar Lampung<b>]</b>[Telepon : </b>0721293258<b>]'),
(12, '2019-02-06 03:00:38', '2', 'Insert', '<b>Insert</b> data supplier pada tabel <b>supplier</b>.<br><b>[Kode Supplier : </b>SP008<b>][Nama Supplier : </b>Enseval<b>][Alamat : </b>Jln. Tembesu No. 20 Campang Raya<b>]</b>[Telepon : </b>07218012266<b>]'),
(13, '2019-02-06 03:00:38', '2', 'Insert', '<b>Insert</b> data supplier pada tabel <b>supplier</b>.<br><b>[Kode Supplier : </b>SP009<b>][Nama Supplier : </b>IGM<b>][Alamat : </b>Jln. KH. Ah. Dahlan No.68 Bandar Lampung<b>]</b>[Telepon : </b>0721487131<b>]'),
(14, '2019-02-06 03:00:38', '2', 'Insert', '<b>Insert</b> data supplier pada tabel <b>supplier</b>.<br><b>[Kode Supplier : </b>SP010<b>][Nama Supplier : </b>Kimia Farma<b>][Alamat : </b>Jln. Tembesu 2 No. 3B Campang Raya<b>]</b>[Telepon : </b>0721789242<b>]'),
(15, '2019-02-06 03:00:38', '2', 'Insert', '<b>Insert</b> data supplier pada tabel <b>supplier</b>.<br><b>[Kode Supplier : </b>SP011<b>][Nama Supplier : </b>MPI<b>][Alamat : </b>Jln. P. Antasari No. 7B Kedamaian<b>]</b>[Telepon : </b>0721263191<b>]'),
(16, '2019-02-06 03:00:38', '2', 'Insert', '<b>Insert</b> data supplier pada tabel <b>supplier</b>.<br><b>[Kode Supplier : </b>SP012<b>][Nama Supplier : </b>Merapi Utama Pharma<b>][Alamat : </b>Jln. Cilosari 23 Cikini Menteng Jakarta<b>]</b>[Telepon : </b>0213141906<b>]'),
(17, '2019-02-06 03:00:38', '2', 'Insert', '<b>Insert</b> data supplier pada tabel <b>supplier</b>.<br><b>[Kode Supplier : </b>SP013<b>][Nama Supplier : </b>Penta Valent<b>][Alamat : </b>Jln. ZA Pagar Alam Gg PU Bandar Lampung<b>]</b>[Telepon : </b>0721359923<b>]'),
(18, '2019-02-06 03:00:38', '2', 'Insert', '<b>Insert</b> data supplier pada tabel <b>supplier</b>.<br><b>[Kode Supplier : </b>SP014<b>][Nama Supplier : </b>PPG<b>][Alamat : </b>Jln. Tembesu No. 15A Campang Raya<b>]</b>[Telepon : </b>0721781135<b>]'),
(19, '2019-02-06 03:00:38', '2', 'Insert', '<b>Insert</b> data supplier pada tabel <b>supplier</b>.<br><b>[Kode Supplier : </b>SP015<b>][Nama Supplier : </b>Rajawali Nusindo<b>][Alamat : </b>Jln. Denpasar Raya, Kuningan Jakarta<b>]</b>[Telepon : </b>0721261373<b>]'),
(20, '2019-02-06 03:00:38', '2', 'Insert', '<b>Insert</b> data supplier pada tabel <b>supplier</b>.<br><b>[Kode Supplier : </b>SP016<b>][Nama Supplier : </b>Sawah Besar Farmasi<b>][Alamat : </b>Jln. Mawar No. 2 Pahoman Bandar Lampung<b>]</b>[Telepon : </b>0721269896<b>]'),
(21, '2019-02-06 03:00:38', '2', 'Insert', '<b>Insert</b> data supplier pada tabel <b>supplier</b>.<br><b>[Kode Supplier : </b>SP017<b>][Nama Supplier : </b>Tri Sapta Jaya<b>][Alamat : </b>Perum Korpri Blok B9 Sukarame<b>]</b>[Telepon : </b>0721890511<b>]'),
(22, '2019-02-06 03:00:38', '2', 'Insert', '<b>Insert</b> data supplier pada tabel <b>supplier</b>.<br><b>[Kode Supplier : </b>SP018<b>][Nama Supplier : </b>United Dico Citas<b>][Alamat : </b>Jln. P. Emir Minoor 91<b>]</b>[Telepon : </b>0721251252<b>]'),
(23, '2019-02-06 03:01:56', '2', 'Insert', '<b>Insert</b> data satuan pada tabel <b>satuan</b>.<br><b>[Kode Satuan : </b>1<b>][Nama Satuan : </b>Tube<b>]'),
(24, '2019-02-06 03:02:02', '2', 'Insert', '<b>Insert</b> data satuan pada tabel <b>satuan</b>.<br><b>[Kode Satuan : </b>2<b>][Nama Satuan : </b>Strip<b>]'),
(25, '2019-02-06 03:02:09', '2', 'Insert', '<b>Insert</b> data satuan pada tabel <b>satuan</b>.<br><b>[Kode Satuan : </b>3<b>][Nama Satuan : </b>Kotak<b>]'),
(26, '2019-02-06 03:02:15', '2', 'Insert', '<b>Insert</b> data satuan pada tabel <b>satuan</b>.<br><b>[Kode Satuan : </b>4<b>][Nama Satuan : </b>Box<b>]'),
(27, '2019-02-06 03:02:20', '2', 'Insert', '<b>Insert</b> data satuan pada tabel <b>satuan</b>.<br><b>[Kode Satuan : </b>5<b>][Nama Satuan : </b>Botol<b>]'),
(28, '2019-02-06 03:03:03', '2', 'Insert', '<b>Insert</b> data obat pada tabel <b>obat</b>.<br><b>[Kode Obat : </b>OB0001<b>][Nama Obat : </b>Woods Biru<b>][Harga Beli : </b>12000<b>][Harga Jual : </b>16000<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>2<b>]'),
(29, '2019-02-06 03:03:20', '2', 'Insert', '<b>Insert</b> data obat pada tabel <b>obat</b>.<br><b>[Kode Obat : </b>OB0002<b>][Nama Obat : </b>Woods Merah<b>][Harga Beli : </b>12000<b>][Harga Jual : </b>16000<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>2<b>]'),
(30, '2019-02-06 03:04:35', '2', 'Insert', '<b>Insert</b> data obat pada tabel <b>obat</b>.<br><b>[Kode Obat : </b>OB0003<b>][Nama Obat : </b>OBH Nellco<b>][Harga Beli : </b>20625<b>][Harga Jual : </b>27500<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>2<b>]'),
(31, '2019-02-06 03:05:12', '2', 'Insert', '<b>Insert</b> data obat pada tabel <b>obat</b>.<br><b>[Kode Obat : </b>OB0004<b>][Nama Obat : </b>Sakatonik ABC<b>][Harga Beli : </b>9000<b>][Harga Jual : </b>12000<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>2<b>]'),
(32, '2019-02-06 03:05:39', '2', 'Insert', '<b>Insert</b> data obat pada tabel <b>obat</b>.<br><b>[Kode Obat : </b>OB0005<b>][Nama Obat : </b>Scotts Emulsion<b>][Harga Beli : </b>31500<b>][Harga Jual : </b>42000<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>2<b>]'),
(33, '2019-02-06 03:06:18', '2', 'Insert', '<b>Insert</b> data obat pada tabel <b>obat</b>.<br><b>[Kode Obat : </b>OB0006<b>][Nama Obat : </b>Sakatonik Liver<b>][Harga Beli : </b>7500<b>][Harga Jual : </b>10000<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>2<b>]'),
(34, '2019-02-06 03:06:45', '2', 'Insert', '<b>Insert</b> data obat pada tabel <b>obat</b>.<br><b>[Kode Obat : </b>OB0007<b>][Nama Obat : </b>Stimuno Syr<b>][Harga Beli : </b>18000<b>][Harga Jual : </b>24000<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>2<b>]'),
(35, '2019-02-06 03:07:07', '2', 'Insert', '<b>Insert</b> data obat pada tabel <b>obat</b>.<br><b>[Kode Obat : </b>OB0008<b>][Nama Obat : </b>Imboost Syr<b>][Harga Beli : </b>51750<b>][Harga Jual : </b>69000<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>2<b>]'),
(36, '2019-02-06 03:08:02', '2', 'Insert', '<b>Insert</b> data obat pada tabel <b>obat</b>.<br><b>[Kode Obat : </b>OB0009<b>][Nama Obat : </b>Neuremacyl Tab<b>][Harga Beli : </b>63750<b>][Harga Jual : </b>85000<b>][Kode Satuan : </b>4<b>][Minimum Stok : </b>2<b>]'),
(37, '2019-02-06 03:08:27', '2', 'Insert', '<b>Insert</b> data obat pada tabel <b>obat</b>.<br><b>[Kode Obat : </b>OB0010<b>][Nama Obat : </b>Pilkita<b>][Harga Beli : </b>112500<b>][Harga Jual : </b>150000<b>][Kode Satuan : </b>4<b>][Minimum Stok : </b>2<b>]'),
(38, '2019-02-06 03:08:50', '2', 'Insert', '<b>Insert</b> data obat pada tabel <b>obat</b>.<br><b>[Kode Obat : </b>OB0011<b>][Nama Obat : </b>Fatigon Spirit<b>][Harga Beli : </b>48750<b>][Harga Jual : </b>65000<b>][Kode Satuan : </b>4<b>][Minimum Stok : </b>2<b>]'),
(39, '2019-02-06 03:10:07', '2', 'Insert', '<b>Insert</b> data obat pada tabel <b>obat</b>.<br><b>[Kode Obat : </b>OB0012<b>][Nama Obat : </b>Bisolvon Kids<b>][Harga Beli : </b>21750<b>][Harga Jual : </b>29000<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>2<b>]'),
(40, '2019-02-06 03:10:37', '2', 'Insert', '<b>Insert</b> data obat pada tabel <b>obat</b>.<br><b>[Kode Obat : </b>OB0013<b>][Nama Obat : </b>Bisolvon Extra<b>][Harga Beli : </b>28125<b>][Harga Jual : </b>37500<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>2<b>]'),
(41, '2019-02-06 03:11:21', '2', 'Insert', '<b>Insert</b> data obat pada tabel <b>obat</b>.<br><b>[Kode Obat : </b>OB0014<b>][Nama Obat : </b>OBH Combi Anak<b>][Harga Beli : </b>8625<b>][Harga Jual : </b>11500<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>2<b>]'),
(42, '2019-02-06 03:11:41', '2', 'Insert', '<b>Insert</b> data obat pada tabel <b>obat</b>.<br><b>[Kode Obat : </b>OB0015<b>][Nama Obat : </b>OBH Combi Dewasa<b>][Harga Beli : </b>8625<b>][Harga Jual : </b>11500<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>2<b>]'),
(43, '2019-02-06 03:13:32', '2', 'Insert', '<b>Insert</b> data pembelian pada tabel <b>pembelian</b>.<br><b>[No. Pembelian : </b>BL02201900001<b>][Tanggal : </b>2019-02-06<b>][Kode Supplier : </b>SP005<b>][No. Nota : </b>BSN201902000012<b>][Total Bayar : </b>285000<b>]'),
(44, '2019-02-06 03:13:33', '2', 'Insert', '<b>Insert</b> detail data pembelian obat pada tabel <b>pembelian_detail</b>.<br><b>[No. Pembelian : </b>BL02201900001<b>][Kode Obat : </b>OB0006<b>][Harga Beli : </b>7500<b>][Jumlah Beli : </b>10<b>][Total Harga : </b>75000<b>]'),
(45, '2019-02-06 03:13:33', '2', 'Insert', '<b>Insert</b> detail data pembelian obat pada tabel <b>pembelian_detail</b>.<br><b>[No. Pembelian : </b>BL02201900001<b>][Kode Obat : </b>OB0004<b>][Harga Beli : </b>9000<b>][Jumlah Beli : </b>10<b>][Total Harga : </b>90000<b>]'),
(46, '2019-02-06 03:13:33', '2', 'Insert', '<b>Insert</b> detail data pembelian obat pada tabel <b>pembelian_detail</b>.<br><b>[No. Pembelian : </b>BL02201900001<b>][Kode Obat : </b>OB0002<b>][Harga Beli : </b>12000<b>][Jumlah Beli : </b>5<b>][Total Harga : </b>60000<b>]'),
(47, '2019-02-06 03:13:33', '2', 'Insert', '<b>Insert</b> detail data pembelian obat pada tabel <b>pembelian_detail</b>.<br><b>[No. Pembelian : </b>BL02201900001<b>][Kode Obat : </b>OB0001<b>][Harga Beli : </b>12000<b>][Jumlah Beli : </b>5<b>][Total Harga : </b>60000<b>]'),
(48, '2019-02-06 03:15:45', '2', 'Insert', '<b>Insert</b> data pembelian pada tabel <b>pembelian</b>.<br><b>[No. Pembelian : </b>BL02201900002<b>][Tanggal : </b>2019-02-06<b>][Kode Supplier : </b>SP010<b>][No. Nota : </b>KMF201902000035<b>][Total Bayar : </b>598125<b>]'),
(49, '2019-02-06 03:15:45', '2', 'Insert', '<b>Insert</b> detail data pembelian obat pada tabel <b>pembelian_detail</b>.<br><b>[No. Pembelian : </b>BL02201900002<b>][Kode Obat : </b>OB0007<b>][Harga Beli : </b>18000<b>][Jumlah Beli : </b>5<b>][Total Harga : </b>90000<b>]'),
(50, '2019-02-06 03:15:46', '2', 'Insert', '<b>Insert</b> detail data pembelian obat pada tabel <b>pembelian_detail</b>.<br><b>[No. Pembelian : </b>BL02201900002<b>][Kode Obat : </b>OB0008<b>][Harga Beli : </b>51750<b>][Jumlah Beli : </b>5<b>][Total Harga : </b>258750<b>]'),
(51, '2019-02-06 03:15:46', '2', 'Insert', '<b>Insert</b> detail data pembelian obat pada tabel <b>pembelian_detail</b>.<br><b>[No. Pembelian : </b>BL02201900002<b>][Kode Obat : </b>OB0013<b>][Harga Beli : </b>28125<b>][Jumlah Beli : </b>5<b>][Total Harga : </b>140625<b>]'),
(52, '2019-02-06 03:15:46', '2', 'Insert', '<b>Insert</b> detail data pembelian obat pada tabel <b>pembelian_detail</b>.<br><b>[No. Pembelian : </b>BL02201900002<b>][Kode Obat : </b>OB0012<b>][Harga Beli : </b>21750<b>][Jumlah Beli : </b>5<b>][Total Harga : </b>108750<b>]'),
(53, '2019-02-06 03:17:12', '2', 'Insert', '<b>Insert</b> data pembelian pada tabel <b>pembelian</b>.<br><b>[No. Pembelian : </b>BL02201900003<b>][Tanggal : </b>2019-02-06<b>][Kode Supplier : </b>SP012<b>][No. Nota : </b>MUP201902000045<b>][Total Bayar : </b>275625<b>]'),
(54, '2019-02-06 03:17:12', '2', 'Insert', '<b>Insert</b> detail data pembelian obat pada tabel <b>pembelian_detail</b>.<br><b>[No. Pembelian : </b>BL02201900003<b>][Kode Obat : </b>OB0015<b>][Harga Beli : </b>8625<b>][Jumlah Beli : </b>10<b>][Total Harga : </b>86250<b>]'),
(55, '2019-02-06 03:17:12', '2', 'Insert', '<b>Insert</b> detail data pembelian obat pada tabel <b>pembelian_detail</b>.<br><b>[No. Pembelian : </b>BL02201900003<b>][Kode Obat : </b>OB0014<b>][Harga Beli : </b>8625<b>][Jumlah Beli : </b>10<b>][Total Harga : </b>86250<b>]'),
(56, '2019-02-06 03:17:12', '2', 'Insert', '<b>Insert</b> detail data pembelian obat pada tabel <b>pembelian_detail</b>.<br><b>[No. Pembelian : </b>BL02201900003<b>][Kode Obat : </b>OB0003<b>][Harga Beli : </b>20625<b>][Jumlah Beli : </b>5<b>][Total Harga : </b>103125<b>]'),
(57, '2019-02-06 03:18:04', '2', 'Insert', '<b>Insert</b> data pembelian pada tabel <b>pembelian</b>.<br><b>[No. Pembelian : </b>BL02201900004<b>][Tanggal : </b>2019-02-06<b>][Kode Supplier : </b>SP017<b>][No. Nota : </b>TSJ201902000011<b>][Total Bayar : </b>1282500<b>]'),
(58, '2019-02-06 03:18:04', '2', 'Insert', '<b>Insert</b> detail data pembelian obat pada tabel <b>pembelian_detail</b>.<br><b>[No. Pembelian : </b>BL02201900004<b>][Kode Obat : </b>OB0005<b>][Harga Beli : </b>31500<b>][Jumlah Beli : </b>5<b>][Total Harga : </b>157500<b>]'),
(59, '2019-02-06 03:18:04', '2', 'Insert', '<b>Insert</b> detail data pembelian obat pada tabel <b>pembelian_detail</b>.<br><b>[No. Pembelian : </b>BL02201900004<b>][Kode Obat : </b>OB0009<b>][Harga Beli : </b>63750<b>][Jumlah Beli : </b>5<b>][Total Harga : </b>318750<b>]'),
(60, '2019-02-06 03:18:05', '2', 'Insert', '<b>Insert</b> detail data pembelian obat pada tabel <b>pembelian_detail</b>.<br><b>[No. Pembelian : </b>BL02201900004<b>][Kode Obat : </b>OB0010<b>][Harga Beli : </b>112500<b>][Jumlah Beli : </b>5<b>][Total Harga : </b>562500<b>]'),
(61, '2019-02-06 03:18:05', '2', 'Insert', '<b>Insert</b> detail data pembelian obat pada tabel <b>pembelian_detail</b>.<br><b>[No. Pembelian : </b>BL02201900004<b>][Kode Obat : </b>OB0011<b>][Harga Beli : </b>48750<b>][Jumlah Beli : </b>5<b>][Total Harga : </b>243750<b>]'),
(62, '2019-02-06 03:22:22', '3', 'Insert', '<b>Insert</b> data penjualan pada tabel <b>penjualan</b>.<br><b>[No. Penjualan : </b>JL02201900001<b>][Tanggal : </b>2019-02-06<b>][Total Bayar : </b>85000<b>][Uang Bayar : </b>100000<b>]'),
(63, '2019-02-06 03:22:22', '3', 'Insert', '<b>Insert</b> detail data penjualan obat pada tabel <b>penjualan_detail</b>.<br><b>[No. Penjualan : </b>JL02201900001<b>][Kode Obat : </b>OB0008<b>][Harga Beli : </b>51750<b>][Harga Jual : </b>69000<b>][Jumlah Jual : </b>1<b>][Total Harga : </b>69000<b>]'),
(64, '2019-02-06 03:22:22', '3', 'Insert', '<b>Insert</b> detail data penjualan obat pada tabel <b>penjualan_detail</b>.<br><b>[No. Penjualan : </b>JL02201900001<b>][Kode Obat : </b>OB0001<b>][Harga Beli : </b>12000<b>][Harga Jual : </b>16000<b>][Jumlah Jual : </b>1<b>][Total Harga : </b>16000<b>]'),
(65, '2019-02-06 03:23:26', '3', 'Insert', '<b>Insert</b> data penjualan pada tabel <b>penjualan</b>.<br><b>[No. Penjualan : </b>JL02201900002<b>][Tanggal : </b>2019-02-06<b>][Total Bayar : </b>11500<b>][Uang Bayar : </b>15000<b>]'),
(66, '2019-02-06 03:23:26', '3', 'Insert', '<b>Insert</b> detail data penjualan obat pada tabel <b>penjualan_detail</b>.<br><b>[No. Penjualan : </b>JL02201900002<b>][Kode Obat : </b>OB0014<b>][Harga Beli : </b>8625<b>][Harga Jual : </b>11500<b>][Jumlah Jual : </b>1<b>][Total Harga : </b>11500<b>]'),
(67, '2019-02-06 03:24:18', '3', 'Insert', '<b>Insert</b> data penjualan pada tabel <b>penjualan</b>.<br><b>[No. Penjualan : </b>JL02201900003<b>][Tanggal : </b>2019-02-06<b>][Total Bayar : </b>134000<b>][Uang Bayar : </b>150000<b>]'),
(68, '2019-02-06 03:24:18', '3', 'Insert', '<b>Insert</b> detail data penjualan obat pada tabel <b>penjualan_detail</b>.<br><b>[No. Penjualan : </b>JL02201900003<b>][Kode Obat : </b>OB0008<b>][Harga Beli : </b>51750<b>][Harga Jual : </b>69000<b>][Jumlah Jual : </b>1<b>][Total Harga : </b>69000<b>]'),
(69, '2019-02-06 03:24:18', '3', 'Insert', '<b>Insert</b> detail data penjualan obat pada tabel <b>penjualan_detail</b>.<br><b>[No. Penjualan : </b>JL02201900003<b>][Kode Obat : </b>OB0011<b>][Harga Beli : </b>48750<b>][Harga Jual : </b>65000<b>][Jumlah Jual : </b>1<b>][Total Harga : </b>65000<b>]'),
(70, '2019-02-06 03:24:54', '3', 'Insert', '<b>Insert</b> data penjualan pada tabel <b>penjualan</b>.<br><b>[No. Penjualan : </b>JL02201900004<b>][Tanggal : </b>2019-02-06<b>][Total Bayar : </b>81000<b>][Uang Bayar : </b>100000<b>]'),
(71, '2019-02-06 03:24:54', '3', 'Insert', '<b>Insert</b> detail data penjualan obat pada tabel <b>penjualan_detail</b>.<br><b>[No. Penjualan : </b>JL02201900004<b>][Kode Obat : </b>OB0008<b>][Harga Beli : </b>51750<b>][Harga Jual : </b>69000<b>][Jumlah Jual : </b>1<b>][Total Harga : </b>69000<b>]'),
(72, '2019-02-06 03:24:54', '3', 'Insert', '<b>Insert</b> detail data penjualan obat pada tabel <b>penjualan_detail</b>.<br><b>[No. Penjualan : </b>JL02201900004<b>][Kode Obat : </b>OB0004<b>][Harga Beli : </b>9000<b>][Harga Jual : </b>12000<b>][Jumlah Jual : </b>1<b>][Total Harga : </b>12000<b>]'),
(73, '2019-02-06 03:26:39', '3', 'Insert', '<b>Insert</b> data penjualan pada tabel <b>penjualan</b>.<br><b>[No. Penjualan : </b>JL02201900005<b>][Tanggal : </b>2019-02-06<b>][Total Bayar : </b>65000<b>][Uang Bayar : </b>65000<b>]'),
(74, '2019-02-06 03:26:39', '3', 'Insert', '<b>Insert</b> detail data penjualan obat pada tabel <b>penjualan_detail</b>.<br><b>[No. Penjualan : </b>JL02201900005<b>][Kode Obat : </b>OB0011<b>][Harga Beli : </b>48750<b>][Harga Jual : </b>65000<b>][Jumlah Jual : </b>1<b>][Total Harga : </b>65000<b>]'),
(75, '2019-06-25 14:26:50', '1', 'Update', '<b>Update</b> data pengguna pada tabel <b>sys_users</b>.<br><b>Data Lama = [ID User : </b>1<b>][Nama User : </b>Indra Styawantoro<b>][Username : </b>indrasatya<b>][Password : </b>2437018d9a925c9fce796b99bfb9591728c5f208<b>][Hak Akses : </b>Super Admin<b>][Blokir : </b>Tidak<b>],<br> Data Baru = [ID User : </b>1<b>][Nama User : </b>Indra Styawantoro<b>][Username : </b>admin<b>][Password : </b>adcd7048512e64b48da55b027577886ee5a36350<b>][Hak Akses : </b>Super Admin<b>][Blokir : </b>Tidak<b>]</b>'),
(76, '2019-06-25 14:27:05', '1', 'Delete', '<b>Delete</b> data penjualan pada tabel <b>penjualan</b>. <br> <b>[No. Penjualan : </b>JL02201900005<b>][Tanggal : </b>06-02-2019<b>][Total Bayar : </b>65000<b>]'),
(77, '2019-06-25 14:27:17', '1', 'Delete', '<b>Delete</b> data penjualan pada tabel <b>penjualan</b>. <br> <b>[No. Penjualan : </b>JL02201900004<b>][Tanggal : </b>06-02-2019<b>][Total Bayar : </b>81000<b>]'),
(78, '2019-06-25 14:27:20', '1', 'Delete', '<b>Delete</b> data penjualan pada tabel <b>penjualan</b>. <br> <b>[No. Penjualan : </b>JL02201900003<b>][Tanggal : </b>06-02-2019<b>][Total Bayar : </b>134000<b>]'),
(79, '2019-06-25 14:27:22', '1', 'Delete', '<b>Delete</b> data penjualan pada tabel <b>penjualan</b>. <br> <b>[No. Penjualan : </b>JL02201900002<b>][Tanggal : </b>06-02-2019<b>][Total Bayar : </b>11500<b>]'),
(80, '2019-06-25 14:27:24', '1', 'Delete', '<b>Delete</b> data penjualan pada tabel <b>penjualan</b>. <br> <b>[No. Penjualan : </b>JL02201900001<b>][Tanggal : </b>06-02-2019<b>][Total Bayar : </b>85000<b>]'),
(81, '2019-06-25 14:27:30', '1', 'Delete', '<b>Delete</b> data pembelian pada tabel <b>pembelian</b>. <br> <b>[No. Pembelian : </b>BL02201900004<b>][Tanggal : </b>06-02-2019<b>][Kode Supplier : </b>SP017<b>][No. Nota : </b>TSJ201902000011<b>][Total Bayar : </b>1282500<b>]'),
(82, '2019-06-25 14:27:32', '1', 'Delete', '<b>Delete</b> data pembelian pada tabel <b>pembelian</b>. <br> <b>[No. Pembelian : </b>BL02201900003<b>][Tanggal : </b>06-02-2019<b>][Kode Supplier : </b>SP012<b>][No. Nota : </b>MUP201902000045<b>][Total Bayar : </b>275625<b>]'),
(83, '2019-06-25 14:27:33', '1', 'Delete', '<b>Delete</b> data pembelian pada tabel <b>pembelian</b>. <br> <b>[No. Pembelian : </b>BL02201900002<b>][Tanggal : </b>06-02-2019<b>][Kode Supplier : </b>SP010<b>][No. Nota : </b>KMF201902000035<b>][Total Bayar : </b>598125<b>]'),
(84, '2019-06-25 14:27:35', '1', 'Delete', '<b>Delete</b> data pembelian pada tabel <b>pembelian</b>. <br> <b>[No. Pembelian : </b>BL02201900001<b>][Tanggal : </b>06-02-2019<b>][Kode Supplier : </b>SP005<b>][No. Nota : </b>BSN201902000012<b>][Total Bayar : </b>285000<b>]'),
(85, '2019-06-25 14:28:38', '1', 'Update', '<b>Update</b> data pengguna pada tabel <b>sys_users</b>.<br><b>Data Lama = [ID User : </b>1<b>][Nama User : </b>Indra Styawantoro<b>][Username : </b>admin<b>][Password : </b>adcd7048512e64b48da55b027577886ee5a36350<b>][Hak Akses : </b>Super Admin<b>][Blokir : </b>Tidak<b>],<br> Data Baru = [ID User : </b>1<b>][Nama User : </b>Rifcy Rizaldy<b>][Username : </b>admin<b>][Password : </b>adcd7048512e64b48da55b027577886ee5a36350<b>][Hak Akses : </b>Super Admin<b>][Blokir : </b>Tidak<b>]</b>'),
(86, '2019-06-25 14:28:51', '1', 'Update', '<b>Update</b> data pengguna pada tabel <b>sys_users</b>.<br><b>Data Lama = [ID User : </b>3<b>][Nama User : </b>Kadina Putri<b>][Username : </b>kadina<b>][Password : </b>b09f2e9039a74ed9b05e3275c21d2bafd9778f8d<b>][Hak Akses : </b>Cashier<b>][Blokir : </b>Tidak<b>],<br> Data Baru = [ID User : </b>3<b>][Nama User : </b>Kadina Putri<b>][Username : </b>123<b>][Password : </b>adcd7048512e64b48da55b027577886ee5a36350<b>][Hak Akses : </b>Cashier<b>][Blokir : </b>Tidak<b>]</b>'),
(87, '2019-06-25 14:29:42', '1', 'Update', '<b>Update</b> data pengguna pada tabel <b>sys_users</b>.<br><b>Data Lama = [ID User : </b>2<b>][Nama User : </b>Danang Kesuma<b>][Username : </b>danang<b>][Password : </b>f8966cc671220b4858818afca4a8c9eedbeb6a5d<b>][Hak Akses : </b>Purchasing<b>][Blokir : </b>Tidak<b>],<br> Data Baru = [ID User : </b>2<b>][Nama User : </b>Danang Kesuma<b>][Username : </b>danang<b>][Password : </b>f8966cc671220b4858818afca4a8c9eedbeb6a5d<b>][Hak Akses : </b>Purchasing<b>][Blokir : </b>Tidak<b>]</b>'),
(88, '2019-08-19 14:07:25', '1', 'Update', '<b>Update</b> data pengguna pada tabel <b>sys_users</b>.<br><b>Data Lama = [ID User : </b>3<b>][Nama User : </b>Kadina Putri<b>][Username : </b>123<b>][Password : </b>adcd7048512e64b48da55b027577886ee5a36350<b>][Hak Akses : </b>Cashier<b>][Blokir : </b>Tidak<b>],<br> Data Baru = [ID User : </b>3<b>][Nama User : </b>Kadina Putri<b>][Username : </b>kasir<b>][Password : </b>adcd7048512e64b48da55b027577886ee5a36350<b>][Hak Akses : </b>Cashier<b>][Blokir : </b>Tidak<b>]</b>'),
(89, '2019-08-19 14:07:42', '1', 'Update', '<b>Update</b> data pengguna pada tabel <b>sys_users</b>.<br><b>Data Lama = [ID User : </b>2<b>][Nama User : </b>Danang Kesuma<b>][Username : </b>danang<b>][Password : </b>f8966cc671220b4858818afca4a8c9eedbeb6a5d<b>][Hak Akses : </b>Purchasing<b>][Blokir : </b>Tidak<b>],<br> Data Baru = [ID User : </b>2<b>][Nama User : </b>Danang Kesuma<b>][Username : </b>purchasing<b>][Password : </b>adcd7048512e64b48da55b027577886ee5a36350<b>][Hak Akses : </b>Purchasing<b>][Blokir : </b>Tidak<b>]</b>'),
(90, '2019-08-19 14:52:03', '1', 'Insert', '<b>Insert</b> data pembelian pada tabel <b>pembelian</b>.<br><b>[No. Pembelian : </b>BL08201900001<b>][Tanggal : </b>2019-08-19<b>][Kode Supplier : </b>SP002<b>][No. Nota : </b>12345<b>][Total Bayar : </b>1575000<b>]'),
(91, '2019-08-19 14:52:03', '1', 'Insert', '<b>Insert</b> detail data pembelian obat pada tabel <b>pembelian_detail</b>.<br><b>[No. Pembelian : </b>BL08201900001<b>][Kode Obat : </b>OB0005<b>][Harga Beli : </b>31500<b>][Jumlah Beli : </b>50<b>][Total Harga : </b>1575000<b>]'),
(92, '2019-08-22 14:51:02', '3', 'Insert', '<b>Insert</b> data penjualan pada tabel <b>penjualan</b>.<br><b>[No. Penjualan : </b>JL08201900001<b>][Tanggal : </b>2019-08-22<b>][Total Bayar : </b>1680000<b>][Uang Bayar : </b>50000000<b>]'),
(93, '2019-08-22 14:51:02', '3', 'Insert', '<b>Insert</b> detail data penjualan obat pada tabel <b>penjualan_detail</b>.<br><b>[No. Penjualan : </b>JL08201900001<b>][Kode Obat : </b>OB0005<b>][Harga Beli : </b>31500<b>][Harga Jual : </b>42000<b>][Jumlah Jual : </b>40<b>][Total Harga : </b>1680000<b>]'),
(94, '2020-06-30 12:58:00', '1', 'Update', '<b>Update</b> data konfigurasi aplikasi pada tabel <b>sys_config</b>.<br><b>Data Lama = [Nama Apotek : </b>APOTEK NUSANTARA<b>][Alamat : </b>Rajabasa, Bandar Lampung, Lampung<b>][Telepon : </b>081377783334<b>][Email : </b>apoteknusantara@gmail.com<b>][Website : </b>www.apoteknusantara.com<b>][Logo : </b>logo.png<b>],<br> Data Baru = [Nama Apotek : </b>APOTEK KURNIA<b>][Alamat : </b>Pisangan, Bontang Selatan, BONTANG<b>][Telepon : </b>081245876598<b>][Email : </b>apotekkurnia@gmail.com<b>][Website : </b>www.apotekkurnia.com<b>][Logo : </b>logo.png<b>]</b>'),
(95, '2020-06-30 12:58:50', '1', 'Update', '<b>Update</b> data pengguna pada tabel <b>sys_users</b>.<br><b>Data Lama = [ID User : </b>3<b>][Nama User : </b>Kadina Putri<b>][Username : </b>kasir<b>][Password : </b>adcd7048512e64b48da55b027577886ee5a36350<b>][Hak Akses : </b>Cashier<b>][Blokir : </b>Tidak<b>],<br> Data Baru = [ID User : </b>3<b>][Nama User : </b>Putri Sulis<b>][Username : </b>kasir<b>][Password : </b>adcd7048512e64b48da55b027577886ee5a36350<b>][Hak Akses : </b>Cashier<b>][Blokir : </b>Tidak<b>]</b>'),
(96, '2020-06-30 12:59:20', '1', 'Update', '<b>Update</b> data pengguna pada tabel <b>sys_users</b>.<br><b>Data Lama = [ID User : </b>2<b>][Nama User : </b>Danang Kesuma<b>][Username : </b>purchasing<b>][Password : </b>adcd7048512e64b48da55b027577886ee5a36350<b>][Hak Akses : </b>Purchasing<b>][Blokir : </b>Tidak<b>],<br> Data Baru = [ID User : </b>2<b>][Nama User : </b>Romi Santos<b>][Username : </b>purchasing<b>][Password : </b>adcd7048512e64b48da55b027577886ee5a36350<b>][Hak Akses : </b>Purchasing<b>][Blokir : </b>Tidak<b>]</b>'),
(97, '2020-06-30 12:59:34', '1', 'Update', '<b>Update</b> data pengguna pada tabel <b>sys_users</b>.<br><b>Data Lama = [ID User : </b>1<b>][Nama User : </b>Rifcy Rizaldy<b>][Username : </b>admin<b>][Password : </b>adcd7048512e64b48da55b027577886ee5a36350<b>][Hak Akses : </b>Super Admin<b>][Blokir : </b>Tidak<b>],<br> Data Baru = [ID User : </b>1<b>][Nama User : </b>Rony Gunawan<b>][Username : </b>admin<b>][Password : </b>adcd7048512e64b48da55b027577886ee5a36350<b>][Hak Akses : </b>Super Admin<b>][Blokir : </b>Tidak<b>]</b>'),
(98, '2020-10-27 10:12:21', '1', 'Update', '<b>Update</b> data konfigurasi aplikasi pada tabel <b>sys_config</b>.<br><b>Data Lama = [Nama Apotek : </b>APOTEK KURNIA<b>][Alamat : </b>Pisangan, Bontang Selatan, BONTANG<b>][Telepon : </b>081245876598<b>][Email : </b>apotekkurnia@gmail.com<b>][Website : </b>www.apotekkurnia.com<b>][Logo : </b>logo.png<b>],<br> Data Baru = [Nama Apotek : </b>Puan Kopi<b>][Alamat : </b>Perum Korpri Blok 1 C No.26, Jl. Abdi Praja, Sepinggan Baru, Balikpapan Selatan<b>][Telepon : </b>081245876598<b>][Email : </b>puankopi@gmail.com<b>][Website : </b>www.puankopi.com<b>][Logo : </b>IMG-20190708-WA0005.jpg<b>]</b>'),
(99, '2020-10-29 10:50:09', '1', 'Insert', '<b>Insert</b> data pengguna pada tabel <b>sys_users</b>.<br><b>[ID User : </b>4<b>][Nama User : </b>Brian manuel<b>][Username : </b>brian<b>][Password : </b>5fa52aecbae954e6e27aedbc7246bfd8ca21542a<b>][Hak Akses : </b>Leader<b>][Blokir : </b>Tidak<b>]</b>'),
(100, '2020-10-29 12:06:35', '1', 'Update', '<b>Update</b> data pengguna pada tabel <b>sys_users</b>.<br><b>Data Lama = [ID User : </b>1<b>][Nama User : </b>Rony Gunawan<b>][Username : </b>admin<b>][Password : </b>adcd7048512e64b48da55b027577886ee5a36350<b>][Hak Akses : </b>Super Admin<b>][Blokir : </b>Tidak<b>],<br> Data Baru = [ID User : </b>1<b>][Nama User : </b>Arief Maulana<b>][Username : </b>admin<b>][Password : </b>adcd7048512e64b48da55b027577886ee5a36350<b>][Hak Akses : </b>Super Admin<b>][Blokir : </b>Tidak<b>]</b>'),
(101, '2020-10-29 12:06:53', '1', 'Update', '<b>Update</b> data pengguna pada tabel <b>sys_users</b>.<br><b>Data Lama = [ID User : </b>2<b>][Nama User : </b>Romi Santos<b>][Username : </b>purchasing<b>][Password : </b>adcd7048512e64b48da55b027577886ee5a36350<b>][Hak Akses : </b>Purchasing<b>][Blokir : </b>Tidak<b>],<br> Data Baru = [ID User : </b>2<b>][Nama User : </b>Rizal Mahadewa<b>][Username : </b>purchasing<b>][Password : </b>adcd7048512e64b48da55b027577886ee5a36350<b>][Hak Akses : </b>Purchasing<b>][Blokir : </b>Tidak<b>]</b>'),
(102, '2020-10-29 12:07:09', '1', 'Update', '<b>Update</b> data pengguna pada tabel <b>sys_users</b>.<br><b>Data Lama = [ID User : </b>2<b>][Nama User : </b>Rizal Mahadewa<b>][Username : </b>purchasing<b>][Password : </b>adcd7048512e64b48da55b027577886ee5a36350<b>][Hak Akses : </b>Purchasing<b>][Blokir : </b>Tidak<b>],<br> Data Baru = [ID User : </b>2<b>][Nama User : </b>Rizal Mahadewa<b>][Username : </b>purchasing<b>][Password : </b>9b0d0a56c581466ed62b0969c734334d4032f37e<b>][Hak Akses : </b>Purchasing<b>][Blokir : </b>Tidak<b>]</b>'),
(103, '2020-10-29 12:07:22', '1', 'Update', '<b>Update</b> data pengguna pada tabel <b>sys_users</b>.<br><b>Data Lama = [ID User : </b>1<b>][Nama User : </b>Arief Maulana<b>][Username : </b>admin<b>][Password : </b>adcd7048512e64b48da55b027577886ee5a36350<b>][Hak Akses : </b>Super Admin<b>][Blokir : </b>Tidak<b>],<br> Data Baru = [ID User : </b>1<b>][Nama User : </b>Arief Maulana<b>][Username : </b>admin<b>][Password : </b>036d0ef7567a20b5a4ad24a354ea4a945ddab676<b>][Hak Akses : </b>Super Admin<b>][Blokir : </b>Tidak<b>]</b>'),
(104, '2020-10-29 12:09:09', '1', 'Update', '<b>Update</b> data pengguna pada tabel <b>sys_users</b>.<br><b>Data Lama = [ID User : </b>3<b>][Nama User : </b>Putri Sulis<b>][Username : </b>kasir<b>][Password : </b>adcd7048512e64b48da55b027577886ee5a36350<b>][Hak Akses : </b>Cashier<b>][Blokir : </b>Tidak<b>],<br> Data Baru = [ID User : </b>3<b>][Nama User : </b>Putri Cantika<b>][Username : </b>kasir<b>][Password : </b>530ec425c4f8c5cada655313e5bcbf92a819f837<b>][Hak Akses : </b>Cashier<b>][Blokir : </b>Tidak<b>]</b>'),
(105, '2020-10-31 07:58:37', '1', 'Update', '<b>Update</b> data pengguna pada tabel <b>sys_users</b>.<br><b>Data Lama = [ID User : </b>2<b>][Nama User : </b>Rizal Mahadewa<b>][Username : </b>purchasing<b>][Password : </b>9b0d0a56c581466ed62b0969c734334d4032f37e<b>][Hak Akses : </b>Purchasing<b>][Blokir : </b>Tidak<b>],<br> Data Baru = [ID User : </b>2<b>][Nama User : </b>Rizal Mahadewa<b>][Username : </b>purchasing<b>][Password : </b>9b0d0a56c581466ed62b0969c734334d4032f37e<b>][Hak Akses : </b>Purchasing<b>][Blokir : </b>Tidak<b>]</b>'),
(106, '2020-11-06 03:57:31', '1', 'Update', '<b>Update</b> data konfigurasi aplikasi pada tabel <b>sys_config</b>.<br><b>Data Lama = [Nama Apotek : </b>Puan Kopi<b>][Alamat : </b>Perum Korpri Blok 1 C No.26, Jl. Abdi Praja, Sepinggan Baru, Balikpapan Selatan<b>][Telepon : </b>081245876598<b>][Email : </b>puankopi@gmail.com<b>][Website : </b>www.puankopi.com<b>][Logo : </b>IMG-20190708-WA0005.jpg<b>],<br> Data Baru = [Nama Apotek : </b>PÃºan Kopi<b>][Alamat : </b>Perum Korpri Blok 1 C No.26, Jl. Abdi Praja, Sepinggan Baru, Balikpapan Selatan<b>][Telepon : </b>081245876598<b>][Email : </b>puankopi@gmail.com<b>][Website : </b>www.puankopi.com<b>][Logo : </b>IMG-20190708-WA0005.jpg<b>]</b>'),
(107, '2020-11-06 16:00:02', '1', 'Insert', '<b>Insert</b> data pembelian pada tabel <b>pembelian</b>.<br><b>[No. Pembelian : </b>BL11202000002<b>][Tanggal : </b>2020-11-06<b>][Kode Supplier : </b>SP001<b>][No. Nota : </b>32421<b>][Total Bayar : </b>36000<b>]'),
(108, '2020-11-06 16:02:38', '1', 'Insert', '<b>Insert</b> data pembelian pada tabel <b>pembelian</b>.<br><b>[No. Pembelian : </b>BL11202000003<b>][Tanggal : </b>2020-11-06<b>][Kode Supplier : </b>SP004<b>][No. Nota : </b>11111<b>][Total Bayar : </b>140625<b>]'),
(109, '2020-11-06 16:05:54', '1', 'Insert', '<b>Insert</b> data pembelian pada tabel <b>pembelian</b>.<br><b>[No. Pembelian : </b>BL11202000004<b>][Tanggal : </b>2020-11-06<b>][Kode Supplier : </b>SP018<b>][No. Nota : </b>45231<b>][Total Bayar : </b>225000<b>]'),
(110, '2020-11-06 16:10:51', '1', 'Insert', '<b>Insert</b> data pembelian pada tabel <b>pembelian</b>.<br><b>[No. Pembelian : </b>BL11202000005<b>][Tanggal : </b>2020-11-06<b>][Kode Supplier : </b>SP015<b>][No. Nota : </b>98212<b>][Total Bayar : </b>146250<b>]'),
(111, '2020-11-06 16:20:47', '1', 'Insert', '<b>Insert</b> data pembelian pada tabel <b>pembelian</b>.<br><b>[No. Pembelian : </b>BL11202000006<b>][Tanggal : </b>2020-11-06<b>][Kode Supplier : </b>SP014<b>][No. Nota : </b>56232<b>][Total Bayar : </b>43500<b>]'),
(112, '2020-11-06 16:20:47', '1', 'Insert', '<b>Insert</b> detail data pembelian bahan baku pada tabel <b>pembelian_detail</b>.<br><b>[No. Pembelian : </b>BL11202000006<b>][Kode Bahan Baku : </b>OB0012<b>][Harga Beli : </b>21750<b>][Jumlah Beli : </b>2<b>][Total Harga : </b>43500<b>]'),
(113, '2020-11-06 16:22:06', '1', 'Delete', '<b>Delete</b> data pembelian pada tabel <b>pembelian</b>. <br> <b>[No. Pembelian : </b>BL11202000005<b>][Tanggal : </b>06-11-2020<b>][Kode Supplier : </b>SP015<b>][No. Nota : </b>98212<b>][Total Bayar : </b>146250<b>]'),
(114, '2020-11-06 16:23:03', '1', 'Insert', '<b>Insert</b> data pembelian pada tabel <b>pembelian</b>.<br><b>[No. Pembelian : </b>BL11202000007<b>][Tanggal : </b>2020-11-06<b>][Kode Supplier : </b>SP012<b>][No. Nota : </b>56463<b>][Total Bayar : </b>304500<b>]'),
(115, '2020-11-06 16:23:03', '1', 'Insert', '<b>Insert</b> detail data pembelian bahan baku pada tabel <b>pembelian_detail</b>.<br><b>[No. Pembelian : </b>BL11202000007<b>][Kode Bahan Baku : </b>OB0008<b>][Harga Beli : </b>51750<b>][Jumlah Beli : </b>4<b>][Total Harga : </b>207000<b>]'),
(116, '2020-11-06 16:23:04', '1', 'Insert', '<b>Insert</b> detail data pembelian bahan baku pada tabel <b>pembelian_detail</b>.<br><b>[No. Pembelian : </b>BL11202000007<b>][Kode Bahan Baku : </b>OB0011<b>][Harga Beli : </b>48750<b>][Jumlah Beli : </b>2<b>][Total Harga : </b>97500<b>]'),
(117, '2020-11-06 16:38:41', '1', 'Delete', '<b>Delete</b> data pembelian pada tabel <b>pembelian</b>. <br> <b>[No. Pembelian : </b>BL11202000002<b>][Tanggal : </b>06-11-2020<b>][Kode Supplier : </b>SP001<b>][No. Nota : </b>32421<b>][Total Bayar : </b>36000<b>]'),
(118, '2020-11-06 16:38:54', '1', 'Delete', '<b>Delete</b> data pembelian pada tabel <b>pembelian</b>. <br> <b>[No. Pembelian : </b>BL11202000003<b>][Tanggal : </b>06-11-2020<b>][Kode Supplier : </b>SP004<b>][No. Nota : </b>11111<b>][Total Bayar : </b>140625<b>]'),
(119, '2020-11-06 16:50:57', '1', 'Delete', '<b>Delete</b> data pembelian pada tabel <b>pembelian</b>. <br> <b>[No. Pembelian : </b>BL11202000005<b>][Tanggal : </b>06-11-2020<b>][Kode Supplier : </b>SP015<b>][No. Nota : </b>98212<b>][Total Bayar : </b>146250<b>]'),
(120, '2020-11-06 16:51:12', '1', 'Delete', '<b>Delete</b> data pembelian pada tabel <b>pembelian</b>. <br> <b>[No. Pembelian : </b>BL11202000002<b>][Tanggal : </b>06-11-2020<b>][Kode Supplier : </b>SP001<b>][No. Nota : </b>32421<b>][Total Bayar : </b>36000<b>]'),
(121, '2020-11-06 18:03:28', '1', 'Delete', '<b>Delete</b> data pembelian pada tabel <b>pembelian</b>. <br> <b>[No. Pembelian : </b>BL11202000003<b>][Tanggal : </b>06-11-2020<b>][Kode Supplier : </b>SP004<b>][No. Nota : </b>11111<b>][Total Bayar : </b>140625<b>]'),
(122, '2020-11-06 18:03:30', '1', 'Delete', '<b>Delete</b> data pembelian pada tabel <b>pembelian</b>. <br> <b>[No. Pembelian : </b>BL11202000004<b>][Tanggal : </b>06-11-2020<b>][Kode Supplier : </b>SP018<b>][No. Nota : </b>45231<b>][Total Bayar : </b>225000<b>]'),
(123, '2020-11-06 18:23:29', '1', 'Delete', '<b>Delete</b> data pembelian pada tabel <b>pembelian</b>. <br> <b>[No. Pembelian : </b>BL11202000006<b>][Tanggal : </b>06-11-2020<b>][Kode Supplier : </b>SP014<b>][No. Nota : </b>56232<b>][Total Bayar : </b>43500<b>]'),
(124, '2020-11-06 18:23:32', '1', 'Delete', '<b>Delete</b> data pembelian pada tabel <b>pembelian</b>. <br> <b>[No. Pembelian : </b>BL11202000007<b>][Tanggal : </b>06-11-2020<b>][Kode Supplier : </b>SP012<b>][No. Nota : </b>56463<b>][Total Bayar : </b>304500<b>]'),
(125, '2020-11-06 18:37:12', '1', 'Insert', '<b>Insert</b> data pembelian pada tabel <b>pembelian</b>.<br><b>[No. Pembelian : </b>BL11202000002<b>][Tanggal : </b>2020-11-07<b>][Kode Supplier : </b>SP002<b>][No. Nota : </b>12321<b>][Total Bayar : </b>36000<b>]'),
(126, '2020-11-06 18:37:12', '1', 'Insert', '<b>Insert</b> detail data pembelian bahan baku pada tabel <b>pembelian_detail</b>.<br><b>[No. Pembelian : </b>BL11202000002<b>][Kode Bahan Baku : </b>OB0002<b>][Harga Beli : </b>12000<b>][Jumlah Beli : </b>3<b>][Total Harga : </b>36000<b>]'),
(127, '2020-11-07 05:58:45', '1', 'Insert', '<b>Insert</b> data pembelian pada tabel <b>pembelian</b>.<br><b>[No. Pembelian : </b>BL11202000003<b>][Tanggal : </b>2020-11-07<b>][Kode Supplier : </b>SP001<b>][No. Nota : </b>23213<b>][Total Bayar : </b>60000<b>]'),
(128, '2020-11-07 05:58:45', '1', 'Insert', '<b>Insert</b> detail data pembelian bahan baku pada tabel <b>pembelian_detail</b>.<br><b>[No. Pembelian : </b>BL11202000003<b>][Kode Bahan Baku : </b>OB0001<b>][Harga Beli : </b>12000<b>][Jumlah Beli : </b>5<b>][Total Harga : </b>60000<b>]'),
(129, '2020-11-07 06:00:10', '1', 'Insert', '<b>Insert</b> data pembelian pada tabel <b>pembelian</b>.<br><b>[No. Pembelian : </b>BL11202000004<b>][Tanggal : </b>2020-11-07<b>][Kode Supplier : </b>SP002<b>][No. Nota : </b>54321<b>][Total Bayar : </b>43125<b>]'),
(130, '2020-11-07 06:00:10', '1', 'Insert', '<b>Insert</b> detail data pembelian bahan baku pada tabel <b>pembelian_detail</b>.<br><b>[No. Pembelian : </b>BL11202000004<b>][Kode Bahan Baku : </b>OB0015<b>][Harga Beli : </b>8625<b>][Jumlah Beli : </b>5<b>][Total Harga : </b>43125<b>]'),
(131, '2020-11-07 06:17:50', '1', 'Insert', '<b>Insert</b> data penjualan pada tabel <b>penjualan</b>.<br><b>[No. Penjualan : </b>JL11202000002<b>][Tanggal : </b>2020-11-07<b>][Total Bayar : </b>115000<b>][Uang Bayar : </b>150000<b>]'),
(132, '2020-11-07 06:24:03', '1', 'Delete', '<b>Delete</b> data penjualan pada tabel <b>penjualan</b>. <br> <b>[No. Penjualan : </b>JL11202000002<b>][Tanggal : </b>07-11-2020<b>][Total Bayar : </b>115000<b>]'),
(133, '2020-11-07 06:30:11', '1', 'Delete', '<b>Delete</b> data penjualan pada tabel <b>penjualan</b>. <br> <b>[No. Penjualan : </b>JL11202000002<b>][Tanggal : </b>07-11-2020<b>][Total Bayar : </b>115000<b>]'),
(134, '2020-11-07 06:34:12', '1', 'Update', '<b>Update</b> data bahan baku pada tabel <b>bahan baku</b>.<br><b>Data Lama = [Kode Bahan Baku : </b>OB0015<b>][Nama Bahan Baku : </b>OBH Combi Dewasa<b>][Harga Beli : </b>8625<b>][Harga Jual : </b>11500<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>2<b>][Stok : </b>25<b>],<br> Data Baru = [Kode Bahan Baku : </b>OB0015<b>][Nama Bahan Baku : </b>OBH Combi Dewasa<b>][Harga Beli : </b>8625<b>][Harga Jual : </b>11500<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>5<b>][Stok : </b>25<b>]</b>'),
(135, '2020-11-07 07:47:20', '1', 'Insert', '<b>Insert</b> data penjualan pada tabel <b>penjualan</b>.<br><b>[No. Penjualan : </b>JL11202000002<b>][Tanggal : </b>2020-11-07<b>][Total Bayar : </b>115000<b>][Uang Bayar : </b>150000<b>]'),
(136, '2020-11-07 07:47:20', '1', 'Insert', '<b>Insert</b> detail data penjualan bahan baku pada tabel <b>penjualan_detail</b>.<br><b>[No. Penjualan : </b>JL11202000002<b>][Kode Bahan Baku : </b>OB0015<b>][Harga Beli : </b>8625<b>][Harga Jual : </b>11500<b>][Jumlah Jual : </b>10<b>][Total Harga : </b>115000<b>]'),
(137, '2020-11-10 15:20:56', '1', 'Insert', '<b>Insert</b> data pembelian pada tabel <b>pembelian</b>.<br><b>[No. Pembelian : </b>BL11202000005<b>][Tanggal : </b>2020-11-10<b>][Kode Supplier : </b>SP004<b>][No. Nota : </b>01234<b>][Total Bayar : </b>43125<b>]'),
(138, '2020-11-10 15:20:56', '1', 'Insert', '<b>Insert</b> detail data pembelian bahan baku pada tabel <b>pembelian_detail</b>.<br><b>[No. Pembelian : </b>BL11202000005<b>][Kode Bahan Baku : </b>OB0015<b>][Harga Beli : </b>8625<b>][Jumlah Beli : </b>5<b>][Total Harga : </b>43125<b>]'),
(139, '2020-11-10 16:55:29', '1', 'Update', '<b>Update</b> data bahan baku pada tabel <b>bahan baku</b>.<br><b>Data Lama = [Kode Bahan Baku : </b>OB0015<b>][Nama Bahan Baku : </b>OBH Combi Dewasa<b>][Harga Beli : </b>8625<b>][Harga Jual : </b>11500<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>5<b>][Stok : </b>20<b>],<br> Data Baru = [Kode Bahan Baku : </b>OB0015<b>][Nama Bahan Baku : </b>OBH Combi Dewasa<b>][Harga Beli : </b>8625<b>][Harga Jual : </b>11500<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>5<b>][Stok : </b>20<b>]</b>'),
(140, '2020-11-14 09:52:29', '1', 'Update', '<b>Update</b> data supplier pada tabel <b>supplier</b>.<br><b>Data Lama = [Kode Supplier : </b>SP004<b>][Nama Supplier : </b>Aria Jiwa Farma<b>][Alamat : </b>Jln. Alam Hijau No. 16 Way Halim<b>]</b>[Telepon : </b>0721703500<b>],<br> Data Baru = [Kode Supplier : </b>SP004<b>][Nama Supplier : </b>Aria Jiwa<b>][Alamat : </b>Jln. Alam Hijau No. 16 Way Halim<b>]</b>[Telepon : </b>0721703500<b>]</b>'),
(141, '2020-11-14 09:53:18', '1', 'Update', '<b>Update</b> data supplier pada tabel <b>supplier</b>.<br><b>Data Lama = [Kode Supplier : </b>SP016<b>][Nama Supplier : </b>Sawah Besar Farmasi<b>][Alamat : </b>Jln. Mawar No. 2 Pahoman Bandar Lampung<b>]</b>[Telepon : </b>0721269896<b>],<br> Data Baru = [Kode Supplier : </b>SP016<b>][Nama Supplier : </b>Setya Besar<b>][Alamat : </b>Jln. Mawar No. 2 Pahoman Bandar Lampung<b>]</b>[Telepon : </b>0721269896<b>]</b>'),
(142, '2020-11-14 09:53:30', '1', 'Update', '<b>Update</b> data supplier pada tabel <b>supplier</b>.<br><b>Data Lama = [Kode Supplier : </b>SP012<b>][Nama Supplier : </b>Merapi Utama Pharma<b>][Alamat : </b>Jln. Cilosari 23 Cikini Menteng Jakarta<b>]</b>[Telepon : </b>0213141906<b>],<br> Data Baru = [Kode Supplier : </b>SP012<b>][Nama Supplier : </b>Merapi Utama<b>][Alamat : </b>Jln. Cilosari 23 Cikini Menteng Jakarta<b>]</b>[Telepon : </b>0213141906<b>]</b>'),
(143, '2020-11-14 09:55:36', '1', 'Update', '<b>Update</b> data bahan baku pada tabel <b>bahan baku</b>.<br><b>Data Lama = [Kode Bahan Baku : </b>OB0001<b>][Nama Bahan Baku : </b>Woods Biru<b>][Harga Beli : </b>12000<b>][Harga Jual : </b>16000<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>2<b>][Stok : </b>21<b>],<br> Data Baru = [Kode Bahan Baku : </b>OB0001<b>][Nama Bahan Baku : </b>Sirup Caramel<b>][Harga Beli : </b>35000<b>][Harga Jual : </b>16000<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>2<b>][Stok : </b>21<b>]</b>'),
(144, '2020-11-14 09:57:48', '1', 'Update', '<b>Update</b> data bahan baku pada tabel <b>bahan baku</b>.<br><b>Data Lama = [Kode Bahan Baku : </b>OB0002<b>][Nama Bahan Baku : </b>Woods Merah<b>][Harga Beli : </b>12000<b>][Harga Jual : </b>16000<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>2<b>][Stok : </b>45<b>],<br> Data Baru = [Kode Bahan Baku : </b>OB0002<b>][Nama Bahan Baku : </b>Sirup Mint<b>][Harga Beli : </b>35000<b>][Harga Jual : </b>16000<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>2<b>][Stok : </b>45<b>]</b>'),
(145, '2020-11-14 09:58:26', '1', 'Delete', '<b>Delete</b> data pembelian pada tabel <b>pembelian</b>. <br> <b>[No. Pembelian : </b>BL11202000005<b>][Tanggal : </b>10-11-2020<b>][Kode Supplier : </b>SP004<b>][No. Nota : </b>01234<b>][Total Bayar : </b>43125<b>]'),
(146, '2020-11-14 09:58:29', '1', 'Delete', '<b>Delete</b> data pembelian pada tabel <b>pembelian</b>. <br> <b>[No. Pembelian : </b>BL11202000004<b>][Tanggal : </b>07-11-2020<b>][Kode Supplier : </b>SP002<b>][No. Nota : </b>54321<b>][Total Bayar : </b>43125<b>]'),
(147, '2020-11-14 09:58:32', '1', 'Delete', '<b>Delete</b> data pembelian pada tabel <b>pembelian</b>. <br> <b>[No. Pembelian : </b>BL11202000003<b>][Tanggal : </b>07-11-2020<b>][Kode Supplier : </b>SP001<b>][No. Nota : </b>23213<b>][Total Bayar : </b>60000<b>]'),
(148, '2020-11-14 09:58:34', '1', 'Delete', '<b>Delete</b> data pembelian pada tabel <b>pembelian</b>. <br> <b>[No. Pembelian : </b>BL11202000002<b>][Tanggal : </b>07-11-2020<b>][Kode Supplier : </b>SP002<b>][No. Nota : </b>12321<b>][Total Bayar : </b>36000<b>]'),
(149, '2020-11-14 09:58:37', '1', 'Delete', '<b>Delete</b> data pembelian pada tabel <b>pembelian</b>. <br> <b>[No. Pembelian : </b>BL08201900001<b>][Tanggal : </b>19-08-2019<b>][Kode Supplier : </b>SP002<b>][No. Nota : </b>12345<b>][Total Bayar : </b>1575000<b>]'),
(150, '2020-11-14 09:59:03', '1', 'Update', '<b>Update</b> data bahan baku pada tabel <b>bahan baku</b>.<br><b>Data Lama = [Kode Bahan Baku : </b>OB0003<b>][Nama Bahan Baku : </b>OBH Nellco<b>][Harga Beli : </b>20625<b>][Harga Jual : </b>27500<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>2<b>][Stok : </b>54<b>],<br> Data Baru = [Kode Bahan Baku : </b>OB0003<b>][Nama Bahan Baku : </b>Sirup Strawberry<b>][Harga Beli : </b>35000<b>][Harga Jual : </b>27500<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>2<b>][Stok : </b>54<b>]</b>'),
(151, '2020-11-14 10:01:19', '1', 'Update', '<b>Update</b> data bahan baku pada tabel <b>bahan baku</b>.<br><b>Data Lama = [Kode Bahan Baku : </b>OB0004<b>][Nama Bahan Baku : </b>Sakatonik ABC<b>][Harga Beli : </b>9000<b>][Harga Jual : </b>12000<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>2<b>][Stok : </b>23<b>],<br> Data Baru = [Kode Bahan Baku : </b>OB0004<b>][Nama Bahan Baku : </b>Sirup Aren<b>][Harga Beli : </b>35000<b>][Harga Jual : </b>12000<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>2<b>][Stok : </b>23<b>]</b>'),
(152, '2020-11-14 10:03:05', '1', 'Update', '<b>Update</b> data bahan baku pada tabel <b>bahan baku</b>.<br><b>Data Lama = [Kode Bahan Baku : </b>OB0005<b>][Nama Bahan Baku : </b>Scotts Emulsion<b>][Harga Beli : </b>31500<b>][Harga Jual : </b>42000<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>2<b>][Stok : </b>-40<b>],<br> Data Baru = [Kode Bahan Baku : </b>OB0005<b>][Nama Bahan Baku : </b>Sirup Hazelnut<b>][Harga Beli : </b>35000<b>][Harga Jual : </b>16000<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>2<b>][Stok : </b>-40<b>]</b>'),
(153, '2020-11-14 10:03:43', '1', 'Update', '<b>Update</b> data bahan baku pada tabel <b>bahan baku</b>.<br><b>Data Lama = [Kode Bahan Baku : </b>OB0006<b>][Nama Bahan Baku : </b>Sakatonik Liver<b>][Harga Beli : </b>7500<b>][Harga Jual : </b>10000<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>2<b>][Stok : </b>0<b>],<br> Data Baru = [Kode Bahan Baku : </b>OB0006<b>][Nama Bahan Baku : </b>Sirup Coklat<b>][Harga Beli : </b>35000<b>][Harga Jual : </b>10000<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>2<b>][Stok : </b>0<b>]</b>'),
(154, '2020-11-14 10:03:58', '1', 'Update', '<b>Update</b> data bahan baku pada tabel <b>bahan baku</b>.<br><b>Data Lama = [Kode Bahan Baku : </b>OB0007<b>][Nama Bahan Baku : </b>Stimuno Syr<b>][Harga Beli : </b>18000<b>][Harga Jual : </b>24000<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>2<b>][Stok : </b>1<b>],<br> Data Baru = [Kode Bahan Baku : </b>OB0007<b>][Nama Bahan Baku : </b>Sirup Greentea<b>][Harga Beli : </b>18000<b>][Harga Jual : </b>24000<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>2<b>][Stok : </b>1<b>]</b>'),
(155, '2020-11-14 10:04:40', '1', 'Update', '<b>Update</b> data bahan baku pada tabel <b>bahan baku</b>.<br><b>Data Lama = [Kode Bahan Baku : </b>OB0008<b>][Nama Bahan Baku : </b>Imboost Syr<b>][Harga Beli : </b>51750<b>][Harga Jual : </b>69000<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>2<b>][Stok : </b>2<b>],<br> Data Baru = [Kode Bahan Baku : </b>OB0008<b>][Nama Bahan Baku : </b>Sirup Bubble Gum<b>][Harga Beli : </b>35000<b>][Harga Jual : </b>69000<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>2<b>][Stok : </b>2<b>]</b>'),
(156, '2020-11-14 10:05:47', '1', 'Update', '<b>Update</b> data bahan baku pada tabel <b>bahan baku</b>.<br><b>Data Lama = [Kode Bahan Baku : </b>OB0009<b>][Nama Bahan Baku : </b>Neuremacyl Tab<b>][Harga Beli : </b>63750<b>][Harga Jual : </b>85000<b>][Kode Satuan : </b>4<b>][Minimum Stok : </b>2<b>][Stok : </b>1<b>],<br> Data Baru = [Kode Bahan Baku : </b>OB0009<b>][Nama Bahan Baku : </b>Susu Kental Manis<b>][Harga Beli : </b>112000<b>][Harga Jual : </b>85000<b>][Kode Satuan : </b>4<b>][Minimum Stok : </b>2<b>][Stok : </b>1<b>]</b>');
INSERT INTO `sys_audit_trail` (`id`, `tanggal`, `username`, `aksi`, `keterangan`) VALUES
(157, '2020-11-14 10:06:31', '1', 'Update', '<b>Update</b> data bahan baku pada tabel <b>bahan baku</b>.<br><b>Data Lama = [Kode Bahan Baku : </b>OB0010<b>][Nama Bahan Baku : </b>Pilkita<b>][Harga Beli : </b>112500<b>][Harga Jual : </b>150000<b>][Kode Satuan : </b>4<b>][Minimum Stok : </b>2<b>][Stok : </b>54<b>],<br> Data Baru = [Kode Bahan Baku : </b>OB0010<b>][Nama Bahan Baku : </b>Susu Original<b>][Harga Beli : </b>95600<b>][Harga Jual : </b>150000<b>][Kode Satuan : </b>4<b>][Minimum Stok : </b>2<b>][Stok : </b>54<b>]</b>'),
(158, '2020-11-14 10:06:53', '1', 'Update', '<b>Update</b> data bahan baku pada tabel <b>bahan baku</b>.<br><b>Data Lama = [Kode Bahan Baku : </b>OB0011<b>][Nama Bahan Baku : </b>Fatigon Spirit<b>][Harga Beli : </b>48750<b>][Harga Jual : </b>65000<b>][Kode Satuan : </b>4<b>][Minimum Stok : </b>2<b>][Stok : </b>42<b>],<br> Data Baru = [Kode Bahan Baku : </b>OB0011<b>][Nama Bahan Baku : </b>Susu Coconut<b>][Harga Beli : </b>100500<b>][Harga Jual : </b>65000<b>][Kode Satuan : </b>4<b>][Minimum Stok : </b>2<b>][Stok : </b>42<b>]</b>'),
(159, '2020-11-14 10:09:20', '1', 'Update', '<b>Update</b> data satuan pada tabel <b>satuan</b>.<br><b>Data Lama = [Kode Satuan : </b>2<b>][Nama Satuan : </b>Strip<b>],<br> Data Baru = [Kode Satuan : </b>2<b>][Nama Satuan : </b>Roll<b>]</b>'),
(160, '2020-11-14 10:11:15', '1', 'Update', '<b>Update</b> data satuan pada tabel <b>satuan</b>.<br><b>Data Lama = [Kode Satuan : </b>1<b>][Nama Satuan : </b>Tube<b>],<br> Data Baru = [Kode Satuan : </b>1<b>][Nama Satuan : </b>Slop<b>]</b>'),
(161, '2020-11-14 10:12:28', '1', 'Update', '<b>Update</b> data bahan baku pada tabel <b>bahan baku</b>.<br><b>Data Lama = [Kode Bahan Baku : </b>OB0012<b>][Nama Bahan Baku : </b>Bisolvon Kids<b>][Harga Beli : </b>21750<b>][Harga Jual : </b>29000<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>2<b>][Stok : </b>12<b>],<br> Data Baru = [Kode Bahan Baku : </b>OB0012<b>][Nama Bahan Baku : </b>Gelas Plastik<b>][Harga Beli : </b>50000<b>][Harga Jual : </b>29000<b>][Kode Satuan : </b>1<b>][Minimum Stok : </b>2<b>][Stok : </b>12<b>]</b>'),
(162, '2020-11-14 10:13:15', '1', 'Update', '<b>Update</b> data bahan baku pada tabel <b>bahan baku</b>.<br><b>Data Lama = [Kode Bahan Baku : </b>OB0013<b>][Nama Bahan Baku : </b>Bisolvon Extra<b>][Harga Beli : </b>28125<b>][Harga Jual : </b>37500<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>2<b>][Stok : </b>15<b>],<br> Data Baru = [Kode Bahan Baku : </b>OB0013<b>][Nama Bahan Baku : </b>Cup Sealer<b>][Harga Beli : </b>75000<b>][Harga Jual : </b>37500<b>][Kode Satuan : </b>2<b>][Minimum Stok : </b>2<b>][Stok : </b>15<b>]</b>'),
(163, '2020-11-14 10:16:14', '1', 'Update', '<b>Update</b> data bahan baku pada tabel <b>bahan baku</b>.<br><b>Data Lama = [Kode Bahan Baku : </b>OB0014<b>][Nama Bahan Baku : </b>OBH Combi Anak<b>][Harga Beli : </b>8625<b>][Harga Jual : </b>11500<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>2<b>][Stok : </b>30<b>],<br> Data Baru = [Kode Bahan Baku : </b>OB0014<b>][Nama Bahan Baku : </b>Botol 500ml<b>][Harga Beli : </b>15000<b>][Harga Jual : </b>11500<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>2<b>][Stok : </b>30<b>]</b>'),
(164, '2020-11-14 10:16:46', '1', 'Update', '<b>Update</b> data bahan baku pada tabel <b>bahan baku</b>.<br><b>Data Lama = [Kode Bahan Baku : </b>OB0015<b>][Nama Bahan Baku : </b>OBH Combi Dewasa<b>][Harga Beli : </b>8625<b>][Harga Jual : </b>11500<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>2<b>][Stok : </b>-5<b>],<br> Data Baru = [Kode Bahan Baku : </b>OB0015<b>][Nama Bahan Baku : </b>Botol 1000ml<b>][Harga Beli : </b>25000<b>][Harga Jual : </b>11500<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>2<b>][Stok : </b>-5<b>]</b>'),
(165, '2020-11-14 10:17:41', '1', 'Update', '<b>Update</b> data supplier pada tabel <b>supplier</b>.<br><b>Data Lama = [Kode Supplier : </b>SP010<b>][Nama Supplier : </b>Kimia Farma<b>][Alamat : </b>Jln. Tembesu 2 No. 3B Campang Raya<b>]</b>[Telepon : </b>0721789242<b>],<br> Data Baru = [Kode Supplier : </b>SP010<b>][Nama Supplier : </b>Darma Refill<b>][Alamat : </b>Jln. Tembesu 2 No. 3B Campang Raya<b>]</b>[Telepon : </b>0721789242<b>]</b>'),
(166, '2020-11-14 22:07:26', '1', 'Insert', '<b>Insert</b> data pembelian pada tabel <b>pembelian</b>.<br><b>[No. Pembelian : </b>BL11202000001<b>][Tanggal : </b>2020-11-15<b>][Kode Supplier : </b>SP005<b>][No. Nota : </b>123456<b>][Total Bayar : </b>210000<b>]'),
(167, '2020-11-14 22:07:26', '1', 'Insert', '<b>Insert</b> detail data pembelian bahan baku pada tabel <b>pembelian_detail</b>.<br><b>[No. Pembelian : </b>BL11202000001<b>][Kode Bahan Baku : </b>OB0001<b>][Harga Beli : </b>35000<b>][Jumlah Beli : </b>6<b>][Total Harga : </b>210000<b>]'),
(168, '2020-11-14 22:08:37', '1', 'Insert', '<b>Insert</b> data pembelian pada tabel <b>pembelian</b>.<br><b>[No. Pembelian : </b>BL11202000002<b>][Tanggal : </b>2020-11-15<b>][Kode Supplier : </b>SP001<b>][No. Nota : </b>234567<b>][Total Bayar : </b>1750000<b>]'),
(169, '2020-11-14 22:08:37', '1', 'Insert', '<b>Insert</b> detail data pembelian bahan baku pada tabel <b>pembelian_detail</b>.<br><b>[No. Pembelian : </b>BL11202000002<b>][Kode Bahan Baku : </b>OB0005<b>][Harga Beli : </b>35000<b>][Jumlah Beli : </b>50<b>][Total Harga : </b>1750000<b>]'),
(170, '2020-11-14 22:09:44', '1', 'Update', '<b>Update</b> data konfigurasi aplikasi pada tabel <b>sys_config</b>.<br><b>Data Lama = [Nama Kedai Kopi : </b>PÃºan Kopi<b>][Alamat : </b>Perum Korpri Blok 1 C No.26, Jl. Abdi Praja, Sepinggan Baru, Balikpapan Selatan<b>][Telepon : </b>081245876598<b>][Email : </b>puankopi@gmail.com<b>][Website : </b>www.puankopi.com<b>][Logo : </b>IMG-20190708-WA0005.jpg<b>],<br> Data Baru = [Nama Kedai Kopi : </b>Puan Kopi<b>][Alamat : </b>Perum Korpri Blok 1 C No.26, Jl. Abdi Praja, Sepinggan Baru, Balikpapan Selatan<b>][Telepon : </b>081245876598<b>][Email : </b>puankopi@gmail.com<b>][Website : </b>www.puankopi.com<b>][Logo : </b>IMG-20190708-WA0005.jpg<b>]</b>'),
(171, '2020-11-14 23:28:06', '1', 'Insert', '<b>Insert</b> data pembelian pada tabel <b>pembelian</b>.<br><b>[No. Pembelian : </b>BL11202000003<b>][Tanggal : </b>2020-11-15<b>][Kode Supplier : </b>SP001<b>][No. Nota : </b>123032<b>][Total Bayar : </b>125000<b>]'),
(172, '2020-11-14 23:28:06', '1', 'Insert', '<b>Insert</b> detail data pembelian bahan baku pada tabel <b>pembelian_detail</b>.<br><b>[No. Pembelian : </b>BL11202000003<b>][Kode Bahan Baku : </b>OB0015<b>][Harga Beli : </b>25000<b>][Jumlah Beli : </b>5<b>][Total Harga : </b>125000<b>]'),
(173, '2020-11-15 01:46:07', '0', 'Insert', '<b>Insert</b> data bahan baku pada tabel <b>bahan baku</b>.<br><b>[Kode Bahan Baku : </b>TES01<b>][Nama Bahan Baku : </b>tesss<b>][Harga Beli : </b>1<b>][Harga Jual : </b>1<b>][Kode Satuan : </b>1<b>][Minimum Stok : </b>1<b>]'),
(174, '2020-11-15 07:04:20', '1', 'Insert', '<b>Insert</b> data bahan baku pada tabel <b>bahan baku</b>.<br><b>[Kode Bahan Baku : </b>OB0016<b>][Nama Bahan Baku : </b>Biji Kopi<b>][Harga Beli : </b>250000<b>][Harga Jual : </b>0<b>][Kode Satuan : </b>4<b>][Minimum Stok : </b>2<b>]'),
(175, '2020-11-15 08:04:19', '1', 'Insert', '<b>Insert</b> data penjualan pada tabel <b>penjualan</b>.<br><b>[No. Penjualan : </b>JL11202000003<b>][Tanggal : </b>2020-11-15<b>][Total Bayar : </b>0<b>][Uang Bayar : </b>0<b>]'),
(176, '2020-11-15 08:08:28', '1', 'Delete', '<b>Delete</b> data penjualan pada tabel <b>penjualan</b>. <br> <b>[No. Penjualan : </b>JL11202000003<b>][Tanggal : </b>15-11-2020<b>][Total Bayar : </b><b>]'),
(177, '2020-11-15 08:10:38', '1', 'Insert', '<b>Insert</b> data penjualan pada tabel <b>penjualan</b>.<br><b>[No. Penjualan : </b>JL11202000003<b>][Tanggal : </b>2020-11-15<b>][Total Bayar : </b>0<b>][Uang Bayar : </b>0<b>]'),
(178, '2020-11-15 08:11:59', '1', 'Insert', '<b>Insert</b> data penjualan pada tabel <b>penjualan</b>.<br><b>[No. Penjualan : </b>JL11202000003<b>][Tanggal : </b>2020-11-15<b>][Total Bayar : </b>0<b>][Uang Bayar : </b>0<b>]'),
(179, '2020-11-15 08:14:55', '1', 'Insert', '<b>Insert</b> data penjualan pada tabel <b>penjualan</b>.<br><b>[No. Penjualan : </b>JL11202000003<b>][Tanggal : </b>2020-11-15<b>][Total Bayar : </b>0<b>][Uang Bayar : </b>0<b>]'),
(180, '2020-11-15 08:16:30', '1', 'Insert', '<b>Insert</b> data penjualan pada tabel <b>penjualan</b>.<br><b>[No. Penjualan : </b>JL11202000003<b>][Tanggal : </b>2020-11-15<b>][Total Bayar : </b>0<b>][Uang Bayar : </b>0<b>]'),
(181, '2020-11-15 08:21:14', '1', 'Insert', '<b>Insert</b> data penjualan pada tabel <b>penjualan</b>.<br><b>[No. Penjualan : </b>JL11202000003<b>][Tanggal : </b>2020-11-15<b>]'),
(182, '2020-11-15 08:23:20', '1', 'Insert', '<b>Insert</b> data penjualan pada tabel <b>penjualan</b>.<br><b>[No. Penjualan : </b>JL11202000004<b>][Tanggal : </b>2020-11-15<b>]'),
(183, '2020-11-15 08:24:10', '1', 'Insert', '<b>Insert</b> data penjualan pada tabel <b>penjualan</b>.<br><b>[No. Penjualan : </b>JL11202000005<b>][Tanggal : </b>2020-11-15<b>]'),
(184, '2020-11-15 08:24:10', '1', 'Insert', '<b>Insert</b> detail data penjualan bahan baku pada tabel <b>penjualan_detail</b>.<br><b>[No. Penjualan : </b>JL11202000005<b>][Kode Bahan Baku : </b>OB0001<b>][Harga Beli : </b>0<b>][Harga Jual : </b>0<b>][Jumlah Jual : </b>0<b>][Total Harga : </b>0<b>]'),
(185, '2020-11-15 08:24:10', '1', 'Update', '<b>Update</b> data bahan baku pada tabel <b>bahan baku</b>.<br><b>Data Lama = [Kode Bahan Baku : </b>OB0001<b>][Nama Bahan Baku : </b>Sirup Caramel<b>][Harga Beli : </b>35000<b>][Harga Jual : </b>16000<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>2<b>][Stok : </b>22<b>],<br> Data Baru = [Kode Bahan Baku : </b>OB0001<b>][Nama Bahan Baku : </b>Sirup Caramel<b>][Harga Beli : </b>35000<b>][Harga Jual : </b>16000<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>2<b>][Stok : </b>22<b>]</b>'),
(186, '2020-11-15 08:27:50', '1', 'Insert', '<b>Insert</b> data penjualan pada tabel <b>penjualan</b>.<br><b>[No. Penjualan : </b>JL11202000006<b>][Tanggal : </b>2020-11-15<b>]'),
(187, '2020-11-15 08:27:51', '1', 'Insert', '<b>Insert</b> detail data penjualan bahan baku pada tabel <b>penjualan_detail</b>.<br><b>[No. Penjualan : </b>JL11202000006<b>][Kode Bahan Baku : </b>OB0004<b>][Harga Beli : </b>0<b>][Harga Jual : </b>0<b>][Jumlah Jual : </b>0<b>][Total Harga : </b>0<b>]'),
(188, '2020-11-15 08:27:51', '1', 'Update', '<b>Update</b> data bahan baku pada tabel <b>bahan baku</b>.<br><b>Data Lama = [Kode Bahan Baku : </b>OB0004<b>][Nama Bahan Baku : </b>Sirup Aren<b>][Harga Beli : </b>35000<b>][Harga Jual : </b>12000<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>2<b>][Stok : </b>23<b>],<br> Data Baru = [Kode Bahan Baku : </b>OB0004<b>][Nama Bahan Baku : </b>Sirup Aren<b>][Harga Beli : </b>35000<b>][Harga Jual : </b>12000<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>2<b>][Stok : </b>23<b>]</b>'),
(189, '2020-11-15 08:27:51', '1', 'Insert', '<b>Insert</b> detail data penjualan bahan baku pada tabel <b>penjualan_detail</b>.<br><b>[No. Penjualan : </b>JL11202000006<b>][Kode Bahan Baku : </b>OB0003<b>][Harga Beli : </b>0<b>][Harga Jual : </b>0<b>][Jumlah Jual : </b>0<b>][Total Harga : </b>0<b>]'),
(190, '2020-11-15 08:27:51', '1', 'Update', '<b>Update</b> data bahan baku pada tabel <b>bahan baku</b>.<br><b>Data Lama = [Kode Bahan Baku : </b>OB0003<b>][Nama Bahan Baku : </b>Sirup Strawberry<b>][Harga Beli : </b>35000<b>][Harga Jual : </b>27500<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>2<b>][Stok : </b>54<b>],<br> Data Baru = [Kode Bahan Baku : </b>OB0003<b>][Nama Bahan Baku : </b>Sirup Strawberry<b>][Harga Beli : </b>35000<b>][Harga Jual : </b>27500<b>][Kode Satuan : </b>5<b>][Minimum Stok : </b>2<b>][Stok : </b>54<b>]</b>'),
(191, '2020-11-15 08:30:18', '1', 'Insert', '<b>Insert</b> data penjualan pada tabel <b>penjualan</b>.<br><b>[No. Penjualan : </b>JL11202000007<b>][Tanggal : </b>2020-11-15<b>]'),
(192, '2020-11-15 08:30:18', '1', 'Insert', '<b>Insert</b> detail data penjualan bahan baku pada tabel <b>penjualan_detail</b>.<br><b>[No. Penjualan : </b>JL11202000007<b>][Kode Bahan Baku : </b>OB0005<b>][Harga Beli : </b>0<b>][Harga Jual : </b>0<b>][Jumlah Jual : </b>3<b>][Total Harga : </b>0<b>]'),
(193, '2020-11-15 08:32:09', '1', 'Insert', '<b>Insert</b> data penjualan pada tabel <b>penjualan</b>.<br><b>[No. Penjualan : </b>JL11202000003<b>][Tanggal : </b>2020-11-15<b>]'),
(194, '2020-11-15 08:32:09', '1', 'Insert', '<b>Insert</b> detail data penjualan bahan baku pada tabel <b>penjualan_detail</b>.<br><b>[No. Penjualan : </b>JL11202000003<b>][Kode Bahan Baku : </b>OB0003<b>][Harga Beli : </b>0<b>][Harga Jual : </b>0<b>][Jumlah Jual : </b>5<b>][Total Harga : </b>0<b>]'),
(195, '2020-11-15 08:32:09', '1', 'Insert', '<b>Insert</b> detail data penjualan bahan baku pada tabel <b>penjualan_detail</b>.<br><b>[No. Penjualan : </b>JL11202000003<b>][Kode Bahan Baku : </b>OB0001<b>][Harga Beli : </b>0<b>][Harga Jual : </b>0<b>][Jumlah Jual : </b>5<b>][Total Harga : </b>0<b>]'),
(196, '2020-11-15 08:32:59', '1', 'Insert', '<b>Insert</b> data penjualan pada tabel <b>penjualan</b>.<br><b>[No. Penjualan : </b>JL11202000004<b>][Tanggal : </b>2020-11-15<b>]'),
(197, '2020-11-15 08:32:59', '1', 'Insert', '<b>Insert</b> detail data penjualan bahan baku pada tabel <b>penjualan_detail</b>.<br><b>[No. Penjualan : </b>JL11202000004<b>][Kode Bahan Baku : </b>OB0001<b>][Harga Beli : </b>0<b>][Harga Jual : </b>0<b>][Jumlah Jual : </b>7<b>][Total Harga : </b>0<b>]'),
(198, '2020-11-15 08:37:13', '1', 'Insert', '<b>Insert</b> data penjualan pada tabel <b>penjualan</b>.<br><b>[No. Penjualan : </b>JL11202000005<b>][Tanggal : </b>2020-11-15<b>]'),
(199, '2020-11-15 08:37:13', '1', 'Insert', '<b>Insert</b> detail data penjualan bahan baku pada tabel <b>penjualan_detail</b>.<br><b>[No. Penjualan : </b>JL11202000005<b>][Kode Bahan Baku : </b>OB0002<b>][Harga Beli : </b>0<b>][Harga Jual : </b>0<b>][Jumlah Jual : </b>5<b>][Total Harga : </b>0<b>]'),
(200, '2020-11-20 12:35:20', '1', 'Update', '<b>Update</b> data supplier pada tabel <b>supplier</b>.<br><b>Data Lama = [Kode Supplier : </b>SP001<b>][Nama Supplier : </b>AAM<b>][Alamat : </b>Jln. Kenanga No. 26 Rawa Laut Tanjung Karang<b>]</b>[Telepon : </b>0721262639<b>],<br> Data Baru = [Kode Supplier : </b>SP001<b>][Nama Supplier : </b>AAM<b>][Alamat : </b>Jln. Kenanga No. 26 Rawa Laut Tanjung Karang<b>]</b>[Telepon : </b>0721262639<b>]</b>'),
(201, '2020-11-20 12:44:58', '1', 'Insert', '<b>Insert</b> data backup database pada tabel <b>sys_database</b>.<br><b>[ID : </b>1<b>][Nama File : </b>20201120_194456_database.sql.gz<b>][Ukuran File : </b>10 KB<b>]</b>'),
(202, '2020-11-29 01:33:32', '1', 'Insert', '<b>Insert</b> data pembelian pada tabel <b>pembelian</b>.<br><b>[No. Pembelian : </b>BL11202000004<b>][Tanggal : </b>2020-11-29<b>][Kode Supplier : </b>SP003<b>][No. Nota : </b>423512<b>][Total Bayar : </b>210000<b>]'),
(203, '2020-11-29 01:33:32', '1', 'Insert', '<b>Insert</b> detail data pembelian bahan baku pada tabel <b>pembelian_detail</b>.<br><b>[No. Pembelian : </b>BL11202000004<b>][Kode Bahan Baku : </b>OB0006<b>][Harga Beli : </b>35000<b>][Jumlah Beli : </b>4<b>][Total Harga : </b>140000<b>]'),
(204, '2020-11-29 01:33:32', '1', 'Insert', '<b>Insert</b> detail data pembelian bahan baku pada tabel <b>pembelian_detail</b>.<br><b>[No. Pembelian : </b>BL11202000004<b>][Kode Bahan Baku : </b>OB0001<b>][Harga Beli : </b>35000<b>][Jumlah Beli : </b>2<b>][Total Harga : </b>70000<b>]');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sys_config`
--

CREATE TABLE `sys_config` (
  `id` tinyint(1) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `telepon` varchar(13) NOT NULL,
  `email` varchar(30) NOT NULL,
  `website` varchar(30) NOT NULL,
  `logo` varchar(30) NOT NULL,
  `updated_user` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sys_config`
--

INSERT INTO `sys_config` (`id`, `nama`, `alamat`, `telepon`, `email`, `website`, `logo`, `updated_user`, `updated_date`) VALUES
(1, 'Puan Kopi', 'Perum Korpri Blok 1 C No.26, Jl. Abdi Praja, Sepinggan Baru, Balikpapan Selatan', '081245876598', 'puankopi@gmail.com', 'www.puankopi.com', 'IMG-20190708-WA0005.jpg', 1, '2020-11-15 05:09:44');

--
-- Trigger `sys_config`
--
DELIMITER $$
CREATE TRIGGER `config_update` AFTER UPDATE ON `sys_config` FOR EACH ROW BEGIN
INSERT INTO sys_audit_trail (username,aksi,keterangan) VALUES (NEW.updated_user,'Update',CONCAT('<b>Update</b> data konfigurasi aplikasi pada tabel <b>sys_config</b>.<br><b>Data Lama = [Nama Kedai Kopi : </b>',OLD.nama,'<b>][Alamat : </b>',OLD.alamat,'<b>][Telepon : </b>',OLD.telepon,'<b>][Email : </b>',OLD.email,'<b>][Website : </b>',OLD.website,'<b>][Logo : </b>',OLD.logo,'<b>],<br> Data Baru = [Nama Kedai Kopi : </b>',NEW.nama,'<b>][Alamat : </b>',NEW.alamat,'<b>][Telepon : </b>',NEW.telepon,'<b>][Email : </b>',NEW.email,'<b>][Website : </b>',NEW.website,'<b>][Logo : </b>',NEW.logo,'<b>]</b>' ));
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `sys_database`
--

CREATE TABLE `sys_database` (
  `id` int(11) NOT NULL,
  `nama_file` varchar(50) NOT NULL,
  `ukuran_file` varchar(10) NOT NULL,
  `created_user` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sys_database`
--

INSERT INTO `sys_database` (`id`, `nama_file`, `ukuran_file`, `created_user`, `created_date`) VALUES
(1, '20201120_194456_database.sql.gz', '10 KB', 1, '2020-11-20 12:44:58');

--
-- Trigger `sys_database`
--
DELIMITER $$
CREATE TRIGGER `database_insert` AFTER INSERT ON `sys_database` FOR EACH ROW BEGIN
INSERT INTO sys_audit_trail (username,aksi,keterangan) VALUES (NEW.created_user,'Insert',CONCAT('<b>Insert</b> data backup database pada tabel <b>sys_database</b>.<br><b>[ID : </b>',NEW.id,'<b>][Nama File : </b>',NEW.nama_file,'<b>][Ukuran File : </b>',NEW.ukuran_file,'<b>]</b>' ));
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `sys_users`
--

CREATE TABLE `sys_users` (
  `id_user` int(11) NOT NULL,
  `nama_user` varchar(30) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(45) NOT NULL,
  `hak_akses` enum('Super Admin','Purchasing','Cashier','Leader','Visitors','Suppliers') NOT NULL,
  `blokir` enum('Ya','Tidak') NOT NULL,
  `created_user` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_user` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sys_users`
--

INSERT INTO `sys_users` (`id_user`, `nama_user`, `username`, `password`, `hak_akses`, `blokir`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
(1, 'Arief Maulana', 'admin', '036d0ef7567a20b5a4ad24a354ea4a945ddab676', 'Super Admin', 'Tidak', 1, '2019-02-06 02:55:53', 1, '2020-10-29 19:07:22'),
(2, 'Rizal Mahadewa', 'purchasing', '9b0d0a56c581466ed62b0969c734334d4032f37e', 'Purchasing', 'Tidak', 1, '2019-02-06 02:56:09', 1, '2020-10-31 14:58:37'),
(3, 'Putri Cantika', 'kasir', '530ec425c4f8c5cada655313e5bcbf92a819f837', 'Cashier', 'Tidak', 1, '2019-02-06 02:56:24', 1, '2020-10-29 19:09:09'),
(4, 'Brian manuel', 'brian', '5fa52aecbae954e6e27aedbc7246bfd8ca21542a', 'Leader', 'Tidak', 1, '2020-10-29 10:50:09', NULL, NULL);

--
-- Trigger `sys_users`
--
DELIMITER $$
CREATE TRIGGER `users_insert` AFTER INSERT ON `sys_users` FOR EACH ROW BEGIN
INSERT INTO sys_audit_trail (username,aksi,keterangan) VALUES (NEW.created_user,'Insert',CONCAT('<b>Insert</b> data pengguna pada tabel <b>sys_users</b>.<br><b>[ID User : </b>',NEW.id_user,'<b>][Nama User : </b>',NEW.nama_user,'<b>][Username : </b>',NEW.username,'<b>][Password : </b>',NEW.password,'<b>][Hak Akses : </b>',NEW.hak_akses,'<b>][Blokir : </b>',NEW.blokir,'<b>]</b>' ));
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `users_update` AFTER UPDATE ON `sys_users` FOR EACH ROW BEGIN
INSERT INTO sys_audit_trail (username,aksi,keterangan) VALUES (NEW.updated_user,'Update',CONCAT('<b>Update</b> data pengguna pada tabel <b>sys_users</b>.<br><b>Data Lama = [ID User : </b>',OLD.id_user,'<b>][Nama User : </b>',OLD.nama_user,'<b>][Username : </b>',OLD.username,'<b>][Password : </b>',OLD.password,'<b>][Hak Akses : </b>',OLD.hak_akses,'<b>][Blokir : </b>',OLD.blokir,'<b>],<br> Data Baru = [ID User : </b>',NEW.id_user,'<b>][Nama User : </b>',NEW.nama_user,'<b>][Username : </b>',NEW.username,'<b>][Password : </b>',NEW.password,'<b>][Hak Akses : </b>',NEW.hak_akses,'<b>][Blokir : </b>',NEW.blokir,'<b>]</b>' ));
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur untuk view `jumlah_pemakaian_bahan_baku`
--
DROP TABLE IF EXISTS `jumlah_pemakaian_bahan_baku`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `jumlah_pemakaian_bahan_baku`  AS  select `a`.`no_penjualan` AS `no_penjualan`,`a`.`tanggal` AS `tanggal`,(select sum(`e`.`jumlah_jual`) from `penjualan_detail` `e` where (`e`.`no_penjualan` = `a`.`no_penjualan`)) AS `total_pemakaian` from `penjualan` `a` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bahanbaku`
--
ALTER TABLE `bahanbaku`
  ADD PRIMARY KEY (`kode_bahanbaku`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id_feedback`),
  ADD KEY `kode_supplier` (`kode_supplier`);

--
-- Indexes for table `pembelian`
--
ALTER TABLE `pembelian`
  ADD PRIMARY KEY (`no_pembelian`);

--
-- Indexes for table `pembelian_detail`
--
ALTER TABLE `pembelian_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`no_penjualan`);

--
-- Indexes for table `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`id_rating`);

--
-- Indexes for table `satuan`
--
ALTER TABLE `satuan`
  ADD PRIMARY KEY (`kode_satuan`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`kode_supplier`);

--
-- Indexes for table `sys_audit_trail`
--
ALTER TABLE `sys_audit_trail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sys_config`
--
ALTER TABLE `sys_config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sys_database`
--
ALTER TABLE `sys_database`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sys_users`
--
ALTER TABLE `sys_users`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pembelian_detail`
--
ALTER TABLE `pembelian_detail`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `satuan`
--
ALTER TABLE `satuan`
  MODIFY `kode_satuan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sys_audit_trail`
--
ALTER TABLE `sys_audit_trail`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=205;
--
-- AUTO_INCREMENT for table `sys_config`
--
ALTER TABLE `sys_config`
  MODIFY `id` tinyint(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sys_database`
--
ALTER TABLE `sys_database`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sys_users`
--
ALTER TABLE `sys_users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
