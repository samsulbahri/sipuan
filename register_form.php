<?php
// panggil file "config.php" untuk koneksi ke database
require_once "config/config.php";

try {
    // siapkan "data"
    $id = 1;

    // sql statement untuk menampilkan data dari tabel "sys_config" berdasarkan "id"
    $query = "SELECT nama, logo FROM sys_config WHERE id=:id";
    // membuat prepared statements
    $stmt = $pdo->prepare($query);

    // hubungkan "data" dengan prepared statements
    $stmt->bindParam(':id', $id);

    // eksekusi query
    $stmt->execute();

    // ambil data hasil query
    $data = $stmt->fetch(PDO::FETCH_ASSOC);
    // tampilkan data
    $nama = $data['nama'];
    $logo = $data['logo'];

    // tutup koneksi
    $pdo = null;
} catch (PDOException $e) {
    // tampilkan pesan kesalahan
    echo "Query Error : " . $e->getMessage();
}
?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Aplikasi Sistem Informasi Puan Kopi">
    <meta name="keywords" content="Aplikasi Sistem Informasi Puan Kopi">
    <meta name="author" content="Kelompok 4">

    <!-- Favicon icon -->
    <link rel="shortcut icon" href="assets/img/IMG-20190708-WA0005.jpg">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-4.1.3/css/bootstrap.min.css">
    <!-- Font Awesome CSS -->
    <link rel="stylesheet" type="text/css" href="assets/plugins/fontawesome-free-5.5.0-web/css/all.min.css">
    <!-- Animate CSS -->
    <link rel="stylesheet" type="text/css" href="assets/plugins/animate/animate.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="assets/css/login.css">

    <!-- Title -->
    <title>SI PÚAN KOPI</title>
</head>

<body>
    <!-- Form Login -->
    <form class="form-signin" id="formRegister">
        <div class="text-center mb-4">
            <!-- Logo -->
            <img class="brand mb-3" src="assets/img/<?php echo $logo; ?>" alt="ToroLab">
            <!-- Nama Puan Kopi -->
            <h1 class="h3 mb-4 font-weight-normal"><?php echo $nama; ?></h1>
        </div>

        <hr>
        <!-- isi form login -->
        <div class="form-group mt-4">
            <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Lengkap" autocomplete="off">
        </div>

        <div class="form-group">
            <input type="text" class="form-control" id="username" name="username" placeholder="Username" autocomplete="off">
        </div>

        <div class="form-group">
            <input type="password" class="form-control" id="password" name="password" placeholder="Password" autocomplete="off">
        </div>

        <div class="form-check form-check-inline mb-4">
            <input type="checkbox" class="form-check-input" id="tampil_password">
            <label class="form-check-label">Tampilkan Password</label>
        </div>

        <button type="button" class="btn btn-lg btn-success btn-block" id="btnRegister">
            <i class="fas fa-sign-in-alt title-icon"></i> Daftar
        </button>

        <p class="text-center mt-2 text-muted">Sudah punya akun? <a href="login">Masuk sekarang.</a></p>

        <p class="mt-5 mb-3 text-muted text-center">
            &copy; 2020 - <a class="text-success">Kelompok 4</a>
        </p>
    </form>

    <!-- Optional JavaScript -->
    <!-- jQuery -->
    <script type="text/javascript" src="assets/js/jquery-3.3.1.js"></script>
    <!-- Bootstrap JS -->
    <script type="text/javascript" src="assets/plugins/bootstrap-4.1.3/js/bootstrap.min.js"></script>
    <!-- Fontawesome Plugin JS -->
    <script type="text/javascript" src="assets/plugins/fontawesome-free-5.5.0-web/js/all.min.js"></script>
    <!-- Notify JS -->
    <script type="text/javascript" src="assets/plugins/bootstrap-notify/bootstrap-notify.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            // ===================================== Tampilkan Password =====================================
            $('#tampil_password').click(function() {
                // jika diceklis, maka ubah atribut "type=text" untuk menampilkan password
                if ($(this).is(':checked')) {
                    $('#password').attr('type', 'text');
                }
                // jika tidak diceklis, maka ubah atribut "type=password" untuk menyembunyikan password
                else {
                    $('#password').attr('type', 'password');
                }
            });
            // ==============================================================================================

            // =========================================== Login ============================================
            $('#btnRegister').click(function() {
                // validasi form input
                // jika "username" kosong
                if ($('#nama').val() == "") {
                    // focus ke input "password"
                    $("#nama").focus();
                    // tampilkan peringatan data tidak boleh kosong
                    $.notify({
                        title: '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                        message: 'Nama Lengkap tidak boleh kosong.'
                    }, {
                        type: 'warning'
                    });
                } else if ($('#username').val() == "") {
                    // focus ke input "username"
                    $("#username").focus();
                    // tampilkan peringatan data tidak boleh kosong
                    $.notify({
                        title: '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                        message: 'Username tidak boleh kosong.'
                    }, {
                        type: 'warning'
                    });

                }
                // jika "password" kosong
                else if ($('#password').val() == "") {
                    // focus ke input "password"
                    $("#password").focus();
                    // tampilkan peringatan data tidak boleh kosong
                    $.notify({
                        title: '<i class="fas fa-exclamation-triangle title-icon"></i><strong>Peringatan!</strong><br>',
                        message: 'Password tidak boleh kosong.'
                    }, {
                        type: 'warning'
                    });
                }
                // jika semua data sudah terisi, jalankan perintah login
                else {
                    // membuat variabel untuk menampung data dari form login
                    var data = $('#formRegister').serialize();

                    $.ajax({
                        type: "POST", // mengirim data dengan method POST
                        url: "register-action", // proses pengecekan login berdasakan username dan password
                        data: data, // data yang dikirim
                        beforeSend: function() { // proses sebelum data dikirim
                            // tampilkan pesan pengecekan login
                            $.notify('<i class="fas fa-sync title-icon"></i> Memeriksa Data Anda ... <br><br>', {
                                allow_dismiss: false,
                                showProgressbar: true
                            });
                        },
                        success: function(result) { // ketika proses pengecekan login selesai
                            // jika login berhasil
                            if (result === "sukses") {
                                // set waktu pesan tampil
                                setTimeout(function() {
                                    // tampilkan pesan berhasil login
                                    $.notify({
                                        title: '<i class="fas fa-check-circle title-icon"></i><strong>Login Berhasil!</strong><br>',
                                        message: 'Anda akan diarahkan ke Halaman User.. <br><br>'
                                    }, {
                                        type: 'success',
                                        allow_dismiss: false,
                                        showProgressbar: true
                                    });

                                    // alihkan ke halaman admin
                                    setTimeout('window.location = "beranda";', 3000);
                                }, 3000);
                            }
                            // jika login gagal
                            else if (result === "sudah ada") {
                                // set waktu pesan tampil
                                setTimeout(function() {
                                    // tampilkan pesan gagal login
                                    $.notify({
                                        title: '<i class="fas fa-times-circle title-icon"></i><strong>Gagal Login!</strong><br>',
                                        message: 'Informasi yang anda masukkan telah terdaftar'
                                    }, {
                                        type: 'danger'
                                    });
                                }, 3000);
                            }
                            // jika error
                            else {
                                // set waktu pesan tampil
                                setTimeout(function() {
                                    // tampilkan pesan kesalahan
                                    $.notify({
                                        title: '<i class="fas fa-times-circle title-icon"></i><strong>Gagal Login!</strong><br>',
                                        message: 'Query Error : ' + result
                                    }, {
                                        type: 'danger'
                                    });
                                }, 3000);
                            }
                        }
                    });
                    return false;
                }
            });
        });
        // ==============================================================================================
    </script>
</body>

</html>