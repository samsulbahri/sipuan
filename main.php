<?php
session_start();      // memulai session

// fungsi untuk pengecekan status login user
// jika user belum login, alihkan ke halaman "login-error"
if (empty($_SESSION['username']) && empty($_SESSION['password'])) {
    echo "<meta http-equiv='refresh' content='0; url=login-error'>";
}
// jika user sudah login
else {
    // panggil file "config.php" untuk koneksi ke database
    require_once "config/config.php";

    try {
        // siapkan "data"
        $id = 1;

        // sql statement untuk menampilkan data dari tabel "sys_config" berdasarkan "id"
        $query = "SELECT nama, logo FROM sys_config WHERE id=:id";
        // membuat prepared statements
        $stmt = $pdo->prepare($query);

        // hubungkan "data" dengan prepared statements
        $stmt->bindParam(':id', $id);

        // eksekusi query
        $stmt->execute();

        // ambil data hasil query
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        // tampilkan data
        $nama = $data['nama'];
        $logo = $data['logo'];
    } catch (PDOException $e) {
        // tampilkan pesan kesalahan
        echo "Query Error : " . $e->getMessage();
    }
?>
    <!doctype html>
    <html lang="en">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Sistem Informasi Puan Kopi">
        <meta name="keywords" content="Sistem Informasi Puan Kopi">
        <meta name="author" content="Rizky Febrian Nur">

        <!-- Favicon icon -->
        <link rel="shortcut icon" href="assets/img/PuanKopi.jpg">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-4.1.3/css/bootstrap.min.css">
        <!-- DataTables CSS -->
        <link rel="stylesheet" type="text/css" href="assets/plugins/DataTables/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" type="text/css" href="assets/plugins/DataTables/css/responsive.bootstrap4.min.css">
        <!-- Datepicker CSS -->
        <link rel="stylesheet" type="text/css" href="assets/plugins/datepicker/css/datepicker.min.css">
        <!-- Font Awesome CSS -->
        <link rel="stylesheet" type="text/css" href="assets/plugins/fontawesome-free-5.5.0-web/css/all.min.css">
        <!-- Animate CSS -->
        <link rel="stylesheet" type="text/css" href="assets/plugins/animate/animate.css">
        <!-- Sweetalert CSS -->
        <link rel="stylesheet" type="text/css" href="assets/plugins/sweetalert/css/sweetalert.css">
        <!-- Chosen CSS -->
        <link rel="stylesheet" type="text/css" href="assets/plugins/chosen-bootstrap-4/css/chosen.css">
        <!-- Custom CSS -->
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <!-- jQuery -->
        <script type="text/javascript" src="assets/js/jquery-3.3.1.js"></script>

        <style>
            .orange {
                color: orange;
            }
        </style>

        <!-- Title -->
        <title>Sistem Informasi Púan Kopi</title>
    </head>

    <body>
        <!-- Navbar Brand -->
        <nav class="navbar navbar-expand-md fixed-top navbar-light bg-success">
            <div class="container-fluid">
                <!-- Logo dan Nama Konter -->
                <a class="navbar-brand" href="beranda">
                    <img src="assets/img/<?php echo $logo; ?>" class="d-inline-block brand align-top title-icon" alt="ToroLab">
                    <span class="title-brand"><?php echo $nama; ?></span>
                </a>
                <!-- Profil User -->
                <div class="mr-4">
                    <ul class="navbar-nav ml-auto mr-4">
                        <li class="nav-item dropdown">
                            <a class="dropdown-toggle username" href="javascript:void(0);" id="navbarUser" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-user title-icon"></i> <?php echo $_SESSION['nama_user']; ?>
                            </a>
                            <!-- Menu User -->
                            <div class="dropdown-menu" aria-labelledby="navbarUser">
                                <a class="dropdown-item menu" id="password" href="password">
                                    <i class="fas fa-angle-right title-icon"></i> Ubah Password
                                </a>

                                <div class="dropdown-divider"></div>

                                <a class="dropdown-item" id="logout" href="javascript:void(0);">
                                    <i class="fas fa-angle-right title-icon"></i> Logout
                                </a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- Navbar Menu -->
        <nav class="navbar navbar-expand-md fixed-top navbar-light navbar-menu bg-light shadow-sm">
            <div class="container-fluid">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <!-- Menu Aplikasi -->
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <ul class="navbar-nav">
                        <!-- panggil file "navbar_menu.php" untuk menampilkan menu -->
                        <?php include "navbar_menu.php"; ?>
                    </ul>
                </div>
            </div>
        </nav>

        <main role="main" class="container-fluid">
            <div class="content">
                <!-- panggil file "content.php" untuk menampilkan halaman konten -->
                <?php include "content.php"; ?>
            </div>
        </main>

        <!-- Footer -->
        <div class="container-fluid">
            <footer class="pt-4 my-md-4 pt-md-3 border-top">
                <div class="row">
                    <div class="col-12 col-md center">
                        &copy; 2020 - <a class="text-success">Kelompok 4</a>
                    </div>
                </div>
            </footer>
        </div>

        <!-- Optional JavaScript -->
        <!-- Popper JS -->
        <script type="text/javascript" src="assets/plugins/popper-1.14.6/popper.min.js"></script>
        <!-- Bootstrap JS -->
        <script type="text/javascript" src="assets/plugins/bootstrap-4.1.3/js/bootstrap.min.js"></script>
        <!-- Fontawesome Plugin JS -->
        <script type="text/javascript" src="assets/plugins/fontawesome-free-5.5.0-web/js/all.min.js"></script>
        <!-- DataTables Plugin JS -->
        <script type="text/javascript" src="assets/plugins/DataTables/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="assets/plugins/DataTables/js/dataTables.bootstrap4.min.js"></script>
        <script type="text/javascript" src="assets/plugins/DataTables/js/dataTables.responsive.min.js"></script>
        <script type="text/javascript" src="assets/plugins/DataTables/js/responsive.bootstrap4.min.js"></script>
        <!-- datepicker Plugin JS -->
        <script type="text/javascript" src="assets/plugins/datepicker/js/bootstrap-datepicker.min.js"></script>
        <!-- SweetAlert Plugin JS -->
        <script type="text/javascript" src="assets/plugins/sweetalert/js/sweetalert.min.js"></script>
        <!-- Notify JS -->
        <script type="text/javascript" src="assets/plugins/bootstrap-notify/bootstrap-notify.min.js"></script>
        <!-- Chosen Plugin JS -->
        <script type="text/javascript" src="assets/plugins/chosen-bootstrap-4/js/chosen.jquery.js"></script>

        <!-- Custom JS -->
        <!-- Format Rupiah / angka -->
        <script type="text/javascript" src="config/fungsi_format_rupiah.js"></script>
        <!-- Validasi karakter yang boleh diinpukan pada form -->
        <script type="text/javascript" src="config/fungsi_validasi_karakter.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                // ======================================= initiate plugin =======================================
                // tooltip
                $(function() {
                    $("tbody").tooltip({
                        selector: '[data-toggle="tooltip"]',
                        container: 'tbody'
                    });
                })
                // ===============================================================================================

                // ============================================= Logout ==========================================
                $("#logout").click(function() {
                    // tampilkan notifikasi saat akan logout
                    swal({
                            title: "Apakah Anda Yakin?",
                            text: "Anda akan keluar dari aplikasi.",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ya, Logout",
                            closeOnConfirm: false
                        },
                        // jika confirm button dipilih
                        function() {
                            // alihkan ke halaman "logout"
                            window.location = "logout";
                        });
                });
                // ================================================================================================
            });
        </script>
    </body>

    </html>
<?php } ?>